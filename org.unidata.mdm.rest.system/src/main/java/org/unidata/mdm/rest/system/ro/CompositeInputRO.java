package org.unidata.mdm.rest.system.ro;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;

/**
 * The base input composite object.
 * Contains fields for dynamic processing by system modules
 *
 * @author Alexandr Serov
 * @since 28.09.2020
 **/
public abstract class CompositeInputRO implements RestInputSource {

    /**
     * Another optional information (Relations, Classifiers and etc.)
     */
    @JsonProperty("payload")
    private final Map<String, JsonNode> payload = new HashMap<>();

    @JsonAnySetter
    public void setPayload(Map<String, JsonNode> payload) {
        this.payload.putAll(payload);
    }

    @Override
    public Map<String, JsonNode> getAny() {
        return payload;
    }
}
