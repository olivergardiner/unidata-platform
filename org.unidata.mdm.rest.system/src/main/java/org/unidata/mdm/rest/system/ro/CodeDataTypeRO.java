/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.system.ro;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Supported simple data types.
 */
public enum CodeDataTypeRO {
    /**
     * String, as defined in XSD.
     */
    STRING("String"),
    /**
     * Integer, as defined in XSD.
     */
    INTEGER("Integer");
    /**
     * The value, really used for marshaling / unmarshaling.
     */
    private final String value;
    /**
     * Constructor.
     * @param v the value
     */
    private CodeDataTypeRO(String v) {
        value = v;
    }
    /**
     * @return the value
     */
    @JsonValue
    public String value() {
        return value;
    }
    /**
     * From value creator.
     * @param v the value
     * @return enum instance
     */
    @JsonCreator
    public static CodeDataTypeRO fromValue(String v) {
        for (CodeDataTypeRO c: CodeDataTypeRO.values()) {
            if (StringUtils.equals(v, c.value())) {
                return c;
            }
        }
        return null;
    }
}
