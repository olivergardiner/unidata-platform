package org.unidata.mdm.rest.system.ro;

import java.io.Serializable;
import java.util.Objects;

/**
 * Details of the operation
 *
 * @author Alexandr Serov
 * @since 23.09.2020
 **/
public class ResultDetailsRO implements Serializable {

    /**
     * Details message
     */
    private String message;

    /**
     * Details severity
     */
    private Severity severity;

    /**
     * Default constructor
     */
    public ResultDetailsRO() {
    }

    /**
     * Result details constructor
     *
     * @param message details message
     * @param severity details severity
     */
    public ResultDetailsRO(String message, Severity severity) {
        this.message = message;
        this.severity = severity;
    }

    /**
     * Create new result details with info severity
     *
     * @param message details message
     * @return new result details instance
     */
    public static ResultDetailsRO infoDetails(String message) {
        return new ResultDetailsRO(message, Severity.INFO);
    }

    /**
     * Create new result details with warn severity
     *
     * @param message details message
     * @return new result details instance
     */
    public static ResultDetailsRO warnDetails(String message) {
        return new ResultDetailsRO(message, Severity.WARN);
    }

    /**
     * Create new result details with error severity
     *
     * @param message details message
     * @return new result details instance
     */
    public static ResultDetailsRO errorDetails(String message) {
        return new ResultDetailsRO(message, Severity.ERROR);
    }

    public String toString() {
        return message + " [" + severity + "]";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ResultDetailsRO that = (ResultDetailsRO) o;
        return Objects.equals(message, that.message) &&
            severity == that.severity;
    }

    @Override
    public int hashCode() {
        return Objects.hash(message, severity);
    }

    /**
     * @return details message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message details message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return details severity
     */
    public Severity getSeverity() {
        return severity;
    }

    /**
     * @param severity details severity
     */
    public void setSeverity(Severity severity) {
        this.severity = severity;
    }

    /**
     * Details severity
     */
    public enum Severity {

        /**
         * Information
         */
        INFO,

        /**
         * Warning
         */
        WARN,

        /**
         * Error
         */
        ERROR
    }
}