package org.unidata.mdm.rest.system.service;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.unidata.mdm.system.dto.OutputContainer;
import org.unidata.mdm.system.type.rendering.AnyType;
import org.unidata.mdm.system.type.rendering.FragmentField;
import org.unidata.mdm.system.type.rendering.OutputFragmentRenderer;
import org.unidata.mdm.system.type.rendering.OutputSink;

/**
 * Typed output render
 *
 * @author Alexandr Serov
 * @since 05.10.2020
 **/
public abstract class TypedRestOutputRenderer<T extends OutputSink, C extends OutputContainer> implements OutputFragmentRenderer {

    private static final int DEFAULT_ORDER = 0;

    private static final String ILLEGAL_SINK_TYPE_ERROR_MESSAGE = "Unexpected sink type %s. Expected type %s";

    private static final String ILLEGAL_CONTAINER_TYPE_ERROR_MESSAGE = "Unexpected container type %s. Expected type %s";

    private static final String PAYLOAD_FIELD_NAME = "payload";

    private static final Logger LOGGER = LoggerFactory.getLogger(TypedRestOutputRenderer.class);

    /**
     * Type of processed object
     */
    private final Class<T> type;
    /**
     * Type of processed container
     */
    private final Class<C> containerType;

    /**
     * Rendering fields
     */
    private final Map<String, Class<?>> fields;

    /**
     * Fields to publish in schema
     */
    private final List<FragmentField> fragments;

    /**
     * Supported versions
     */
    private final Set<String> supportedVersions;

    /**
     * Rendering order
     */
    private final int order;

    /**
     * @param type Type of processed object
     * @param containerType expected type of output container
     * @param fieldName field of OutputSink class
     * @param fields Rendering fields
     * @param supportedVersions versions supported by the component
     * @param order order
     */
    public TypedRestOutputRenderer(Class<T> type, Class<C> containerType, String fieldName, Map<String, Class<?>> fields, Set<String> supportedVersions, int order) {
        this.type = type;
        this.containerType = containerType;
        this.fields = Collections.unmodifiableMap(fields);
        this.supportedVersions = ObjectUtils.defaultIfNull(supportedVersions, Collections.emptySet());
        this.order = order;
        Map<String, AnyType> types = new HashMap<>();
        fields.forEach((partName, fieldClass) ->
            types.put(partName, AnyType.anyType(fieldClass))
        );
        fragments = Collections.singletonList(FragmentField.fragmentField(type, fieldName, types));
    }

    /**
     * @param type Type of processed object
     * @param containerType expected type of output container
     * @param fields Rendering fields
     * @param supportedVersions versions supported by the component
     * @param order order
     */
    public TypedRestOutputRenderer(Class<T> type, Class<C> containerType, Map<String, Class<?>> fields, Set<String> supportedVersions, int order) {
        this(type, containerType, PAYLOAD_FIELD_NAME, fields, supportedVersions, order);
    }

    /**
     * @param type Type of processed object
     * @param containerType expected type of output container
     * @param fieldName field of OutputSink class
     * @param fields Rendering fields
     */
    public TypedRestOutputRenderer(Class<T> type, Class<C> containerType, String fieldName, Map<String, Class<?>> fields) {
        this(type, containerType, fieldName, fields, Collections.singleton(FragmentField.CURRENT_VERSION), DEFAULT_ORDER);
    }

    /**
     * @param type Type of processed object
     * @param containerType expected type of output container
     * @param fields Rendering fields
     */
    public TypedRestOutputRenderer(Class<T> type, Class<C> containerType, Map<String, Class<?>> fields) {
        this(type, containerType, PAYLOAD_FIELD_NAME, fields, Collections.singleton(FragmentField.CURRENT_VERSION), DEFAULT_ORDER);
    }

    protected abstract void render(C container, T sink);

    @Override
    public void render(String version, OutputContainer container, OutputSink sink) {
        Objects.requireNonNull(container, "OutputContainer can't be null");
        Objects.requireNonNull(sink, "OutputSink can't be null");
        if (!acceptVersion(version)) {
            LOGGER.debug("Skip unsupported version: {}", version);
        } else {
            Class<?> containerClass = container.getClass();
            if (containerType.isAssignableFrom(containerClass)) {
                Class<?> sinkClass = sink.getClass();
                if (type.isAssignableFrom(sinkClass)) {
                    render(containerType.cast(container), type.cast(sink));
                } else {
                    throw new IllegalArgumentException(String.format(ILLEGAL_SINK_TYPE_ERROR_MESSAGE, sinkClass, type));
                }
            } else {
                throw new IllegalArgumentException(String.format(ILLEGAL_CONTAINER_TYPE_ERROR_MESSAGE, containerClass, containerType));
            }
        }
    }

    /**
     * Checks if the render version is acceptable
     *
     * @param version api version
     * @return true if version in list
     */
    private boolean acceptVersion(String version) {
        return supportedVersions.isEmpty() || supportedVersions.contains(version);
    }

    /**
     * @return rendered field part types
     */
    public Map<String, Class<?>> getFields() {
        return fields;
    }

    /**
     * @return rendered fragments
     */
    @Override
    public List<FragmentField> fragmentFields() {
        return fragments;
    }


    /**
     * @return order
     */
    @Override
    public int order() {
        return order;
    }
}
