package org.unidata.mdm.rest.system.ro;

import java.util.List;

/**
 * Base result object
 *
 * @author Alexandr Serov
 * @since 23.09.2020
 **/
public abstract class DetailedOutputRO {

    /**
     * Result details
     */
    private List<ResultDetailsRO> details;

    /**
     * @return result details
     */
    public List<ResultDetailsRO> getDetails() {
        return details;
    }

    /**
     * Setup output details
     *
     * @param details result details
     */
    public void setDetails(List<ResultDetailsRO> details) {
        this.details = details;
    }
}
