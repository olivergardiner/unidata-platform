package org.unidata.mdm.rest.system.service;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import com.fasterxml.jackson.databind.JsonNode;
import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.unidata.mdm.rest.system.ro.RestInputSource;
import org.unidata.mdm.system.context.InputCollector;
import org.unidata.mdm.system.type.pipeline.fragment.InputFragmentCollector;
import org.unidata.mdm.system.type.rendering.AnyType;
import org.unidata.mdm.system.type.rendering.FragmentField;
import org.unidata.mdm.system.type.rendering.InputFragmentRenderer;
import org.unidata.mdm.system.type.rendering.InputSource;
import org.unidata.mdm.system.util.JsonUtils;

/**
 * Typed input renderer for rest requests
 * Checks input arguments and unpacks properties needed for rendering
 *
 * @author Alexandr Serov
 * @since 20.09.2020
 **/
public abstract class TypedRestInputRenderer<T extends RestInputSource> implements InputFragmentRenderer {

    private static final int DEFAULT_ORDER = 0;

    private static final String ILLEGAL_SOURCE_TYPE_ERROR_MESSAGE = "Unexpected source type %s. Expected type %s";

    private static final String ILLEGAL_COLLECTOR_TYPE_ERROR_MESSAGE = "Unexpected collector type %s. Expected type %s";

    private static final String PAYLOAD_FIELD_NAME = "payload";

    private static final Logger LOGGER = LoggerFactory.getLogger(TypedRestInputRenderer.class);

    /**
     * Type of processed object
     */
    private final Class<T> type;

    /**
     * Rendering fields
     */
    private final Map<String, Class<?>> fields;

    /**
     * Fields to publish in schema
     */
    private final List<FragmentField> fragments;

    /**
     * Supported versions
     */
    private final Set<String> supportedVersions;

    /**
     * Rendering order
     */
    private final int order;

    /**
     * @param type Type of processed object
     * @param fields Rendering fields
     * @param supportedVersions versions supported by the component
     * @param order order
     */
    public TypedRestInputRenderer(Class<T> type, String fieldName, Map<String, Class<?>> fields, Set<String> supportedVersions, int order) {
        this.fields = fields;
        this.type = type;
        this.order = order;
        this.supportedVersions = ObjectUtils.defaultIfNull(supportedVersions, Collections.emptySet());
        Map<String, AnyType> types = new HashMap<>();
        fields.forEach((partName, fieldClass) ->
            types.put(partName, AnyType.anyType(fieldClass))
        );
        fragments = Collections.singletonList(FragmentField.fragmentField(type, fieldName, types));
    }

    /**
     * @param type Type of processed object
     * @param fields Rendering fields
     * @param supportedVersions versions supported by the component
     * @param order order
     */
    public TypedRestInputRenderer(Class<T> type, Map<String, Class<?>> fields, Set<String> supportedVersions, int order) {
        this(type, PAYLOAD_FIELD_NAME, fields, supportedVersions, order);
    }

    /**
     * @param type Type of processed object
     * @param fieldName field of InputSource class
     * @param fields Rendering fields
     */
    public TypedRestInputRenderer(Class<T> type, String fieldName, Map<String, Class<?>> fields) {
        this(type, fieldName, fields, Collections.singleton(FragmentField.CURRENT_VERSION), DEFAULT_ORDER);
    }

    /**
     * @param type Type of processed object
     * @param fields Rendering fields
     */
    public TypedRestInputRenderer(Class<T> type, Map<String, Class<?>> fields) {
        this(type, PAYLOAD_FIELD_NAME, fields, Collections.singleton(FragmentField.CURRENT_VERSION), DEFAULT_ORDER);
    }

    /**
     * Render object fields
     *
     * @param collector fragment collector
     * @param source source object
     * @param fieldValues resolved fragment fields
     */
    protected abstract void renderFields(InputFragmentCollector<?> collector, T source, Map<String, Object> fieldValues);

    @Override
    public void render(String version, InputCollector collector, InputSource source) {
        Objects.requireNonNull(collector, "Collector can't be null");
        Objects.requireNonNull(source, "Source can't be null");
        if (!acceptVersion(version)) {
            LOGGER.debug("Skip unsupported version: {}", version);
        } else {
            if (collector instanceof InputFragmentCollector) {
                Class<?> sourceClass = source.getClass();
                if (type.isAssignableFrom(sourceClass)) {
                    T typed = type.cast(source);
                    Map<String, JsonNode> renderedFields = ObjectUtils.defaultIfNull(typed.getAny(), Collections.emptyMap());
                    Map<String, Object> fieldValues = new HashMap<>();
                    fields.forEach((fieldName, fieldClass) -> {
                        JsonNode node = renderedFields.get(fieldName);
                        if (node != null && !node.isNull()) {
                            fieldValues.put(fieldName, JsonUtils.read(node, fieldClass));
                        }
                    });
                    renderFields((InputFragmentCollector<?>) collector, typed, fieldValues);
                } else {
                    throw new IllegalArgumentException(String.format(ILLEGAL_SOURCE_TYPE_ERROR_MESSAGE, sourceClass, type));
                }
            } else {
                throw new IllegalArgumentException(String.format(ILLEGAL_COLLECTOR_TYPE_ERROR_MESSAGE, collector.getClass(), type));
            }
        }
    }

    /**
     * Checks if the render version is acceptable
     *
     * @param version api version
     * @return true if version in list
     */
    private boolean acceptVersion(String version) {
        return supportedVersions.isEmpty() || supportedVersions.contains(version);
    }

    @Override
    public List<FragmentField> fragmentFields() {
        return fragments;
    }

    @Override
    public int order() {
        return order;
    }
}
