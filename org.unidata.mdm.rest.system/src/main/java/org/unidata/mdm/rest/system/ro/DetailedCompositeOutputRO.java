package org.unidata.mdm.rest.system.ro;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;

/**
 * The base output composite object.
 * Contains fields for dynamic processing by system modules
 *
 * @author Alexandr Serov
 * @since 28.09.2020
 **/
public abstract class DetailedCompositeOutputRO extends DetailedOutputRO implements RestOutputSink {

    /**
     * Dynamic object fields
     */
    @JsonProperty("payload")
    private final Map<String, JsonNode> payload = new HashMap<>();

    /**
     * Adds a part to output.
     *
     * @param name the property name
     * @param value the value
     */
    @Override
    public void setAny(String name, JsonNode value) {
        payload.put(name, value);
    }

    /**
     * @return output parts
     */
    @JsonAnyGetter
    public Map<String, JsonNode> getPayload() {
        return payload;
    }
}
