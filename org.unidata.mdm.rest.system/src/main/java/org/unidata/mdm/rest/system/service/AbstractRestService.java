/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.system.service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.unidata.mdm.rest.system.ro.RestResponse;
import org.unidata.mdm.system.util.ClientIpUtil;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/*
 http://cxf.apache.org/docs/jax-rs-data-bindings.html#JAX-RSDataBindings-JSONsupport
 http://cxf.apache.org/docs/jax-rs-basics.html
 */

@Produces({ "application/json" })
@Consumes({ "application/json" })
// , "application/x-www-form-urlencoded"
public abstract class AbstractRestService {
    /**
     * Logger for this class.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractRestService.class);
    /**
     * Default object mapper.
     */
    @Autowired
    @Qualifier("objectMapper")
    protected ObjectMapper defaultMapper;
    /**
     * HTTP servlet request.
     */
    @Context
    private HttpServletRequest hsr;
    /**
     * Allow options requests.
     */
    public static final String OPTIONS_REQUEST = "OPTIONS";

    protected Response ok(Object entity) {
        return Response.ok(entity).allow(OPTIONS_REQUEST).build();
    }

    protected Response okRestResponse(Object value) {
        return ok(new RestResponse<>(value));
    }

    protected Response error(Object entity){
        return Response
                .status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity(entity)
                .type(MediaType.APPLICATION_JSON_TYPE)
                .encoding(StandardCharsets.UTF_8.name())
                .build();
    }

    protected Response okOrNotFound(Object entity) {
        if (entity == null) {
            return notFound();
        }
        return ok(entity);
    }

    protected Response accepted() {
        return Response.accepted().build();
    }

    protected Response notAuthorized(Object entity) {
        return Response.status(Response.Status.UNAUTHORIZED).entity(entity).build();
    }

    protected Response notFound() {
        return Response.status(Response.Status.NOT_FOUND).build();
    }

    protected HttpServletRequest getHSR() {
        return this.hsr;
    }

    protected String getClientIp() {
        return ClientIpUtil.clientIp(getHSR());
    }

    protected String getServerIp() {
        return  getHSR() == null ?  null : getHSR().getLocalAddr();
    }

    /**
     * Local (in-place) unrest of a content string.
     *
     * @param content
     *            the string
     * @param cls
     *            class to map the content to
     * @return new instance
     */
    public <T> T unrestInplace(String content, Class<T> cls) {
        try {
            if (!StringUtils.isBlank(content)) {
                return defaultMapper.readValue(content, cls);
            }
        } catch (JsonParseException e) {
            LOGGER.warn("Caught a 'JsonParseException' while local unrest. {}", e.getMessage(), e);
        } catch (JsonMappingException e) {
            LOGGER.warn("Caught a 'JsonMappingException' while local unrest. {}", e.getMessage(), e);
        } catch (IOException e) {
            LOGGER.warn("Caught a 'IOException' while doing unrest. {}", e.getMessage(), e);
        }

        return null;
    }
}
