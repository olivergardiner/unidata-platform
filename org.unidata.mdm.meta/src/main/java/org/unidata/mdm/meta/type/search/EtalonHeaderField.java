package org.unidata.mdm.meta.type.search;

import org.unidata.mdm.search.type.FieldType;
import org.unidata.mdm.search.type.IndexField;

/**
 * @author Mikhail Mikhailov on Mar 13, 2020
 */
public enum EtalonHeaderField implements IndexField {
    /**
     * Record etalon id - maps to ES root id system field.
     */
    FIELD_ETALON_ID("$etalon_id", FieldType.STRING),
    /**
     * The join field for the hierarchie.
     */
    FIELD_JOIN("$j", FieldType.JOIN);

    EtalonHeaderField(String field, FieldType type) {
        this.field = field;
        this.type = type;
    }
    /**
     * The name.
     */
    private final String field;
    /**
     * The type.
     */
    private final FieldType type;
    /**
     * {@inheritDoc}
     */
    @Override
    public FieldType getFieldType() {
        return type;
    }
    /**
     * @return the field
     */
    @Override
    public String getName() {
        return field;
    }
}
