package org.unidata.mdm.meta.type.model.measurement;

import java.io.Serializable;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;


public class MeasurementUnit implements Serializable {

    private final static long serialVersionUID = 987654321L;

    @JacksonXmlProperty(isAttribute = true)
    protected String id;

    @JacksonXmlProperty(isAttribute = true)
    protected String shortName;

    @JacksonXmlProperty(isAttribute = true)
    protected String displayName;

    @JacksonXmlProperty(isAttribute = true)
    protected boolean base;

    @JacksonXmlProperty(isAttribute = true)
    protected String conversionFunction;

    public String getId() {
        return id;
    }

    public void setId(String value) {
        this.id = value;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String value) {
        this.shortName = value;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String value) {
        this.displayName = value;
    }

    public boolean isBase() {
        return base;
    }

    public void setBase(boolean value) {
        this.base = value;
    }

    public String getConversionFunction() {
        return conversionFunction;
    }

    public void setConversionFunction(String value) {
        this.conversionFunction = value;
    }

    public MeasurementUnit withId(String value) {
        setId(value);
        return this;
    }

    public MeasurementUnit withShortName(String value) {
        setShortName(value);
        return this;
    }

    public MeasurementUnit withDisplayName(String value) {
        setDisplayName(value);
        return this;
    }

    public MeasurementUnit withBase(boolean value) {
        setBase(value);
        return this;
    }

    public MeasurementUnit withConvectionFunction(String value) {
        setConversionFunction(value);
        return this;
    }

}
