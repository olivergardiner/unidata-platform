package org.unidata.mdm.meta.type.model.entities;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Collections;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.unidata.mdm.meta.type.model.CustomProperty;
import org.unidata.mdm.meta.type.model.PeriodBoundary;
import org.unidata.mdm.meta.type.model.ValueGenerationStrategy;
import org.unidata.mdm.meta.type.model.attributes.ArrayMetaModelAttribute;
import org.unidata.mdm.meta.type.model.attributes.AttributeGroup;
import org.unidata.mdm.meta.type.model.attributes.ComplexMetaModelAttribute;
import org.unidata.mdm.meta.type.model.attributes.SimpleMetaModelAttribute;
import org.unidata.mdm.meta.type.model.merge.MergeSettings;

import static org.unidata.mdm.meta.type.model.ModelNamespace.META_MODEL_NAMESPACE;


public class Entity extends NestedEntity implements Serializable {

    private final static long serialVersionUID = 987654321L;

    @JacksonXmlProperty(namespace = META_MODEL_NAMESPACE)
    protected ValueGenerationStrategy externalIdGenerationStrategy;
    @JacksonXmlProperty(isAttribute = true)
    protected boolean dashboardVisible;
    @JacksonXmlProperty(isAttribute = true)
    protected String groupName;

    public ValueGenerationStrategy getExternalIdGenerationStrategy() {
        return externalIdGenerationStrategy;
    }

    public void setExternalIdGenerationStrategy(ValueGenerationStrategy value) {
        this.externalIdGenerationStrategy = value;
    }

    public boolean isDashboardVisible() {
        return dashboardVisible;
    }

    public void setDashboardVisible(Boolean value) {
        this.dashboardVisible = value;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String value) {
        this.groupName = value;
    }

    public Entity withExternalIdGenerationStrategy(ValueGenerationStrategy value) {
        setExternalIdGenerationStrategy(value);
        return this;
    }

    public Entity withDashboardVisible(boolean value) {
        setDashboardVisible(value);
        return this;
    }

    public Entity withGroupName(String value) {
        setGroupName(value);
        return this;
    }

    @Override
    public Entity withComplexAttribute(ComplexMetaModelAttribute... values) {
        if (values != null) {
            Collections.addAll(getComplexAttribute(), values);
        }
        return this;
    }

    @Override
    public Entity withComplexAttribute(Collection<ComplexMetaModelAttribute> values) {
        if (values != null && !values.isEmpty()) {
            getComplexAttribute().addAll(values);
        }
        return this;
    }

    @Override
    public Entity withSimpleAttribute(SimpleMetaModelAttribute... values) {
        if (values != null) {
            Collections.addAll(getSimpleAttribute(), values);
        }
        return this;
    }

    @Override
    public Entity withSimpleAttribute(Collection<SimpleMetaModelAttribute> values) {
        if (values != null && !values.isEmpty()) {
            getSimpleAttribute().addAll(values);
        }
        return this;
    }

    @Override
    public Entity withArrayAttribute(ArrayMetaModelAttribute... values) {
        if (values != null) {
            Collections.addAll(getArrayAttribute(), values);
        }
        return this;
    }

    @Override
    public Entity withArrayAttribute(Collection<ArrayMetaModelAttribute> values) {
        if (values != null && !values.isEmpty()) {
            getArrayAttribute().addAll(values);
        }
        return this;
    }

    @Override
    public Entity withMergeSettings(MergeSettings value) {
        setMergeSettings(value);
        return this;
    }

    @Override
    public Entity withValidityPeriod(PeriodBoundary value) {
        setValidityPeriod(value);
        return this;
    }

    @Override
    public Entity withAttributeGroups(AttributeGroup... values) {
        if (values != null) {
            Collections.addAll(getAttributeGroups(), values);
        }
        return this;
    }

    @Override
    public Entity withAttributeGroups(Collection<AttributeGroup> values) {
        if (values != null && !values.isEmpty()) {
            getAttributeGroups().addAll(values);
        }
        return this;
    }

    @Override
    public Entity withRelationGroups(RelationGroup... values) {
        if (values != null) {
            Collections.addAll(getRelationGroups(), values);
        }
        return this;
    }

    @Override
    public Entity withRelationGroups(Collection<RelationGroup> values) {
        if (values != null && !values.isEmpty()) {
            getRelationGroups().addAll(values);
        }
        return this;
    }

    @Override
    public Entity withCustomProperties(CustomProperty... values) {
        if (values != null) {
            Collections.addAll(getCustomProperties(), values);
        }
        return this;
    }

    @Override
    public Entity withCustomProperties(Collection<CustomProperty> values) {
        if (values != null && !values.isEmpty()) {
            getCustomProperties().addAll(values);
        }
        return this;
    }

    @Override
    public Entity withName(String value) {
        setName(value);
        return this;
    }

    @Override
    public Entity withDisplayName(String value) {
        setDisplayName(value);
        return this;
    }

    @Override
    public Entity withDescription(String value) {
        setDescription(value);
        return this;
    }

    @Override
    public Entity withVersion(Long value) {
        setVersion(value);
        return this;
    }

    @Override
    public Entity withUpdatedAt(LocalDateTime value) {
        setUpdatedAt(value);
        return this;
    }

    @Override
    public Entity withCreateAt(LocalDateTime value) {
        setCreateAt(value);
        return this;
    }

}
