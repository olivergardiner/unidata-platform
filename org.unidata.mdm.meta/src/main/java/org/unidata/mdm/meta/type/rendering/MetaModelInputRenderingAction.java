/*
  Unidata Platform
  Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.

  Commercial License
  This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.

  Please see the Unidata Licensing page at: https://unidata-platform.com/license/
  For clarification or additional options, please contact: info@unidata-platform.com
  -------
  Disclaimer:
  -------
  THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
  REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
  IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
  FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
  THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.meta.type.rendering;

import org.unidata.mdm.system.type.rendering.InputRenderingAction;

/**
 * @author Alexey Tsarapkin
 */
public enum MetaModelInputRenderingAction implements InputRenderingAction {
    /**
     * GET entity meta model
     */
    GET_ENTITY_MODEL_INPUT,
    /**
     * GET lookup meta model
     */
    GET_LOOKUP_MODEL_INPUT,
    /**
     * Upsert entity meta model
     */
    UPSERT_ENTITY_MODEL_INPUT,
    /**
     * Upsert lookup meta model
     */
    UPSERT_LOOKUP_MODEL_INPUT,
    /**
     * metaGraph to UpsertFragments
     */
    APPLY_META_MODEL_GRAPH,
    /**
     * Publish meta model
     */
    PUBLISH_META_MODEL_INPUT,
    /**
     * DELETE entity
     */
    DELETE_ENTITY_META_MODEL_INPUT,
    /**
     * DELETE lookup
     */
    DELETE_LOOKUP_META_MODEL_INPUT,
    /**
     * DELETE_META_MODEL_DRAFT
     */
    DELETE_META_MODEL_DRAFT;
}
