/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.meta.util;

import java.io.IOException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.unidata.mdm.meta.type.model.entities.EntitiesGroup;
import org.unidata.mdm.meta.type.model.entities.Entity;
import org.unidata.mdm.meta.type.model.enumeration.EnumerationDataType;
import org.unidata.mdm.meta.type.model.entities.LookupEntity;
import org.unidata.mdm.meta.type.model.measurement.MeasurementValues;
import org.unidata.mdm.meta.type.model.Model;
import org.unidata.mdm.meta.type.model.entities.NestedEntity;
import org.unidata.mdm.meta.type.model.entities.Relation;
import org.unidata.mdm.meta.type.model.SourceSystem;
import org.unidata.mdm.meta.exception.MetaExceptionIds;
import org.unidata.mdm.system.exception.PlatformFailureException;
import org.unidata.mdm.system.util.AbstractJaxbUtils;

/**
 * @author Mikhail Mikhailov on Oct 4, 2019
 * JAXB stuff, related to meta model.
 * TODO: Get rid of JAXB.
 */
public final class MetaJaxbUtils extends AbstractJaxbUtils {
    /**
     * Logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(MetaJaxbUtils.class);
    /**
     * Object factory param.
     */
    private static final String OBJECT_FACTORY_PARAM = "com.sun.xml.bind.ObjectFactory";
    /**
     * Meta root package.
     */
    private static final String META_ROOT_PACKAGE = "org.unidata.mdm.meta";
    /**
     * Meta model namespace URI.
     */
    public static final String META_URI = "http://meta.mdm.unidata.org/";
    /**
     * XSD dateTime date format.
     */
    public static final String XSD_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ssX";

    private static final com.unidata.mdm.security.ObjectFactory SECURITY_FACTORY =
            new com.unidata.mdm.security.ObjectFactory();

    public static com.unidata.mdm.security.ObjectFactory getSecurityFactory() {
        return SECURITY_FACTORY;
    }


    /**
     * Constructor.
     */
    private MetaJaxbUtils() {
        super();
    }


    /**
     * Unmarshal model.
     *
     * @param s source
     * @return model
     */
    public static Model unmarshalMetaModel(String s) {
        try {
            return unmarshalObject(Model.class, s);
        } catch (Exception je) {
            final String message = "Cannot unmarshall model from [{}]";
            LOGGER.error(message, s, je);
            throw new PlatformFailureException(message, je, MetaExceptionIds.EX_META_CANNOT_UNMARSHAL_MODEL);
        }
    }

    /**
     * Marshal the model.
     *
     * @param m the model
     * @return string
     */
    public static String marshalMetaModel(Model m) {
        try {
            return marshalObject(m);
        } catch (Exception je) {
            final String message = "Cannot marshall model from [{}]";
            LOGGER.error(message, m, je);
            throw new PlatformFailureException(message, je, MetaExceptionIds.EX_META_CANNOT_MARSHAL_MODEL);
        }
    }

    /**
     * Marshal a {@link SourceSystem}.
     *
     * @param record the record
     * @return string
     */
    public static String marshalSourceSystem(SourceSystem record) {
        try {
            return marshalObject(record);
        } catch (Exception je) {
            final String message = "Cannot marshall source system record from [{}]";
            LOGGER.warn(message, record, je);
            throw new PlatformFailureException(message, je, MetaExceptionIds.EX_META_CANNOT_MARSHAL_SOURCE_SYSTEM, record);
        }
    }

    /**
     * Marshal a {@link EnumerationDataType}.
     *
     * @param record the record
     * @return string
     */
    public static String marshalEnumeration(EnumerationDataType record) {
        try {
            return marshalObject(record);
        } catch (Exception je) {
            final String message = "Cannot marshall enumeration record from [{}]";
            LOGGER.warn(message, record, je);
            throw new PlatformFailureException(message, je, MetaExceptionIds.EX_META_CANNOT_MARSHAL_ENUMERATION, record);
        }
    }

    /**
     * Marshal a {@link LookupEntity}.
     *
     * @param record the record
     * @return string
     */
    public static String marshalLookupEntity(LookupEntity record) {
        try {
            return marshalObject(record);
        } catch (Exception je) {
            final String message = "Cannot marshall lookup entity record from [{}]";
            LOGGER.warn(message, record, je);
            throw new PlatformFailureException(message, je, MetaExceptionIds.EX_META_CANNOT_MARSHAL_LOOKUP_ENTITY, record);
        }
    }

    /**
     * Marshal a {@link NestedEntity}.
     *
     * @param record the record
     * @return string
     */
    public static String marshalNestedEntity(NestedEntity record) {
        try {
            return marshalObject(record);
        } catch (Exception je) {
            final String message = "Cannot marshall nested entity record from [{}]";
            LOGGER.warn(message, record, je);
            throw new PlatformFailureException(message, je, MetaExceptionIds.EX_META_CANNOT_MARSHAL_NESTED_ENTITY, record);
        }
    }

    /**
     * Marshal a {@link Entity}.
     *
     * @param record the record
     * @return string
     */
    public static String marshalEntity(Entity record) {
        try {
            return marshalObject(record);
        } catch (Exception je) {
            final String message = "Cannot marshall entity record from [{}]";
            LOGGER.warn(message, record, je);
            throw new PlatformFailureException(message, je, MetaExceptionIds.EX_META_CANNOT_MARSHAL_ENTITY, record);
        }
    }

    /**
     * Marshal a {@link Entity}.
     *
     * @param record the record
     * @return string
     */
    public static String marshalEntitiesGroup(EntitiesGroup record) {
        try {
            return marshalObject(record);
        } catch (Exception je) {
            final String message = "Cannot marshall entities group record from [{}]";
            LOGGER.warn(message, record, je);
            throw new PlatformFailureException(message, je, MetaExceptionIds.EX_META_CANNOT_MARSHAL_ENTITIES_GROUP, record);
        }
    }


    /**
     * Marshal a {@link Relation}.
     *
     * @param record the record
     * @return string
     */
    public static String marshalRelation(Relation record) {
        try {
            return marshalObject(record);
        } catch (Exception je) {
            final String message = "Cannot marshall entity record from [{}]";
            LOGGER.warn(message, record, je);
            throw new PlatformFailureException(message, je, MetaExceptionIds.EX_META_CANNOT_MARSHAL_RELATION, record);
        }
    }

    /**
     * Unmarshals a {@link SourceSystem}.
     *
     * @param s string
     * @return record or null
     */
    public static SourceSystem unmarshalSourceSystem(String s) {
        try {
            return unmarshalObject(SourceSystem.class, s);
        } catch (Exception je) {
            final String message = "Cannot unmarshall source system from [{}]";
            LOGGER.warn(message, s, je);
            throw new PlatformFailureException(message, je, MetaExceptionIds.EX_META_CANNOT_UNMARSHAL_SOURCE_SYSTEM, s);
        }
    }

    /**
     * Unmarshals a {@link EnumerationDataType}.
     *
     * @param s string
     * @return record or null
     */
    public static EnumerationDataType unmarshalEnumeration(String s) {
        try {
            return unmarshalObject(EnumerationDataType.class, s);
        } catch (Exception je) {
            final String message = "Cannot unmarshall enumeration from [{}]";
            LOGGER.warn(message, s, je);
            throw new PlatformFailureException(message, je, MetaExceptionIds.EX_META_CANNOT_UNMARSHAL_ENUMERATION, s);
        }
    }

    /**
     * Unmarshals a {@link LookupEntity}.
     *
     * @param s string
     * @return record or null
     */
    public static LookupEntity unmarshalLookupEntity(String s) {
        try {
            return unmarshalObject(LookupEntity.class, s);
        } catch (Exception je) {
            final String message = "Cannot unmarshall lookup entity from [{}]";
            LOGGER.warn(message, s, je);
            throw new PlatformFailureException(message, je, MetaExceptionIds.EX_META_CANNOT_UNMARSHAL_LOOKUP_ENTITY, s);
        }
    }

    /**
     * Unmarshals a {@link NestedEntity}.
     *
     * @param s string
     * @return record or null
     */
    public static NestedEntity unmarshalNestedEntity(String s) {
        try {
            return unmarshalObject(NestedEntity.class, s);
        } catch (Exception je) {
            final String message = "Cannot unmarshall nested entity from [{}]";
            LOGGER.warn(message, s, je);
            throw new PlatformFailureException(message, je, MetaExceptionIds.EX_META_CANNOT_UNMARSHAL_NESTED_ENTITY, s);
        }
    }

    /**
     * Unmarshals a {@link Entity}.
     *
     * @param s string
     * @return record or null
     */
    public static Entity unmarshalEntity(String s) {
        try {
            return unmarshalObject(Entity.class, s);
        } catch (Exception je) {
            final String message = "Cannot unmarshall entity from [{}]";
            LOGGER.warn(message, s, je);
            throw new PlatformFailureException(message, je, MetaExceptionIds.EX_META_CANNOT_UNMARSHAL_ENTITY, s);
        }
    }

    /**
     * Unmarshals a {@link Relation}.
     *
     * @param s string
     * @return record or null
     */
    public static Relation unmarshalRelation(String s) {
        try {
            return unmarshalObject(Relation.class, s);
        } catch (Exception je) {
            final String message = "Cannot unmarshall relation from [{}]";
            LOGGER.warn(message, s, je);
            throw new PlatformFailureException(message, je, MetaExceptionIds.EX_META_CANNOT_UNMARSHAL_RELATION, s);
        }
    }

    /**
     * Unmarshals a {@link EntitiesGroup}.
     *
     * @param s string
     * @return record or null
     */
    public static EntitiesGroup unmarshalGroup(String s) {
        try {
            return unmarshalObject(EntitiesGroup.class, s);
        } catch (Exception je) {
            final String message = "Cannot unmarshall group from [{}]";
            LOGGER.warn(message, s, je);
            throw new PlatformFailureException(message, je, MetaExceptionIds.EX_META_CANNOT_UNMARSHAL_GROUP, s);
        }
    }

    /**
     * Creates model from uploaded document.
     *
     * @param is the input stream
     * @return model
     * @throws Exception
     */
    public static Model createModelFromInputStream(InputStream is) throws IOException {
        return unmarshalObject(Model.class, is);
    }

    public static MeasurementValues createMeasurementValuesFromInputStream(InputStream is) throws IOException {
        return unmarshalObject(MeasurementValues.class, is);
    }

}
