package org.unidata.mdm.meta.type.model.attributes;

import org.unidata.mdm.meta.type.model.MetaModelAttribute;

/**
 * Searchable model attribute
 *
 * @author Alexandr Serov
 * @since 26.08.2020
 **/
public interface SearchableMetaModelAttribute extends MetaModelAttribute {

    String EMPTY_MASK = "";

    boolean isSearchable();

    void setSearchable(boolean value);

    boolean isDisplayable();

    void setDisplayable(boolean value);

    boolean isMainDisplayable();

    void setMainDisplayable(boolean value);

    String getMask();

    void setMask(String value);

}
