/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.meta.type.input.meta;

/**
 * @author maria.chistyakova
 * @since 22.04.2020
 */
public class GroupEdge {
    /**
     * The from.
     */
    private GroupVertex from;

    /**
     * The to.
     */
    private GroupVertex to;

    /**
     * Gets the from.
     *
     * @return the from
     */
    public GroupVertex getFrom() {
        return from;
    }

    /**
     * Sets the from.
     *
     * @param from the new from
     */
    public void setFrom(GroupVertex from) {
        this.from = from;
    }

    /**
     * Gets the to.
     *
     * @return the to
     */
    public GroupVertex getTo() {
        return to;
    }

    /**
     * Sets the to.
     *
     * @param to the new to
     */
    public void setTo(GroupVertex to) {
        this.to = to;
    }
}
