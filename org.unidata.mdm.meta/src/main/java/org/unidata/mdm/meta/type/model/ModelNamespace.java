package org.unidata.mdm.meta.type.model;

import java.io.Serializable;

/**
 * Constants for customizing object serialization
 *
 * @author Alexandr Serov
 * @since 29.07.2020
 **/
public interface ModelNamespace extends Serializable {

    /**
     * module namespace
     */
    String META_MODEL_NAMESPACE = "http://meta.mdm.unidata.org/";
}
