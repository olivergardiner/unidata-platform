package org.unidata.mdm.meta.type.model;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import static org.unidata.mdm.meta.type.model.ModelNamespace.META_MODEL_NAMESPACE;


public class SourceSystem extends VersionedObject implements MetaModelElement {

    private final static long serialVersionUID = 987654321L;

    @JacksonXmlProperty(namespace = META_MODEL_NAMESPACE)
    protected List<CustomProperty> customProperties;

    @JacksonXmlProperty(isAttribute = true)
    protected String name;

    @JacksonXmlProperty(isAttribute = true)
    protected BigInteger weight;

    @JacksonXmlProperty(isAttribute = true)
    protected String description;

    @JacksonXmlProperty(isAttribute = true)
    protected boolean admin;

    public List<CustomProperty> getCustomProperties() {
        if (customProperties == null) {
            customProperties = new ArrayList<>();
        }
        return this.customProperties;
    }

    @Override
    public void setCustomProperties(List<CustomProperty> customProperties) {
        this.customProperties = customProperties;
    }

    public String getName() {
        return name;
    }

    public void setName(String value) {
        this.name = value;
    }

    public BigInteger getWeight() {
        return weight;
    }

    public void setWeight(BigInteger value) {
        this.weight = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String value) {
        this.description = value;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean value) {
        this.admin = value;
    }

    public SourceSystem withCustomProperties(CustomProperty... values) {
        if (values!= null) {
            Collections.addAll(getCustomProperties(), values);
        }
        return this;
    }

    public SourceSystem withCustomProperties(Collection<CustomProperty> values) {
        if (values!= null) {
            getCustomProperties().addAll(values);
        }
        return this;
    }

    public SourceSystem withName(String value) {
        setName(value);
        return this;
    }

    public SourceSystem withWeight(BigInteger value) {
        setWeight(value);
        return this;
    }

    public SourceSystem withDescription(String value) {
        setDescription(value);
        return this;
    }

    public SourceSystem withAdmin(boolean value) {
        setAdmin(value);
        return this;
    }

    @Override
    public SourceSystem withVersion(Long value) {
        setVersion(value);
        return this;
    }

    @Override
    public SourceSystem withUpdatedAt(LocalDateTime value) {
        setUpdatedAt(value);
        return this;
    }

    @Override
    public SourceSystem withCreateAt(LocalDateTime value) {
        setCreateAt(value);
        return this;
    }

}
