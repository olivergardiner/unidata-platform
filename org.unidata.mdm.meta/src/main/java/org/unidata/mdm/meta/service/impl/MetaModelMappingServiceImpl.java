/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.meta.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.ILock;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.type.model.ModelSearchObject;
import org.unidata.mdm.core.util.SecurityUtils;
import org.unidata.mdm.meta.type.model.attributes.ArrayMetaModelAttribute;
import org.unidata.mdm.meta.type.model.ArrayValueType;
import org.unidata.mdm.meta.type.model.attributes.CodeMetaModelAttribute;
import org.unidata.mdm.meta.type.model.attributes.ComplexMetaModelAttribute;
import org.unidata.mdm.meta.type.model.entities.ComplexAttributesHolderEntity;
import org.unidata.mdm.meta.type.model.CustomProperty;
import org.unidata.mdm.meta.type.model.entities.Entity;
import org.unidata.mdm.meta.type.model.entities.LookupEntity;
import org.unidata.mdm.meta.type.model.entities.NestedEntity;
import org.unidata.mdm.meta.type.model.entities.Relation;
import org.unidata.mdm.meta.type.model.attributes.SimpleMetaModelAttribute;
import org.unidata.mdm.meta.type.model.SimpleDataType;
import org.unidata.mdm.meta.dto.GetEntityDTO;
import org.unidata.mdm.meta.exception.MetaExceptionIds;
import org.unidata.mdm.meta.service.MetaCustomPropertiesConstants;
import org.unidata.mdm.meta.service.MetaModelMappingService;
import org.unidata.mdm.meta.service.MetaModelService;
import org.unidata.mdm.meta.type.search.EntityIndexType;
import org.unidata.mdm.meta.type.search.EtalonIndexType;
import org.unidata.mdm.meta.type.search.ModelHeaderField;
import org.unidata.mdm.meta.type.search.ModelIndexType;
import org.unidata.mdm.meta.type.search.RecordHeaderField;
import org.unidata.mdm.meta.type.search.RelationHeaderField;
import org.unidata.mdm.search.context.IndexRequestContext;
import org.unidata.mdm.search.context.MappingRequestContext;
import org.unidata.mdm.search.context.SearchRequestContext;
import org.unidata.mdm.search.service.SearchService;
import org.unidata.mdm.search.type.form.FieldsGroup;
import org.unidata.mdm.search.type.form.FormField;
import org.unidata.mdm.search.type.indexing.Indexing;
import org.unidata.mdm.search.type.indexing.IndexingField;
import org.unidata.mdm.search.type.mapping.Mapping;
import org.unidata.mdm.search.type.mapping.MappingField;
import org.unidata.mdm.search.type.mapping.impl.BooleanMappingField;
import org.unidata.mdm.search.type.mapping.impl.CompositeMappingField;
import org.unidata.mdm.search.type.mapping.impl.DateMappingField;
import org.unidata.mdm.search.type.mapping.impl.DoubleMappingField;
import org.unidata.mdm.search.type.mapping.impl.LongMappingField;
import org.unidata.mdm.search.type.mapping.impl.StringMappingField;
import org.unidata.mdm.search.type.mapping.impl.TimeMappingField;
import org.unidata.mdm.search.type.mapping.impl.TimestampMappingField;
import org.unidata.mdm.search.type.query.SearchQuery;
import org.unidata.mdm.search.util.SearchUtils;
import org.unidata.mdm.system.exception.PlatformFailureException;

/**
 * @author Mikhail Mikhailov on Oct 14, 2019
 */
@Component
public class MetaModelMappingServiceImpl implements MetaModelMappingService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MetaModelMappingServiceImpl.class);

    private static final String CREATE_INDEX_LOCK_NAME = "createMetaIndexLock";

    @Autowired
    private MetaModelService metaModelService;

    @Autowired
    private SearchService searchService;

    @Autowired
    private HazelcastInstance hazelcastInstance;
    /**
     * Constructor.
     */
    public MetaModelMappingServiceImpl() {
        super();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void afterModuleStartup() {
        EtalonIndexType.ETALON.addChild(EntityIndexType.RECORD);
        EtalonIndexType.ETALON.addChild(EntityIndexType.RELATION);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void afterPlatformStartup() {

        // Ensure, service indexes created
        ensureMetaModelIndex();

        // UN-6722
        List<NestedEntity> nested = metaModelService.getNestedEntitiesList();
        List<Entity> entities = metaModelService.getEntitiesList();
        List<LookupEntity> lookups = metaModelService.getLookupEntitiesList();
        List<Relation> relations = metaModelService.getRelationsList();
        String storageId = SecurityUtils.getCurrentUserStorageId();

        entities.forEach(entityDef -> updateEntityMapping(storageId, false, entityDef, nested));
        lookups.forEach(entityDef -> updateLookupMapping(storageId, false, entityDef));
        relations.forEach(relationDef -> updateRelationMapping(storageId, null, relationDef));
    }

    @Override
    public void ensureMetaModelIndex() {

        final Mapping modelHeaders = new Mapping(ModelIndexType.MODEL)
            .withFields(
                new StringMappingField(ModelHeaderField.FIELD_NAME.getName())
                    .withDocValue(true),
                new StringMappingField(ModelHeaderField.FIELD_DISPLAY_NAME.getName())
                    .withDocValue(true),
                new StringMappingField(ModelHeaderField.FIELD_SEARCH_OBJECTS.getName())
                    .withDocValue(true),
                new StringMappingField(ModelHeaderField.FIELD_VALUE.getName())
                    .withDocValue(true));

        final ILock createIndexLock = hazelcastInstance.getLock(CREATE_INDEX_LOCK_NAME);
        try {
            if (createIndexLock.tryLock(5, TimeUnit.SECONDS)) {
                try {
                    MappingRequestContext mCtx = MappingRequestContext.builder()
                            .entity(ModelIndexType.INDEX_NAME)
                            .storageId(SecurityUtils.getCurrentUserStorageId())
                            .mapping(modelHeaders)
                            .build();

                    searchService.process(mCtx);
                } finally {
                    createIndexLock.unlock();
                }
            } else {
                final String message = "Cannot aquire model index create lock.";
                LOGGER.error(message);
                throw new PlatformFailureException(message, MetaExceptionIds.EX_META_INDEX_LOCK_TIME_OUT);
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            final String message = "Cannot aquire model index create lock.";
            LOGGER.error(message);
            throw new PlatformFailureException(message, e, MetaExceptionIds.EX_META_INDEX_LOCK_TIME_OUT);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void cleanMetaModelIndex() {
        searchService.deleteFoundResult(
            SearchRequestContext.builder(ModelIndexType.MODEL, ModelIndexType.INDEX_NAME)
                .skipEtalonId(true)
                .fetchAll(true)
                .storageId(SecurityUtils.getCurrentUserStorageId())
                .build());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void putToMetaModelIndex(Collection<ModelSearchObject> objects) {

        if (CollectionUtils.isEmpty(objects)) {
            return;
        }

        List<Indexing> indexings = objects.stream()
                .filter(Objects::nonNull)
                .map(element -> {
                    removeFromMetaModelIndex(element.getEntityName());
                    return element.getSearchElements().entries().stream()
                        .map(entry ->
                            new Indexing(ModelIndexType.MODEL, null)
                                .withFields(
                                        IndexingField.of(ModelHeaderField.FIELD_SEARCH_OBJECTS.getName(), entry.getKey()),
                                        IndexingField.of(ModelHeaderField.FIELD_VALUE.getName(), entry.getValue()),
                                        IndexingField.of(ModelHeaderField.FIELD_NAME.getName(), element.getEntityName()),
                                        IndexingField.of(ModelHeaderField.FIELD_DISPLAY_NAME.getName(), element.getDisplayName())
                                ))
                        .collect(Collectors.toList());
                })
                .flatMap(Collection::stream)
                .collect(Collectors.toList());

        IndexRequestContext irc = IndexRequestContext.builder()
                .entity(ModelIndexType.INDEX_NAME)
                .storageId(SecurityUtils.getCurrentUserStorageId())
                .refresh(true)
                .index(indexings)
                .build();

        searchService.process(irc);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dropAllEntityIndexes(String storageId) {

        String selectedStorageId = Objects.isNull(storageId) ? SecurityUtils.getCurrentUserStorageId() : storageId;
        Stream<String> entities = metaModelService.getEntitiesList().stream().map(Entity::getName);
        Stream<String> lookupEntities = metaModelService.getLookupEntitiesList().stream().map(LookupEntity::getName);
        Stream.concat(entities, lookupEntities).forEach(ent ->
            searchService.dropIndex(
                MappingRequestContext.builder()
                    .storageId(selectedStorageId)
                    .entity(ent)
                    .drop(true)
                    .build()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeFromMetaModelIndex(String... entityNames) {

        if (Objects.isNull(entityNames)) {
            return;
        }

        String storageId = SecurityUtils.getCurrentUserStorageId();
        List<FormField> fields = Stream.of(entityNames)
            .map(name -> FormField.exact(ModelHeaderField.FIELD_NAME, name))
            .collect(Collectors.toList());

        if (fields.isEmpty()) {
            return;
        }

        searchService.deleteFoundResult(SearchRequestContext.builder(ModelIndexType.MODEL, ModelIndexType.INDEX_NAME)
                .skipEtalonId(true)
                .query(SearchQuery.formQuery(FieldsGroup.or(fields)))
                .storageId(storageId)
                .build());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateEntityMappings(String storageId, boolean force, List<String> names) {

        if (CollectionUtils.isEmpty(names)) {
            return;
        }

        updateEntityMappings(storageId, force, names.toArray(new String[names.size()]));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateRelationMappings(String storageId, String entityName, List<String> relationNames) {

        if (CollectionUtils.isEmpty(relationNames)) {
            return;
        }

        updateRelationMappings(storageId, entityName, relationNames.toArray(new String[relationNames.size()]));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateEntityMappings(String storageId, boolean force, String... names) {

        String selectedStorageId = Objects.isNull(storageId) ? SecurityUtils.getCurrentUserStorageId() : storageId;
        for (String entityName : names) {

            boolean isEntity = metaModelService.isEntity(entityName);
            boolean isLookup = !isEntity && metaModelService.isLookupEntity(entityName);

            if (!isEntity && !isLookup) {
                LOGGER.info("Meta oject with name {} not found. Skipping.", entityName);
                continue;
            }

            if (isEntity) {
                GetEntityDTO dto = metaModelService.getEntityById(entityName);
                updateEntityMapping(selectedStorageId, force, dto.getEntity(), dto.getRefs());
            } else {
                LookupEntity entity = metaModelService.getLookupEntityById(entityName);
                updateLookupMapping(selectedStorageId, force, entity);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateRelationMappings(String storageId, String entityName, String... relationNames) {

        String selectedStorageId = Objects.isNull(storageId) ? SecurityUtils.getCurrentUserStorageId() : storageId;
        for (String relationName : relationNames) {

            Relation relation = metaModelService.getRelationById(entityName);

            if (Objects.isNull(relation)) {
                LOGGER.info("Meta oject with name {} not found. Skipping.", relationName);
                continue;
            }

            updateRelationMapping(selectedStorageId, entityName, relation);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateRelationMapping(String storageId, String entityName, Relation relation) {

        if (Objects.isNull(relation)) {
            return;
        }
        LOGGER.info("updateRelationMapping entityName - {}", relation.getName());
        String selectedStorageId = Objects.isNull(storageId) ? SecurityUtils.getCurrentUserStorageId() : storageId;

        boolean processFromSide = Objects.isNull(entityName) || relation.getFromEntity().equals(entityName);
        boolean processToSide = Objects.isNull(entityName) || relation.getToEntity().equals(entityName);

        MappingRequestContext ctx;
        if (processFromSide) {
            ctx = processRelationMappings(selectedStorageId, relation, relation.getFromEntity());
            if (Objects.nonNull(ctx)) {
                searchService.process(ctx);
            }
        }

        if (processToSide) {
            ctx = processRelationMappings(selectedStorageId, relation, relation.getToEntity());
            if (Objects.nonNull(ctx)) {
                searchService.process(ctx);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateEntityMapping(String storageId, boolean force, Entity entity, List<NestedEntity> refs) {

        if (Objects.isNull(entity)) {
            return;
        }
        LOGGER.info("updateEntityMapping entityName - {}", entity.getName());

        String selectedStorageId = Objects.isNull(storageId) ? SecurityUtils.getCurrentUserStorageId() : storageId;
        MappingRequestContext ctx = processEntityMappings(selectedStorageId, force, entity, Objects.isNull(refs) ? Collections.emptyList() : refs);

        searchService.process(ctx);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateLookupMapping(String storageId, boolean force, LookupEntity entity) {

        if (Objects.isNull(entity)) {
            return;
        }
        LOGGER.info("updateLookupMapping entityName - {}", entity.getName());
        String selectedStorageId = Objects.isNull(storageId) ? SecurityUtils.getCurrentUserStorageId() : storageId;
        MappingRequestContext ctx = processLookupMappings(selectedStorageId, force, entity);

        searchService.process(ctx);
    }

    private MappingRequestContext processEntityMappings(String storageId, boolean force, Entity entity, List<NestedEntity> refs) {

        String shards = entity.getCustomProperties().stream()
                .filter(cp -> MetaCustomPropertiesConstants.SEARCH_SHARDS_NUMBER.equals(cp.getName()))
                .map(CustomProperty::getValue)
                .findAny()
                .orElse(null);

        String replicas = entity.getCustomProperties().stream()
                .filter(cp -> MetaCustomPropertiesConstants.SEARCH_REPLICAS_NUMBER.equals(cp.getName()))
                .map(CustomProperty::getValue)
                .findAny()
                .orElse(null);

        // after upgrade elasticsearch to 6.4 refactoring this place.
        String tokenize = entity.getCustomProperties().stream()
            .filter(cp -> MetaCustomPropertiesConstants.CUSTOM_TOKENIZE_PROPERTY.equals(cp.getName()))
            .map(CustomProperty::getValue)
            .findAny()
            .orElse(null);
        // custom score strategy
        String scoreStrategy = entity.getCustomProperties().stream()
            .filter(cp -> MetaCustomPropertiesConstants.SEARCH_SIMILARITY_TYPE.equals(cp.getName()))
            .map(CustomProperty::getValue)
            .findAny()
            .orElse(null);

        Map<String, Object> scoreStrategyParams = entity.getCustomProperties().stream()
            .filter(cp -> cp.getName().startsWith(MetaCustomPropertiesConstants.SEARCH_SIMILARITY_PARAM))
            .collect(Collectors.toMap(cp -> StringUtils.substringAfter(cp.getName(), MetaCustomPropertiesConstants.SEARCH_SIMILARITY_PARAM),
                CustomProperty::getValue));

        MappingRequestContext ctx = MappingRequestContext.builder()
                .storageId(storageId)
                .entity(entity.getName())
                .shards(StringUtils.isNotBlank(shards) ? Integer.parseInt(shards) : 0)
                .replicas(StringUtils.isNotBlank(replicas) ? Integer.parseInt(replicas) : 0)
                .whitespace(StringUtils.equals(tokenize, "whitespace"))
                .forceCreate(force)
                .mappings(
                        new Mapping(EtalonIndexType.ETALON),
                        new Mapping(EntityIndexType.RECORD).withFields(() -> processEntityMappingFields(entity, refs)))
                .build();

        ctx.scoreStrategy(scoreStrategy);
        ctx.scoreStrategyParams(scoreStrategyParams);

        return ctx;
    }

    private MappingRequestContext processLookupMappings(String storageId, boolean force, LookupEntity entity) {

        String shards = entity.getCustomProperties().stream()
                .filter(cp -> MetaCustomPropertiesConstants.SEARCH_SHARDS_NUMBER.equals(cp.getName()))
                .map(CustomProperty::getValue)
                .findAny()
                .orElse(null);

        String replicas = entity.getCustomProperties().stream()
                .filter(cp -> MetaCustomPropertiesConstants.SEARCH_REPLICAS_NUMBER.equals(cp.getName()))
                .map(CustomProperty::getValue)
                .findAny()
                .orElse(null);

        // custom score strategy
        String scoreStrategy = entity.getCustomProperties().stream()
            .filter(cp -> MetaCustomPropertiesConstants.SEARCH_SIMILARITY_TYPE.equals(cp.getName()))
            .map(CustomProperty::getValue)
            .findAny()
            .orElse(null);

        Map<String, Object> scoreStrategyParams = entity.getCustomProperties().stream()
            .filter(cp -> cp.getName().startsWith(MetaCustomPropertiesConstants.SEARCH_SIMILARITY_PARAM))
            .collect(Collectors.toMap(cp -> StringUtils.substringAfter(cp.getName(), MetaCustomPropertiesConstants.SEARCH_SIMILARITY_PARAM),
                CustomProperty::getValue));

        MappingRequestContext ctx = MappingRequestContext.builder()
                .storageId(storageId)
                .entity(entity.getName())
                .shards(StringUtils.isNotBlank(shards) ? Integer.parseInt(shards) : 0)
                .replicas(StringUtils.isNotBlank(replicas) ? Integer.parseInt(replicas) : 0)
                .forceCreate(force)
                .mappings(
                        new Mapping(EtalonIndexType.ETALON),
                        new Mapping(EntityIndexType.RECORD).withFields(() -> processLookupMappingFields(entity)))
                .build();

        ctx.scoreStrategy(scoreStrategy);
        ctx.scoreStrategyParams(scoreStrategyParams);

        return ctx;
    }

    private MappingRequestContext processRelationMappings(String storageId, Relation def, String sideEntityName) {

        String shards = def.getCustomProperties().stream()
                .filter(cp -> MetaCustomPropertiesConstants.SEARCH_SHARDS_NUMBER.equals(cp.getName()))
                .map(CustomProperty::getValue)
                .findAny()
                .orElse(null);

        String replicas = def.getCustomProperties().stream()
                .filter(cp -> MetaCustomPropertiesConstants.SEARCH_REPLICAS_NUMBER.equals(cp.getName()))
                .map(CustomProperty::getValue)
                .findAny()
                .orElse(null);

        return MappingRequestContext.builder()
                .storageId(storageId)
                .entity(sideEntityName)
                .shards(StringUtils.isNotBlank(shards) ? Integer.parseInt(shards) : 0)
                .replicas(StringUtils.isNotBlank(replicas) ? Integer.parseInt(replicas) : 0)
                .mappings(new Mapping(EntityIndexType.RELATION).withFields(() -> processRelationMappingFields(def)))
                .build();

    }

    private List<MappingField> processEntityMappingFields(Entity def, List<NestedEntity> refs) {

        final List<MappingField> recordHeaders = Arrays.asList(
            new StringMappingField(RecordHeaderField.FIELD_ETALON_ID.getName())
                .withDocValue(true),
            new StringMappingField(RecordHeaderField.FIELD_PERIOD_ID.getName())
                .withDocValue(true),
            new StringMappingField(RecordHeaderField.FIELD_ORIGINATOR.getName())
                .withDocValue(true),
            new TimestampMappingField(RecordHeaderField.FIELD_FROM.getName())
                .withFormat(SearchUtils.DEFAULT_TIMESTAMP_TARGET_FORMAT)
                .withDefaultValue(SearchUtils.ES_MIN_FROM),
            new TimestampMappingField(RecordHeaderField.FIELD_TO.getName())
                .withFormat(SearchUtils.DEFAULT_TIMESTAMP_TARGET_FORMAT)
                .withDefaultValue(SearchUtils.ES_MAX_TO),
            new TimestampMappingField(RecordHeaderField.FIELD_CREATED_AT.getName()),
            new TimestampMappingField(RecordHeaderField.FIELD_UPDATED_AT.getName()),
            new BooleanMappingField(RecordHeaderField.FIELD_PENDING.getName())
                .withDefaultValue(Boolean.FALSE),
            new BooleanMappingField(RecordHeaderField.FIELD_PUBLISHED.getName())
                .withDefaultValue(Boolean.TRUE),
            new BooleanMappingField(RecordHeaderField.FIELD_DELETED.getName())
                .withDefaultValue(Boolean.FALSE),
            new BooleanMappingField(RecordHeaderField.FIELD_INACTIVE.getName())
                .withDefaultValue(Boolean.FALSE),
            new StringMappingField(RecordHeaderField.FIELD_OPERATION_TYPE.getName())
                .withDocValue(true),
            new StringMappingField(RecordHeaderField.FIELD_EXTERNAL_KEYS.getName())
                .withDocValue(true)
        );

        List<MappingField> result = new ArrayList<>(recordHeaders.size()
                + def.getSimpleAttribute().size()
                + def.getArrayAttribute().size()
                + def.getComplexAttribute().size());

        result.addAll(recordHeaders);
        result.addAll(processComplexEntityMappingFields(def, refs));

        return result;
    }

    private List<MappingField> processLookupMappingFields(LookupEntity entity) {

        final List<MappingField> recordHeaders = Arrays.asList(
            new StringMappingField(RecordHeaderField.FIELD_ETALON_ID.getName())
                .withDocValue(true),
            new StringMappingField(RecordHeaderField.FIELD_PERIOD_ID.getName())
                .withDocValue(true),
            new StringMappingField(RecordHeaderField.FIELD_ORIGINATOR.getName())
                .withDocValue(true),
            new TimestampMappingField(RecordHeaderField.FIELD_FROM.getName())
                .withFormat(SearchUtils.DEFAULT_TIMESTAMP_TARGET_FORMAT)
                .withDefaultValue(SearchUtils.ES_MIN_FROM),
            new TimestampMappingField(RecordHeaderField.FIELD_TO.getName())
                .withFormat(SearchUtils.DEFAULT_TIMESTAMP_TARGET_FORMAT)
                .withDefaultValue(SearchUtils.ES_MAX_TO),
            new TimestampMappingField(RecordHeaderField.FIELD_CREATED_AT.getName()),
            new TimestampMappingField(RecordHeaderField.FIELD_UPDATED_AT.getName()),
            new BooleanMappingField(RecordHeaderField.FIELD_PENDING.getName())
                .withDefaultValue(Boolean.FALSE),
            new BooleanMappingField(RecordHeaderField.FIELD_PUBLISHED.getName())
                .withDefaultValue(Boolean.TRUE),
            new BooleanMappingField(RecordHeaderField.FIELD_DELETED.getName())
                .withDefaultValue(Boolean.FALSE),
            new BooleanMappingField(RecordHeaderField.FIELD_INACTIVE.getName())
                .withDefaultValue(Boolean.FALSE),
            new StringMappingField(RecordHeaderField.FIELD_OPERATION_TYPE.getName())
                .withDocValue(true),
            new StringMappingField(RecordHeaderField.FIELD_EXTERNAL_KEYS.getName())
                .withDocValue(true)
        );

        List<MappingField> result = new ArrayList<>(recordHeaders.size()
                + entity.getSimpleAttribute().size()
                + entity.getArrayAttribute().size()
                + entity.getAliasCodeAttributes().size()
                + 1);

        result.addAll(recordHeaders);

        // 1. Code
        Collection<CodeMetaModelAttribute> codeAttributes = new ArrayList<>();
        codeAttributes.add(entity.getCodeAttribute());
        codeAttributes.addAll(entity.getAliasCodeAttributes());
        for (CodeMetaModelAttribute codeAttribute : codeAttributes) {

            switch (codeAttribute.getSimpleDataType()) {
            case STRING:
                result.add(new StringMappingField(codeAttribute.getName()));
                break;
            case INTEGER:
                result.add(new LongMappingField(codeAttribute.getName()));
                break;
            default:
                break;
            }
        }

        // 2. Simple attributes
        for (SimpleMetaModelAttribute simpleAttr : entity.getSimpleAttribute()) {

            MappingField f = processSimpleAttribute(simpleAttr);
            if (Objects.isNull(f)) {
                continue;
            }

            result.add(f);
        }

        // 3. Array attributes
        for (ArrayMetaModelAttribute arrayAttr : entity.getArrayAttribute()) {

            MappingField f = processArrayAttribute(arrayAttr);
            if (Objects.isNull(f)) {
                continue;
            }

            result.add(f);
        }

        return result;
    }

    private List<MappingField> processRelationMappingFields(Relation def) {

        final List<MappingField> relationHeaders = Arrays.asList(
            new StringMappingField(RelationHeaderField.FIELD_ETALON_ID.getName())
                .withDocValue(true),
            new StringMappingField(RelationHeaderField.FIELD_FROM_ETALON_ID.getName())
                .withDocValue(true),
            new StringMappingField(RelationHeaderField.FIELD_TO_ETALON_ID.getName())
                .withDocValue(true),
            new StringMappingField(RelationHeaderField.FIELD_PERIOD_ID.getName())
                .withDocValue(true),
            new StringMappingField(RelationHeaderField.FIELD_RELATION_NAME.getName())
                .withDocValue(true),
            new StringMappingField(RelationHeaderField.FIELD_RELATION_TYPE.getName())
                .withDocValue(true),
            new StringMappingField(RelationHeaderField.FIELD_ORIGINATOR.getName())
                .withDocValue(true),
            new StringMappingField(RelationHeaderField.FIELD_OPERATION_TYPE.getName())
                .withDocValue(true),
            new TimestampMappingField(RelationHeaderField.FIELD_FROM.getName())
                .withFormat(SearchUtils.DEFAULT_TIMESTAMP_TARGET_FORMAT)
                .withDefaultValue(SearchUtils.ES_MIN_FROM),
            new TimestampMappingField(RelationHeaderField.FIELD_TO.getName())
                .withFormat(SearchUtils.DEFAULT_TIMESTAMP_TARGET_FORMAT)
                .withDefaultValue(SearchUtils.ES_MAX_TO),
            new TimestampMappingField(RelationHeaderField.FIELD_CREATED_AT.getName()),
            new TimestampMappingField(RelationHeaderField.FIELD_UPDATED_AT.getName()),
            new BooleanMappingField(RelationHeaderField.FIELD_PENDING.getName())
                .withDefaultValue(Boolean.FALSE),
            new BooleanMappingField(RelationHeaderField.FIELD_PUBLISHED.getName())
                .withDefaultValue(Boolean.TRUE),
            new BooleanMappingField(RelationHeaderField.FIELD_DELETED.getName())
                .withDefaultValue(Boolean.FALSE),
            new BooleanMappingField(RelationHeaderField.FIELD_INACTIVE.getName())
                .withDefaultValue(Boolean.FALSE),
            new BooleanMappingField(RelationHeaderField.FIELD_DIRECTION_FROM.getName())
                .withDefaultValue(Boolean.FALSE)
        );

        List<MappingField> result = new ArrayList<>(relationHeaders.size()
                + def.getArrayAttribute().size()
                + def.getSimpleAttribute().size()
                + def.getComplexAttribute().size());

        result.addAll(relationHeaders);
        result.addAll(processComplexEntityMappingFields(def, Collections.emptyList()));

        return result;
    }

    private List<MappingField> processComplexEntityMappingFields(ComplexAttributesHolderEntity def, List<NestedEntity> refs) {

        List<MappingField> result = new ArrayList<>(
                  def.getArrayAttribute().size()
                + def.getSimpleAttribute().size()
                + def.getComplexAttribute().size());

        // 1. Simple attributes
        for (SimpleMetaModelAttribute simpleAttr : def.getSimpleAttribute()) {

            MappingField f = processSimpleAttribute(simpleAttr);
            if (Objects.isNull(f)) {
                continue;
            }

            result.add(f);
        }

        // 2. Array attributes
        for (ArrayMetaModelAttribute arrayAttr : def.getArrayAttribute()) {

            MappingField f = processArrayAttribute(arrayAttr);
            if (Objects.isNull(f)) {
                continue;
            }

            result.add(f);
        }

        // 3. Complex attributes
        for (ComplexMetaModelAttribute complexAttr : def.getComplexAttribute()) {

            String nestedEntityName = complexAttr.getNestedEntityName();
            NestedEntity nested = refs.stream()
                    .filter(nestedEntityDef -> nestedEntityDef.getName().equals(nestedEntityName))
                    .findFirst()
                    .orElse(null);

            if (nested == null) {
                String message = "Invalid model. Nested entity [{}] from complex attribute [{}] not found.";
                LOGGER.warn(message, nestedEntityName, complexAttr.getName());
                throw new PlatformFailureException(message,
                        MetaExceptionIds.EX_META_MAPPING_NESTED_ENTITY_NOT_FOUND,
                        complexAttr.getNestedEntityName(), complexAttr.getName());
            }

            Collection<MappingField> fields = processComplexEntityMappingFields(nested, refs);
            if (CollectionUtils.isEmpty(fields)) {
                continue;
            }

            result.add(new CompositeMappingField(complexAttr.getName())
                    .withFields(fields));
        }

        return result;
    }

    /**
     * Processes an array attribute from EntityDef.
     * @param attr    the attribute
     * @param builder the builder
     *
     * @return the builder
     * @throws IOException
     */
    private MappingField processArrayAttribute(ArrayMetaModelAttribute attr) {

        if (attr == null) {
            return null;
        }

        boolean analyzed = true;
        String name = attr.getName();
        ArrayValueType originalType = null;
        if (StringUtils.isNotBlank(attr.getLookupEntityType())) {
            originalType = attr.getLookupEntityCodeAttributeType();
            analyzed = false;
        } else if (attr.getArrayValueType() != null) {
            originalType = attr.getArrayValueType();
        } else {
            originalType = ArrayValueType.STRING;
        }

        switch (originalType) {
        case STRING:
            return new StringMappingField(name)
                    .withMorphologicalAnalysis(attr.isSearchMorphologically())
                    .withCaseInsensitive(attr.isSearchCaseInsensitive())
                    .withAnalyzed(analyzed);
        case INTEGER:
            return new LongMappingField(name);
        case NUMBER:
            return new DoubleMappingField(name);
        case DATE:
            return new DateMappingField(name);
        case TIME:
            return new TimeMappingField(name);
        case TIMESTAMP:
            return new TimestampMappingField(name);
        default:
            break;
        }

        return null;
    }

    /**
     * Processes a simple attribute from EntityDef.
     * @param attr    the attribute
     * @param builder the builder
     *
     * @return the builder
     * @throws IOException
     */
    private MappingField processSimpleAttribute(SimpleMetaModelAttribute attr) {

        if (attr == null) {
            return null;
        }

        boolean analyzed = true;
        String name = attr.getName();
        SimpleDataType originalType = null;
        if (attr.getSimpleDataType() != null) {
            originalType = attr.getSimpleDataType();
        } else if (StringUtils.isNotBlank(attr.getEnumDataType())
                || StringUtils.isNotBlank(attr.getLinkDataType())
                || CollectionUtils.isNotEmpty(attr.getDictionaryDataType())) {
            originalType = SimpleDataType.STRING;
            analyzed = false;
        } else if (StringUtils.isNotBlank(attr.getLookupEntityType())) {
            originalType = attr.getLookupEntityCodeAttributeType();
            analyzed = false;
        } else {
            originalType = SimpleDataType.STRING;
        }

        switch (originalType) {
        case BLOB:
        case CLOB:
        case STRING:
            return new StringMappingField(name)
                    .withMorphologicalAnalysis(attr.isSearchMorphologically())
                    .withCaseInsensitive(attr.isSearchCaseInsensitive())
                    .withAnalyzed(analyzed && originalType == SimpleDataType.STRING);
        case BOOLEAN:
            return new BooleanMappingField(name);
        case INTEGER:
            return new LongMappingField(name);
        case MEASURED:
        case NUMBER:
            return new DoubleMappingField(name);
        case DATE:
            return new DateMappingField(name);
        case TIME:
            return new TimeMappingField(name);
        case TIMESTAMP:
            return new TimestampMappingField(name);
        default:
            break;
        }

        return null;
    }
}
