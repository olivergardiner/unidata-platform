/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.meta.type.input.meta;

import org.apache.commons.lang3.StringUtils;
import org.unidata.mdm.meta.type.model.entities.EntitiesGroup;

/**
 * @author maria.chistyakova
 * @since 21.04.2020
 */
public class GroupVertex {
    /**
     * The id.
     */
    private String id;

    /**
     * The value.
     */
    private EntitiesGroup value;

    /**
     * Instantiates a new group vertex.
     *
     * @param value the value
     * @param path the path
     */
    public GroupVertex(EntitiesGroup value, String path) {
        this.value = value;
        this.id = StringUtils.isEmpty(path) ? value.getGroupName() : String.join(".", path, value.getGroupName());
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public EntitiesGroup getValue() {
        return value;
    }

    /**
     * Sets the value.
     *
     * @param value the new value
     */
    public void setValue(EntitiesGroup value) {
        this.value = value;
    }


}
