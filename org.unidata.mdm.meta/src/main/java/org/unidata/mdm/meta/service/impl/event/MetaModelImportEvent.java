package org.unidata.mdm.meta.service.impl.event;

import org.unidata.mdm.system.type.event.AbstractMulticastEvent;

public class MetaModelImportEvent extends AbstractMulticastEvent {

    public static final String OPERATION_ID = "MetaModelImportOperation";
    public static final String START_MAINTENANCE_STATUS = "START_MAINTENANCE";
    public static final String STOP_MAINTENANCE_STATUS = "STOP_MAINTENANCE";

    private String metaModelImportStatus;

    public MetaModelImportEvent(String metaModelImportStatus, String typeName, String id) {
        super(typeName, id);
        this.metaModelImportStatus = metaModelImportStatus;
    }

    public String getMetaModelImportStatus() {
        return metaModelImportStatus;
    }

    public void setMetaModelImportStatus(String metaModelImportStatus) {
        this.metaModelImportStatus = metaModelImportStatus;
    }

    @Override
    public boolean isLocalSource() {
        return true;
    }
}
