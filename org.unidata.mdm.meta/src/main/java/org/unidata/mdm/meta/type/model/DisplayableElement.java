package org.unidata.mdm.meta.type.model;


/**
 * Metamodel element
 *
 * @author Alexandr Serov
 * @since 27.08.2020
 **/
public interface DisplayableElement extends MetaModelElement {

    /**
     * @return element display name
     */
    String getDisplayName();

    /**
     * @param displayName element display name
     */
    void setDisplayName(String displayName);

    /**
     * @return element description
     */
    String getDescription();

    /**
     * @param description element description
     */
    void setDescription(String description);


}
