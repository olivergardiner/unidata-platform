/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.meta.service.converters;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.unidata.mdm.security.UserDef;
import com.unidata.mdm.security.UserPasswordDef;
import com.unidata.mdm.security.UserPropertyDef;
import org.apache.commons.collections4.CollectionUtils;
import org.unidata.mdm.core.dto.PasswordDTO;
import org.unidata.mdm.core.dto.UserDTO;
import org.unidata.mdm.core.dto.UserPropertyDTO;
import org.unidata.mdm.core.type.security.Endpoint;
import org.unidata.mdm.core.type.security.Role;
import org.unidata.mdm.meta.util.MetaJaxbUtils;

public final class UserObjectsToXmlDefinitionsConverter {
    private UserObjectsToXmlDefinitionsConverter() {
    }

    public static List<UserDef> convertUsers(final Collection<UserDTO> users) {
        return convertUsers(users, Collections.emptyList());
    }

    public static List<UserDef> convertUsers(final Collection<UserDTO> users, final Collection<PasswordDTO> passwords) {
        if (CollectionUtils.isEmpty(users)) {
            return Collections.emptyList();
        }
        final Map<String, List<PasswordDTO>> usersPasswords = passwords.stream()
                .collect(Collectors.groupingBy(p -> p.getUser().getLogin()));
        return users.stream()
                .map(u -> UserObjectsToXmlDefinitionsConverter.toXmlObject(u, usersPasswords.get(u.getLogin())))
                .collect(Collectors.toList());
    }

    public static UserDef toXmlObject(final UserDTO userDTO, final Collection<PasswordDTO> userPasswords) {
        return MetaJaxbUtils.getSecurityFactory().createUserDef()
                .withLogin(userDTO.getLogin())
                .withEmail(userDTO.getEmail())
                .withFirstName(userDTO.getFirstName())
                .withLastName(userDTO.getLastName())
                .withActive(userDTO.isActive())
                .withAdmin(userDTO.isAdmin())
                .withExternal(userDTO.isExternal())
                .withEmailNotification(userDTO.isEmailNotification())
                .withLocale(userDTO.getLocale() != null ? userDTO.getLocale().toLanguageTag() : null)
                .withCreatedAt(MetaJaxbUtils.localTimestampValueToXMGregorianCalendar(userDTO.getCreatedAt()))
                .withCreatedBy(userDTO.getCreatedBy())
                .withUpdatedAt(MetaJaxbUtils.localTimestampValueToXMGregorianCalendar(userDTO.getUpdatedAt()))
                .withUpdatedBy(userDTO.getUpdatedBy())
                .withSource(userDTO.getSecurityDataSource())
                .withApis(
                        CollectionUtils.isNotEmpty(userDTO.getEndpoints()) ?
                                userDTO.getEndpoints().stream().map(Endpoint::getName).collect(Collectors.toList()) :
                                Collections.emptyList()
                )
                .withPropertiesValues(
                        CommonSecurityObjectsToXmlDefinitionsConverter.convertProperties(userDTO.getCustomProperties())
                )
                .withRoles(
                        CollectionUtils.isNotEmpty(userDTO.getRoles()) ?
                                userDTO.getRoles().stream().map(Role::getName).collect(Collectors.toList()) :
                                Collections.emptyList()
                )
                .withLabels(
                        CommonSecurityObjectsToXmlDefinitionsConverter.convertSecurityLabels(userDTO.getSecurityLabels())
                ).withPasswords(
                        convertUserPasswords(userPasswords)
                );
    }

    private static List<UserPasswordDef> convertUserPasswords(Collection<PasswordDTO> userPasswords) {
        if (CollectionUtils.isEmpty(userPasswords)) {
            return null;
        }
        return userPasswords.stream()
                .map(p -> MetaJaxbUtils.getSecurityFactory().createUserPasswordDef()
                        .withPasswordText(p.getHashedText())
                        .withActive(p.isActive())
                )
                .collect(Collectors.toList());
    }


    public static List<UserPropertyDef> convertUserProperties(List<UserPropertyDTO> userProperties) {
        return userProperties.stream().map(userPropertyDTO ->
                MetaJaxbUtils.getSecurityFactory().createUserPropertyDef()
                        .withName(userPropertyDTO.getName())
                        .withRequired(userPropertyDTO.isRequired())
                        .withDisplayName(userPropertyDTO.getDisplayName())
        ).collect(Collectors.toList());
    }
}
