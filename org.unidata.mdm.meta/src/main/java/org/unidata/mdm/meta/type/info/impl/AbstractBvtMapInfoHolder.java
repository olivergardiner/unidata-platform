/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.meta.type.info.impl;

import java.util.Map;

import org.unidata.mdm.core.type.model.AttributeModelElement;
import org.unidata.mdm.core.type.model.BvtMapModelElement;

/**
 * @author Mikhail Mikhailov
 * BVT map holder.
 */
public abstract class AbstractBvtMapInfoHolder extends AbstractAttributesInfoHolder implements BvtMapModelElement {

    /**
     * BVT attributes map.
     */
    protected Map<String, Map<String, Integer>> bvtMap;
    /**
     * Constructor.
     * @param attrs
     */
    public AbstractBvtMapInfoHolder(Map<String, AttributeModelElement> attrs, Map<String, Map<String, Integer>> bvtMap) {
        super(attrs);
        this.bvtMap = bvtMap;
    }
    /**
     * @return the bvtMap
     */
    @Override
    public Map<String, Map<String, Integer>> getBvtMap() {
        return bvtMap;
    }
    /**
     * @param bvtMap the bvtMap to set
     */
    public void setBvtMap(Map<String, Map<String, Integer>> bvtMap) {
        this.bvtMap = bvtMap;
    }
}
