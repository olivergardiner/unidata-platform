/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 * 
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.meta.type.info.impl;

import org.unidata.mdm.core.type.model.IdentityModelElement;
import org.unidata.mdm.meta.type.model.SourceSystem;

/**
 * @author Mikhail Mikhailov
 *
 */
public class SourceSystemInfoHolder implements IdentityModelElement {

    /**
     * Source system.
     */
    private final SourceSystem sourceSystem;

    /**
     * Constructor.
     */
    public SourceSystemInfoHolder(SourceSystem sourceSystem) {
        super();
        this.sourceSystem = sourceSystem;
    }

    /**
     * @return the sourceSystem
     */
    public SourceSystem getSourceSystem() {
        return sourceSystem;
    }

    @Override
    public String getId() {
        return sourceSystem.getName();
    }

    @Override
    public Long getVersion() {
        return sourceSystem.getVersion();
    }
}
