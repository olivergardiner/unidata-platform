package org.unidata.mdm.meta.type.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import org.unidata.mdm.meta.type.model.entities.EntitiesGroup;
import org.unidata.mdm.meta.type.model.entities.Entity;
import org.unidata.mdm.meta.type.model.entities.LookupEntity;
import org.unidata.mdm.meta.type.model.entities.NestedEntity;
import org.unidata.mdm.meta.type.model.entities.Relation;
import org.unidata.mdm.meta.type.model.enumeration.EnumerationDataType;
import org.unidata.mdm.meta.type.model.measurement.MeasurementValues;

import static org.unidata.mdm.meta.type.model.ModelNamespace.META_MODEL_NAMESPACE;


@JacksonXmlRootElement(localName = "model", namespace = META_MODEL_NAMESPACE)
@JsonPropertyOrder({"sourceSystems", "enumerations", "lookupEntities", "measurementValues", "nestedEntities", "entities", "relations", "entitiesGroup"})
public class Model implements Serializable {

    private final static long serialVersionUID = 987654321L;

    @JacksonXmlElementWrapper(localName = "sourceSystems", namespace = META_MODEL_NAMESPACE)
    @JacksonXmlProperty(localName = "sourceSystem", namespace = META_MODEL_NAMESPACE)
    protected List<SourceSystem> sourceSystems;

    @JacksonXmlElementWrapper(localName = "enumerations", namespace = META_MODEL_NAMESPACE)
    @JacksonXmlProperty(localName = "enumeration", namespace = META_MODEL_NAMESPACE)
    protected List<EnumerationDataType> enumerations;

    @JacksonXmlElementWrapper(localName = "lookupEntities", namespace = META_MODEL_NAMESPACE)
    @JacksonXmlProperty(localName = "lookupEntity", namespace = META_MODEL_NAMESPACE)
    protected List<LookupEntity> lookupEntities;

    @JacksonXmlProperty(namespace = META_MODEL_NAMESPACE)
    protected MeasurementValues measurementValues;

    @JacksonXmlElementWrapper(localName = "nestedEntities", namespace = META_MODEL_NAMESPACE)
    @JacksonXmlProperty(localName = "nestedEntity", namespace = META_MODEL_NAMESPACE)
    protected List<NestedEntity> nestedEntities;

    @JacksonXmlElementWrapper(localName = "entities", namespace = META_MODEL_NAMESPACE)
    @JacksonXmlProperty(localName = "entity", namespace = META_MODEL_NAMESPACE)
    protected List<Entity> entities;

    @JacksonXmlElementWrapper(localName = "relations", namespace = META_MODEL_NAMESPACE)
    @JacksonXmlProperty(localName = "rel", namespace = META_MODEL_NAMESPACE)
    protected List<Relation> relations;

    @JacksonXmlProperty(namespace = META_MODEL_NAMESPACE)
    protected EntitiesGroup entitiesGroup;

    @JacksonXmlProperty(isAttribute = true)
    protected String storageId;

    public MeasurementValues getMeasurementValues() {
        return measurementValues;
    }

    public void setMeasurementValues(MeasurementValues value) {
        this.measurementValues = value;
    }

    public EntitiesGroup getEntitiesGroup() {
        return entitiesGroup;
    }

    public void setEntitiesGroup(EntitiesGroup value) {
        this.entitiesGroup = value;
    }

    public String getStorageId() {
        return storageId;
    }

    public void setStorageId(String value) {
        this.storageId = value;
    }

    public List<SourceSystem> getSourceSystems() {
        if (sourceSystems == null) {
            sourceSystems = new ArrayList<>();
        }
        return sourceSystems;
    }

    public void setSourceSystems(List<SourceSystem> sourceSystems) {
        this.sourceSystems = sourceSystems;
    }

    public List<EnumerationDataType> getEnumerations() {
        if (enumerations == null) {
            enumerations = new ArrayList<>();
        }
        return enumerations;
    }

    public void setEnumerations(List<EnumerationDataType> enumerations) {
        this.enumerations = enumerations;
    }

    public List<LookupEntity> getLookupEntities() {
        if (lookupEntities == null) {
            lookupEntities = new ArrayList<>();
        }
        return lookupEntities;
    }

    public void setLookupEntities(List<LookupEntity> lookupEntities) {
        this.lookupEntities = lookupEntities;
    }

    public List<NestedEntity> getNestedEntities() {
        if (nestedEntities == null) {
            nestedEntities = new ArrayList<>();
        }
        return nestedEntities;
    }

    public void setNestedEntities(List<NestedEntity> nestedEntities) {
        this.nestedEntities = nestedEntities;
    }

    public List<Entity> getEntities() {
        if (entities == null) {
            entities = new ArrayList<>();
        }
        return entities;
    }

    public void setEntities(List<Entity> entities) {
        this.entities = entities;
    }

    public List<Relation> getRelations() {
        if (relations == null) {
            relations = new ArrayList<>();
        }
        return relations;
    }

    public void setRelations(List<Relation> relations) {
        this.relations = relations;
    }

    public Model withMeasurementValues(MeasurementValues value) {
        setMeasurementValues(value);
        return this;
    }

    public Model withEntitiesGroup(EntitiesGroup value) {
        setEntitiesGroup(value);
        return this;
    }

    public Model withStorageId(String value) {
        setStorageId(value);
        return this;
    }

    public Model withSourceSystems(SourceSystem... values) {
        if (values!= null) {
            Collections.addAll(getSourceSystems(), values);
        }
        return this;
    }

    public Model withSourceSystems(Collection<SourceSystem> values) {
        if (values!= null && !values.isEmpty()) {
            getSourceSystems().addAll(values);
        }
        return this;
    }

    public Model withSourceSystems(List<SourceSystem> sourceSystems) {
        setSourceSystems(sourceSystems);
        return this;
    }

    public Model withEnumerations(EnumerationDataType... values) {
        if (values!= null) {
            Collections.addAll(getEnumerations(), values);
        }
        return this;
    }

    public Model withEnumerations(Collection<EnumerationDataType> values) {
        if (values!= null && !values.isEmpty()) {
            getEnumerations().addAll(values);
        }
        return this;
    }

    public Model withEnumerations(List<EnumerationDataType> enumerations) {
        setEnumerations(enumerations);
        return this;
    }

    public Model withLookupEntities(LookupEntity... values) {
        if (values!= null) {
            Collections.addAll(getLookupEntities(), values);
        }
        return this;
    }

    public Model withLookupEntities(Collection<LookupEntity> values) {
        if (values!= null && !values.isEmpty()) {
            getLookupEntities().addAll(values);
        }
        return this;
    }

    public Model withLookupEntities(List<LookupEntity> lookupEntities) {
        setLookupEntities(lookupEntities);
        return this;
    }

    public Model withNestedEntities(NestedEntity... values) {
        if (values!= null) {
            Collections.addAll(getNestedEntities(), values);
        }
        return this;
    }

    public Model withNestedEntities(Collection<NestedEntity> values) {
        if (values!= null && !values.isEmpty()) {
            getNestedEntities().addAll(values);
        }
        return this;
    }

    public Model withNestedEntities(List<NestedEntity> nestedEntities) {
        setNestedEntities(nestedEntities);
        return this;
    }

    public Model withEntities(Entity... values) {
        if (values!= null) {
            Collections.addAll(getEntities(), values);
        }
        return this;
    }

    public Model withEntities(Collection<Entity> values) {
        if (values!= null && !values.isEmpty()) {
            getEntities().addAll(values);
        }
        return this;
    }

    public Model withEntities(List<Entity> entities) {
        setEntities(entities);
        return this;
    }

    public Model withRelations(Relation... values) {
        if (values!= null) {
            Collections.addAll(getRelations(), values);
        }
        return this;
    }

    public Model withRelations(Collection<Relation> values) {
        if (values!= null && !values.isEmpty()) {
            getRelations().addAll(values);
        }
        return this;
    }

    public Model withRelations(List<Relation> relations) {
        setRelations(relations);
        return this;
    }

}
