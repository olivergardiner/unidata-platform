/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.meta.type.info.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.unidata.mdm.core.context.DataRecordContext;
import org.unidata.mdm.core.type.model.AttributeModelElement;
import org.unidata.mdm.core.type.model.ComplexModelElement;
import org.unidata.mdm.core.type.model.ContainerModelElement;
import org.unidata.mdm.core.type.model.GeneratingModelElement;
import org.unidata.mdm.core.type.model.GenerationStrategyType;
import org.unidata.mdm.core.type.model.MeasuredModelElement;
import org.unidata.mdm.core.type.model.support.AttributeValueGenerator;
import org.unidata.mdm.meta.type.model.MetaModelAttribute;
import org.unidata.mdm.meta.type.model.ValueGenerationStrategy;
import org.unidata.mdm.meta.type.model.attributes.SimpleTypeMetaModelAttribute;
import org.unidata.mdm.meta.type.model.attributes.ArrayMetaModelAttribute;
import org.unidata.mdm.meta.type.model.attributes.AttributeType;
import org.unidata.mdm.meta.type.model.attributes.CodeMetaModelAttribute;
import org.unidata.mdm.meta.type.model.attributes.ComplexMetaModelAttribute;
import org.unidata.mdm.meta.type.model.CustomProperty;
import org.unidata.mdm.meta.type.model.entities.AbstractEntity;
import org.unidata.mdm.meta.type.model.strategy.CustomValueGenerationStrategy;
import org.unidata.mdm.meta.type.model.attributes.SimpleMetaModelAttribute;
import org.unidata.mdm.meta.type.model.SimpleDataType;
import org.unidata.mdm.meta.util.ValueGeneratingUtils;
import org.unidata.mdm.search.type.FieldType;
import org.unidata.mdm.search.type.IndexField;

/**
 * @author Mikhail Mikhailov
 */
public class AttributeInfoHolder implements AttributeModelElement, MeasuredModelElement, ComplexModelElement, GeneratingModelElement, IndexField {
    /**
     * The attribute type.
     */
    private final AttributeType type;
    /**
     * Attribute definition.
     */
    private final MetaModelAttribute attribute;
    /**
     * Entity definition.
     */
    private final ContainerModelElement container;
    /**
     * Parent link.
     */
    private final AttributeModelElement parent;
    /**
     * Children.
     */
    private final List<AttributeModelElement> children = new ArrayList<>();
    /**
     * Calculated path.
     */
    private final String path;
    /**
     * Path broken up in tokens.
     */
    private final String[] tokens;
    /**
     * The depth of this path.
     */
    private final int level;
    /**
     * Order.
     */
    private final int order;
    /**
     * Code alternative flag.
     */
    private final boolean codeAlternative;
    /**
     * Value generator.
     */
    private final AttributeValueGenerator generator;
    /**
     * Constructor.
     * @param attr the attribute
     * @param parent parent link
     * @param path calculated path
     * @param level depth of this attribute
     */
    public AttributeInfoHolder(SimpleMetaModelAttribute attr, AbstractEntity entity, AttributeModelElement parent, String path, int level) {
        this.attribute = attr;
        this.container = new ContainerInfoHolder(entity);
        this.parent = parent;
        this.path = path;
        this.level = level;
        this.order = attr.getOrder();
        this.type = AttributeType.SIMPLE;
        this.tokens = StringUtils.split(path, '.');
        this.codeAlternative = false;
        this.generator = getGenerator(attribute.getValueGenerationStrategy());
    }
    /**
     * Constructor.
     * @param attr the attribute
     * @param parent parent link
     * @param path calculated path
     * @param level depth of this attribute
     */
    public AttributeInfoHolder(ArrayMetaModelAttribute attr, AbstractEntity entity, AttributeModelElement parent, String path, int level) {
        this.attribute = attr;
        this.container = new ContainerInfoHolder(entity);
        this.parent = parent;
        this.path = path;
        this.level = level;
        this.order = attr.getOrder();
        this.type = AttributeType.ARRAY;
        this.tokens = StringUtils.split(path, '.');
        this.codeAlternative = false;
        this.generator = getGenerator(attribute.getValueGenerationStrategy());
    }
    /**
     * Constructor.
     * @param attr the attribute
     * @param parent parent link
     * @param path calculated path
     * @param level depth of this attribute
     * @param isAlternative tells whether this code attr is an alternative one
     */
    public AttributeInfoHolder(CodeMetaModelAttribute attr, AbstractEntity entity, AttributeModelElement parent, String path, int level, boolean isAlternative) {
        this.attribute = attr;
        this.container = new ContainerInfoHolder(entity);
        this.parent = parent;
        this.path = path;
        this.level = level;
        this.order = -1;
        this.type = AttributeType.CODE;
        this.tokens = StringUtils.split(path, '.');
        this.codeAlternative = isAlternative;
        this.generator = getGenerator(attribute.getValueGenerationStrategy());
    }
    /**
     * Constructor.
     * @param attr the attribute
     * @param parent parent link
     * @param path calculated path
     * @param level depth of this attribute
     */
    public AttributeInfoHolder(ComplexMetaModelAttribute attr, AbstractEntity entity, AttributeModelElement parent, String path, int level) {
        this.attribute = attr;
        this.container = new ContainerInfoHolder(entity);
        this.parent = parent;
        this.path = path;
        this.level = level;
        this.order = attr.getOrder();
        this.type = AttributeType.COMPLEX;
        this.tokens = StringUtils.split(path, '.');
        this.codeAlternative = false;
        this.generator = getGenerator(attribute.getValueGenerationStrategy());
    }
    /**
     * @return the attribute
     */
    public MetaModelAttribute getAttribute() {
        return attribute;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public MeasuredModelElement getMeasured() {
        return isMeasured() ? this : null;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public IndexField getIndexed() {
        return isIndexed() ? this : null;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getValueId() {
        return Objects.isNull(((SimpleMetaModelAttribute) attribute).getMeasureSettings())
                ? null
                : ((SimpleMetaModelAttribute) attribute).getMeasureSettings().getValueId();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getDefaultUnitId() {
        return Objects.isNull(((SimpleMetaModelAttribute) attribute).getMeasureSettings())
                ? null
                : ((SimpleMetaModelAttribute) attribute).getMeasureSettings().getDefaultUnitId();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public int getMinCount() {
        return ((ComplexMetaModelAttribute) attribute).getMinCount().intValue();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public int getMaxCount() {
        final BigInteger maxCount = ((ComplexMetaModelAttribute) attribute).getMaxCount();
        return maxCount != null ? maxCount.intValue() : Integer.MAX_VALUE;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getNestedEntityName() {
        return ((ComplexMetaModelAttribute) attribute).getNestedEntityName();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public ComplexModelElement getComplex() {
        return isComplex() ? this : null;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        return attribute.getName();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getDisplayName() {
        return attribute.getDisplayName();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getMask() {

        if (isSimple()) {
            return ((SimpleMetaModelAttribute) attribute).getMask();
        } else if (isArray()) {
            return ((ArrayMetaModelAttribute) attribute).getMask();
        } else if (isCode()) {
            return ((CodeMetaModelAttribute) attribute).getMask();
        }

        return null;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getExchangeSeparator() {

        if (isArray()) {
            return ((ArrayMetaModelAttribute) attribute).getExchangeSeparator();
        }

        return null;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isNullable() {

        if (isSimple()) {
            return ((SimpleMetaModelAttribute) attribute).isNullable();
        } else if (isArray()) {
            return ((ArrayMetaModelAttribute) attribute).isNullable();
        } else if (isCode()) {
            return ((CodeMetaModelAttribute) attribute).isNullable();
        }

        return false;
    }
    /**
     * {@inheritDoc}
     * TODO: Calc value statically -- all fields are final.
     */
    @Override
    public AttributeValueType getValueType() {

        if (isComplex()) {
            return AttributeValueType.NONE;
        } else if (isEnumValue() || isLinkTemplate() || isDictionaryType()) {
            return AttributeValueType.STRING;
        }

        SimpleDataType t = isLookupLink()
                ? getLookupEntityCodeAttributeType()
                : getSimpleDataType();

        if (t == null) {
            return AttributeValueType.NONE;
        }

        switch (t) {
        case ANY:
            return AttributeValueType.ANY;
        case BLOB:
            return AttributeValueType.BLOB;
        case BOOLEAN:
            return AttributeValueType.BOOLEAN;
        case CLOB:
            return AttributeValueType.CLOB;
        case DATE:
            return AttributeValueType.DATE;
        case INTEGER:
            return AttributeValueType.INTEGER;
        case MEASURED:
            return AttributeValueType.MEASURED;
        case NUMBER:
            return AttributeValueType.NUMBER;
        case STRING:
            return AttributeValueType.STRING;
        case TIME:
            return AttributeValueType.TIME;
        case TIMESTAMP:
            return AttributeValueType.TIMESTAMP;
        default:
            break;
        }

        return null;
    }
    /**
     * Gets lookup link name.
     * @return the name
     */
    @Override
    public String getLookupLinkName() {

        if (isSimple()) {
            return ((SimpleMetaModelAttribute) attribute).getLookupEntityType();
        } else if (isArray()) {
            return ((ArrayMetaModelAttribute) attribute).getLookupEntityType();
        }

        return null;
    }

    /**
     * Get lookup entity display attributes.
     * @return the name
     */
    @Override
    public List<String> getLookupEntityDisplayAttributes() {

        if (isSimple()) {
            return new ArrayList<>(((SimpleMetaModelAttribute) attribute).getLookupEntityDisplayAttributes());
        } else if (isArray()) {
            return new ArrayList<>(((ArrayMetaModelAttribute) attribute).getLookupEntityDisplayAttributes());
        }

        return Collections.emptyList();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, String> getCustomProperties() {
        return attribute.getCustomProperties().stream()
                .collect(Collectors.toMap(CustomProperty::getName, CustomProperty::getValue));
    }
    /**
     * Show attr names for display or not.
     * @return true, if so, false otherwise
     */
    @Override
    public boolean showFieldNamesInDisplay() {

        if (isSimple()) {
            return (((SimpleMetaModelAttribute) attribute).isUseAttributeNameForDisplay());
        } else if (isArray()) {
            return (((ArrayMetaModelAttribute) attribute).isUseAttributeNameForDisplay());
        }

        return false;
    }
    /**
     * Gets the enum name.
     * @return enum name
     */
    @Override
    public String getEnumName() {
        return isEnumValue() ? ((SimpleMetaModelAttribute) attribute).getEnumDataType() : null;
    }

    /**
     * Gets the template value.
     * @return template value
     */
    @Override
    public String getLinkTemplate() {
        return isLinkTemplate() ? ((SimpleMetaModelAttribute) attribute).getLinkDataType() : null;
    }
    /**
     * @return the path
     */
    @Override
    public String getPath() {
        return path;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isAnalyzed() {
        return getValueType() == AttributeValueType.STRING
                && !isClob()
                && !isBlob()
                && !isEnumValue()
                && !isLinkTemplate()
                && !isDictionaryType()
                && !isCode()
                && !isCodeAlternative()
                && !isLookupLink();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public FieldType getFieldType() {
        return isComplex() ? FieldType.COMPOSITE : getValueType().toSearchType();
    }
    /**
     * @return the entity
     */
    @Override
    public ContainerModelElement getContainer() {
        return container;
    }
    /**
     * @return the parent
     */
    @Override
    public AttributeModelElement getParent() {
        return parent;
    }
    /**
     * @return the children
     */
    @Override
    public List<AttributeModelElement> getChildren() {
        return children;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public GeneratingModelElement getGenerating() {
        return isGenerating() ? this : null;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isGenerating() {
        return Objects.nonNull(generator);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Object generate(DataRecordContext input) {
        return generator.generate(this, input);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public GenerationStrategyType getStrategyType() {

        if (isGenerating()) {

            switch (attribute.getValueGenerationStrategy().getStrategyType()) {
                case CONCAT:
                    return GenerationStrategyType.CONCAT;
                case CUSTOM:
                    return GenerationStrategyType.CUSTOM;
                case RANDOM:
                    return GenerationStrategyType.RANDOM;
                case SEQUENCE:
                    return GenerationStrategyType.SEQUENCE;
            }
        }

        return null;
    }
    /**
     * Has parent or not.
     * @return true, if has
     */
    @Override
    public boolean hasParent() {
        return parent != null;
    }
    /**
     * Has children or not.
     * @return true, if has
     */
    @Override
    public boolean hasChildren() {
        return !children.isEmpty();
    }
    /**
     * @return the type
     */
    @Override
    public String getTypeName() {
        return type.name();
    }
    /**
     * @return the level
     */
    @Override
    public int getLevel() {
        return level;
    }
    /**
     * @return the order
     */
    @Override
    public int getOrder() {
        return order;
    }
    @Override
    public boolean isSimple() {
        return type == AttributeType.SIMPLE;
    }

    @Override
    public boolean isCode() {
        return type == AttributeType.CODE;
    }

    @Override
    public boolean isArray() {
        return type == AttributeType.ARRAY;
    }

    @Override
    public boolean isComplex() {
        return type == AttributeType.COMPLEX;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isLookupLink() {
        return (isSimple() && StringUtils.isNotBlank(((SimpleMetaModelAttribute) attribute).getLookupEntityType()))
            || (isArray() && StringUtils.isNotBlank(((ArrayMetaModelAttribute) attribute).getLookupEntityType()));
    }

    @Override
    public boolean isLinkTemplate() {
        return isSimple() && StringUtils.isNotBlank(((SimpleMetaModelAttribute) attribute).getLinkDataType());
    }

    @Override
    public boolean isEnumValue() {
        return isSimple() && StringUtils.isNotBlank(((SimpleMetaModelAttribute) attribute).getEnumDataType());
    }

    @Override
    public boolean isDictionaryType() {
        if (isSimple()) {
            return CollectionUtils.isNotEmpty(((SimpleMetaModelAttribute) attribute).getDictionaryDataType());
        } else if (isArray()){
            return CollectionUtils.isNotEmpty(((ArrayMetaModelAttribute) attribute).getDictionaryDataType());
        }
        return false;
    }

    @Override
    public boolean isMeasured() {
        return isSimple() && ((SimpleMetaModelAttribute) attribute).getSimpleDataType() == SimpleDataType.MEASURED;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isIndexed() {
        // Nothing is checked for now.
        // 'searchable' and 'hidden' flags can be checked in the future
        return isComplex() || getValueType().toSearchType() != null;
    }

    @Override
    public boolean isBlob() {
        return isSimple() && ((SimpleMetaModelAttribute) attribute).getSimpleDataType() == SimpleDataType.BLOB;
    }

    @Override
    public boolean isClob() {
        return isSimple() && ((SimpleMetaModelAttribute) attribute).getSimpleDataType() == SimpleDataType.CLOB;
    }

    @Override
    public boolean isDate() {
        return isSimple() && (
                ((SimpleMetaModelAttribute) attribute).getSimpleDataType() == SimpleDataType.DATE ||
                ((SimpleMetaModelAttribute) attribute).getSimpleDataType() == SimpleDataType.TIME ||
                ((SimpleMetaModelAttribute) attribute).getSimpleDataType() == SimpleDataType.TIMESTAMP
        );
    }

    @Override
    public boolean isUnique() {
        return isCode() || (isSimple() && ((SimpleMetaModelAttribute) attribute).isUnique());
    }

    @Override
    public boolean hasMask() {
        return (isSimple() && StringUtils.isNotBlank(((SimpleMetaModelAttribute) attribute).getMask()))
            || (isCode() && StringUtils.isNotBlank(((CodeMetaModelAttribute) attribute).getMask()))
            || (isArray() && StringUtils.isNotBlank(((ArrayMetaModelAttribute) attribute).getMask()));
    }

    @Override
    public boolean isOfPath(String path) {

        if (StringUtils.length(path) == 0 && level == 0) {
            return true;
        }

        String[] parts = StringUtils.split(path, '.');
        if (level != parts.length) {
            return false;
        }

        for (int i = level - 1; i >= 0; i--) {
            if (!StringUtils.equals(tokens[i], parts[i])) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return the codeAlternative
     */
    @Override
    public boolean isCodeAlternative() {
        return codeAlternative;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isMainDisplayable() {

        if (isCode() || isSimple()) {
            return ((SimpleTypeMetaModelAttribute) attribute).isMainDisplayable();
        } else if (isArray()) {
            return ((ArrayMetaModelAttribute) attribute).isMainDisplayable();
        }

        return false;
    }
    /**
     * @return the SimpleDataType of attribute or null
     */
    private SimpleDataType getSimpleDataType() {

        if (isSimple()) {
            return ((SimpleMetaModelAttribute) attribute).getSimpleDataType();
        } else if (isArray()) {
            return ((ArrayMetaModelAttribute) attribute).getArrayValueType().value();
        } else if (isCode()) {
            return ((CodeMetaModelAttribute) attribute).getSimpleDataType();
        }

        return null;
    }
    /**
     * @return the SimpleDataType of attribute or null
     */
    private SimpleDataType getLookupEntityCodeAttributeType() {

        if (isLookupLink()) {

            if (isSimple()) {
                return ((SimpleMetaModelAttribute) attribute).getLookupEntityCodeAttributeType();
            }

            if (isArray()) {
                return ((ArrayMetaModelAttribute) attribute).getLookupEntityCodeAttributeType().value();
            }
        }

        return null;
    }

    private AttributeValueGenerator getGenerator(ValueGenerationStrategy s) {

        if (Objects.nonNull(s)) {
            switch (s.getStrategyType()) {
            case CONCAT:
                return ValueGeneratingUtils.CONCAT_ATTRIBUTE_GENERATOR;
            case RANDOM:
                return ValueGeneratingUtils.RANDOM_ATTRIBUTE_GENERATOR;
            case CUSTOM:
                return ValueGeneratingUtils.defineAttributeCustomValueGenerator(((CustomValueGenerationStrategy) s).getClassName());
            default:
                break;
            }
        }

        return null;
    }
    /**
     *
     * @author Mikhail Mikhailov
     * Short top container digest.
     */
    private class ContainerInfoHolder implements ContainerModelElement {
        /**
         * Container name.
         */
        private final String name;
        /**
         * Container's display name.
         */
        private final String displayName;
        /**
         * Constructor.
         * @param name the name
         * @param displayName the display name
         */
        public ContainerInfoHolder(AbstractEntity entity) {
            super();
            this.name = entity.getName();
            this.displayName = entity.getDisplayName();
        }
        /**
         * {@inheritDoc}
         */
        @Override
        public String getName() {
            return name;
        }
        /**
         * {@inheritDoc}
         */
        @Override
        public String getDisplayName() {
            return displayName;
        }
    }
}
