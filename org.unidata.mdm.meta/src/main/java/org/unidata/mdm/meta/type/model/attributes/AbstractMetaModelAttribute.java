package org.unidata.mdm.meta.type.model.attributes;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.unidata.mdm.meta.type.model.CustomProperty;
import org.unidata.mdm.meta.type.model.MetaModelAttribute;
import org.unidata.mdm.meta.type.model.ValueGenerationStrategy;

import static org.unidata.mdm.meta.type.model.ModelNamespace.META_MODEL_NAMESPACE;


@JsonPropertyOrder({"customProperties", "valueGenerationStrategy"})
public abstract class AbstractMetaModelAttribute implements MetaModelAttribute {

    private final static long serialVersionUID = 987654321L;

    @JacksonXmlProperty(namespace = META_MODEL_NAMESPACE)
    protected List<CustomProperty> customProperties;
    @JacksonXmlProperty(namespace = META_MODEL_NAMESPACE)
    protected ValueGenerationStrategy valueGenerationStrategy;
    @JacksonXmlProperty(isAttribute = true)
    protected String name;
    @JacksonXmlProperty(isAttribute = true)
    protected String displayName;
    @JacksonXmlProperty(isAttribute = true)
    protected String description;
    @JacksonXmlProperty(isAttribute = true)
    protected boolean readOnly;
    @JacksonXmlProperty(isAttribute = true)
    protected boolean hidden;

    @Override
    public List<CustomProperty> getCustomProperties() {
        if (customProperties == null) {
            customProperties = new ArrayList<>();
        }
        return this.customProperties;
    }

    public void setCustomProperties(List<CustomProperty> customProperties) {
        this.customProperties = customProperties;
    }

    @Override
    public ValueGenerationStrategy getValueGenerationStrategy() {
        return valueGenerationStrategy;
    }

    public void setValueGenerationStrategy(ValueGenerationStrategy value) {
        this.valueGenerationStrategy = value;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String value) {
        this.name = value;
    }

    @Override
    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String value) {
        this.displayName = value;
    }

    @Override
    public String getDescription() {
        return description;
    }

    public void setDescription(String value) {
        this.description = value;
    }

    @Override
    public boolean isReadOnly() {
        return readOnly;
    }

    public void setReadOnly(boolean value) {
        this.readOnly = value;
    }

    @Override
    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean value) {
        this.hidden = value;
    }

}
