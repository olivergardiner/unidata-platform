package org.unidata.mdm.meta.type.model;

/**
 * Value generation strategy type
 **/
public enum ValueGenerationStrategyType {

    RANDOM,
    CONCAT,
    SEQUENCE,
    CUSTOM;

    public String value() {
        return name();
    }

    public static ValueGenerationStrategyType fromValue(String v) {
        return valueOf(v);
    }

}
