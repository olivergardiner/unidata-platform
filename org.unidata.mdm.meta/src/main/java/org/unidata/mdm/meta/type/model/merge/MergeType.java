package org.unidata.mdm.meta.type.model.merge;

public enum MergeType {

    BVR,
    BVT;

    public String value() {
        return name();
    }

    public static MergeType fromValue(String v) {
        return valueOf(v);
    }

}
