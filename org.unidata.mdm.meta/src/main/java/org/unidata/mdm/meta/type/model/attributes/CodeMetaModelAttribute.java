package org.unidata.mdm.meta.type.model.attributes;


import java.util.List;

import org.unidata.mdm.meta.type.model.CustomProperty;
import org.unidata.mdm.meta.type.model.SimpleDataType;
import org.unidata.mdm.meta.type.model.ValueGenerationStrategy;

public class CodeMetaModelAttribute extends AbstractSimpleMetaModelAttribute {

    private final static long serialVersionUID = 987654321L;

    public CodeMetaModelAttribute withSimpleDataType(SimpleDataType value) {
        super.setSimpleDataType(value);
        return this;
    }

    public CodeMetaModelAttribute withNullable(boolean nullable) {
        super.setNullable(nullable);
        return this;
    }

    public CodeMetaModelAttribute withUnique(boolean unique) {
        super.setUnique(unique);
        return this;
    }

    public CodeMetaModelAttribute withSearchable(boolean searchable) {
        super.setSearchable(searchable);
        return this;
    }

    public CodeMetaModelAttribute withDisplayable(boolean displayable) {
        super.setDisplayable(displayable);
        return this;
    }

    public CodeMetaModelAttribute withMainDisplayable(boolean mainDisplayable) {
        super.setMainDisplayable(mainDisplayable);
        return this;
    }

    public CodeMetaModelAttribute withMask(String mask) {
        super.setMask(mask);
        return this;
    }

    public CodeMetaModelAttribute withCustomProperties(List<CustomProperty> customProperties) {
        super.setCustomProperties(customProperties);
        return this;
    }

    public CodeMetaModelAttribute withValueGenerationStrategy(ValueGenerationStrategy value) {
        super.setValueGenerationStrategy(value);
        return this;
    }

    public CodeMetaModelAttribute withName(String value) {
        super.setName(value);
        return this;
    }

    public CodeMetaModelAttribute withDisplayName(String value) {
        super.setDisplayName(value);
        return this;
    }

    public CodeMetaModelAttribute withDescription(String value) {
        super.setDescription(value);
        return this;
    }

    public CodeMetaModelAttribute withReadOnly(boolean value) {
        super.setReadOnly(value);
        return this;
    }

    public CodeMetaModelAttribute withHidden(boolean value) {
        super.setHidden(value);
        return this;
    }
}
