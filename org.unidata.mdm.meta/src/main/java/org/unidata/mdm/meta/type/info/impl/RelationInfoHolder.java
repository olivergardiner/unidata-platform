/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.meta.type.info.impl;

import java.util.Collections;
import java.util.Date;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.unidata.mdm.core.type.model.AttributeModelElement;
import org.unidata.mdm.core.type.model.EntityModelElement;
import org.unidata.mdm.core.type.model.IdentityModelElement;
import org.unidata.mdm.core.type.model.IndexedModelElement;
import org.unidata.mdm.meta.type.model.PeriodBoundary;
import org.unidata.mdm.meta.type.model.CustomProperty;
import org.unidata.mdm.meta.type.model.entities.Relation;
import org.unidata.mdm.meta.type.model.entities.AbstractEntity;
import org.unidata.mdm.search.type.IndexField;
import org.unidata.mdm.system.util.ConvertUtils;

/**
 * @author Mikhail Mikhailov
 *         Relation wrapper.
 */
public class RelationInfoHolder extends AbstractAttributesInfoHolder implements EntityModelElement, IdentityModelElement, IndexedModelElement {
    /**
     * Relation.
     */
    private final Relation relation;
    /**
     * This entity validity start.
     */
    private final Date validityStart;
    /**
     * This entity validity end.
     */
    private final Date validityEnd;
    /**
     * The index fields.
     */
    private final Map<String, IndexField> indexFields;
    /**
     * Constructor.
     */
    public RelationInfoHolder(Relation relation, final Map<String, AttributeModelElement> attrs) {
        super(attrs);
        this.relation = relation;
        PeriodBoundary validityPeriod = relation.getValidityPeriod();
        this.validityStart = validityPeriod != null ? ConvertUtils.localDateTime2Date(validityPeriod.getStart()) : null;
        this.validityEnd = validityPeriod != null ? ConvertUtils.localDateTime2Date(validityPeriod.getEnd()): null;
        this.indexFields = MapUtils.isEmpty(attrs)
                ? Collections.emptyMap()
                : attrs.entrySet().stream()
                    .filter(e -> e.getValue().isIndexed())
                    .map(e -> Pair.of(e.getKey(), e.getValue().getIndexed()))
                    .collect(Collectors.toMap(Pair::getKey, Pair::getValue));
    }
    /**
     * @return the relation
     */
    public Relation getRelation() {
        return relation;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getId() {
        return relation.getName();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Long getVersion() {
        return relation.getVersion();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        return relation.getName();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getDisplayName() {
        return relation.getDisplayName();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isRelation() {
        return true;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isIndexed() {
        return true;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public IndexedModelElement getIndexed() {
        return this;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, IndexField> getIndexFields() {
        return indexFields;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, String> getCustomProperties() {
        return relation.getCustomProperties().stream()
                .collect(Collectors.toMap(CustomProperty::getName, CustomProperty::getValue));
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Date getValidityStart() {
        return validityStart;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Date getValidityEnd() {
        return validityEnd;
    }
    /**
     * {@inheritDoc}
     */
    public AbstractEntity getAbstractEntity() {
        return getRelation();
    }
}
