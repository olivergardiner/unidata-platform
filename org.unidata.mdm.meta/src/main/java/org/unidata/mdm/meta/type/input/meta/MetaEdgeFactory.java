/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.meta.type.input.meta;

import java.io.Serializable;

import org.jgrapht.EdgeFactory;

/**
 * A factory for creating MetaEdge objects.
 */
public class MetaEdgeFactory implements EdgeFactory<MetaVertex, MetaEdge<MetaVertex>>, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /* (non-Javadoc)
     * @see org.jgrapht.EdgeFactory#createEdge(java.lang.Object, java.lang.Object)
     */
    @Override
    public MetaEdge<MetaVertex> createEdge(MetaVertex sourceVertex, MetaVertex targetVertex) {
        return new MetaEdge<>(sourceVertex, targetVertex);
    }

}
