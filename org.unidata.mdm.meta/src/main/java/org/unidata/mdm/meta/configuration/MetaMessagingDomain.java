package org.unidata.mdm.meta.configuration;

import org.unidata.mdm.meta.module.MetaModule;
import org.unidata.mdm.meta.type.messaging.MetaTypes;
import org.unidata.mdm.system.type.messaging.DomainType;
import org.unidata.mdm.system.util.ResourceUtils;

/**
 * Meta messaging domain(s).
 * @author Mikhail Mikhailov on Jul 15, 2020
 */
public class MetaMessagingDomain {
    /**
     * Constructor.
     */
    private MetaMessagingDomain() {
        super();
    }
    /**
     * Domain name.
     */
    public static final String NAME = "meta";
    /**
     * The sole core domain.
     */
    public static final DomainType DOMAIN =
            DomainType.ofLocalized(NAME, ResourceUtils.asString("classpath:/routes/meta.xml"))
                .withDescription(MetaModule.MODULE_ID + ".messaging.domain.description")
                .withMessageTypes(MetaTypes.TYPES);
}
