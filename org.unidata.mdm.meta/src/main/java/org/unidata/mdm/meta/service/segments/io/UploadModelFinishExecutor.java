/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.meta.service.segments.io;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.meta.context.UploadModelRequestContext;
import org.unidata.mdm.meta.module.MetaModule;
import org.unidata.mdm.meta.service.MetaModelImportService;
import org.unidata.mdm.meta.type.input.meta.MetaGraph;
import org.unidata.mdm.system.type.pipeline.Finish;
import org.unidata.mdm.system.type.pipeline.Start;

/**
 * @author maria.chistyakova
 * @since 27.02.2020
 */
@Component(UploadModelFinishExecutor.SEGMENT_ID)
public class UploadModelFinishExecutor extends Finish<UploadModelRequestContext, MetaGraph> {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = MetaModule.MODULE_ID + "[UPLOAD_MODEL_FINISH]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = MetaModule.MODULE_ID + ".upload.model.finish.description";

    /**
     * Constructor.
     */
    public UploadModelFinishExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION, MetaGraph.class);
    }

    @Autowired
    MetaModelImportService metaModelImportService;

    /**
     * {@inheritDoc}
     */
    @Override
    public MetaGraph finish(UploadModelRequestContext ctx) {
        // no validation, no converters, nothing
        metaModelImportService.cleanupTree(
                ctx.getZipPath(),
                ctx.getRootPath()
        );
        return ctx.getMetaGraph();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?> start) {
        return UploadModelRequestContext.class.isAssignableFrom(start.getInputTypeClass());
    }
}
