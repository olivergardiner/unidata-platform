package org.unidata.mdm.meta.type.model.attributes;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.unidata.mdm.meta.type.model.SimpleDataType;

/**
 * Base implementation for simple attribute
 */
public abstract class AbstractSimpleMetaModelAttribute extends AbstractSearchableMetaModelAttribute implements SimpleTypeMetaModelAttribute {

    private final static long serialVersionUID = 987654321L;

    @JacksonXmlProperty(isAttribute = true)
    protected SimpleDataType simpleDataType;
    @JacksonXmlProperty(isAttribute = true)
    protected boolean nullable;
    @JacksonXmlProperty(isAttribute = true)
    protected boolean unique;

    @Override
    public SimpleDataType getSimpleDataType() {
        return simpleDataType;
    }

    @Override
    public void setSimpleDataType(SimpleDataType value) {
        this.simpleDataType = value;
    }

    public boolean isNullable() {
        return nullable;
    }

    public void setNullable(boolean nullable) {
        this.nullable = nullable;
    }

    public boolean isUnique() {
        return unique;
    }

    public void setUnique(boolean unique) {
        this.unique = unique;
    }

}
