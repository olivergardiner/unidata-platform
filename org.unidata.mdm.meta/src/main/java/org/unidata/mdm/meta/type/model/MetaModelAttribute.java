package org.unidata.mdm.meta.type.model;


/**
 * Meta model attribute definition
 *
 * @author Alexandr Serov
 * @since 25.08.2020
 **/
public interface MetaModelAttribute extends DisplayableElement {

    void setValueGenerationStrategy(ValueGenerationStrategy value);

    ValueGenerationStrategy getValueGenerationStrategy();

    boolean isReadOnly();

    void setReadOnly(boolean readOnly);

    boolean isHidden();

    void setHidden(boolean hidden);
}
