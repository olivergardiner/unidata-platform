/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 * 
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.meta.type.info.impl;

import java.util.Collection;
import java.util.HashMap;

import org.unidata.mdm.core.type.model.IdentityModelElement;
import org.unidata.mdm.meta.type.model.entities.EntitiesGroup;
import org.unidata.mdm.meta.type.model.entities.Entity;
import org.unidata.mdm.meta.type.model.entities.LookupEntity;

public class EntitiesGroupWrapper implements IdentityModelElement {

    private final EntitiesGroup entitiesGroup;

    private final String wrapperId;

    private final HashMap<String, Entity> nestedEntities = new HashMap<>();

    private final HashMap<String, LookupEntity> nestedLookupEntities = new HashMap<>();

    public EntitiesGroupWrapper(EntitiesGroup entitiesGroup, String wrapperId) {
        this.entitiesGroup = entitiesGroup;
        this.wrapperId = wrapperId;
    }

    @Override
    public String getId() {
        return entitiesGroup.getGroupName();
    }

    @Override
    public Long getVersion() {
        return entitiesGroup.getVersion();
    }

    public void addLookupEntityToGroup(LookupEntity lookupEntityDef) {
        nestedLookupEntities.put(lookupEntityDef.getName(), lookupEntityDef);
    }

    public void addEntityToGroup(Entity entity) {
        nestedEntities.put(entity.getName(), entity);
    }

    public Collection<Entity> getNestedEntites() {
        return nestedEntities.values();
    }

    public Collection<LookupEntity> getNestedLookupEntities() {
        return nestedLookupEntities.values();
    }

    public boolean removeEntity(String entityName) {
        return nestedEntities.remove(entityName) != null;
    }

    public boolean removeLookupEntity(String lookupEntityName) {
        return nestedLookupEntities.remove(lookupEntityName) != null;
    }

    public String getWrapperId() {
        return wrapperId;
    }

    public EntitiesGroup getEntitiesGroupDef() {
        return entitiesGroup;
    }
}
