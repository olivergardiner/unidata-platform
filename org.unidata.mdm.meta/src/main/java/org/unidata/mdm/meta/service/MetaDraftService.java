/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.meta.service;

import java.util.List;

import org.unidata.mdm.core.type.measurement.MeasurementValue;
import org.unidata.mdm.meta.type.model.entities.EntitiesGroup;
import org.unidata.mdm.meta.type.model.entities.Entity;
import org.unidata.mdm.meta.type.model.enumeration.EnumerationDataType;
import org.unidata.mdm.meta.type.model.entities.LookupEntity;
import org.unidata.mdm.meta.type.model.Model;
import org.unidata.mdm.meta.type.model.entities.NestedEntity;
import org.unidata.mdm.meta.type.model.entities.Relation;
import org.unidata.mdm.meta.type.model.SourceSystem;
import org.unidata.mdm.meta.context.DeleteModelRequestContext;
import org.unidata.mdm.meta.context.UpdateModelRequestContext;
import org.unidata.mdm.meta.dto.GetEntitiesByRelationSideDTO;
import org.unidata.mdm.meta.dto.GetEntitiesGroupsDTO;
import org.unidata.mdm.meta.dto.GetEntityDTO;
import org.unidata.mdm.meta.type.RelativeDirection;

/**
 * The Interface MetaDraftService.
 */
public interface MetaDraftService {
	/**
	 * Apply.
	 */
	void apply();

	/**
	 * Removes the draft.
	 */
	void removeDraft();
	/**
	 * Load active draft.
	 */
	void loadActiveDraft();

	/**
	 * Gets the entities groups.
	 *
	 * @return the entities groups
	 */
	GetEntitiesGroupsDTO getEntitiesGroups();

	/**
	 * Update.
	 *
	 * @param ctx the ctx
	 */
	void update(UpdateModelRequestContext ctx);

	/**
	 * Gets the enumerations list.
	 *
	 * @return the enumerations list
	 */
	List<EnumerationDataType> getEnumerationsList();

	/**
	 * Gets the entities list.
	 *
	 * @return the entities list
	 */
	List<Entity> getEntitiesList();

	/**
	 * Gets the entity by id.
	 *
	 * @param id the id
	 * @return the entity by id
	 */
	GetEntityDTO getEntityById(String id);

	Entity getEntityByIdNoDeps(String id);

	/**
	 * Removes the.
	 *
	 * @param ctx the ctx
	 */
	void remove(DeleteModelRequestContext ctx);

	/**
	 * Gets the lookup entities list.
	 *
	 * @return the lookup entities list
	 */
	List<LookupEntity> getLookupEntitiesList();

	/**
	 * Gets the lookup entity by id.
	 *
	 * @param id the id
	 * @return the lookup entity by id
	 */
	LookupEntity getLookupEntityById(String id);

	/**
	 * Gets the relations list.
	 *
	 * @return the relations list
	 */
	List<Relation> getRelationsList();

	/**
	 * Create new draft
	 * if changeActive, then copy from meta model cached, status "active"
	 * else
	 * draft in status "false" and copy from the version for DEFAULT draft storage
	 *
	 * one time - one active draft
	 *
	 * @param changeActive the is active
	 */
	void refreshDraft(boolean changeActive);

	/**
	 * Gets the entities filtered by relation side.
	 *
	 * @param entityName the entity name
	 * @param to the to
	 * @return the entities filtered by relation side
	 */
	GetEntitiesByRelationSideDTO getEntitiesFilteredByRelationSide(String entityName, RelativeDirection to);

	/**
	 * Gets the source systems list.
	 *
	 * @return the source systems list
	 */
	List<SourceSystem> getSourceSystemsList();

	/**
	 * Gets the all values.
	 *
	 * @return the all values
	 */
	List<MeasurementValue> getAllValues();

	/**
	 * Gets the value by id.
	 *
	 * @param id the id
	 * @return the value by id
	 */
	MeasurementValue getValueById(String id);

	/**
	 * Removes the value.
	 *
	 * @param measureValueId the measure value id
	 * @return true, if successful
	 */
	boolean removeValue(String measureValueId);

	/**
	 * Batch remove.
	 *
	 * @param measureValueIds the measure value ids
	 * @param b the b
	 * @param c the c
	 * @return true, if successful
	 */
	boolean batchRemove(List<String> measureValueIds, boolean b, boolean c);

	/**
	 * Save value.
	 *
	 * @param value the value
	 */
	void saveValue(MeasurementValue value);

	/**
	 * Gets the root group.
	 *
	 * @param storageId the storage id
	 * @return the root group
	 */
	EntitiesGroup getRootGroup(String storageId);

	/**
	 * Gets the relation by id.
	 *
	 * @param name the name
	 * @return the relation by id
	 */
	Relation getRelationById(String name);

	/**
	 * Export model.
	 *
	 * @param storageId the storage id
	 * @return the model
	 */
	Model exportModel(String storageId);

	/**
	 * Gets the enumeration by id.
	 *
	 * @param enumDataType the enum data type
	 * @return the enumeration by id
	 */
	EnumerationDataType getEnumerationById(String enumDataType);

	/**
	 * Gets the source system by id.
	 *
	 * @param sourceSystem the source system
	 * @return the source system by id
	 */
	SourceSystem getSourceSystemById(String sourceSystem);

	/**
	 * Gets nested entities list.
	 *
	 * @return the nested entities list
	 */
	List<NestedEntity> getNestedEntitiesList();

	/**
	 * Gets the nested entity by id.
	 *
	 * @param elementName the element name
	 * @return the nested entity by id
	 */
	NestedEntity getNestedEntityById(String elementName);

	void initDraftService();

}