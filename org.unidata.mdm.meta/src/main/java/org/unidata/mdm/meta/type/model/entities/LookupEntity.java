package org.unidata.mdm.meta.type.model.entities;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.unidata.mdm.meta.type.model.CustomProperty;
import org.unidata.mdm.meta.type.model.MetaModelEntity;
import org.unidata.mdm.meta.type.model.PeriodBoundary;
import org.unidata.mdm.meta.type.model.ValueGenerationStrategy;
import org.unidata.mdm.meta.type.model.attributes.ArrayMetaModelAttribute;
import org.unidata.mdm.meta.type.model.attributes.AttributeGroup;
import org.unidata.mdm.meta.type.model.attributes.CodeMetaModelAttribute;
import org.unidata.mdm.meta.type.model.attributes.SimpleMetaModelAttribute;
import org.unidata.mdm.meta.type.model.merge.MergeSettings;

import static org.unidata.mdm.meta.type.model.ModelNamespace.META_MODEL_NAMESPACE;

public class LookupEntity extends AbstractEntity implements MetaModelEntity {

    private final static long serialVersionUID = 987654321L;

    @JacksonXmlProperty(namespace = META_MODEL_NAMESPACE)
    protected ValueGenerationStrategy externalIdGenerationStrategy;

    @JacksonXmlProperty(namespace = META_MODEL_NAMESPACE)
    protected CodeMetaModelAttribute codeAttribute;

    @JacksonXmlProperty(namespace = META_MODEL_NAMESPACE)
    protected List<CodeMetaModelAttribute> aliasCodeAttributes;

    @JacksonXmlProperty(isAttribute = true)
    protected boolean dashboardVisible;

    @JacksonXmlProperty(isAttribute = true)
    protected String groupName;

    public ValueGenerationStrategy getExternalIdGenerationStrategy() {
        return externalIdGenerationStrategy;
    }

    public void setExternalIdGenerationStrategy(ValueGenerationStrategy value) {
        this.externalIdGenerationStrategy = value;
    }

    public CodeMetaModelAttribute getCodeAttribute() {
        return codeAttribute;
    }

    public void setCodeAttribute(CodeMetaModelAttribute value) {
        this.codeAttribute = value;
    }

    public List<CodeMetaModelAttribute> getAliasCodeAttributes() {
        if (aliasCodeAttributes == null) {
            aliasCodeAttributes = new ArrayList<>();
        }
        return this.aliasCodeAttributes;
    }

    public boolean isDashboardVisible() {
        return dashboardVisible;
    }

    public void setDashboardVisible(Boolean value) {
        this.dashboardVisible = value;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String value) {
        this.groupName = value;
    }

    public LookupEntity withExternalIdGenerationStrategy(ValueGenerationStrategy value) {
        setExternalIdGenerationStrategy(value);
        return this;
    }

    public LookupEntity withCodeAttribute(CodeMetaModelAttribute value) {
        setCodeAttribute(value);
        return this;
    }

    public LookupEntity withAliasCodeAttributes(CodeMetaModelAttribute... values) {
        if (values != null) {
            Collections.addAll(getAliasCodeAttributes(), values);
        }
        return this;
    }

    public LookupEntity withAliasCodeAttributes(Collection<CodeMetaModelAttribute> values) {
        if (values != null && !values.isEmpty()) {
            getAliasCodeAttributes().addAll(values);
        }
        return this;
    }

    public LookupEntity withDashboardVisible(Boolean value) {
        setDashboardVisible(value);
        return this;
    }

    public LookupEntity withGroupName(String value) {
        setGroupName(value);
        return this;
    }

    @Override
    public LookupEntity withSimpleAttribute(SimpleMetaModelAttribute... values) {
        if (values != null) {
            Collections.addAll(getSimpleAttribute(), values);
        }
        return this;
    }

    @Override
    public LookupEntity withSimpleAttribute(Collection<SimpleMetaModelAttribute> values) {
        if (values != null && !values.isEmpty()) {
            getSimpleAttribute().addAll(values);
        }
        return this;
    }

    @Override
    public LookupEntity withArrayAttribute(ArrayMetaModelAttribute... values) {
        if (values != null) {
            Collections.addAll(getArrayAttribute(), values);
        }
        return this;
    }

    @Override
    public LookupEntity withArrayAttribute(Collection<ArrayMetaModelAttribute> values) {
        if (values != null && !values.isEmpty()) {
            getArrayAttribute().addAll(values);
        }
        return this;
    }

    @Override
    public LookupEntity withMergeSettings(MergeSettings value) {
        setMergeSettings(value);
        return this;
    }

    @Override
    public LookupEntity withValidityPeriod(PeriodBoundary value) {
        setValidityPeriod(value);
        return this;
    }

    @Override
    public LookupEntity withAttributeGroups(AttributeGroup... values) {
        if (values != null) {
            Collections.addAll(getAttributeGroups(), values);
        }
        return this;
    }

    @Override
    public LookupEntity withAttributeGroups(Collection<AttributeGroup> values) {
        if (values != null && !values.isEmpty()) {
            getAttributeGroups().addAll(values);
        }
        return this;
    }

    @Override
    public LookupEntity withRelationGroups(RelationGroup... values) {
        if (values != null) {
            Collections.addAll(getRelationGroups(), values);
        }
        return this;
    }

    @Override
    public LookupEntity withRelationGroups(Collection<RelationGroup> values) {
        if (values != null && !values.isEmpty()) {
            getRelationGroups().addAll(values);
        }
        return this;
    }

    @Override
    public LookupEntity withCustomProperties(CustomProperty... values) {
        if (values != null) {
            Collections.addAll(getCustomProperties(), values);
        }
        return this;
    }

    @Override
    public LookupEntity withCustomProperties(Collection<CustomProperty> values) {
        if (values != null && !values.isEmpty()) {
            getCustomProperties().addAll(values);
        }
        return this;
    }

    @Override
    public LookupEntity withName(String value) {
        setName(value);
        return this;
    }

    @Override
    public LookupEntity withDisplayName(String value) {
        setDisplayName(value);
        return this;
    }

    @Override
    public LookupEntity withDescription(String value) {
        setDescription(value);
        return this;
    }

    @Override
    public LookupEntity withVersion(Long value) {
        setVersion(value);
        return this;
    }

    @Override
    public LookupEntity withUpdatedAt(LocalDateTime value) {
        setUpdatedAt(value);
        return this;
    }

    @Override
    public LookupEntity withCreateAt(LocalDateTime value) {
        setCreateAt(value);
        return this;
    }
}
