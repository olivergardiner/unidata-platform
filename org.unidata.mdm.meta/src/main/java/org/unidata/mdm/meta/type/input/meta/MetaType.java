/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.meta.type.input.meta;

import java.io.Serializable;

/**
 * todo: refactor to class, move CUSTOM_CF, COMPOSITE_CF, etc to modules
 * The Enum MetaType.
 */
public enum MetaType implements Serializable {

    /**
     * The entity.
     */
    ENTITY,

    /**
     * The lookup.
     */
    LOOKUP,

    /**
     * The enum.
     */
    ENUM,

    /**
     * The classifier.
     */
    CLASSIFIER,

    /**
     * The measure.
     */
    MEASURE,

    /**
     * The custom cf.
     */
    CF,

    /**
     * The custom cf.
     */
    CUSTOM_CF,

    /**
     * The composite cf.
     */
    COMPOSITE_CF,

    /**
     * The match rule.
     */
    MATCH_RULE,

    /**
     * The merge rule.
     */
    MERGE_RULE,

    /**
     * The sorce system.
     */
    SOURCE_SYSTEM,
    /**
     * The zip.
     */
    ZIP,

    /**
     * The relation.
     */
    RELATION,
    NESTED_ENTITY,
    GROUPS
}
