package org.unidata.mdm.meta.type.model.attributes;

import java.io.Serializable;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;


public class AttributeMeasurementSettings implements Serializable {

    private final static long serialVersionUID = 987654321L;
    @JacksonXmlProperty(isAttribute = true)
    protected String valueId;
    @JacksonXmlProperty(isAttribute = true)
    protected String defaultUnitId;

    public String getValueId() {
        return valueId;
    }

    public void setValueId(String value) {
        this.valueId = value;
    }

    public String getDefaultUnitId() {
        return defaultUnitId;
    }

    public void setDefaultUnitId(String value) {
        this.defaultUnitId = value;
    }

    public AttributeMeasurementSettings withValueId(String value) {
        setValueId(value);
        return this;
    }

    public AttributeMeasurementSettings withDefaultUnitId(String value) {
        setDefaultUnitId(value);
        return this;
    }

}
