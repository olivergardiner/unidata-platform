package org.unidata.mdm.meta.type.model.attributes;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.google.common.base.MoreObjects;
import org.unidata.mdm.meta.type.model.ArrayValueType;
import org.unidata.mdm.meta.type.model.CustomProperty;
import org.unidata.mdm.meta.type.model.OrderedElement;
import org.unidata.mdm.meta.type.model.ValueGenerationStrategy;

import static org.unidata.mdm.meta.type.model.ModelNamespace.META_MODEL_NAMESPACE;


@JsonPropertyOrder({"lookupEntityDisplayAttributes", "lookupEntitySearchAttributes", "dictionaryDataType"})
public class ArrayMetaModelAttribute extends AbstractSearchableMetaModelAttribute implements OrderedElement, SearchableMetaModelAttribute {

    private final static long serialVersionUID = 987654321L;

    private static final String DEFAULT_EXCHANGE_SEPARATOR = "|";

    @JacksonXmlProperty(namespace = META_MODEL_NAMESPACE)
    protected List<String> lookupEntityDisplayAttributes;

    @JacksonXmlProperty(namespace = META_MODEL_NAMESPACE)
    protected List<String> lookupEntitySearchAttributes;

    @JacksonXmlProperty(namespace = META_MODEL_NAMESPACE)
    protected List<String> dictionaryDataType;

    @JacksonXmlProperty(isAttribute = true)
    protected boolean useAttributeNameForDisplay;

    @JacksonXmlProperty(isAttribute = true)
    protected ArrayValueType arrayValueType;

    @JacksonXmlProperty(isAttribute = true)
    protected boolean nullable;

    @JacksonXmlProperty(isAttribute = true)
    protected boolean searchMorphologically;

    @JacksonXmlProperty(isAttribute = true)
    protected boolean searchCaseInsensitive;

    @JacksonXmlProperty(isAttribute = true)
    protected String lookupEntityType;

    @JacksonXmlProperty(isAttribute = true)
    protected ArrayValueType lookupEntityCodeAttributeType;

    @JacksonXmlProperty(isAttribute = true)
    protected String exchangeSeparator;

    @JacksonXmlProperty(isAttribute = true)
    protected int order;

    // WITH
    public ArrayMetaModelAttribute withSearchable(boolean searchable) {
        super.setSearchable(searchable);
        return this;
    }

    public ArrayMetaModelAttribute withDisplayable(boolean displayable) {
        super.setDisplayable(displayable);
        return this;
    }

    public ArrayMetaModelAttribute withMainDisplayable(boolean mainDisplayable) {
        super.setMainDisplayable(mainDisplayable);
        return this;
    }

    public ArrayMetaModelAttribute withMask(String mask) {
        super.setMask(mask);
        return this;
    }

    public ArrayMetaModelAttribute withCustomProperties(List<CustomProperty> customProperties) {
        super.setCustomProperties(customProperties);
        return this;
    }

    public ArrayMetaModelAttribute withValueGenerationStrategy(ValueGenerationStrategy value) {
        super.setValueGenerationStrategy(value);
        return this;
    }

    public ArrayMetaModelAttribute withName(String value) {
        super.setName(value);
        return this;
    }

    public ArrayMetaModelAttribute withDisplayName(String value) {
        super.setDisplayName(value);
        return this;
    }

    public ArrayMetaModelAttribute withDescription(String value) {
        super.setDescription(value);
        return this;
    }

    public ArrayMetaModelAttribute withReadOnly(boolean value) {
        super.setReadOnly(value);
        return this;
    }

    public ArrayMetaModelAttribute withHidden(boolean value) {
        super.setHidden(value);
        return this;
    }

    public ArrayMetaModelAttribute withLookupEntityDisplayAttributes(String... lookupEntityDisplayAttributes) {
        if (lookupEntityDisplayAttributes != null) {
            Collections.addAll(getLookupEntityDisplayAttributes(), lookupEntityDisplayAttributes);
        }
        return this;
    }

    public ArrayMetaModelAttribute withLookupEntityDisplayAttributes(Collection<String> lookupEntityDisplayAttributes) {
        if (lookupEntityDisplayAttributes != null && !lookupEntityDisplayAttributes.isEmpty()) {
            getLookupEntityDisplayAttributes().addAll(lookupEntityDisplayAttributes);
        }
        return this;
    }


    public ArrayMetaModelAttribute withLookupEntitySearchAttributes(String... lookupEntitySearchAttributes) {
        if (lookupEntitySearchAttributes != null) {
            Collections.addAll(getLookupEntitySearchAttributes(), lookupEntitySearchAttributes);
        }
        return this;
    }

    public ArrayMetaModelAttribute withLookupEntitySearchAttributes(Collection<String> lookupEntitySearchAttributes) {
        if (lookupEntitySearchAttributes != null && !lookupEntitySearchAttributes.isEmpty()) {
            getLookupEntitySearchAttributes().addAll(lookupEntitySearchAttributes);
        }
        return this;
    }

    public ArrayMetaModelAttribute withDictionaryDataType(String... dictionaryDataType) {
        if (dictionaryDataType != null) {
            Collections.addAll(getDictionaryDataType(), dictionaryDataType);
        }
        return this;
    }

    public ArrayMetaModelAttribute withDictionaryDataType(Collection<String> dictionaryDataType) {
        if (dictionaryDataType != null && !dictionaryDataType.isEmpty()) {
            getDictionaryDataType().addAll(dictionaryDataType);
        }
        return this;
    }

    // WITH

    public List<String> getLookupEntityDisplayAttributes() {
        if (lookupEntityDisplayAttributes == null) {
            lookupEntityDisplayAttributes = new ArrayList<String>();
        }
        return this.lookupEntityDisplayAttributes;
    }

    public List<String> getLookupEntitySearchAttributes() {
        if (lookupEntitySearchAttributes == null) {
            lookupEntitySearchAttributes = new ArrayList<>();
        }
        return this.lookupEntitySearchAttributes;
    }

    public List<String> getDictionaryDataType() {
        if (dictionaryDataType == null) {
            dictionaryDataType = new ArrayList<>();
        }
        return this.dictionaryDataType;
    }

    public void setLookupEntityDisplayAttributes(List<String> lookupEntityDisplayAttributes) {
        this.lookupEntityDisplayAttributes = lookupEntityDisplayAttributes;
    }

    public void setLookupEntitySearchAttributes(List<String> lookupEntitySearchAttributes) {
        this.lookupEntitySearchAttributes = lookupEntitySearchAttributes;
    }

    public void setDictionaryDataType(List<String> dictionaryDataType) {
        this.dictionaryDataType = dictionaryDataType;
    }

    public boolean isUseAttributeNameForDisplay() {
        return useAttributeNameForDisplay;
    }

    public void setUseAttributeNameForDisplay(boolean useAttributeNameForDisplay) {
        this.useAttributeNameForDisplay = useAttributeNameForDisplay;
    }

    public ArrayValueType getArrayValueType() {
        return arrayValueType;
    }

    public void setArrayValueType(ArrayValueType arrayValueType) {
        this.arrayValueType = arrayValueType;
    }

    public boolean isNullable() {
        return nullable;
    }

    public void setNullable(boolean nullable) {
        this.nullable = nullable;
    }

    public boolean isSearchMorphologically() {
        return searchMorphologically;
    }

    public void setSearchMorphologically(boolean searchMorphologically) {
        this.searchMorphologically = searchMorphologically;
    }

    public boolean isSearchCaseInsensitive() {
        return searchCaseInsensitive;
    }

    public void setSearchCaseInsensitive(boolean searchCaseInsensitive) {
        this.searchCaseInsensitive = searchCaseInsensitive;
    }

    public String getLookupEntityType() {
        return lookupEntityType;
    }

    public void setLookupEntityType(String lookupEntityType) {
        this.lookupEntityType = lookupEntityType;
    }

    public ArrayValueType getLookupEntityCodeAttributeType() {
        return lookupEntityCodeAttributeType;
    }

    public void setLookupEntityCodeAttributeType(ArrayValueType lookupEntityCodeAttributeType) {
        this.lookupEntityCodeAttributeType = lookupEntityCodeAttributeType;
    }

    public String getExchangeSeparator() {
        return MoreObjects.firstNonNull(exchangeSeparator, DEFAULT_EXCHANGE_SEPARATOR);
    }

    public void setExchangeSeparator(String exchangeSeparator) {
        this.exchangeSeparator = exchangeSeparator;
    }

    @Override
    public int getOrder() {
        return order;
    }

    @Override
    public void setOrder(int order) {
        this.order = order;
    }
}
