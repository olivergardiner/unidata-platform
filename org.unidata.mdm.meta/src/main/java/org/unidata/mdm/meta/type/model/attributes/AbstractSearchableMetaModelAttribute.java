package org.unidata.mdm.meta.type.model.attributes;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.google.common.base.MoreObjects;

/**
 * Base implementation from searchable attributes
 *
 * @author Alexandr Serov
 * @since 26.08.2020
 * @see SearchableMetaModelAttribute
 **/
public abstract class AbstractSearchableMetaModelAttribute extends AbstractMetaModelAttribute implements SearchableMetaModelAttribute {

    @JacksonXmlProperty(isAttribute = true)
    protected boolean searchable;
    @JacksonXmlProperty(isAttribute = true)
    protected boolean displayable;
    @JacksonXmlProperty(isAttribute = true)
    protected boolean mainDisplayable;
    @JacksonXmlProperty(isAttribute = true)
    protected String mask;

    @Override
    public boolean isSearchable() {
        return searchable;
    }

    @Override
    public void setSearchable(boolean searchable) {
        this.searchable = searchable;
    }

    @Override
    public boolean isDisplayable() {
        return displayable;
    }

    @Override
    public void setDisplayable(boolean displayable) {
        this.displayable = displayable;
    }

    @Override
    public boolean isMainDisplayable() {
        return mainDisplayable;
    }

    @Override
    public void setMainDisplayable(boolean mainDisplayable) {
        this.mainDisplayable = mainDisplayable;
    }

    @Override
    public String getMask() {
        return MoreObjects.firstNonNull(mask, EMPTY_MASK);
    }

    @Override
    public void setMask(String mask) {
        this.mask = mask;
    }

}
