/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 * 
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.meta.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.unidata.mdm.core.dto.SecuredResourceDTO;
import org.unidata.mdm.core.service.RoleService;
import org.unidata.mdm.core.service.SecurityService;
import org.unidata.mdm.core.type.measurement.MeasurementValue;
import org.unidata.mdm.core.type.model.AttributeModelElement;
import org.unidata.mdm.core.type.security.Right;
import org.unidata.mdm.core.type.security.SecuredResourceCategory;
import org.unidata.mdm.core.type.security.SecuredResourceType;
import org.unidata.mdm.core.util.SecurityUtils;
import org.unidata.mdm.meta.type.model.entities.EntitiesGroup;
import org.unidata.mdm.meta.type.model.entities.Entity;
import org.unidata.mdm.meta.type.model.entities.AbstractEntity;
import org.unidata.mdm.meta.type.model.enumeration.EnumerationDataType;
import org.unidata.mdm.meta.type.model.entities.LookupEntity;
import org.unidata.mdm.meta.type.model.entities.NestedEntity;
import org.unidata.mdm.meta.type.model.entities.Relation;
import org.unidata.mdm.meta.type.model.SourceSystem;
import org.unidata.mdm.meta.context.DeleteModelRequestContext;
import org.unidata.mdm.meta.context.GetModelRequestContext;
import org.unidata.mdm.meta.context.UpdateModelRequestContext;
import org.unidata.mdm.meta.dto.GetEntitiesGroupsDTO;
import org.unidata.mdm.meta.dto.GetEntityDTO;
import org.unidata.mdm.meta.dto.GetModelDTO;
import org.unidata.mdm.meta.dto.GetModelEnumerationDTO;
import org.unidata.mdm.meta.dto.GetModelLookupDTO;
import org.unidata.mdm.meta.dto.GetModelMeasurementValueDTO;
import org.unidata.mdm.meta.dto.GetModelRelationDTO;
import org.unidata.mdm.meta.dto.GetModelSourceSystemDTO;
import org.unidata.mdm.meta.exception.MetaExceptionIds;
import org.unidata.mdm.meta.service.MetaDraftService;
import org.unidata.mdm.meta.service.MetaMeasurementService;
import org.unidata.mdm.meta.service.MetaModelValidationComponent;
import org.unidata.mdm.meta.type.info.impl.EntitiesGroupWrapper;
import org.unidata.mdm.meta.util.ModelUtils;
import org.unidata.mdm.system.exception.PlatformFailureException;

@Service("metaModelService")
public class SecureMetaModelService extends BaseMetaModelService  {
    /**
     * The MDS.
     */
    @Autowired
    private MetaDraftService metaDraftService;
    /**
     * Logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(SecureMetaModelService.class);

    /**
     * Security service.
     */
    @Autowired(required = false)
    private SecurityService securityService;

    /**
     * The validation service.
     */
    @Autowired(required = false)
    private MetaModelValidationComponent validationComponent;

    /**
     * Role service. Contains methods for role management.
     */
    @Autowired
    private RoleService roleServiceExt;
    /**
     * Measured values service.
     */
    @Autowired
    private MetaMeasurementService metaMeasurementService;
    /**
     * {@inheritDoc}
     */
    @Override
    public GetModelDTO getModel(GetModelRequestContext ctx) {

        GetModelDTO result = new GetModelDTO();

        processEntities(ctx, result);
        processLookups(ctx, result);
        processRelations(ctx, result);
        processEnumerations(ctx, result);
        processSourceSystems(ctx, result);
        processMeasuredValues(ctx, result);
        processEntityGroups(ctx, result);

        return result;
    }

    private void processEntities(GetModelRequestContext ctx, GetModelDTO dto) {

        if (!ctx.isAllEntities() && CollectionUtils.isEmpty(ctx.getEntityIds())) {
            return;
        }

        List<GetEntityDTO> entities;
        if (ctx.isAllEntities()) {
            entities = getAllEntitiesDto(ctx);
        } else {
            entities = getEntitiesDtoFiltredByEntityIds(ctx);
        }

        dto.setEntities(entities);
    }

    private List<GetEntityDTO> getEntitiesDtoFiltredByEntityIds(GetModelRequestContext ctx) {
        List<GetEntityDTO> entities;
        entities = ctx.getEntityIds().stream()
                .map(name -> {

                    if (ctx.isReduced()) {
                        Entity entity = ctx.isDraft()
                                ? metaDraftService.getEntityByIdNoDeps(name)
                                : getEntityByIdNoDeps(name);
                        return Objects.isNull(entity) ? null : new GetEntityDTO(entity, null, null);
                    }

                    return ctx.isDraft() ?  metaDraftService.getEntityById(name) : getEntityById(name);
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
        return entities;
    }

    private List<GetEntityDTO> getAllEntitiesDto(GetModelRequestContext ctx) {
        List<GetEntityDTO> entities;
        entities = ctx.isDraft()
            ? metaDraftService.getEntitiesList().stream()
                    .map(entity -> {
                        if (ctx.isReduced()) {
                            return new GetEntityDTO(entity, null, null);
                        }

                        return metaDraftService.getEntityById(entity.getName());
                    })
                    .collect(Collectors.toList())
            : getEntitiesList().stream()
                    .map(entity -> {
                        if (ctx.isReduced()) {
                            return new GetEntityDTO(entity, null, null);
                        }

                        return getEntityById(entity.getName());
                    })
                    .collect(Collectors.toList());
        return entities;
    }

    private void processLookups(GetModelRequestContext ctx, GetModelDTO dto) {

        if (!ctx.isAllLookups() && CollectionUtils.isEmpty(ctx.getLookupIds())) {
            return;
        }

        List<GetModelLookupDTO> entities;
        if (ctx.isAllEntities()) {
            entities = ctx.isDraft()
                ? metaDraftService.getLookupEntitiesList().stream().map(GetModelLookupDTO::new).collect(Collectors.toList())
                : getLookupEntitiesList().stream().map(GetModelLookupDTO::new).collect(Collectors.toList());
        } else {
            entities = ctx.getLookupIds().stream()
                    .map(name -> {

                        LookupEntity lookup = ctx.isDraft()
                                ? metaDraftService.getLookupEntityById(name)
                                : getLookupEntityById(name);

                        return Objects.isNull(lookup) ? null : new GetModelLookupDTO(lookup);
                    })
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
        }

        dto.setLookups(entities);
    }

    private void processRelations(GetModelRequestContext ctx, GetModelDTO dto) {

        if (!ctx.isAllRelations() && CollectionUtils.isEmpty(ctx.getRelationIds())) {
            return;
        }

        List<GetModelRelationDTO> relations;
        if (ctx.isAllEntities()) {
            relations = ctx.isDraft()
                ? metaDraftService.getRelationsList().stream().map(GetModelRelationDTO::new).collect(Collectors.toList())
                : getRelationsList().stream().map(GetModelRelationDTO::new).collect(Collectors.toList());
        } else {
            relations = ctx.getRelationIds().stream()
                    .map(name -> {

                        Relation relation;
                        if (ctx.isDraft()) {
                            relation = metaDraftService.getRelationById(name);
                        } else {
                            relation = getRelationById(name);
                        }

                        return Objects.nonNull(relation) ? new GetModelRelationDTO(relation) : null;
                    })
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
        }

        dto.setRelations(relations);
    }

    private void processEnumerations(GetModelRequestContext ctx, GetModelDTO dto) {

        if (!ctx.isAllEnumerations() && CollectionUtils.isEmpty(ctx.getEnumerationIds())) {
            return;
        }

        List<GetModelEnumerationDTO> enumerations;
        if (ctx.isAllEnumerations()) {
            enumerations = ctx.isDraft()
                    ? metaDraftService.getEnumerationsList().stream().map(GetModelEnumerationDTO::new).collect(Collectors.toList())
                    : getEnumerationsList().stream().map(GetModelEnumerationDTO::new).collect(Collectors.toList());
        } else {
            enumerations = ctx.getEnumerationIds().stream()
                    .map(name -> {

                        EnumerationDataType type = ctx.isDraft() ? metaDraftService.getEnumerationById(name) : getEnumerationById(name);
                        if (Objects.isNull(type)) {
                            return null;
                        }

                        return new GetModelEnumerationDTO(type);
                    })
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
        }

        dto.setEnumerations(enumerations);
    }

    private void processSourceSystems(GetModelRequestContext ctx, GetModelDTO dto) {

        if (!ctx.isAllSourceSystems() && CollectionUtils.isEmpty(ctx.getSourceSystemIds())) {
            return;
        }

        List<GetModelSourceSystemDTO> sourceSystems;
        if (ctx.isAllSourceSystems()) {
            sourceSystems = ctx.isDraft()
                    ? metaDraftService.getSourceSystemsList().stream().map(GetModelSourceSystemDTO::new).collect(Collectors.toList())
                    : getSourceSystemsList().stream().map(GetModelSourceSystemDTO::new).collect(Collectors.toList());
        } else {
            sourceSystems = ctx.getSourceSystemIds().stream()
                    .map(name -> {

                        SourceSystem sourceSystem = ctx.isDraft() ? metaDraftService.getSourceSystemById(name) : getSourceSystemById(name);
                        if (Objects.isNull(sourceSystem)) {
                            return null;
                        }

                        return new GetModelSourceSystemDTO(sourceSystem);
                    })
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
        }

        dto.setSourceSystems(sourceSystems);
    }

    private void processMeasuredValues(GetModelRequestContext ctx, GetModelDTO dto) {

        if (!ctx.isAllMeasuredValues() && CollectionUtils.isEmpty(ctx.getMeasuredValueIds())) {
            return;
        }

        List<GetModelMeasurementValueDTO> measurementValues;
        if (ctx.isAllSourceSystems()) {
            measurementValues = ctx.isDraft()
                    ? metaDraftService.getAllValues().stream().map(GetModelMeasurementValueDTO::new).collect(Collectors.toList())
                    : metaMeasurementService.getAllValues().stream().map(GetModelMeasurementValueDTO::new).collect(Collectors.toList());
        } else {
            measurementValues = ctx.getMeasuredValueIds().stream()
                    .map(name -> {

                        MeasurementValue val = ctx.isDraft() ? metaDraftService.getValueById(name) : metaMeasurementService.getValueById(name);
                        if (Objects.isNull(val)) {
                            return null;
                        }

                        return new GetModelMeasurementValueDTO(val);
                    })
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
        }

        dto.setMeasurementValues(measurementValues);
    }

    private void processEntityGroups(GetModelRequestContext ctx, GetModelDTO dto) {

        if (!ctx.isAllEntityGroups() && CollectionUtils.isEmpty(ctx.getEntityGroupIds())) {
            return;
        }

        GetEntitiesGroupsDTO groups = ctx.isDraft() ? metaDraftService.getEntitiesGroups() : getEntitiesGroups();
        if (Objects.isNull(groups) || groups.getGroups().isEmpty()) {
            return;
        }

        if (CollectionUtils.isNotEmpty(ctx.getEntityGroupIds())) {

            Iterator<Entry<String, EntitiesGroup>> p = groups.getGroups().entrySet().iterator();
            while (p.hasNext()) {

                Entry<String, EntitiesGroup> entry = p.next();
                if (!ctx.getEntityGroupIds().contains(entry.getKey())) {
                    p.remove();
                    groups.getNested().remove(entry.getValue());
                }
            }
        }

        if (MapUtils.isEmpty(groups.getGroups())) {
            return;
        }

        dto.setEntityGroups(groups);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteModel(DeleteModelRequestContext ctx) {
        if (ctx.isDraft()) {
            metaDraftService.remove(ctx);
            return;
        }
        roleServiceExt.deleteResources(ctx.getLookupEntitiesIds());
        roleServiceExt.deleteResources(ctx.getEntitiesIds());
        super.deleteModel(ctx);
//        throw new RuntimeException("vk");
    }
    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void upsertModel(UpdateModelRequestContext ctx) {

        if (ctx.isDraft()){
            metaDraftService.update(ctx);
            return;
        }


        if (ctx.getUpsertType() == UpdateModelRequestContext.ModelUpsertType.ADDITION) {
            mergeGroups(ctx);
        }

        validationComponent.validateUpdateModelContext(ctx);

        if (ctx.getUpsertType() == UpdateModelRequestContext.ModelUpsertType.FULLY_NEW) {
            roleServiceExt.dropResources(SecuredResourceCategory.META_MODEL);
        }

        for(Entity entityDef : ctx.getEntityUpdate()){
            String name = entityDef.getName();
            Map<String, AttributeModelElement> oldAttributes = new HashMap<>(getAttributesInfoMap(name));

            Map<String, AttributeModelElement> newAttributes = ModelUtils.createAttributesMap(entityDef, ctx.getNestedEntityUpdate());

            oldAttributes.entrySet().removeIf(
                    oldAttr -> newAttributes.keySet()
                            .stream()
                            .anyMatch(newAttr -> newAttr.equals(oldAttr.getKey())));
            dropAttributeResourceFromEntity(entityDef, oldAttributes);
            createResourceFromEntity(entityDef, newAttributes);
        }

        for(LookupEntity lookupEntityDef : ctx.getLookupEntityUpdate()){
            String name = lookupEntityDef.getName();
            Map<String, AttributeModelElement> oldAttributes = new HashMap<>(getAttributesInfoMap(name));

            Map<String, AttributeModelElement> newAttributes = ModelUtils.createAttributesMap(lookupEntityDef, Collections.emptyList());

            oldAttributes.entrySet().removeIf(
                    oldAttr -> newAttributes.keySet()
                            .stream()
                            .anyMatch(newAttr -> newAttr.equals(oldAttr.getKey())));
            dropAttributeResourceFromEntity(lookupEntityDef, oldAttributes);
            createResourceFromEntity(lookupEntityDef, newAttributes);
        }

        super.upsertModel(ctx);
    }

    @Nonnull
    @Override
    public List<Entity> getEntitiesList() {
        List<Entity> unSecureResult = super.getEntitiesList();
        Collection<String> names = unSecureResult.stream().map(Entity::getName).collect(Collectors.toList());
        Collection<String> filtredNames = filter(names);
        List<Entity> result = unSecureResult.stream().filter(entity -> filtredNames.contains(entity.getName())).collect(Collectors.toList());
        return Collections.unmodifiableList(result);
    }

    @Nonnull
    @Override
    public List<LookupEntity> getLookupEntitiesList() {
        List<LookupEntity> unSecureResult = super.getLookupEntitiesList();
        Collection<String> names = unSecureResult.stream().map(LookupEntity::getName).collect(Collectors.toList());
        Collection<String> filtredNames = filter(names);
        List<LookupEntity> result = unSecureResult.stream().filter(entity -> filtredNames.contains(entity.getName())).collect(Collectors.toList());
        return Collections.unmodifiableList(result);
    }

    @Nonnull
    @Override
    public List<LookupEntity> getUnfilteredLookupEntitiesList() {
        return super.getLookupEntitiesList();
    }

    @Override
    @Nullable
    public GetEntityDTO getEntityById(String id) {
        return super.getEntityById(id);
//        if (entityDTO == null) {
//            return null;
//        }
//        Collection<String> filteredResult = filter(Collections.singletonList(entityDTO.getEntity().getName()));
//        if (filteredResult.isEmpty()) {
//            return null;
//        } else {
//            return entityDTO;
//        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GetEntitiesGroupsDTO getEntitiesGroups() {

        Collection<EntitiesGroupWrapper> entitiesGroupWrappers = super.getAllGroupWrappers();
        Collection<String> entitiesNames = new ArrayList<>();

        entitiesGroupWrappers.stream()
                .map(EntitiesGroupWrapper::getNestedEntites)
                .flatMap(Collection::stream)
                .map(Entity::getName)
                .collect(Collectors.toCollection(() -> entitiesNames));

        entitiesGroupWrappers.stream()
                .map(EntitiesGroupWrapper::getNestedLookupEntities)
                .flatMap(Collection::stream)
                .map(LookupEntity::getName)
                .collect(Collectors.toCollection(() -> entitiesNames));

        Collection<String> filteredResult = filter(entitiesNames);

        Map<String, EntitiesGroup> defs = new HashMap<>(entitiesGroupWrappers.size());
        Map<EntitiesGroup, Pair<List<Entity>, List<LookupEntity>>> nested = new HashMap<>(entitiesGroupWrappers.size());
        for (EntitiesGroupWrapper entitiesGroupWrapper : entitiesGroupWrappers) {

            List<Entity> entities =
                entitiesGroupWrapper.getNestedEntites()
                        .stream()
                        .filter(entityDef -> filteredResult.contains(entityDef.getName()))
                        .collect(Collectors.toList());

            List<LookupEntity> lookupEntities =
                entitiesGroupWrapper.getNestedLookupEntities()
                        .stream()
                        .filter(entityDef -> filteredResult.contains(entityDef.getName()))
                        .collect(Collectors.toList());

            defs.put(entitiesGroupWrapper.getWrapperId(), entitiesGroupWrapper.getEntitiesGroupDef());
            nested.put(entitiesGroupWrapper.getEntitiesGroupDef(), Pair.of(entities, lookupEntities));
        }

        return new GetEntitiesGroupsDTO(defs, nested);
    }

    @Override
    @Nullable
    public LookupEntity getLookupEntityById(String id) {
        return super.getLookupEntityById(id);
//        if (lookupEntityDef == null) {
//            return null;
//        }
//        Collection<String> filteredResult = filter(Collections.singletonList(lookupEntityDef.getName()));
//        if (filteredResult.isEmpty()) {
//            return null;
//        } else {
//            return lookupEntityDef;
//        }
    }


    @Nonnull
    private Collection<String> filter(@Nullable Collection<String> uniqueResourcesNames) {
        if (uniqueResourcesNames == null || uniqueResourcesNames.isEmpty()) {
            return Collections.emptyList();
        }

        String token = SecurityUtils.getCurrentUserToken();
        // TODO: hack for import utility
        try {
            if (token == null || securityService.getUserByToken(token).isAdmin()) {
                return uniqueResourcesNames;
            }

            List<? extends Right> srds = securityService.getRightsByToken(token);
            if (srds.stream().anyMatch(srd ->
            	StringUtils.equals(srd.getSecuredResource().getName(), SecurityUtils.ADMIN_SYSTEM_MANAGEMENT)
            	||StringUtils.equals(srd.getSecuredResource().getName(), SecurityUtils.ADMIN_DATA_MANAGEMENT_RESOURCE_NAME))) {
                return uniqueResourcesNames;
            }

            List<String> filteredResult = new ArrayList<>();
            srds.forEach(
                    srd -> {
                        Optional<String> opt = uniqueResourcesNames.stream()
                                .filter(r -> (r.equals(srd.getSecuredResource().getName())
                                )).findFirst();
                        opt.ifPresent(filteredResult::add);
                    });
            return filteredResult;
        } catch (Exception e) {
            final String message = "Metadata service failed to retrieve data [{}].";
            LOGGER.error(message, e);
            throw new PlatformFailureException(message, e, MetaExceptionIds.EX_META_ENTITY_NOT_FOUND);
        }
    }

    @Override
	public void applyDraft(String draftId) {
		// TODO Auto-generated method stub

	}

    /* (non-Javadoc)
     * @see com.unidata.mdm.backend.service.security.ISecurityService#createResourceFromLookup(java.util.List)
     */
    protected void createResourceFromLookup(List<LookupEntity> lookupEntityUpdate) {
        if (CollectionUtils.isEmpty(lookupEntityUpdate)) {
            return;
        }
        List<SecuredResourceDTO> resources = new ArrayList<>();
        for (LookupEntity def : lookupEntityUpdate) {

            Map<String, AttributeModelElement> attrs = ModelUtils.createAttributesMap(def, Collections.emptyList());
            SecuredResourceDTO resource = createResources(def.getName(), def.getDisplayName(),
                    SecuredResourceCategory.META_MODEL, attrs);
            resources.add(resource);
        }

        roleServiceExt.createResources(resources);
    }

    /* (non-Javadoc)
     * @see com.unidata.mdm.backend.service.security.ISecurityService#createResourceFromEntity(java.util.List)
     */
    protected void createResourceFromEntity(List<Entity> entityUpdate, List<NestedEntity> refs) {
        if (CollectionUtils.isEmpty(entityUpdate)) {
            return;
        }
        List<SecuredResourceDTO> resources = new ArrayList<>();
        for (Entity def : entityUpdate) {

            Map<String, AttributeModelElement> attrs = ModelUtils.createAttributesMap(def, refs);
            SecuredResourceDTO resource = createResources(def.getName(), def.getDisplayName(),
                    SecuredResourceCategory.META_MODEL, attrs);
            resources.add(resource);
        }

        roleServiceExt.createResources(resources);
    }

    protected void createResourceFromEntity(AbstractEntity entityDef, Map<String, AttributeModelElement> attrs) {
        SecuredResourceDTO resource = createResources(entityDef.getName(), entityDef.getDisplayName(),
                SecuredResourceCategory.META_MODEL, attrs);
        roleServiceExt.createResources(Collections.singletonList(resource));
    }

    protected void dropAttributeResourceFromEntity(AbstractEntity entityDef, Map<String, AttributeModelElement> deletedAttrs) {
        List<String> deleteResources = deletedAttrs.values()
                .stream()
                .map(attr -> String.join(".", entityDef.getName(), attr.getPath()))
                .collect(Collectors.toList());
        deleteResources(deleteResources);
    }

    /* (non-Javadoc)
     * @see com.unidata.mdm.backend.service.security.ISecurityService#deleteResources(java.util.List)
     */
    protected void deleteResources(List<String> resources) {

        if (CollectionUtils.isEmpty(resources)) {
            return;
        }

        for (String resource : resources) {
            roleServiceExt.deleteResource(resource);
        }
    }

    /**
     * TODO move to own service.
     * Creates a resource.
     *
     * @param holder the name of the attribute
     * @param topLevelName name of the top level object
     * @param parent parent
     * @return {@link SecuredResourceDTO}
     */
    private SecuredResourceDTO createResource(AttributeModelElement holder, String topLevelName, SecuredResourceDTO parent) {

        SecuredResourceDTO resource = new SecuredResourceDTO();
        resource.setName(String.join(".", topLevelName, holder.getPath()));
        resource.setDisplayName(holder.getDisplayName());
        resource.setParent(parent);
        resource.setCreatedAt(new Date());
        resource.setCreatedBy(SecurityUtils.getCurrentUserName());
        resource.setType(SecuredResourceType.USER_DEFINED);
        resource.setCategory(parent.getCategory());
        resource.setUpdatedAt(new Date());
        resource.setUpdatedBy(SecurityUtils.getCurrentUserName());

        if (holder.hasChildren()) {
            List<SecuredResourceDTO> children = new ArrayList<>();
            for (AttributeModelElement child : holder.getChildren()) {
                children.add(createResource(child, topLevelName, resource));
            }

            resource.setChildren(children);
        }

        return resource;
    }

    /**
     * Create resource
     *
     * @param name - name of resource
     * @param displayName - display name of resource
     * @param category the category to set
     * @param attrs attributes
     * @return resource
     */
    private SecuredResourceDTO createResources(String name, String displayName, SecuredResourceCategory category,
                                               Map<String, AttributeModelElement> attrs) {

        // 1. Top level object
        SecuredResourceDTO resource = new SecuredResourceDTO();
        resource.setName(name);
        resource.setDisplayName(displayName);
        resource.setCreatedAt(new Date());
        resource.setCreatedBy(SecurityUtils.getCurrentUserName());
        resource.setType(SecuredResourceType.USER_DEFINED);
        resource.setCategory(category);
        resource.setUpdatedAt(new Date());
        resource.setUpdatedBy(SecurityUtils.getCurrentUserName());

        // 2. Attributes. Only top level are processed
        List<SecuredResourceDTO> children = new ArrayList<>();
        for (Entry<String, AttributeModelElement> entry : attrs.entrySet()) {

            if (entry.getValue().hasParent()) {
                continue;
            }

            children.add(createResource(entry.getValue(), name, resource));
        }


        resource.setChildren(children);
        return resource;
    }
}
