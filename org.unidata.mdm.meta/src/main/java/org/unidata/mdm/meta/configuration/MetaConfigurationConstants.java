/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 *
 */
package org.unidata.mdm.meta.configuration;

import org.unidata.mdm.meta.module.MetaModule;

/**
 * @author mikhail
 * Configuration constants.
 */
public final class MetaConfigurationConstants {
	/**
	 * Disabling constructor.
	 */
	private MetaConfigurationConstants() {
		super();
	}
	/**
     * Core data source properties prefix.
     */
    public static final String META_DATASOURCE_PROPERTIES_PREFIX = MetaModule.MODULE_ID + ".datasource.";
    /**
     * Meta storage schema name.
     */
    public static final String META_SCHEMA_NAME = "org_unidata_mdm_meta";
    /**
     * Meta migration log name.
     */
    public static final String META_MIGRATION_LOG_NAME = "meta_change_log";
	// Properties
	/**
     * Global validity period start.
     * Old unidata.validity.period.start
     */
    public static final String PROPERTY_VALIDITY_PERIOD_START = MetaModule.MODULE_ID + ".validity.period.start";
    /**
     * Global validity period end.
     * Old unidata.validity.period.end
     */
    public static final String PROPERTY_VALIDITY_PERIOD_END = MetaModule.MODULE_ID + ".validity.period.end";
    /**
     * Granularity mode.
     * Former unidata.validity.period.mode
     */
    public static final String PROPERTY_VALIDITY_PERIOD_MODE = MetaModule.MODULE_ID + ".validity.period.mode";
    /**
     * Validity period group.
     */
    public static final String PROPERTY_VALIDITY_PERIOD_GROUP = MetaModule.MODULE_ID + ".validity.period.group";
    /**
     * Validity period group.
     */
    public static final String PROPERTY_INDEXING_GROUP = MetaModule.MODULE_ID + ".indexing.group";
    /**
     * Entity indexing shards number.
     * Former unidata.search.entity.shards.number
     */
    public static final String PROPERTY_INDEXING_ENTITY_SHARDS = MetaModule.MODULE_ID + ".indexing.entity.shards";
    /**
     * Entity indexing replicas number.
     * Former unidata.search.entity.replicas.number
     */
    public static final String PROPERTY_INDEXING_ENTITY_REPLICAS = MetaModule.MODULE_ID + ".indexing.entity.replicas";
    /**
     * Lookup primary shards number.
     * Former unidata.search.lookup.shards.number
     */
    public static final String PROPERTY_INDEXING_LOOKUP_SHARDS = MetaModule.MODULE_ID + ".indexing.lookup.shards";
    /**
     * Lookup replicas number.
     * Former unidata.search.lookup.replicas.number.
     */
    public static final String PROPERTY_INDEXING_LOOKUP_REPLICAS = MetaModule.MODULE_ID + ".indexing.lookup.replicas";
    // END of Properties
}
