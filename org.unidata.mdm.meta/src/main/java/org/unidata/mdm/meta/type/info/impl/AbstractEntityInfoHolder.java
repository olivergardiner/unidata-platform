package org.unidata.mdm.meta.type.info.impl;

import java.util.Map;
import java.util.Objects;

import org.unidata.mdm.core.context.DataRecordContext;
import org.unidata.mdm.core.type.model.AttributeModelElement;
import org.unidata.mdm.core.type.model.EntityModelElement;
import org.unidata.mdm.core.type.model.GeneratingModelElement;
import org.unidata.mdm.core.type.model.GenerationStrategyType;
import org.unidata.mdm.core.type.model.support.ExternalIdValueGenerator;
import org.unidata.mdm.meta.type.model.ValueGenerationStrategy;
import org.unidata.mdm.meta.type.model.entities.AbstractEntity;
import org.unidata.mdm.meta.type.model.strategy.CustomValueGenerationStrategy;
import org.unidata.mdm.meta.type.model.entities.Entity;
import org.unidata.mdm.meta.type.model.entities.LookupEntity;
import org.unidata.mdm.meta.util.ValueGeneratingUtils;

/**
 * @author Mikhail Mikhailov on May 18, 2020
 */
public abstract class AbstractEntityInfoHolder<E extends AbstractEntity> extends AbstractBvtMapInfoHolder implements GeneratingModelElement {
    /**
     * Value generator.
     */
    protected final ExternalIdValueGenerator generator;
    /**
     * The abstract entity.
     */
    protected final E entity;
    /**
     * Constructor.
     * @param attrs
     * @param bvtMap
     */
    public AbstractEntityInfoHolder(
            Map<String, AttributeModelElement> attrs,
            Map<String, Map<String, Integer>> bvtMap,
            E entity) {
        super(attrs, bvtMap);
        this.entity = entity;
        this.generator = getGenerator();
    }
    /**
     * @return the entity
     */
    public E getEntity() {
        return entity;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Object generate(DataRecordContext input) {
        return generator.generate((EntityModelElement) this, input);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public GenerationStrategyType getStrategyType() {

        if (!(entity instanceof LookupEntity)
         && !(entity instanceof Entity)) {
            return null;
        }

        ValueGenerationStrategy s = entity instanceof LookupEntity
                ? ((LookupEntity) entity).getExternalIdGenerationStrategy()
                : ((Entity) entity).getExternalIdGenerationStrategy();

        if (Objects.nonNull(s)) {
            switch (s.getStrategyType()) {
                case CONCAT:
                    return GenerationStrategyType.CONCAT;
                case CUSTOM:
                    return GenerationStrategyType.CUSTOM;
                case RANDOM:
                    return GenerationStrategyType.RANDOM;
                case SEQUENCE:
                    return GenerationStrategyType.SEQUENCE;
            }
        }

        return null;
    }
    /**
     * Creates generating element.
     * @param s the definition
     * @return
     */
    private ExternalIdValueGenerator getGenerator() {

        if (!(entity instanceof LookupEntity)
         && !(entity instanceof Entity)) {
            return null;
        }

        ValueGenerationStrategy s = entity instanceof LookupEntity
                ? ((LookupEntity) entity).getExternalIdGenerationStrategy()
                : ((Entity) entity).getExternalIdGenerationStrategy();

        if (Objects.nonNull(s)) {
            switch (s.getStrategyType()) {
            case CONCAT:
                return ValueGeneratingUtils.CONCAT_ENTITY_GENERATOR;
            case RANDOM:
                return ValueGeneratingUtils.RANDOM_ENTITY_GENERATOR;
            case CUSTOM:
                return ValueGeneratingUtils.defineEntityCustomValueGenerator(((CustomValueGenerationStrategy) s).getClassName());
            default:
                break;
            }
        }

        return null;
    }
}
