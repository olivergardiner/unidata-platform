package org.unidata.mdm.meta.type.model.entities;

import static org.unidata.mdm.meta.type.model.ModelNamespace.META_MODEL_NAMESPACE;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.unidata.mdm.meta.type.model.CustomProperty;
import org.unidata.mdm.meta.type.model.MetaModelEntity;
import org.unidata.mdm.meta.type.model.PeriodBoundary;
import org.unidata.mdm.meta.type.model.VersionedObject;
import org.unidata.mdm.meta.type.model.attributes.ArrayMetaModelAttribute;
import org.unidata.mdm.meta.type.model.attributes.AttributeGroup;
import org.unidata.mdm.meta.type.model.attributes.SimpleMetaModelAttribute;
import org.unidata.mdm.meta.type.model.merge.MergeSettings;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class AbstractEntity extends VersionedObject implements MetaModelEntity {

    private final static long serialVersionUID = 987654321L;

    @JacksonXmlProperty(namespace = META_MODEL_NAMESPACE)
    protected MergeSettings mergeSettings;
    @JacksonXmlProperty(namespace = META_MODEL_NAMESPACE)
    protected PeriodBoundary validityPeriod;
    @JacksonXmlProperty(namespace = META_MODEL_NAMESPACE)
    protected List<AttributeGroup> attributeGroups;
    @JacksonXmlProperty(namespace = META_MODEL_NAMESPACE)
    protected List<RelationGroup> relationGroups;
    @JacksonXmlProperty(namespace = META_MODEL_NAMESPACE)
    protected List<CustomProperty> customProperties;
    @JacksonXmlProperty(isAttribute = true)
    protected String name;
    @JacksonXmlProperty(isAttribute = true)
    protected String displayName;
    @JacksonXmlProperty(isAttribute = true)
    protected String description;

    @JacksonXmlProperty(namespace = META_MODEL_NAMESPACE)
    protected List<SimpleMetaModelAttribute> simpleAttribute;

    @JacksonXmlProperty(namespace = META_MODEL_NAMESPACE)
    protected List<ArrayMetaModelAttribute> arrayAttribute;

    @Override
    public List<SimpleMetaModelAttribute> getSimpleAttribute() {
        if (simpleAttribute == null) {
            simpleAttribute = new ArrayList<>();
        }
        return this.simpleAttribute;
    }

    @Override
    public List<ArrayMetaModelAttribute> getArrayAttribute() {
        if (arrayAttribute == null) {
            arrayAttribute = new ArrayList<>();
        }
        return this.arrayAttribute;
    }

    @Override
    public void setSimpleAttribute(List<SimpleMetaModelAttribute> simpleAttribute) {
        this.simpleAttribute = simpleAttribute;
    }

    @Override
    public void setArrayAttribute(List<ArrayMetaModelAttribute> arrayAttribute) {
        this.arrayAttribute = arrayAttribute;
    }

    public AbstractEntity withSimpleAttribute(SimpleMetaModelAttribute... values) {
        if (values!= null) {
            Collections.addAll(getSimpleAttribute(), values);
        }
        return this;
    }

    public AbstractEntity withSimpleAttribute(Collection<SimpleMetaModelAttribute> values) {
        if (values!= null && !values.isEmpty()) {
            getSimpleAttribute().addAll(values);
        }
        return this;
    }

    public AbstractEntity withArrayAttribute(ArrayMetaModelAttribute... values) {
        if (values!= null) {
            Collections.addAll(getArrayAttribute(), values);
        }
        return this;
    }

    public AbstractEntity withArrayAttribute(Collection<ArrayMetaModelAttribute> values) {
        if (values!= null && !values.isEmpty()) {
            getArrayAttribute().addAll(values);
        }
        return this;
    }

    public AbstractEntity withMergeSettings(MergeSettings value) {
        setMergeSettings(value);
        return this;
    }

    public AbstractEntity withValidityPeriod(PeriodBoundary value) {
        setValidityPeriod(value);
        return this;
    }

    public AbstractEntity withAttributeGroups(AttributeGroup... values) {
        if (values!= null) {
            Collections.addAll(getAttributeGroups(), values);
        }
        return this;
    }

    public AbstractEntity withAttributeGroups(Collection<AttributeGroup> values) {
        if (values!= null && !values.isEmpty()) {
            getAttributeGroups().addAll(values);
        }
        return this;
    }

    public AbstractEntity withRelationGroups(RelationGroup... values) {
        if (values!= null) {
            Collections.addAll(getRelationGroups(), values);
        }
        return this;
    }

    public AbstractEntity withRelationGroups(Collection<RelationGroup> values) {
        if (values!= null) {
            getRelationGroups().addAll(values);
        }
        return this;
    }

    public AbstractEntity withCustomProperties(CustomProperty... values) {
        if (values!= null) {
            Collections.addAll(getCustomProperties(), values);
        }
        return this;
    }

    public AbstractEntity withCustomProperties(Collection<CustomProperty> values) {
        if (values!= null && !values.isEmpty()) {
            getCustomProperties().addAll(values);
        }
        return this;
    }

    public AbstractEntity withName(String value) {
        setName(value);
        return this;
    }

    public AbstractEntity withDisplayName(String value) {
        setDisplayName(value);
        return this;
    }

    public AbstractEntity withDescription(String value) {
        setDescription(value);
        return this;
    }

    @Override
    public AbstractEntity withVersion(Long value) {
        setVersion(value);
        return this;
    }

    @Override
    public AbstractEntity withUpdatedAt(LocalDateTime value) {
        setUpdatedAt(value);
        return this;
    }

    @Override
    public AbstractEntity withCreateAt(LocalDateTime value) {
        setCreateAt(value);
        return this;
    }

    @Override
    public MergeSettings getMergeSettings() {
        return mergeSettings;
    }

    @Override
    public void setMergeSettings(MergeSettings mergeSettings) {
        this.mergeSettings = mergeSettings;
    }

    @Override
    public PeriodBoundary getValidityPeriod() {
        return validityPeriod;
    }

    @Override
    public void setValidityPeriod(PeriodBoundary validityPeriod) {
        this.validityPeriod = validityPeriod;
    }

    @Override
    public List<AttributeGroup> getAttributeGroups() {
        return Objects.isNull(attributeGroups) ? Collections.emptyList() : attributeGroups;
    }

    public void setAttributeGroups(List<AttributeGroup> attributeGroups) {
        this.attributeGroups = attributeGroups;
    }

    @Override
    public List<RelationGroup> getRelationGroups() {
        return Objects.isNull(relationGroups) ? Collections.emptyList() : relationGroups;
    }

    public void setRelationGroups(List<RelationGroup> relationGroups) {
        this.relationGroups = relationGroups;
    }

    @Override
    public List<CustomProperty> getCustomProperties() {
        return Objects.isNull(customProperties) ? Collections.emptyList() : customProperties;
    }

    @Override
    public void setCustomProperties(List<CustomProperty> customProperties) {
        this.customProperties = customProperties;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getDisplayName() {
        return displayName;
    }

    @Override
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }
}
