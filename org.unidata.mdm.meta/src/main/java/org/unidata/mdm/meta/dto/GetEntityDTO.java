/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 * 
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.meta.dto;

import java.util.List;

import org.unidata.mdm.meta.type.model.entities.Entity;
import org.unidata.mdm.meta.type.model.entities.NestedEntity;
import org.unidata.mdm.meta.type.model.entities.Relation;

/**
 * @author Mikhail Mikhailov
 * Entity metadata container.
 */
public class GetEntityDTO {
    /**
     * Entity.
     */
    private final Entity entity;
    /**
     * References.
     */
    private final List<NestedEntity> refs;
    /**
     * Relations.
     */
    private final List<Relation> relations;
    /**
     * Constructor.
     */
    public GetEntityDTO(Entity entity, List<NestedEntity> refs, List<Relation> relations) {
        super();
        this.entity = entity;
        this.refs = refs;
        this.relations = relations;
    }
    /**
     * @return the entity
     */
    public Entity getEntity() {
        return entity;
    }
    /**
     * @return the refs
     */
    public List<NestedEntity> getRefs() {
        return refs;
    }
    /**
     * @return the relations
     */
    public List<Relation> getRelations() {
        return relations;
    }
}
