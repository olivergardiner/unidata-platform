package org.unidata.mdm.meta.type.search;

import java.util.Set;
import javax.annotation.Nonnull;

import org.unidata.mdm.core.type.search.DataIndexType;
import org.unidata.mdm.search.type.HierarchicalIndexType;
import org.unidata.mdm.search.type.HierarchicalTopIndexType;
import org.unidata.mdm.search.type.IndexField;
import org.unidata.mdm.search.type.IndexType;
import org.unidata.mdm.system.type.support.IdentityHashSet;

/**
 * @author Mikhail Mikhailov on Mar 27, 2020
 */
public enum EtalonIndexType implements DataIndexType, HierarchicalTopIndexType {
    /**
     * Etalon type - the top type.
     */
    ETALON("etalon");
    /**
     * Container for the children.
     */
    private static final Set<HierarchicalIndexType> CHILDREN = new IdentityHashSet<>();
    /**
     * Name of type
     */
    private final String type;
    /**
     * Constructor.
     * @param type the name of the type
     */
    EtalonIndexType(String type) {
        this.type = type;
    }
    /**
     * @return name of type
     */
    @Nonnull
    @Override
    public String getName() {
        return type;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isRelated(IndexType searchType) {
        return searchType instanceof DataIndexType;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isTopType() {
        return true;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public HierarchicalTopIndexType getTopType() {
        return this;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public IndexField getJoinField() {
        return EtalonHeaderField.FIELD_JOIN;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void addChild(HierarchicalIndexType type) {
        CHILDREN.add(type);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Set<HierarchicalIndexType> getChildren() {
        return CHILDREN;
    }
}
