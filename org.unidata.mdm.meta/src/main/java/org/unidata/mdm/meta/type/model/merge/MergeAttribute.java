package org.unidata.mdm.meta.type.model.merge;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.unidata.mdm.meta.type.model.SourceSystem;

import static org.unidata.mdm.meta.type.model.ModelNamespace.META_MODEL_NAMESPACE;


public class MergeAttribute implements Serializable {

    private final static long serialVersionUID = 987654321L;

    @JacksonXmlProperty(isAttribute = true)
    protected String name;

    @JacksonXmlElementWrapper(localName = "sourceSystemsConfig", namespace = META_MODEL_NAMESPACE)
    @JacksonXmlProperty(localName = "sourceSystem", namespace = META_MODEL_NAMESPACE)
    protected List<SourceSystem> sourceSystemsConfigs;

    public String getName() {
        return name;
    }

    public void setName(String value) {
        this.name = value;
    }

    public List<SourceSystem> getSourceSystemsConfigs() {
        if (sourceSystemsConfigs == null) {
            sourceSystemsConfigs = new ArrayList<>();
        }
        return sourceSystemsConfigs;
    }

    public void setSourceSystemsConfigs(List<SourceSystem> sourceSystemsConfigs) {
        this.sourceSystemsConfigs = sourceSystemsConfigs;
    }

    public MergeAttribute withName(String value) {
        setName(value);
        return this;
    }

    public MergeAttribute withSourceSystemsConfigs(SourceSystem... values) {
        if (values!= null) {
            Collections.addAll(getSourceSystemsConfigs(), values);
        }
        return this;
    }

    public MergeAttribute withSourceSystemsConfigs(Collection<SourceSystem> values) {
        if (values!= null) {
            getSourceSystemsConfigs().addAll(values);
        }
        return this;
    }

    public MergeAttribute withSourceSystemsConfigs(List<SourceSystem> sourceSystemsConfigs) {
        setSourceSystemsConfigs(sourceSystemsConfigs);
        return this;
    }

}
