package org.unidata.mdm.meta.type.model.attributes;

import java.math.BigInteger;
import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.google.common.base.MoreObjects;
import org.unidata.mdm.meta.type.model.CustomProperty;
import org.unidata.mdm.meta.type.model.OrderedElement;
import org.unidata.mdm.meta.type.model.ValueGenerationStrategy;


public class ComplexMetaModelAttribute extends AbstractMetaModelAttribute implements OrderedElement {

    private final static long serialVersionUID = 987654321L;

    @JacksonXmlProperty(isAttribute = true)
    protected String nestedEntityName;
    @JacksonXmlProperty(isAttribute = true)
    protected BigInteger minCount;
    @JacksonXmlProperty(isAttribute = true)
    protected BigInteger maxCount;
    @JacksonXmlProperty(isAttribute = true)
    protected String subEntityKeyAttribute;
    @JacksonXmlProperty(isAttribute = true)
    protected int order;

    public ComplexMetaModelAttribute withCustomProperties(List<CustomProperty> customProperties) {
        super.setCustomProperties(customProperties);
        return this;
    }

    public ComplexMetaModelAttribute withValueGenerationStrategy(ValueGenerationStrategy value) {
        super.setValueGenerationStrategy(value);
        return this;
    }

    public ComplexMetaModelAttribute withName(String value) {
        super.setName(value);
        return this;
    }

    public ComplexMetaModelAttribute withDisplayName(String value) {
        super.setDisplayName(value);
        return this;
    }

    public ComplexMetaModelAttribute withDescription(String value) {
        super.setDescription(value);
        return this;
    }

    public ComplexMetaModelAttribute withReadOnly(boolean value) {
        super.setReadOnly(value);
        return this;
    }

    public ComplexMetaModelAttribute withHidden(boolean value) {
        super.setHidden(value);
        return this;
    }

    public ComplexMetaModelAttribute withNestedEntityName(String value) {
        setNestedEntityName(value);
        return this;
    }

    public ComplexMetaModelAttribute withMinCount(BigInteger value) {
        setMinCount(value);
        return this;
    }

    public ComplexMetaModelAttribute withMaxCount(BigInteger value) {
        setMaxCount(value);
        return this;
    }

    public ComplexMetaModelAttribute withSubEntityKeyAttribute(String value) {
        setSubEntityKeyAttribute(value);
        return this;
    }

    public ComplexMetaModelAttribute withOrder(int value) {
        setOrder(value);
        return this;
    }

    public String getNestedEntityName() {
        return nestedEntityName;
    }

    public void setNestedEntityName(String value) {
        this.nestedEntityName = value;
    }

    public BigInteger getMinCount() {
        return minCount;
    }

    public void setMinCount(BigInteger value) {
        this.minCount = value;
    }

    public BigInteger getMaxCount() {
        return maxCount;
    }

    public void setMaxCount(BigInteger value) {
        this.maxCount = value;
    }

    public String getSubEntityKeyAttribute() {
        return subEntityKeyAttribute;
    }

    public void setSubEntityKeyAttribute(String value) {
        this.subEntityKeyAttribute = value;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int value) {
        this.order = value;
    }

}
