/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 * 
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.meta.context;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.unidata.mdm.meta.type.model.entities.EntitiesGroup;
import org.unidata.mdm.meta.type.model.entities.Entity;
import org.unidata.mdm.meta.type.model.enumeration.EnumerationDataType;
import org.unidata.mdm.meta.type.model.entities.LookupEntity;
import org.unidata.mdm.meta.type.model.entities.NestedEntity;
import org.unidata.mdm.meta.type.model.entities.Relation;
import org.unidata.mdm.meta.type.model.attributes.SimpleMetaModelAttribute;
import org.unidata.mdm.meta.type.model.SourceSystem;
import org.unidata.mdm.meta.service.segments.ModelUpsertStartExecutor;
import org.unidata.mdm.system.context.AbstractCompositeRequestContext;
import org.unidata.mdm.system.context.StorageSpecificContext;
import org.unidata.mdm.system.type.pipeline.PipelineInput;

import static java.util.stream.Collectors.toCollection;
import static java.util.stream.Collectors.toList;

/**
 * @author Mikhail Mikhailov
 *         Container for meta model updates.
 */
public class UpdateModelRequestContext
        extends AbstractCompositeRequestContext
        implements MayHaveDraft, PipelineInput, StorageSpecificContext, Serializable {
    /**
     * Generated SVUID.
     */
    private static final long serialVersionUID = 8533210708984117919L;
    /**
     * Top level entity updates
     */
    private final List<Entity> entityUpdate;
    /**
     * Lookup entity updates.
     */
    private final List<LookupEntity> lookupEntityUpdate;
    /**
     * Nested entity updates.
     */
    private final List<NestedEntity> nestedEntityUpdate;
    /**
     * Enumeration updates.
     */
    private final List<EnumerationDataType> enumerationsUpdate;
    /**
     * Source systems updates.
     */
    private final List<SourceSystem> sourceSystemsUpdate;
    /**
     * Relations updates.
     */
    private final List<Relation> relationsUpdate;
    /**
     * Entities group updates.
     */
    private final EntitiesGroup entitiesGroupsUpdate;
    /**
     * Storage ID to apply the updates to.
     */
    private final String storageId;
    /**
     * Drop existing model and create a new one (no merge).
     */
    private final ModelUpsertType upsertType;

    /**
     * Constructor.
     */
    private UpdateModelRequestContext(UpdateModelRequestContextBuilder b) {
        super(b);
        this.entityUpdate = b.entityUpdate;
        this.lookupEntityUpdate = b.lookupEntityUpdate;
        this.nestedEntityUpdate = b.nestedEntityUpdate;
        this.enumerationsUpdate = b.enumerationsUpdate;
        this.sourceSystemsUpdate = b.sourceSystemsUpdate;
        this.relationsUpdate = b.relationsUpdate;
        this.storageId = b.storageId;
        this.upsertType = b.upsertType;
        this.entitiesGroupsUpdate = b.entitiesGroupsUpdate;

        setFlag(MetaContextFlags.FLAG_DRAFT, b.draft);
        setFlag(MetaContextFlags.FLAG_SKIP_REMOVE_ELEMENTS, b.skipRemoveElements);
        setFlag(MetaContextFlags.FLAG_DIRECT, b.direct);
    }

    @Override
    public String getStartTypeId() {
        return ModelUpsertStartExecutor.SEGMENT_ID;
    }

    /**
     * @return the entityUpdate
     */
    public List<Entity> getEntityUpdate() {
        return entityUpdate;
    }

    /**
     * @return the lookupEntityUpdate
     */
    public List<LookupEntity> getLookupEntityUpdate() {
        return lookupEntityUpdate;
    }

    /**
     * @return the nestedEntityUpdate
     */
    public List<NestedEntity> getNestedEntityUpdate() {
        return nestedEntityUpdate;
    }

    /**
     * @return the enumerationsUpdate
     */
    public List<EnumerationDataType> getEnumerationsUpdate() {
        return enumerationsUpdate;
    }

    /**
     * @return the sourceSystemsUpdate
     */
    public List<SourceSystem> getSourceSystemsUpdate() {
        return sourceSystemsUpdate;
    }

    /**
     * @return the relationsUpdate
     */
    public List<Relation> getRelationsUpdate() {
        return relationsUpdate;
    }

    /**
     * @return entitiesGroupsUpdate
     */
    public EntitiesGroup getEntitiesGroupsUpdate() {
        return entitiesGroupsUpdate;
    }

    /**
     * @return the storageId
     */
    @Override
    public String getStorageId() {
        return storageId;
    }

    /**
     * @return flag responsible for cleaning current DB and cache state
     */
    public ModelUpsertType getUpsertType() {
        return upsertType;
    }

    public boolean isSkipRemoveElements() {
        return getFlag(MetaContextFlags.FLAG_SKIP_REMOVE_ELEMENTS);
    }

    /**
     * Has entity update.
     *
     * @return true if so false otherwise
     */
    public boolean hasEntityUpdate() {
        return entityUpdate != null && !entityUpdate.isEmpty();
    }

    /**
     * Has lookup entity update.
     *
     * @return true if so false otherwise
     */
    public boolean hasLookupEntityUpdate() {
        return lookupEntityUpdate != null && !lookupEntityUpdate.isEmpty();
    }

    /**
     * Has nested entity update.
     *
     * @return true if so false otherwise
     */
    public boolean hasNestedEntityUpdate() {
        return nestedEntityUpdate != null && !nestedEntityUpdate.isEmpty();
    }

    /**
     * Has enumeration update.
     *
     * @return true if so false otherwise
     */
    public boolean hasEnumerationUpdate() {
        return enumerationsUpdate != null && !enumerationsUpdate.isEmpty();
    }

    /**
     * Has source systems update.
     *
     * @return true if so false otherwise
     */
    public boolean hasSourceSystemsUpdate() {
        return sourceSystemsUpdate != null && !sourceSystemsUpdate.isEmpty();
    }

    /**
     * Has relations update.
     *
     * @return true if so false otherwise
     */
    public boolean hasRelationsUpdate() {
        return relationsUpdate != null && !relationsUpdate.isEmpty();
    }

    public boolean hasEntitiesGroupUpdate() {
        return entitiesGroupsUpdate != null;
    }

    /**
     * Is direct update.
     * @return true if so false otherwise
     */
    public boolean isDirect() {
        return getFlag(MetaContextFlags.FLAG_DIRECT);
    }

    @Override
    public boolean isDraft() {
        return getFlag(MetaContextFlags.FLAG_DRAFT);
    }

    /**
     * @param predicate - filtering condition
     * @return collection of @link{com.unidata.mdm.meta.SimpleAttributeDef}.
     */
    public Collection<SimpleMetaModelAttribute> getAttributes(Predicate<? super SimpleMetaModelAttribute> predicate) {
        Collection<SimpleMetaModelAttribute> attrsFromLookupEntities = this.getLookupEntityUpdate().stream()
                .map(LookupEntity::getSimpleAttribute)
                .flatMap(Collection::stream)
                .filter(predicate).collect(toList());
        Collection<SimpleMetaModelAttribute> attrsFromEntities = this.getEntityUpdate().stream()
                .map(Entity::getSimpleAttribute)
                .flatMap(Collection::stream)
                .filter(predicate).collect(Collectors.toList());
        Collection<SimpleMetaModelAttribute> attrsFromNestedEntities = this.getNestedEntityUpdate().stream()
                .map(NestedEntity::getSimpleAttribute)
                .flatMap(Collection::stream)
                .filter(predicate).collect(Collectors.toList());
        Collection<SimpleMetaModelAttribute> allAttr = new ArrayList<>(attrsFromEntities.size() + attrsFromLookupEntities.size() + attrsFromNestedEntities.size());
        allAttr.addAll(attrsFromLookupEntities);
        allAttr.addAll(attrsFromEntities);
        allAttr.addAll(attrsFromNestedEntities);
        return allAttr;
    }

    /**
     * @return return names of all included top level model element names
     */
    public Collection<String> getAllTopModelElementNames() {
        Collection<String> allNames = new ArrayList<>();
        if (hasEntitiesGroupUpdate()) {
            allNames.add(getEntitiesGroupsUpdate().getGroupName());
        }

        getNestedEntityUpdate().stream().map(NestedEntity::getName).collect(toCollection(() -> allNames));
        getEntityUpdate().stream().map(Entity::getName).collect(toCollection(() -> allNames));
        getLookupEntityUpdate().stream().map(LookupEntity::getName).collect(toCollection(() -> allNames));
        getRelationsUpdate().stream().map(Relation::getName).collect(toCollection(() -> allNames));
        getSourceSystemsUpdate().stream().map(SourceSystem::getName).collect(toCollection(() -> allNames));
        return allNames;
    }



    public enum ModelUpsertType {
        /**
         * One element update
         */
        PARTIAL_UPDATE,
        /**
         * Model will be recreated
         */
        FULLY_NEW ,
        /**
         * Current model and existed will be merged
         */
        ADDITION
    }

    public static UpdateModelRequestContextBuilder builder() {
        return new UpdateModelRequestContextBuilder();
    }
    /**
     * @author Mikhail Mikhailov
     *         Request context builder.
     */
    public static class UpdateModelRequestContextBuilder extends AbstractCompositeRequestContextBuilder<UpdateModelRequestContextBuilder> {
        /**
         * Top level entity updates
         */
        private List<Entity> entityUpdate = Collections.emptyList();
        /**
         * Lookup entity updates.
         */
        private List<LookupEntity> lookupEntityUpdate = Collections.emptyList();
        /**
         * Nested entity updates.
         */
        private List<NestedEntity> nestedEntityUpdate = Collections.emptyList();
        /**
         * Enumeration updates.
         */
        private List<EnumerationDataType> enumerationsUpdate = Collections.emptyList();
        /**
         * Source systems updates.
         */
        private List<SourceSystem> sourceSystemsUpdate = Collections.emptyList();
        /**
         * Relations updates.
         */
        private List<Relation> relationsUpdate = Collections.emptyList();
        /**
         * Entities group updates
         */
        private EntitiesGroup entitiesGroupsUpdate = null;
        /**
         * Storage ID to apply the updates to.
         */
        private String storageId;

        private ModelUpsertType upsertType = ModelUpsertType.PARTIAL_UPDATE;

        private boolean skipRemoveElements;
        /**
         * Direct update?
         */
        private boolean direct;
        /**
         * is draft version
         */
        private boolean draft;

        /**
         * Constructor.
         */
        public UpdateModelRequestContextBuilder() {
            super();
        }

        /**
         * Sets entity update.
         *
         * @param entityUpdate the update
         * @return self
         */
        public UpdateModelRequestContextBuilder entityUpdate(List<Entity> entityUpdate) {
            this.entityUpdate = entityUpdate;
            return this;
        }

        /**
         * Sets lookup entity update.
         *
         * @param lookupEntityUpdate the update
         * @return self
         */
        public UpdateModelRequestContextBuilder lookupEntityUpdate(List<LookupEntity> lookupEntityUpdate) {
            this.lookupEntityUpdate = lookupEntityUpdate;
            return this;
        }

        /**
         * Sets nested entity update.
         *
         * @param nestedEntityUpdate the update
         * @return self
         */
        public UpdateModelRequestContextBuilder nestedEntityUpdate(List<NestedEntity> nestedEntityUpdate) {
            this.nestedEntityUpdate = nestedEntityUpdate;
            return this;
        }

        /**
         * Sets enumeration update.
         *
         * @param enumerationsUpdate the update
         * @return self
         */
        public UpdateModelRequestContextBuilder enumerationsUpdate(List<EnumerationDataType> enumerationsUpdate) {
            this.enumerationsUpdate = enumerationsUpdate;
            return this;
        }

        /**
         * Sets source systems update.
         *
         * @param sourceSystemsUpdate the update
         * @return self
         */
        public UpdateModelRequestContextBuilder sourceSystemsUpdate(List<SourceSystem> sourceSystemsUpdate) {
            this.sourceSystemsUpdate = sourceSystemsUpdate;
            return this;
        }

        /**
         * Sets relations update.
         *
         * @param relationsUpdate the update
         * @return self
         */
        public UpdateModelRequestContextBuilder relationsUpdate(List<Relation> relationsUpdate) {
            this.relationsUpdate = relationsUpdate;
            return this;
        }
        /**
         * @param entitiesGroupsUpdate
         * @return
         */
        public UpdateModelRequestContextBuilder entitiesGroupsUpdate(EntitiesGroup entitiesGroupsUpdate) {
            this.entitiesGroupsUpdate = entitiesGroupsUpdate;
            return this;
        }
        /**
         * Sets storage ID.
         *
         * @param storageId the ID
         * @return self
         */
        public UpdateModelRequestContextBuilder storageId(String storageId) {
            this.storageId = storageId;
            return this;
        }

        public UpdateModelRequestContextBuilder isForceRecreate(ModelUpsertType upsertType) {
            this.upsertType = upsertType;
            return this;
        }

        public UpdateModelRequestContextBuilder skipRemoveElements(boolean skipRemoveElements) {
            this.skipRemoveElements = skipRemoveElements;
            return this;
        }

        public UpdateModelRequestContextBuilder direct(boolean direct) {
            this.direct = direct;
            return this;
        }

        public UpdateModelRequestContextBuilder draft(boolean draft) {
            this.draft = draft;
            return this;
        }

        /**
         * Builder method.
         *
         * @return new context
         */
        @Override
        public UpdateModelRequestContext build() {
            return new UpdateModelRequestContext(this);
        }


    }
}
