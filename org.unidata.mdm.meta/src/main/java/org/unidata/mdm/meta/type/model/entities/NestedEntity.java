package org.unidata.mdm.meta.type.model.entities;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Collections;

import org.unidata.mdm.meta.type.model.CustomProperty;
import org.unidata.mdm.meta.type.model.PeriodBoundary;
import org.unidata.mdm.meta.type.model.attributes.ArrayMetaModelAttribute;
import org.unidata.mdm.meta.type.model.attributes.AttributeGroup;
import org.unidata.mdm.meta.type.model.attributes.ComplexMetaModelAttribute;
import org.unidata.mdm.meta.type.model.attributes.SimpleMetaModelAttribute;
import org.unidata.mdm.meta.type.model.merge.MergeSettings;

public class NestedEntity extends ComplexAttributesHolderEntity implements Serializable {

    private final static long serialVersionUID = 987654321L;

    @Override
    public NestedEntity withComplexAttribute(ComplexMetaModelAttribute... values) {
        if (values != null) {
            Collections.addAll(getComplexAttribute(), values);
        }
        return this;
    }

    @Override
    public NestedEntity withComplexAttribute(Collection<ComplexMetaModelAttribute> values) {
        if (values != null && !values.isEmpty()) {
            getComplexAttribute().addAll(values);
        }
        return this;
    }

    @Override
    public NestedEntity withSimpleAttribute(SimpleMetaModelAttribute... values) {
        if (values != null) {
            Collections.addAll(getSimpleAttribute(), values);
        }
        return this;
    }

    @Override
    public NestedEntity withSimpleAttribute(Collection<SimpleMetaModelAttribute> values) {
        if (values != null && !values.isEmpty()) {
            getSimpleAttribute().addAll(values);
        }
        return this;
    }

    @Override
    public NestedEntity withArrayAttribute(ArrayMetaModelAttribute... values) {
        if (values != null) {
            Collections.addAll(getArrayAttribute(), values);
        }
        return this;
    }

    @Override
    public NestedEntity withArrayAttribute(Collection<ArrayMetaModelAttribute> values) {
        if (values != null && !values.isEmpty()) {
            getArrayAttribute().addAll(values);
        }
        return this;
    }

    @Override
    public NestedEntity withMergeSettings(MergeSettings value) {
        setMergeSettings(value);
        return this;
    }

    @Override
    public NestedEntity withValidityPeriod(PeriodBoundary value) {
        setValidityPeriod(value);
        return this;
    }

    @Override
    public NestedEntity withAttributeGroups(AttributeGroup... values) {
        if (values != null) {
            Collections.addAll(getAttributeGroups(), values);
        }
        return this;
    }

    @Override
    public NestedEntity withAttributeGroups(Collection<AttributeGroup> values) {
        if (values != null && !values.isEmpty()) {
            getAttributeGroups().addAll(values);
        }
        return this;
    }

    @Override
    public NestedEntity withRelationGroups(RelationGroup... values) {
        if (values != null) {
            Collections.addAll(getRelationGroups(), values);
        }
        return this;
    }

    @Override
    public NestedEntity withRelationGroups(Collection<RelationGroup> values) {
        if (values != null && !values.isEmpty()) {
            getRelationGroups().addAll(values);
        }
        return this;
    }

    @Override
    public NestedEntity withCustomProperties(CustomProperty... values) {
        if (values != null) {
            Collections.addAll(getCustomProperties(), values);
        }
        return this;
    }

    @Override
    public NestedEntity withCustomProperties(Collection<CustomProperty> values) {
        if (values != null && !values.isEmpty()) {
            getCustomProperties().addAll(values);
        }
        return this;
    }

    @Override
    public NestedEntity withName(String value) {
        setName(value);
        return this;
    }

    @Override
    public NestedEntity withDisplayName(String value) {
        setDisplayName(value);
        return this;
    }

    @Override
    public NestedEntity withDescription(String value) {
        setDescription(value);
        return this;
    }

    @Override
    public NestedEntity withVersion(Long value) {
        setVersion(value);
        return this;
    }

    @Override
    public NestedEntity withUpdatedAt(LocalDateTime value) {
        setUpdatedAt(value);
        return this;
    }

    @Override
    public NestedEntity withCreateAt(LocalDateTime value) {
        setCreateAt(value);
        return this;
    }

}
