package org.unidata.mdm.meta.type.model.measurement;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import static org.unidata.mdm.meta.type.model.ModelNamespace.META_MODEL_NAMESPACE;


public class MeasurementValue implements Serializable {

    private final static long serialVersionUID = 987654321L;

    @JacksonXmlProperty(namespace = META_MODEL_NAMESPACE)
    protected List<MeasurementUnit> unit;

    @JacksonXmlProperty(isAttribute = true)
    protected String id;

    @JacksonXmlProperty(isAttribute = true)
    protected String shortName;

    @JacksonXmlProperty(isAttribute = true)
    protected String displayName;

    public List<MeasurementUnit> getUnit() {
        if (unit == null) {
            unit = new ArrayList<MeasurementUnit>();
        }
        return this.unit;
    }

    public String getId() {
        return id;
    }

    public void setId(String value) {
        this.id = value;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String value) {
        this.shortName = value;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String value) {
        this.displayName = value;
    }

    public MeasurementValue withUnit(MeasurementUnit... values) {
        if (values!= null) {
            for (MeasurementUnit value: values) {
                getUnit().add(value);
            }
        }
        return this;
    }

    public MeasurementValue withUnit(Collection<MeasurementUnit> values) {
        if (values!= null) {
            getUnit().addAll(values);
        }
        return this;
    }

    public MeasurementValue withId(String value) {
        setId(value);
        return this;
    }

    public MeasurementValue withShortName(String value) {
        setShortName(value);
        return this;
    }

    public MeasurementValue withDisplayName(String value) {
        setDisplayName(value);
        return this;
    }

}
