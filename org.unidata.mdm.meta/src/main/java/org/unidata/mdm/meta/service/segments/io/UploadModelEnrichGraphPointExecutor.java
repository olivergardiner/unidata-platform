/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.meta.service.segments.io;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.meta.context.UploadModelRequestContext;
import org.unidata.mdm.meta.dto.FullModelDTO;
import org.unidata.mdm.meta.exception.MetaExceptionIds;
import org.unidata.mdm.meta.module.MetaModule;
import org.unidata.mdm.meta.service.MetaModelImportService;
import org.unidata.mdm.meta.type.input.meta.MetaExistence;
import org.unidata.mdm.meta.type.input.meta.MetaType;
import org.unidata.mdm.meta.util.MetaModelGraphComponent;
import org.unidata.mdm.system.exception.PlatformBusinessException;
import org.unidata.mdm.system.type.pipeline.Point;
import org.unidata.mdm.system.type.pipeline.Start;

/**
 * @author maria.chistyakova
 * @since 27.02.2020
 */
@Component(UploadModelEnrichGraphPointExecutor.SEGMENT_ID)
public class UploadModelEnrichGraphPointExecutor extends Point<UploadModelRequestContext> {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = MetaModule.MODULE_ID + "[UPLOAD_MODEL_ENRICH_GRAPH_POINT]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = MetaModule.MODULE_ID + ".upload.model.enrich.graph.description";

    @Autowired
    private MetaModelImportService metaModelImportService;

    @Autowired
    private MetaModelGraphComponent metaModelGraphComponent;

    /**
     * Constructor.
     */
    public UploadModelEnrichGraphPointExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION);
    }

    @Override
    public void point(UploadModelRequestContext ctx) {
        if (!ctx.getMessages().isEmpty()) {
            throw new PlatformBusinessException(
                    "File structure invalid.",
                    MetaExceptionIds.EX_META_IMPORT_MODEL_FILE_STRUCTURE_INVALID,
                    ctx.getMetaGraph().getFileName(),
                    "\n" + String.join("\n", ctx.getMessages())
            );
        }

        FullModelDTO fullModelDTO = metaModelImportService.prepareUploadData(
                ctx.getRootPath(),
                ctx.getZipPath(),
                ctx.getMetaGraph().isOverride(),
                ctx.getMetaGraph().getFileName(),
                ctx.getMetaGraph().getId()
        );

        if (fullModelDTO == null) {
            return;
        }

        // read old structure
        metaModelImportService.fillResponseMetaGraphWithCurrentData(ctx.getMetaGraph());

        // fill  MetaGraph with xml data

        metaModelImportService.enrich(
                ctx.getMetaGraph(),
                MetaExistence.NEW,
                MetaType.values()
        );

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?> start) {
        return false;
    }

}
