package org.unidata.mdm.meta.type.model.enumeration;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.unidata.mdm.meta.type.model.VersionedObject;

import static org.unidata.mdm.meta.type.model.ModelNamespace.META_MODEL_NAMESPACE;


public class EnumerationDataType extends VersionedObject implements Serializable {

    private final static long serialVersionUID = 987654321L;

    @JacksonXmlProperty(namespace = META_MODEL_NAMESPACE)
    protected List<EnumerationValue> enumVal;
    @JacksonXmlProperty(isAttribute = true)
    protected String name;
    @JacksonXmlProperty(isAttribute = true)
    protected String displayName;

    public List<EnumerationValue> getEnumVal() {
        if (enumVal == null) {
            enumVal = new ArrayList<>();
        }
        return this.enumVal;
    }

    public String getName() {
        return name;
    }

    public void setName(String value) {
        this.name = value;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String value) {
        this.displayName = value;
    }

    public EnumerationDataType withEnumVal(EnumerationValue... values) {
        if (values!= null) {
            Collections.addAll(getEnumVal(), values);
        }
        return this;
    }

    public EnumerationDataType withEnumVal(Collection<EnumerationValue> values) {
        if (values!= null && !values.isEmpty()) {
            getEnumVal().addAll(values);
        }
        return this;
    }

    public EnumerationDataType withName(String value) {
        setName(value);
        return this;
    }

    public EnumerationDataType withDisplayName(String value) {
        setDisplayName(value);
        return this;
    }

    @Override
    public EnumerationDataType withVersion(Long value) {
        setVersion(value);
        return this;
    }

    @Override
    public EnumerationDataType withUpdatedAt(LocalDateTime value) {
        setUpdatedAt(value);
        return this;
    }

    @Override
    public EnumerationDataType withCreateAt(LocalDateTime value) {
        setCreateAt(value);
        return this;
    }

}
