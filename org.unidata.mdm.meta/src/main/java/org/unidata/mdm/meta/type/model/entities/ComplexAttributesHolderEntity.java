package org.unidata.mdm.meta.type.model.entities;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.unidata.mdm.meta.type.model.CustomProperty;
import org.unidata.mdm.meta.type.model.PeriodBoundary;
import org.unidata.mdm.meta.type.model.attributes.ArrayMetaModelAttribute;
import org.unidata.mdm.meta.type.model.attributes.AttributeGroup;
import org.unidata.mdm.meta.type.model.attributes.ComplexMetaModelAttribute;
import org.unidata.mdm.meta.type.model.attributes.SimpleMetaModelAttribute;
import org.unidata.mdm.meta.type.model.merge.MergeSettings;

import static org.unidata.mdm.meta.type.model.ModelNamespace.META_MODEL_NAMESPACE;


public class ComplexAttributesHolderEntity extends AbstractEntity {

    private final static long serialVersionUID = 987654321L;

    @JacksonXmlProperty(namespace = META_MODEL_NAMESPACE)
    protected List<ComplexMetaModelAttribute> complexAttribute;

    public List<ComplexMetaModelAttribute> getComplexAttribute() {
        if (complexAttribute == null) {
            complexAttribute = new ArrayList<>();
        }
        return this.complexAttribute;
    }

    public ComplexAttributesHolderEntity withComplexAttribute(ComplexMetaModelAttribute... values) {
        if (values != null) {
            Collections.addAll(getComplexAttribute(), values);
        }
        return this;
    }

    public ComplexAttributesHolderEntity withComplexAttribute(Collection<ComplexMetaModelAttribute> values) {
        if (values != null && !values.isEmpty()) {
            getComplexAttribute().addAll(values);
        }
        return this;
    }

    @Override
    public ComplexAttributesHolderEntity withSimpleAttribute(SimpleMetaModelAttribute... values) {
        if (values != null) {
            Collections.addAll(getSimpleAttribute(), values);
        }
        return this;
    }

    @Override
    public ComplexAttributesHolderEntity withSimpleAttribute(Collection<SimpleMetaModelAttribute> values) {
        if (values != null && !values.isEmpty()) {
            getSimpleAttribute().addAll(values);
        }
        return this;
    }

    @Override
    public ComplexAttributesHolderEntity withArrayAttribute(ArrayMetaModelAttribute... values) {
        if (values != null) {
            Collections.addAll(getArrayAttribute(), values);
        }
        return this;
    }

    @Override
    public ComplexAttributesHolderEntity withArrayAttribute(Collection<ArrayMetaModelAttribute> values) {
        if (values != null) {
            getArrayAttribute().addAll(values);
        }
        return this;
    }

    @Override
    public ComplexAttributesHolderEntity withMergeSettings(MergeSettings value) {
        setMergeSettings(value);
        return this;
    }

    @Override
    public ComplexAttributesHolderEntity withValidityPeriod(PeriodBoundary value) {
        setValidityPeriod(value);
        return this;
    }

    @Override
    public ComplexAttributesHolderEntity withAttributeGroups(AttributeGroup... values) {
        if (values != null) {
            Collections.addAll(getAttributeGroups(), values);
        }
        return this;
    }

    @Override
    public ComplexAttributesHolderEntity withAttributeGroups(Collection<AttributeGroup> values) {
        if (values != null && !values.isEmpty()) {
            getAttributeGroups().addAll(values);
        }
        return this;
    }

    @Override
    public ComplexAttributesHolderEntity withRelationGroups(RelationGroup... values) {
        if (values != null) {
            Collections.addAll(getRelationGroups(), values);
        }
        return this;
    }

    @Override
    public ComplexAttributesHolderEntity withRelationGroups(Collection<RelationGroup> values) {
        if (values != null && !values.isEmpty()) {
            getRelationGroups().addAll(values);
        }
        return this;
    }

    @Override
    public ComplexAttributesHolderEntity withCustomProperties(CustomProperty... values) {
        if (values != null) {
            Collections.addAll(getCustomProperties(), values);
        }
        return this;
    }

    @Override
    public ComplexAttributesHolderEntity withCustomProperties(Collection<CustomProperty> values) {
        if (values != null && !values.isEmpty()) {
            getCustomProperties().addAll(values);
        }
        return this;
    }

    @Override
    public ComplexAttributesHolderEntity withName(String value) {
        setName(value);
        return this;
    }

    @Override
    public ComplexAttributesHolderEntity withDisplayName(String value) {
        setDisplayName(value);
        return this;
    }

    @Override
    public ComplexAttributesHolderEntity withDescription(String value) {
        setDescription(value);
        return this;
    }

    @Override
    public ComplexAttributesHolderEntity withVersion(Long value) {
        setVersion(value);
        return this;
    }

    @Override
    public ComplexAttributesHolderEntity withUpdatedAt(LocalDateTime value) {
        setUpdatedAt(value);
        return this;
    }

    @Override
    public ComplexAttributesHolderEntity withCreateAt(LocalDateTime value) {
        setCreateAt(value);
        return this;
    }

}
