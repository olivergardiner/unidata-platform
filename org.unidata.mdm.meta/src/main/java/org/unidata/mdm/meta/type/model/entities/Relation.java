package org.unidata.mdm.meta.type.model.entities;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.unidata.mdm.meta.type.model.CustomProperty;
import org.unidata.mdm.meta.type.model.PeriodBoundary;
import org.unidata.mdm.meta.type.model.ValueGenerationStrategy;
import org.unidata.mdm.meta.type.model.attributes.ArrayMetaModelAttribute;
import org.unidata.mdm.meta.type.model.attributes.AttributeGroup;
import org.unidata.mdm.meta.type.model.attributes.ComplexMetaModelAttribute;
import org.unidata.mdm.meta.type.model.attributes.SimpleMetaModelAttribute;
import org.unidata.mdm.meta.type.model.merge.MergeSettings;

import static org.unidata.mdm.meta.type.model.ModelNamespace.META_MODEL_NAMESPACE;

public class Relation extends ComplexAttributesHolderEntity implements Serializable {

    private final static long serialVersionUID = 987654321L;

    @JacksonXmlProperty(namespace = META_MODEL_NAMESPACE)
    protected ValueGenerationStrategy externalIdGenerationStrategy;

    @JacksonXmlProperty(namespace = META_MODEL_NAMESPACE)
    protected List<String> toEntityDefaultDisplayAttributes;

    @JacksonXmlProperty(namespace = META_MODEL_NAMESPACE)
    protected List<String> toEntitySearchAttributes;

    @JacksonXmlProperty(isAttribute = true)
    protected boolean useAttributeNameForDisplay;

    @JacksonXmlProperty(isAttribute = true)
    protected String fromEntity;

    @JacksonXmlProperty(isAttribute = true)
    protected String toEntity;

    @JacksonXmlProperty(isAttribute = true)
    protected RelType relType;

    @JacksonXmlProperty(isAttribute = true)
    protected boolean required;

    public ValueGenerationStrategy getExternalIdGenerationStrategy() {
        return externalIdGenerationStrategy;
    }

    public void setExternalIdGenerationStrategy(ValueGenerationStrategy value) {
        this.externalIdGenerationStrategy = value;
    }

    public List<String> getToEntityDefaultDisplayAttributes() {
        if (toEntityDefaultDisplayAttributes == null) {
            toEntityDefaultDisplayAttributes = new ArrayList<>();
        }
        return this.toEntityDefaultDisplayAttributes;
    }

    public List<String> getToEntitySearchAttributes() {
        if (toEntitySearchAttributes == null) {
            toEntitySearchAttributes = new ArrayList<>();
        }
        return this.toEntitySearchAttributes;
    }

    public boolean isUseAttributeNameForDisplay() {
        return useAttributeNameForDisplay;
    }

    public void setUseAttributeNameForDisplay(Boolean value) {
        this.useAttributeNameForDisplay = value;
    }

    public String getFromEntity() {
        return fromEntity;
    }

    public void setFromEntity(String value) {
        this.fromEntity = value;
    }

    public String getToEntity() {
        return toEntity;
    }

    public void setToEntity(String value) {
        this.toEntity = value;
    }

    public RelType getRelType() {
        return relType;
    }

    public void setRelType(RelType value) {
        this.relType = value;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(Boolean value) {
        this.required = value;
    }

    public Relation withExternalIdGenerationStrategy(ValueGenerationStrategy value) {
        setExternalIdGenerationStrategy(value);
        return this;
    }

    public Relation withToEntityDefaultDisplayAttributes(String... values) {
        if (values != null) {
            Collections.addAll(getToEntityDefaultDisplayAttributes(), values);
        }
        return this;
    }

    public Relation withToEntityDefaultDisplayAttributes(Collection<String> values) {
        if (values != null) {
            getToEntityDefaultDisplayAttributes().addAll(values);
        }
        return this;
    }

    public Relation withToEntitySearchAttributes(String... values) {
        if (values != null) {
            Collections.addAll(getToEntitySearchAttributes(), values);
        }
        return this;
    }

    public Relation withToEntitySearchAttributes(Collection<String> values) {
        if (values != null) {
            getToEntitySearchAttributes().addAll(values);
        }
        return this;
    }

    public Relation withUseAttributeNameForDisplay(Boolean value) {
        setUseAttributeNameForDisplay(value);
        return this;
    }

    public Relation withFromEntity(String value) {
        setFromEntity(value);
        return this;
    }

    public Relation withToEntity(String value) {
        setToEntity(value);
        return this;
    }

    public Relation withRelType(RelType value) {
        setRelType(value);
        return this;
    }

    public Relation withRequired(Boolean value) {
        setRequired(value);
        return this;
    }

    @Override
    public Relation withComplexAttribute(ComplexMetaModelAttribute... values) {
        if (values != null) {
            Collections.addAll(getComplexAttribute(), values);
        }
        return this;
    }

    @Override
    public Relation withComplexAttribute(Collection<ComplexMetaModelAttribute> values) {
        if (values != null) {
            getComplexAttribute().addAll(values);
        }
        return this;
    }

    @Override
    public Relation withSimpleAttribute(SimpleMetaModelAttribute... values) {
        if (values != null) {
            Collections.addAll(getSimpleAttribute(), values);
        }
        return this;
    }

    @Override
    public Relation withSimpleAttribute(Collection<SimpleMetaModelAttribute> values) {
        if (values != null) {
            getSimpleAttribute().addAll(values);
        }
        return this;
    }

    @Override
    public Relation withArrayAttribute(ArrayMetaModelAttribute... values) {
        if (values != null) {
            Collections.addAll(getArrayAttribute(), values);
        }
        return this;
    }

    @Override
    public Relation withArrayAttribute(Collection<ArrayMetaModelAttribute> values) {
        if (values != null) {
            getArrayAttribute().addAll(values);
        }
        return this;
    }

    @Override
    public Relation withMergeSettings(MergeSettings value) {
        setMergeSettings(value);
        return this;
    }

    @Override
    public Relation withValidityPeriod(PeriodBoundary value) {
        setValidityPeriod(value);
        return this;
    }

    @Override
    public Relation withAttributeGroups(AttributeGroup... values) {
        if (values != null) {
            Collections.addAll(getAttributeGroups(), values);
        }
        return this;
    }

    @Override
    public Relation withAttributeGroups(Collection<AttributeGroup> values) {
        if (values != null) {
            getAttributeGroups().addAll(values);
        }
        return this;
    }

    @Override
    public Relation withRelationGroups(RelationGroup... values) {
        if (values != null) {
            Collections.addAll(getRelationGroups(), values);
        }
        return this;
    }

    @Override
    public Relation withRelationGroups(Collection<RelationGroup> values) {
        if (values != null) {
            getRelationGroups().addAll(values);
        }
        return this;
    }

    @Override
    public Relation withCustomProperties(CustomProperty... values) {
        if (values != null) {
            Collections.addAll(getCustomProperties(), values);
        }
        return this;
    }

    @Override
    public Relation withCustomProperties(Collection<CustomProperty> values) {
        if (values != null) {
            getCustomProperties().addAll(values);
        }
        return this;
    }

    @Override
    public Relation withName(String value) {
        setName(value);
        return this;
    }

    @Override
    public Relation withDisplayName(String value) {
        setDisplayName(value);
        return this;
    }

    @Override
    public Relation withDescription(String value) {
        setDescription(value);
        return this;
    }

    @Override
    public Relation withVersion(Long value) {
        setVersion(value);
        return this;
    }

    @Override
    public Relation withUpdatedAt(LocalDateTime value) {
        setUpdatedAt(value);
        return this;
    }

    @Override
    public Relation withCreateAt(LocalDateTime value) {
        setCreateAt(value);
        return this;
    }

}
