package org.unidata.mdm.meta.type.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;


public class VersionedObject implements Serializable {

    private final static long serialVersionUID = 987654321L;

    @JacksonXmlProperty(isAttribute = true)
    protected Long version;

    @JacksonXmlProperty(isAttribute = true)
    protected LocalDateTime updatedAt;

    @JacksonXmlProperty(isAttribute = true)
    protected LocalDateTime createAt;

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long value) {
        this.version = value;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime value) {
        this.updatedAt = value;
    }

    public LocalDateTime getCreateAt() {
        return createAt;
    }

    public void setCreateAt(LocalDateTime value) {
        this.createAt = value;
    }

    public VersionedObject withVersion(Long value) {
        setVersion(value);
        return this;
    }

    public VersionedObject withUpdatedAt(LocalDateTime value) {
        setUpdatedAt(value);
        return this;
    }

    public VersionedObject withCreateAt(LocalDateTime value) {
        setCreateAt(value);
        return this;
    }

}
