/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.meta.context;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.unidata.mdm.meta.service.segments.io.UploadModelStartExecutor;
import org.unidata.mdm.meta.type.input.meta.MetaEdgeFactory;
import org.unidata.mdm.meta.type.input.meta.MetaGraph;
import org.unidata.mdm.system.context.AbstractCompositeRequestContext;
import org.unidata.mdm.system.type.pipeline.PipelineInput;
import org.unidata.mdm.system.util.IdUtils;

/**
 * buildUploadModelPreview
 *
 * @author maria.chistyakova
 */
public class UploadModelRequestContext
        extends AbstractCompositeRequestContext
        implements PipelineInput {

    private java.nio.file.Path zipPath;

    public Path getZipPath() {
        return zipPath;
    }

    private java.nio.file.Path rootPath;

    public Path getRootPath() {
        return rootPath;
    }

    public void setRootPath(Path rootPath) {
        this.rootPath = rootPath;
    }

    private List<String> messages = new ArrayList<>();

    private MetaGraph metaGraph;

    public MetaGraph getMetaGraph() {
        return metaGraph;
    }

    /**
     * @return unmodifiable presentation of messages
     */
    public List<String> getMessages() {
        return Collections.unmodifiableList(messages);
    }

    public void addMessages(List<String> messages) {
        if (CollectionUtils.isNotEmpty(messages)) {
            this.messages.addAll(messages);
        }
    }

    public void addMessage(String messages) {
        if (StringUtils.isNoneEmpty(messages)) {
            this.messages.add(messages);
        }
    }


    /**
     * Constructor.
     *
     * @param b the builder.
     */
    private UploadModelRequestContext(Builder b) {
        super(b);
        this.metaGraph = b.metaGraph;
        this.zipPath = b.zipPath;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getStartTypeId() {
        return UploadModelStartExecutor.SEGMENT_ID;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder extends AbstractCompositeRequestContextBuilder<Builder> {

        private MetaGraph metaGraph;

        private java.nio.file.Path zipPath;

        private boolean isOverride;

        private String id;

        /**
         * Constructor.
         */
        protected Builder() {
            super();
        }

        /**
         * optional
         *
         * @param metaGraph
         * @return this
         */
        public Builder setMetaGraph(MetaGraph metaGraph) {
            this.metaGraph = metaGraph;
            return this;
        }

        public Builder setZipPath(Path zipPath) {
            this.zipPath = zipPath;
            return this;
        }

        public Builder setOverride(boolean override) {
            isOverride = override;
            return this;
        }

        /**
         * optional
         *
         * @param id
         * @return this
         */
        public Builder setId(String id) {
            this.id = id;
            return this;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public UploadModelRequestContext build() {

            if (zipPath == null) {
                throw new NullPointerException("zipPath == null");
            }

            if (id == null) {
                this.id = IdUtils.v4String();
            }

            if (metaGraph == null) {
                this.metaGraph = new MetaGraph(new MetaEdgeFactory());
            }

            this.metaGraph.setOverride(isOverride);
            this.metaGraph.setId(id);
            this.metaGraph.setFileName(zipPath.getFileName().toString());


            return new UploadModelRequestContext(this);
        }


    }
}
