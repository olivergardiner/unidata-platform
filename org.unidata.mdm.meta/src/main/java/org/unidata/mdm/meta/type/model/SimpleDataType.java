package org.unidata.mdm.meta.type.model;

import com.fasterxml.jackson.annotation.JsonProperty;


public enum SimpleDataType {

    @JsonProperty("Date")
    DATE("Date"),

    @JsonProperty("Time")
    TIME("Time"),

    @JsonProperty("Timestamp")
    TIMESTAMP("Timestamp"),

    @JsonProperty("String")
    STRING("String"),

    @JsonProperty("Integer")
    INTEGER("Integer"),

    @JsonProperty("Number")
    NUMBER("Number"),

    @JsonProperty("Boolean")
    BOOLEAN("Boolean"),

    @JsonProperty("Blob")
    BLOB("Blob"),

    @JsonProperty("Clob")
    CLOB("Clob"),

    @JsonProperty("Measured")
    MEASURED("Measured"),

    @JsonProperty("Any")
    ANY("Any");

    private final String value;

    SimpleDataType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SimpleDataType fromValue(String v) {
        for (SimpleDataType c: SimpleDataType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
