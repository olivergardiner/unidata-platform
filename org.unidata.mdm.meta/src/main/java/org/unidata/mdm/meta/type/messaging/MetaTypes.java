package org.unidata.mdm.meta.type.messaging;

import org.unidata.mdm.core.type.messaging.CoreSubsystems;
import org.unidata.mdm.meta.module.MetaModule;
import org.unidata.mdm.system.type.messaging.MessageType;

/**
 * @author Mikhail Mikhailov on Jul 15, 2020
 */
public class MetaTypes {
    /**
     * Constructor.
     */
    private MetaTypes() {
        super();
    }
    /**
     * Draft remove audit message.
     */
    public static final MessageType DRAFT_REMOVE = new MessageType("meta-draft-remove")
            .withSubsystem(CoreSubsystems.AUDIT_SUBSYSTEM)
            .withDescription(MetaModule.MODULE_ID + ".messaging.meta-draft-remove")
            .withHeaders(MetaHeaders.CONTEXT);
    /**
     * Draft apply audit message.
     */
    public static final MessageType DRAFT_APPLY = new MessageType("meta-draft-apply")
            .withSubsystem(CoreSubsystems.AUDIT_SUBSYSTEM)
            .withDescription(MetaModule.MODULE_ID + ".messaging.meta-draft-apply")
            .withHeaders(MetaHeaders.CONTEXT);
    /**
     * Draft upsert audit message.
     */
    public static final MessageType DRAFT_UPSERT = new MessageType("meta-draft-upsert")
            .withSubsystem(CoreSubsystems.AUDIT_SUBSYSTEM)
            .withDescription(MetaModule.MODULE_ID + ".messaging.meta-draft-upsert")
            .withHeaders(MetaHeaders.CONTEXT);
    /**
     * Draft delete audit message.
     */
    public static final MessageType DRAFT_DELETE = new MessageType("meta-draft-delete")
            .withSubsystem(CoreSubsystems.AUDIT_SUBSYSTEM)
            .withDescription(MetaModule.MODULE_ID + ".messaging.meta-draft-delete")
            .withHeaders(MetaHeaders.CONTEXT);

    public static final MessageType[] TYPES = {
            DRAFT_APPLY,
            DRAFT_DELETE,
            DRAFT_REMOVE,
            DRAFT_UPSERT
    };
}
