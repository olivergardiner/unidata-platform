package org.unidata.mdm.meta.type.model.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum RelType {

    @JsonProperty("References")
    REFERENCES("References"),

    @JsonProperty("Contains")
    CONTAINS("Contains"),

    @JsonProperty("ManyToMany")
    MANY_TO_MANY("ManyToMany");

    private final String value;

    RelType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RelType fromValue(String v) {
        for (RelType c: RelType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
