/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.meta.type;

/**
 * @author maria.chistyakova
 * @since 20.04.2020
 */
public class InputOutputModelStructureConstants {

    private InputOutputModelStructureConstants() {
    }

    /**
     * The measure.
     */
    public static final String MEASURE = "measure";

    /**
     * The Constant BPM.
     */
    public static final String BPM = "bpm";

    /**
     * The Constant CONFIG.
     */
    public static final String CONFIG = "config";

    /**
     * The Constant CONFIG.
     */
    public static final String SECURITY = "security";

    /**
     * The catalina base.
     */
    public static final String CATALINA_BASE = "catalina.base";

    /**
     * The xml extension.
     */
    public static final String XML_EXTENSION = ".xml";

    /**
     * The jar extension.
     */
    public static final String JAR_EXTENSION = ".jar";

    /**
     * The model.
     */
    public static final String MODEL = "model";
}
