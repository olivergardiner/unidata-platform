package org.unidata.mdm.meta.type.model.entities;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.unidata.mdm.meta.type.model.VersionedObject;

import static org.unidata.mdm.meta.type.model.ModelNamespace.META_MODEL_NAMESPACE;

public class EntitiesGroup extends VersionedObject implements Serializable {

    private final static long serialVersionUID = 987654321L;

    @JacksonXmlProperty(namespace = META_MODEL_NAMESPACE)
    protected List<EntitiesGroup> innerGroups;
    @JacksonXmlProperty(isAttribute = true)
    protected String groupName;
    @JacksonXmlProperty(isAttribute = true)
    protected String title;

    public List<EntitiesGroup> getInnerGroups() {
        if (innerGroups == null) {
            innerGroups = new ArrayList<>();
        }
        return this.innerGroups;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String value) {
        this.groupName = value;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String value) {
        this.title = value;
    }

    public EntitiesGroup withInnerGroups(EntitiesGroup... values) {
        if (values!= null) {
            Collections.addAll(getInnerGroups(), values);
        }
        return this;
    }

    public EntitiesGroup withInnerGroups(Collection<EntitiesGroup> values) {
        if (values!= null && !values.isEmpty()) {
            getInnerGroups().addAll(values);
        }
        return this;
    }

    public EntitiesGroup withGroupName(String value) {
        setGroupName(value);
        return this;
    }

    public EntitiesGroup withTitle(String value) {
        setTitle(value);
        return this;
    }

    @Override
    public EntitiesGroup withVersion(Long value) {
        setVersion(value);
        return this;
    }

    @Override
    public EntitiesGroup withUpdatedAt(LocalDateTime value) {
        setUpdatedAt(value);
        return this;
    }

    @Override
    public EntitiesGroup withCreateAt(LocalDateTime value) {
        setCreateAt(value);
        return this;
    }

}
