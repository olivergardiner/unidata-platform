package org.unidata.mdm.meta.type.model.measurement;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import static org.unidata.mdm.meta.type.model.ModelNamespace.META_MODEL_NAMESPACE;


public class MeasurementValues implements Serializable {

    private final static long serialVersionUID = 987654321L;

    @JacksonXmlProperty(namespace = META_MODEL_NAMESPACE)
    protected List<MeasurementValue> value;

    @JacksonXmlProperty(isAttribute = true)
    protected String id;

    public List<MeasurementValue> getValue() {
        if (value == null) {
            value = new ArrayList<>();
        }
        return this.value;
    }

    public String getId() {
        return id;
    }

    public void setId(String value) {
        this.id = value;
    }

    public MeasurementValues withValue(MeasurementValue... values) {
        if (values!= null) {
            Collections.addAll(getValue(), values);
        }
        return this;
    }

    public MeasurementValues withValue(Collection<MeasurementValue> values) {
        if (values!= null) {
            getValue().addAll(values);
        }
        return this;
    }

    public MeasurementValues withId(String value) {
        setId(value);
        return this;
    }

}
