/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
/**
 *
 */
package org.unidata.mdm.rest.search.ro;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.unidata.mdm.rest.system.ro.ErrorInfo;
import org.unidata.mdm.rest.system.ro.RestOutputSink;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;

/**
 * @author Mikhail Mikhailov
 * Search result wrapper, aiming to help CXF.
 */
public class SearchResultRO implements RestOutputSink {

    /**
     * Optional number of all potential hits.
     */
    @JsonProperty(value = "total_count")
    private long totalCount;

    /**
     * Fields, participating in a query, if any.
     * Null means '_all'. If set, the same fields will be filled in the 'this.preview' field.
     */
    @JsonProperty
    private List<String> fields;

    /**
     * Search hits.
     */
    @JsonProperty
    private List<SearchResultHitRO> hits = new ArrayList<>();
    // Move to any?
    @JsonProperty
    private List<ErrorInfo> errors = new ArrayList<>();

    /**
     * Optional total count limit (max window size).
     */
    @JsonProperty(value = "total_count_limit")
    private long totalCountLimit;

    /**
     * Success variable. Temporary. TODO remove afterwards.
     */
    @JsonProperty(required = true, defaultValue = "true")
    private boolean success = true;
    @JsonProperty(required = false, defaultValue = "true")
    private boolean hasRecords = true;
    /**
     * Max score for search
     */
    @JsonProperty(required = false)
    private Float maxScore;

    private Map<String, JsonNode> any = new HashMap<>();

    /**
     * Constructor.
     */
    public SearchResultRO() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    @JsonAnySetter
    @Override
    public void setAny(String name, JsonNode value) {
        any.put(name, value);
    }

    @JsonAnyGetter
    public Map<String, JsonNode> getAny() {
        return any;
    }

    /**
     * @return the success
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     * @param success the success to set
     */
    public void setSuccess(boolean success) {
        this.success = success;
    }

    /**
     * @return the hits
     */
    public List<SearchResultHitRO> getHits() {
        return hits;
    }

    /**
     * Returns the fields, participated in search action. Null for _all.
     * @return the fields or null
     */
    public List<String> getFields() {
        return fields;
    }

    /**
     * Sets the fields, participating in search action. Null for _all.
     * @param fields the fields to set
     */
    public void setFields(List<String> fields) {
        this.fields = fields;
    }

    /**
     * Gets the total number of potential hits.
     * @return the totalCount
     */
    public long getTotalCount() {
        return totalCount;
    }

    /**
     * Sets the total number of potential hits.
     * @param totalCount the totalCount to set
     */
    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }

	/**
	 * Gets the hsaRecords flag.
	 * @return the hasRecords
	 */
	public boolean isHasRecords() {
		return hasRecords;
	}

	/**
	 * Sets the hsaRecords flag.
	 * @param hasRecords the hasRecords to set
	 */
	public void setHasRecords(boolean hasRecords) {
		this.hasRecords = hasRecords;
	}


    public Float getMaxScore() {
        return maxScore;
    }

    public void setMaxScore(Float maxScore) {
        this.maxScore = maxScore;
    }

    public List<ErrorInfo> getErrors() {
        return errors;
    }

    public void setErrors(List<ErrorInfo> errors) {
        this.errors = errors;
    }


    public long getTotalCountLimit() {
        return totalCountLimit;
    }

    public void setTotalCountLimit(long totalCountLimit) {
        this.totalCountLimit = totalCountLimit;
    }
}
