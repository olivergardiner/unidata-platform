/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */

package org.unidata.mdm.rest.search.ro;

import java.util.Date;
import java.util.List;

import org.unidata.mdm.rest.system.ro.RestInputSource;
import org.unidata.mdm.rest.search.SearchRequestDataType;
import org.unidata.mdm.search.type.search.SearchRequestOperator;
import org.unidata.mdm.search.type.search.SearchRequestType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Mikhail Mikhailov
 * Base class for search requests.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class SearchRequestRO implements RestInputSource {

    /**
     * Entity (index / type) name.
     */
    private String entity;

    /**
     * data type of requested documents
     */
    private SearchRequestDataType dataType = SearchRequestDataType.ETALON_DATA;
    /**
     * Query type.
     */
    private SearchRequestType qtype;
    /**
     * Combination operator.
     */
    private SearchRequestOperator operator;
    /**
     * Fields to return.
     */
    private List<String> returnFields;
    /**
     * Apply the following facets.
     */
    private List<String> facets;
    /**
     * set default sort if sortFields is empty
     */
    private boolean defaultSort = true;

    /**
     * Apply the following sorting settings.
     */
    private List<SearchSortFieldRO> sortFields;
    /**
     * Search after values
     */
    private List<Object> searchAfter;

    /**
     * Return count.
     */
    private int count = 10;
    /**
     * Return page.
     */
    private int page = 0;
    /**
     * Fetch source or not.
     */
    private boolean source;
    /**
     * Return total count.
     */
    private boolean totalCount = true;
    /**
     * Count only or return results too.
     */
    private boolean countOnly = false;
    /**
     * Fetch all without applying a query.
     */
    private boolean fetchAll = false;
    /**
     * Shortcut for all searchable fields.
     */
    private boolean returnAllFields = false;
    /**
     * Date hint.
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS", timezone = "Europe/Moscow")//GMT
    private Date asOf;
    /**
     * Constructor.
     */
    public SearchRequestRO() {
        super();
    }

    /**
     * @return the entity
     */
    public String getEntity() {
        return entity;
    }

    /**
     * @param entity the entity to set
     */
    public void setEntity(String entity) {
        this.entity = entity;
    }

    /**
     * @return - data type
     */
    public SearchRequestDataType getDataType() {
        return dataType;
    }

    /**
     * @param dataType - search data type
     */
    public void setDataType(SearchRequestDataType dataType) {
        this.dataType = dataType;
    }

    /**
     * @return the qtype
     */
    public SearchRequestType getQtype() {
        return qtype;
    }

    /**
     * @param qtype the qtype to set
     */
    public void setQtype(String qtype) {
        this.qtype = SearchRequestType.safeValueOf(qtype);
    }

    /**
     * @return the operator
     */
    public SearchRequestOperator getOperator() {
        return operator;
    }

    /**
     * @param operator the operator to set
     */
    public void setOperator(String operator) {
        this.operator = SearchRequestOperator.safeValueOf(operator);
    }


    /**
     * @return the returnFields
     */
    public List<String> getReturnFields() {
        return returnFields;
    }


    /**
     * @param returnFields the returnFields to set
     */
    public void setReturnFields(List<String> returnFields) {
        this.returnFields = returnFields;
    }

    /**
     * @return the facets
     */
    public List<String> getFacets() {
        return facets;
    }

    /**
     * @param facets the facets to set
     */
    public void setFacets(List<String> facets) {
        this.facets = facets;
    }

    /**
     * @return the count
     */
    public int getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(int count) {
        this.count = count;
    }

    /**
     * @return the page
     */
    public int getPage() {
        return page;
    }

    /**
     * @param page the page to set
     */
    public void setPage(int page) {
        this.page = page;
    }

    /**
     * @return the source
     */
    public boolean isSource() {
        return source;
    }

    /**
     * @param source the source to set
     */
    public void setSource(boolean source) {
        this.source = source;
    }

    /**
     * @return the totalCount
     */
    public boolean isTotalCount() {
        return totalCount;
    }

    /**
     * @param totalCount the totalCount to set
     */
    public void setTotalCount(boolean totalCount) {
        this.totalCount = totalCount;
    }

    /**
     * @return the countOnly
     */
    public boolean isCountOnly() {
        return countOnly;
    }

    /**
     * @param countOnly the countOnly to set
     */
    public void setCountOnly(boolean countOnly) {
        this.countOnly = countOnly;
    }

    /**
     * @return the fetchAll
     */
    public boolean isFetchAll() {
        return fetchAll;
    }

    /**
     * @param fetchAll the fetchAll to set
     */
    public void setFetchAll(boolean fetchAll) {
        this.fetchAll = fetchAll;
    }

    /**
     * @return the returnAllFields
     */
    public boolean isReturnAllFields() {
        return returnAllFields;
    }

    /**
     * @param returnAllFields the returnAllFields to set
     */
    public void setReturnAllFields(boolean returnAllFields) {
        this.returnAllFields = returnAllFields;
    }

    /**
     * @return sortFields
     */
    public List<SearchSortFieldRO> getSortFields() {
        return sortFields;
    }

    /**
     *
     * @param sortFields set of sort fields.
     */
    public void setSortFields(List<SearchSortFieldRO> sortFields) {
        this.sortFields = sortFields;
    }


    /**
     * @return the asOf
     */
    public Date getAsOf() {
        return asOf;
    }


    /**
     * @param asOf the asOf to set
     */
    public void setAsOf(Date asOf) {
        this.asOf = asOf;
    }


    public List<Object> getSearchAfter() {
        return searchAfter;
    }

    public void setSearchAfter(List<Object> searchAfter) {
        this.searchAfter = searchAfter;
    }

    public boolean isDefaultSort() {
        return defaultSort;
    }

    public void setDefaultSort(boolean defaultSort) {
        this.defaultSort = defaultSort;
    }
}
