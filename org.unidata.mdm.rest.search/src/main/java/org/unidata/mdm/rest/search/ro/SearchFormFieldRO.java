/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.search.ro;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.apache.commons.lang3.tuple.Pair;
import org.unidata.mdm.rest.system.ro.SimpleDataType;
import org.unidata.mdm.rest.search.ro.serializer.SearchFormFieldDeserializer;

/**
 * @author Mikhail Mikhailov
 * REST search form field.
 */
@JsonDeserialize(using = SearchFormFieldDeserializer.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchFormFieldRO {

    /**
     * Search type.
     */
    private final SimpleDataType type;
    /**
     * Path.
     */
    private final String path;
    /**
     * Single value.
     */
    private final Object single;
    /**
     * Range.
     */
    private final Pair<Object, Object> range;
    /**
     * Inverted flag
     */
    private final boolean inverted;

    private SearchTypeRO searchTypeRO;


    /**
     * Constructor.
     */
    public SearchFormFieldRO(SimpleDataType type, String path, Object single, boolean inverted, SearchTypeRO searchType) {
        super();
        this.type = type;
        this.path = path;
        this.single = single;
        this.range = null;
        this.inverted = inverted;
        this.searchTypeRO = searchType;
    }

    /**
     * Constructor for ranges.
     */
    public SearchFormFieldRO(SimpleDataType type, String path, Pair<Object, Object> range, boolean inverted, SearchTypeRO searchType) {
        super();
        this.type = type;
        this.path = path;
        this.single = null;
        this.range = range;
        this.inverted = inverted;
        this.searchTypeRO = searchType;
    }

    /**
     * @return the type
     */
    public SimpleDataType getType() {
        return type;
    }


    /**
     * @return the path
     */
    public String getPath() {
        return path;
    }


    /**
     * @return the single
     */
    public Object getSingle() {
        return single;
    }


    /**
     * @return the range
     */
    public Pair<Object, Object> getRange() {
        return range;
    }

    /**
     *
     * @return is it inverted form
     */
    public boolean isInverted() {
        return inverted;
    }

    /**
     *
     * @return is it like form
     */
    public boolean isLike() {
        return SearchTypeRO.LIKE.equals(searchTypeRO);
    }

    /**
     *
     * @return is it start with form
     */
    public boolean isStartWith() {
        return SearchTypeRO.START_WITH.equals(searchTypeRO);
    }
    /**
     *
     * @return is it fuzzy form
     */
    public boolean isFuzzy() {
        return SearchTypeRO.FUZZY.equals(searchTypeRO);
    }

    /**
     * @return the morphological
     */
    public boolean isMorphological() {
        return SearchTypeRO.MORPHOLOGICAL.equals(searchTypeRO);
    }

    public SearchTypeRO getSearchTypeRO() {
        return searchTypeRO;
    }

    public void setSearchTypeRO(SearchTypeRO searchTypeRO) {
        this.searchTypeRO = searchTypeRO;
    }

    public enum SearchTypeRO{
        DEFAULT,
        EXACT,
        FUZZY,
        MORPHOLOGICAL,
        EXIST,
        START_WITH,
        LIKE,
        RANGE
    }
}
