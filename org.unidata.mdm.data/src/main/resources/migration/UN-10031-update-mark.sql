create type update_mark as (update_date timestamptz, updated_by varchar(256));
