create table if not exists favorite_etalons (
    entity_name character varying(255),
    etalon_id   uuid,
    created_at timestamp with time zone DEFAULT now(),
    created_by character varying(255),
    CONSTRAINT pk_favorite_etalons PRIMARY KEY (created_by, etalon_id)
);