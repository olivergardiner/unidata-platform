<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE properties SYSTEM "http://java.sun.com/dtd/properties.dtd">
<properties>
    <entry key="changeEtalonApprovalSQL">
        <![CDATA[
            update record_etalons_p${#} set
                update_date = current_timestamp,
                updated_by = ?,
                approval = ?::approval_state
            where
                id = ?::uuid and approval <> ?::approval_state
        ]]>
    </entry>
    <entry key="changeEtalonsStatusBatchSQL">
        <![CDATA[
            with u (id, update_date, updated_by, status, operation_id) as (
                select * from unnest(?, ?, ?, ?, ?)
            )
            update record_etalons_p${#}
            set 
                update_date    = u.update_date,
                updated_by     = u.updated_by,
                status         = u.status::record_status,
                operation_id   = u.operation_id
            from
                u
            where 
                record_etalons_p${#}.id = u.id
        ]]>
    </entry>
    <entry key="changeOriginsStatusBatchSQL">
        <![CDATA[
            with u (id, update_date, updated_by, status) as (
                select * from unnest(?, ?, ?, ?)
            )
            update record_origins_p${#}
            set 
                update_date = u.update_date,
                updated_by  = u.updated_by,
                status      = u.status
            from
                u
            where 
                record_origins_p${#}.shard = ?
            and record_origins_p${#}.id = u.id
        ]]>
    </entry>
    <entry key="remapOriginRecordsSQL">
        <![CDATA[
            with u (id, new_etalon_id, new_shard, update_date, updated_by) as (
                select * from unnest(?, ?, ?, ?, ?)
            )
            update record_origins
            set 
                update_date = u.update_date,
                updated_by  = u.updated_by,
                etalon_id   = u.new_etalon_id,
                shard       = u.new_shard
            from
                u
            where    
                record_origins.shard = ?
            and record_origins.id = u.id
        ]]>
    </entry>
    <entry key="remapVistoryRecordsSQL">
        <![CDATA[
            with u (origin_id, new_shard) as (
                select * from unnest(?, ?)
            )
            update record_vistory
            set 
                shard = u.new_shard
            from
                u
            where
                record_vistory.shard = ?
            and record_vistory.origin_id = u.origin_id
        ]]>
    </entry>
    <entry key="insertEtalonSQL">
        <![CDATA[
            insert into record_etalons_p${#} (name, shard, create_date, created_by, id, approval, status, operation_id)
            values (?, ?, ?, ?, ?::uuid, ?::approval_state, ?::record_status, ?)
        ]]>
    </entry>
    <entry key="updateEtalonSQL">
        <![CDATA[
            update record_etalons_p${#}
            set update_date = ?, updated_by = ?, status = ?::record_status, approval = ?::approval_state, operation_id = ?
            where id = ?::uuid
        ]]>
    </entry>
    <entry key="insertOriginSQL">
        <![CDATA[
            insert into record_origins_p${#} (id, shard, etalon_id, initial_owner, external_id, source_system, name, create_date, created_by, status, enrichment)
            values (?::uuid, ?, ?::uuid, ?::uuid, ?, ?, ?, ?, ?, ?::record_status, ?)
        ]]>
    </entry>
    <entry key="insertExternalKeySQL">
        <![CDATA[
            insert into record_external_keys_p${#} (ext_shard, ext_key, etalon_id)
            values (?, ?, ?)
        ]]>
    </entry>
    <entry key="updateExternalKeySQL">
        <![CDATA[
            update record_external_keys_p${#} set etalon_id = ? where ext_shard = ? and ext_key = ?
        ]]>
    </entry>
    <entry key="updateExternalKeysSQL">
        <![CDATA[
            with u (ext_key, etalon_id) as (
                select * from unnest(?, ?)
            )
            update 
                record_external_keys_p${#} 
            set 
                etalon_id = u.etalon_id
            from
                u 
            where 
                record_external_keys_p${#}.ext_shard = ? 
            and record_external_keys_p${#}.ext_key = u.ext_key
        ]]>
    </entry>
    <entry key="updateOriginSQL">
        <![CDATA[
            update record_origins_p${#} 
            set update_date = ?, updated_by = ?, status = ?::record_status, etalon_id = ?::uuid 
            where shard = ? and id = ?::uuid 
        ]]>
    </entry>
    <entry key="insertEtalonStateDraftSQL">
        <![CDATA[
            insert into etalons_draft_states(etalon_id, revision, status, created_by)
            select ?::uuid, coalesce((select max(prev.revision) + 1 from etalons_draft_states prev where prev.etalon_id = ?::uuid), 1), ?::record_status, ?
        ]]>
    </entry>
    <entry key="cleanupEtalonStateDraftsSQL">
        <![CDATA[
            delete from etalons_draft_states where etalon_id = ?::uuid
        ]]>
    </entry>
    <entry key="loadLastEtalonStateDraftByEtalonIdSQL">
        <![CDATA[
            select id, etalon_id::text, revision, status::text, create_date, created_by
            from etalons_draft_states where etalon_id = ?::uuid
            order by revision desc
            fetch first 1 rows only
        ]]>
    </entry>
    <entry key="loadDataIdByExternalIdSQL">
        <![CDATA[
            select etalon_id from record_external_keys_p${#} where ext_shard = ? and ext_key = ? 
        ]]>
    </entry>
    <entry key="loadDataIdByLSNSQL">
        <![CDATA[
            select id from record_etalons_p${#} where shard = ? and lsn = ?
        ]]>
    </entry>
    <entry key="loadKeysByEtalonIdSQL">
        <![CDATA[
            select (
                e.shard,
                e.lsn,
                e.id,
                e.name,
                e.status,
                e.approval,
                coalesce((
                    select true
                    from
                        org_unidata_mdm_data.record_vistory_p${#} v, org_unidata_mdm_data.record_origins_p${#} o
                    where
                        o.etalon_id = ?
                    and v.origin_id = o.id
                    and v.approval = 'APPROVED'::org_unidata_mdm_data.approval_state
                    fetch first 1 rows only), false),
                e.create_date,
                e.created_by,
                mark.update_date,
                mark.updated_by,
                (  select array_agg((
                       o.id,
                       o.initial_owner,
                       o.status,
                       o.enrichment,
                       (select max(v.revision) from org_unidata_mdm_data.record_vistory_p${#} v where v.origin_id = o.id),
                       o.source_system,
                       o.create_date,
                       o.created_by,
                       o.update_date,
                       o.updated_by,
                       o.external_id)::org_unidata_mdm_data.record_origin_key)
                   from
                       org_unidata_mdm_data.record_origins_p${#} o
                   where
                       o.etalon_id = ?
            )::org_unidata_mdm_data.record_origin_key[])::org_unidata_mdm_data.record_key
            from
                org_unidata_mdm_data.record_etalons_p${#} e, 
                lateral (
                    select ts as update_date, usr as updated_by from
                        (
                              select update_date as ts, updated_by as usr from org_unidata_mdm_data.record_etalons_p${#} where id = ?
                        union all select update_date as ts, updated_by as usr from org_unidata_mdm_data.record_origins_p${#} where etalon_id = ?
                        union all select create_date as ts, created_by as usr from (
                              select v.create_date, v.created_by
                              from org_unidata_mdm_data.record_origins_p${#} o, org_unidata_mdm_data.record_vistory_p${#} v
                              where
                                  o.etalon_id = ?
                              and v.origin_id = o.id
                              and (v.approval <> 'DECLINED' and v.approval <> 'PENDING')
                              order by v.create_date desc fetch first 1 rows only ) f
                        ) t order by ts desc nulls last fetch first 1 rows only
                ) mark
            where
                e.id = ?
        ]]>
    </entry>
    <entry key="loadKeysByEtalonIdsSQL">
        <![CDATA[
            with ids (id) as (select unnest(?))
            select array_agg((
                select (
                    e.shard,
                    e.lsn,
                    e.id,
                    e.name,
                    e.status,
                    e.approval,
                    coalesce((
                        select true
                        from
                            org_unidata_mdm_data.record_vistory_p${#} v, org_unidata_mdm_data.record_origins_p${#} o
                        where
                            o.etalon_id = i.id
                        and v.origin_id = o.id
                        and v.approval = 'APPROVED'::org_unidata_mdm_data.approval_state
                        fetch first 1 rows only), false),
                    e.create_date,
                    e.created_by,
                    mark.update_date,
                    mark.updated_by,
                    (  select array_agg((
                           o.id,
                           o.initial_owner,
                           o.status,
                           o.enrichment,
                           (select max(v.revision) from org_unidata_mdm_data.record_vistory_p${#} v where v.origin_id = o.id),
                           o.source_system,
                           o.create_date,
                           o.created_by,
                           o.update_date,
                           o.updated_by,
                           o.external_id)::org_unidata_mdm_data.record_origin_key)
                       from
                           org_unidata_mdm_data.record_origins_p${#} o
                       where
                           o.etalon_id = i.id
                )::org_unidata_mdm_data.record_origin_key[])::org_unidata_mdm_data.record_key
                from
                    org_unidata_mdm_data.record_etalons_p${#} e, 
                    lateral (
                        select ts as update_date, usr as updated_by from
                            (
                                  select update_date as ts, updated_by as usr from org_unidata_mdm_data.record_etalons_p${#} where id = i.id
                            union all select update_date as ts, updated_by as usr from org_unidata_mdm_data.record_origins_p${#} where etalon_id = i.id
                            union all select create_date as ts, created_by as usr from (
                                  select v.create_date, v.created_by
                                  from org_unidata_mdm_data.record_origins_p${#} o, org_unidata_mdm_data.record_vistory_p${#} v
                                  where
                                      o.etalon_id = i.id
                                  and v.origin_id = o.id
                                  and (v.approval <> 'DECLINED' and v.approval <> 'PENDING')
                                  order by v.create_date desc fetch first 1 rows only ) f
                            ) t order by ts desc nulls last fetch first 1 rows only
                    ) mark
                where
                    e.id = i.id)
            )::org_unidata_mdm_data.record_key[] from ids i
        ]]>
    </entry>
    <entry key="loadKeysByLSNsSQL">
        <![CDATA[
            with lsns (lsn) as (select unnest(?))
            select array_agg((
                select (
                    e.shard,
                    e.lsn,
                    e.id,
                    e.name,
                    e.status,
                    e.approval,
                    coalesce((
                        select true
                        from
                            org_unidata_mdm_data.record_vistory_p${#} v, org_unidata_mdm_data.record_origins_p${#} o
                        where
                            o.etalon_id = i.id
                        and v.origin_id = o.id
                        and v.approval = 'APPROVED'::org_unidata_mdm_data.approval_state
                        fetch first 1 rows only), false),
                    e.create_date,
                    e.created_by,
                    mark.update_date,
                    mark.updated_by,
                    (  select array_agg((
                           o.id,
                           o.initial_owner,
                           o.status,
                           o.enrichment,
                           (select max(v.revision) from org_unidata_mdm_data.record_vistory_p${#} v where v.origin_id = o.id),
                           o.source_system,
                           o.create_date,
                           o.created_by,
                           o.update_date,
                           o.updated_by,
                           o.external_id)::org_unidata_mdm_data.record_origin_key)
                       from
                           org_unidata_mdm_data.record_origins_p${#} o
                       where
                           o.etalon_id = i.id
                )::org_unidata_mdm_data.record_origin_key[])::org_unidata_mdm_data.record_key
                from
                    org_unidata_mdm_data.record_etalons_p${#} e, 
                    lateral (
                        select ts as update_date, usr as updated_by from
                            (
                                  select update_date as ts, updated_by as usr from org_unidata_mdm_data.record_etalons_p${#} where id = i.id
                            union all select update_date as ts, updated_by as usr from org_unidata_mdm_data.record_origins_p${#} where etalon_id = i.id
                            union all select create_date as ts, created_by as usr from (
                                  select v.create_date, v.created_by
                                  from org_unidata_mdm_data.record_origins_p${#} o, org_unidata_mdm_data.record_vistory_p${#} v
                                  where
                                      o.etalon_id = i.id
                                  and v.origin_id = o.id
                                  and (v.approval <> 'DECLINED' and v.approval <> 'PENDING')
                                  order by v.create_date desc fetch first 1 rows only ) f
                            ) t order by ts desc nulls last fetch first 1 rows only
                    ) mark
                where
                    e.id = i.id)
            )::org_unidata_mdm_data.record_key[] from lsns l, org_unidata_mdm_data.record_etalons_p${#} i
            where 
                i.lsn = l.lsn
            and i.shard = ?
        ]]>
    </entry>
    <entry key="loadDataIdsByExternalIdsSQL">
        <![CDATA[
            with ids (ext_id) as (?)
            select k.etalon_id from record_external_keys_p${#} k, ids i where k.ext_shard = ? and k.ext_key = i.ext_key 
        ]]>
    </entry>
    <!-- Wipe support. -->
    <entry key="deleteCdataByRecordIdSQL">
        <![CDATA[
            delete from character_data where record_id in (select unnest(?))
        ]]>
    </entry>
    <entry key="deleteBdataByRecordIdSQL">
        <![CDATA[
            delete from binary_data where record_id in (select unnest(?))
        ]]>
    </entry>
    <entry key="deleteVistoryByOriginIdSQL">
        <![CDATA[
            delete from record_vistory_p${#} where origin_id in (select unnest(?))
        ]]>
    </entry>
    <entry key="deleteOriginByIdSQL">
        <![CDATA[
            -- shard is needed to force the index into plan
            delete from record_origins_p${#} where shard = ? and id in (select unnest(?))
        ]]>
    </entry>
    <entry key="deleteEtalonByIdSQL">
        <![CDATA[
            delete from record_etalons_p${#} where id in (select unnest(?)) 
        ]]>
    </entry>
    <entry key="deleteExternalKeyByCompactedIdSQL">
        <![CDATA[
            delete from record_external_keys_p${#} where ext_shard = ? and ext_key in (select unnest(?))
        ]]>
    </entry>
    <entry key="cutVistoryByOriginIdSQL">
        <![CDATA[
            delete from record_vistory_p${#} where origin_id in (select unnest(?))
            returning id, origin_id, shard, revision, valid_from, valid_to, data_a, data_b, create_date, created_by, status, approval, shift, operation_type, operation_id, major, minor
        ]]>
    </entry>
    <!-- End Wipe support. -->
    <entry key="loadAllOriginsForEtlaonsQuery">
        <![CDATA[
            select * from record_origins_p${#} where etalon_id in (?)
        ]]>
    </entry>
    <!-- Vistory -->
        <entry key="putVersionJaxbSQL">
        <![CDATA[
            insert into record_vistory_p${#} (
                id,
                origin_id,
                shard,
                revision,
                valid_from,
                valid_to,
                data_a,
                create_date,
                created_by,
                status,
                approval,
                shift,
                operation_type,
                operation_id,
                major,
                minor
            )
            select
                ?::uuid,
                ?::uuid,
                ?,
                coalesce((select (max(prev.revision) + 1) from record_vistory prev where prev.origin_id = ?::uuid and prev.shard = ?), 1),
                ?,
                ?,
                ?,
                coalesce(?, now()),
                ?,
                ?::record_status,
                ?::approval_state,
                ?::data_shift,
                ?::operation_type,
                ?,
                ?,
                ?
        ]]>
    </entry>
    <entry key="putVersionProtostuffSQL">
        <![CDATA[
            insert into record_vistory_p${#} (
                id,
                origin_id,
                shard,
                revision,
                valid_from,
                valid_to,
                data_b,
                create_date,
                created_by,
                status,
                approval,
                shift,
                operation_type,
                operation_id,
                major,
                minor
            )
            select
                ?::uuid,
                ?::uuid,
                ?,
                coalesce((select (max(prev.revision) + 1) from record_vistory prev where prev.origin_id = ?::uuid and prev.shard = ?), 1),
                ?,
                ?,
                ?,
                coalesce(?, now()),
                ?,
                ?::record_status,
                ?::approval_state,
                ?::data_shift,
                ?::operation_type,
                ?,
                ?,
                ?
        ]]>
    </entry>
    <!-- Etalon Id -->
    <entry key="loadVersionsByEtalonIdSQL">
        <![CDATA[
            with
            k (keys) as (
                select case when ? then (
                    select (
                        e.shard,
                        e.lsn,
                        e.id,
                        e.name,
                        e.status,
                        e.approval,
                        coalesce((
                            select true
                            from
                                org_unidata_mdm_data.record_vistory_p${#} v, org_unidata_mdm_data.record_origins_p${#} o
                            where
                                o.etalon_id = ?::uuid
                            and v.origin_id = o.id
                            and v.approval = 'APPROVED'::org_unidata_mdm_data.approval_state
                            fetch first 1 rows only), false),
                        e.create_date,
                        e.created_by,
                        mark.update_date,
                        mark.updated_by,
                        (  select array_agg((
                               o.id,
                               o.initial_owner,
                               o.status,
                               o.enrichment,
                               (select max(v.revision) from org_unidata_mdm_data.record_vistory_p${#} v where v.origin_id = o.id),
                               o.source_system,
                               o.create_date,
                               o.created_by,
                               o.update_date,
                               o.updated_by,
                               o.external_id)::org_unidata_mdm_data.record_origin_key)
                           from
                               org_unidata_mdm_data.record_origins_p${#} o
                           where
                               o.etalon_id = ?::uuid
                    )::org_unidata_mdm_data.record_origin_key[])::org_unidata_mdm_data.record_key
                    from
                        org_unidata_mdm_data.record_etalons_p${#} e, 
                        lateral (
                            select ts as update_date, usr as updated_by from
                                (
                                      select update_date as ts, updated_by as usr from org_unidata_mdm_data.record_etalons_p${#} where id = ?::uuid
                                union all select update_date as ts, updated_by as usr from org_unidata_mdm_data.record_origins_p${#} where etalon_id = ?::uuid
                                union all select create_date as ts, created_by as usr from (
                                      select v.create_date, v.created_by
                                      from org_unidata_mdm_data.record_origins_p${#} o, org_unidata_mdm_data.record_vistory_p${#} v
                                      where
                                          o.etalon_id = ?::uuid
                                      and v.origin_id = o.id
                                      and (v.approval <> 'DECLINED' and v.approval <> 'PENDING')
                                      order by v.create_date desc fetch first 1 rows only ) f
                                ) t order by ts desc nulls last fetch first 1 rows only
                        ) mark
                    where
                        e.id = ?::uuid
                ) else null end
            ),
            l (lud) as (
                select case when ?::varchar is not null then (
                    select max(v.create_date) as lud 
                    from 
                        org_unidata_mdm_data.record_vistory_p${#} v, org_unidata_mdm_data.record_origins_p${#} o
                    where 
                        o.etalon_id = ?::uuid
                    and v.origin_id = o.id
                    and v.operation_id = ?::varchar
                ) else null end
            ),
            r (records) as (
                select
                    array_agg((
                        v.id,
                        v.origin_id,
                        v.shard,
                        v.revision,
                        v.valid_from,
                        v.valid_to,
                        v.create_date,
                        v.created_by,
                        v.status,
                        v.approval,
                        v.shift,
                        v.operation_type,
                        v.operation_id,
                        v.data_a,
                        v.data_b,
                        v.major,
                        v.minor)::org_unidata_mdm_data.record_vistory_data)::org_unidata_mdm_data.record_vistory_data[]
                from
                    org_unidata_mdm_data.record_vistory_p${#} v, (
                        select
                            t.origin_id,
                            max(t.revision) as revision
                        from
                            org_unidata_mdm_data.record_origins_p${#} o, org_unidata_mdm_data.record_vistory_p${#} t, l
                        where
                            o.etalon_id = ?::uuid
                        and t.origin_id = o.id
                        and t.create_date <= (select case when ?::varchar is null then coalesce(?::timestamptz, 'infinity') else l.lud end)
                        and (coalesce(?, now()) between coalesce(t.valid_from, '-infinity') and coalesce(t.valid_to, 'infinity'))
                        and (t.approval <> 'DECLINED'::approval_state and (t.approval <> 'PENDING'::approval_state or (? or t.created_by = ?)))
                        group by t.origin_id
                    ) as s
                where
                    v.origin_id = s.origin_id
                and v.revision = s.revision
            )
            select (keys, records)::org_unidata_mdm_data.record_timeline from k, r
            where ?::timestamptz is null or exists(select true from unnest((r).records) test where ?::timestamptz <= (test).create_date limit 1)
        ]]>
    </entry>
    <entry key="loadUnfilteredVersionsByEtalonIdSQL">
        <![CDATA[
            select
                v.id::text,
                v.origin_id::text,
                v.shard,
                v.revision,
                v.valid_from,
                v.valid_to,
                v.data_a,
                v.data_b,
                v.create_date,
                v.created_by,
                v.status,
                v.approval,
                v.shift,
                v.operation_type,
                v.operation_id,
                v.major,
                v.minor
            from
                record_vistory_p${#} v, (
                    select
                        t.origin_id,
                        max(t.revision) as revision
                    from
                        record_origins_p${#} o, record_vistory_p${#} t
                    where
                        o.etalon_id = ?
                    and t.origin_id = o.id
                    and (? between coalesce(t.valid_from, '-infinity') and coalesce(t.valid_to, 'infinity'))
                    and t.approval <> 'DECLINED'::approval_state
                    group by t.origin_id
                ) as s
            where
                v.origin_id = s.origin_id
            and v.revision = s.revision
        ]]>
    </entry>
    <entry key="loadHistoryVersionsByEtalonIdSQL">
        <![CDATA[
            select
                v.id::text,
                v.origin_id::text,
                v.operation_id,
                v.revision,
                v.valid_from,
                v.valid_to,
                v.data_a,
                v.data_b,
                v.create_date as update_date,
                v.created_by as updated_by,
                v.status,
                v.approval,
                v.shift,
                v.major,
                v.minor,
                v.operation_type,
                o.create_date as create_date,
                o.created_by as created_by,
                o.name,
                o.source_system,
                o.external_id,
                o.enrichment,
                o.status as origin_status
            from
                record_vistory_p${#} v, record_origins_p${#} o
            where
                o.etalon_id = ?
            and v.origin_id = o.id
            and (v.approval <> 'DECLINED'::approval_state and (v.approval <> 'PENDING'::approval_state))
            order by o.source_system, o.external_id, v.revision
        ]]>
    </entry> 
    <entry key="loadTimelineByEtalonIdSQL">
        <![CDATA[
            with
            k (keys) as (
                select case when ? then (
                    select (
                        e.shard,
                        e.lsn,
                        e.id,
                        e.name,
                        e.status,
                        e.approval,
                        coalesce((
                            select true
                            from
                                org_unidata_mdm_data.record_vistory_p${#} v, org_unidata_mdm_data.record_origins_p${#} o
                            where
                                o.etalon_id = ?
                            and v.origin_id = o.id
                            fetch first 1 rows only), false),
                        e.create_date,
                        e.created_by,
                        mark.update_date,
                        mark.updated_by,
                        (  select array_agg((
                               o.id,
                               o.initial_owner,
                               o.status,
                               o.enrichment,
                               (select max(v.revision) from org_unidata_mdm_data.record_vistory_p${#} v where v.origin_id = o.id),
                               o.source_system,
                               o.create_date,
                               o.created_by,
                               o.update_date,
                               o.updated_by,
                               o.external_id)::org_unidata_mdm_data.record_origin_key)
                           from
                               org_unidata_mdm_data.record_origins_p${#} o
                           where
                               o.etalon_id = ?
                    )::org_unidata_mdm_data.record_origin_key[])::org_unidata_mdm_data.record_key
                    from
                        org_unidata_mdm_data.record_etalons_p${#} e, 
                        lateral (
                            select ts as update_date, usr as updated_by from
                                (
                                      select update_date as ts, updated_by as usr from org_unidata_mdm_data.record_etalons_p${#} where id = ?
                                union all select update_date as ts, updated_by as usr from org_unidata_mdm_data.record_origins_p${#} where etalon_id = ?
                                union all select create_date as ts, created_by as usr from (
                                      select v.create_date, v.created_by
                                      from org_unidata_mdm_data.record_origins_p${#} o, org_unidata_mdm_data.record_vistory_p${#} v
                                      where
                                          o.etalon_id = ?
                                      and v.origin_id = o.id
                                      order by v.create_date desc fetch first 1 rows only ) f
                                ) t order by ts desc nulls last fetch first 1 rows only
                        ) mark
                    where
                        e.id = ?
                ) else null end
            ),
            l (opid_lud) as (
                select case when ?::varchar is not null then (
                    select max(v.create_date) as opid_lud 
                    from 
                        org_unidata_mdm_data.record_vistory_p${#} v, org_unidata_mdm_data.record_origins_p${#} o
                    where 
                        o.etalon_id = ?
                    and v.origin_id = o.id
                    and v.operation_id = ?::varchar
                ) else null end
            ),
            r (records) as (

                with recursive t (id, origin_id, valid_from, valid_to, revision, status, approval, last_update) as (

                    select v.id, v.origin_id, v.valid_from, v.valid_to, v.revision, v.status, v.approval, v.create_date
                    from org_unidata_mdm_data.record_vistory_p${#} v,
                        ( select  i.origin_id, max(i.revision) as revision
                          from org_unidata_mdm_data.record_origins_p${#} o, org_unidata_mdm_data.record_vistory_p${#} i
                          where
                              o.etalon_id = ?
                          and i.origin_id = o.id
                          and i.status <> 'MERGED'
                          and i.create_date <= (select case when ?::varchar is null then coalesce(?::timestamptz, 'infinity') else (select l.opid_lud from l) end)
                          group by i.origin_id ) as s
                    where v.origin_id = s.origin_id
                    and v.revision = s.revision
                    union all
                    select v.id, v.origin_id, v.valid_from, v.valid_to, v.revision, v.status, v.approval, v.create_date
                    from org_unidata_mdm_data.record_vistory_p${#} v, t tt
                    where
                        v.origin_id = tt.origin_id
                    and v.revision =
                        ( select max(i.revision) as revision from org_unidata_mdm_data.record_vistory_p${#} i
                          where
                              i.origin_id = tt.origin_id
                          and (coalesce(i.valid_from, '-infinity') < coalesce(tt.valid_from, '-infinity')
                              or coalesce(i.valid_to, 'infinity') > coalesce(tt.valid_to, 'infinity'))
                          and i.revision < tt.revision
                          and i.status <> 'MERGED'
                          and i.create_date <= (select case when ?::varchar is null then coalesce(?::timestamptz, 'infinity') else (select l.opid_lud from l) end))
                )
                select array_agg((
                    v.id,
                    v.origin_id,
                    v.shard,
                    v.revision,
                    v.valid_from,
                    v.valid_to,
                    v.create_date,
                    v.created_by,
                    v.status,
                    v.approval,
                    v.shift,
                    v.operation_type,
                    v.operation_id,
                    case when false then v.data_a else null end,
                    case when ? then v.data_b else null end,
                    v.major,
                    v.minor )::org_unidata_mdm_data.record_vistory_data )::org_unidata_mdm_data.record_vistory_data[]
                from t, org_unidata_mdm_data.record_vistory_p${#} v
                where
                    v.id = t.id
                and v.shard = ?
                and not exists (  
                    select true from t tt
                    where t.origin_id = tt.origin_id
                    and t.revision < tt.revision
                    and (
                        coalesce(t.valid_from, '-infinity') between coalesce(tt.valid_from, '-infinity') and coalesce(tt.valid_to, 'infinity')
                        and coalesce(t.valid_to, 'infinity') between coalesce(tt.valid_from, '-infinity') and coalesce(tt.valid_to, 'infinity')
                    )
                )
            )
            select (keys, records)::org_unidata_mdm_data.record_timeline from k, r
            where ?::timestamptz is null or exists(select true from unnest((r).records) test where ?::timestamptz <= (test).create_date limit 1)
        ]]>
    </entry>
    <entry key="updatePendingVersionsSQL">
        <![CDATA[
            update record_vistory_p${#}
            set approval = ?::approval_state
            from
                record_vistory_p${#} v,
                record_origins_p${#} o
            where
                o.etalon_id = ?
            and o.id = v.origin_id
            and v.approval = 'PENDING'::approval_state
            and record_vistory_p${#}.id = v.id
        ]]>
    </entry>
    <entry key="updateVersionsStatusSQL">
        <![CDATA[
            update record_vistory_p${#}
            set status = :status::record_status
            where id in (:ids)
        ]]>
    </entry>
</properties>
