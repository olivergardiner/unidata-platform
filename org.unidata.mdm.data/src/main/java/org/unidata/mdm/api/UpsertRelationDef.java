package org.unidata.mdm.api;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import static org.unidata.mdm.DataNamespace.DATA_API_NAMESPACE;


/**
 * 
 * Набор связей одного определенного типа.
 *             
 * 
 * <p>Java class for UpsertRelationDef complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UpsertRelationDef"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="record" type="{http://api.mdm.unidata.org/}UpsertRelationRecordDef" maxOccurs="unbounded"/&gt;
 *         &lt;element name="override" type="{http://api.mdm.unidata.org/}UpsertRelationOverrideDef" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
public class UpsertRelationDef implements Serializable {

    private final static long serialVersionUID = 12345L;

    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected List<UpsertRelationRecordDef> records;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected UpsertRelationOverrideDef override;
    @JacksonXmlProperty(isAttribute = true)
    protected String name;

    /**
     * Gets the value of the records property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the records property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRecords().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UpsertRelationRecordDef }
     * 
     * 
     */
    public List<UpsertRelationRecordDef> getRecords() {
        if (records == null) {
            records = new ArrayList<UpsertRelationRecordDef>();
        }
        return this.records;
    }

    /**
     * Gets the value of the override property.
     * 
     * @return
     *     possible object is
     *     {@link UpsertRelationOverrideDef }
     *     
     */
    public UpsertRelationOverrideDef getOverride() {
        return override;
    }

    /**
     * Sets the value of the override property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpsertRelationOverrideDef }
     *     
     */
    public void setOverride(UpsertRelationOverrideDef value) {
        this.override = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    public UpsertRelationDef withRecords(UpsertRelationRecordDef... values) {
        if (values!= null) {
            for (UpsertRelationRecordDef value: values) {
                getRecords().add(value);
            }
        }
        return this;
    }

    public UpsertRelationDef withRecords(Collection<UpsertRelationRecordDef> values) {
        if (values!= null) {
            getRecords().addAll(values);
        }
        return this;
    }

    public UpsertRelationDef withOverride(UpsertRelationOverrideDef value) {
        setOverride(value);
        return this;
    }

    public UpsertRelationDef withName(String value) {
        setName(value);
        return this;
    }

}
