package org.unidata.mdm.api;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.unidata.mdm.data.EtalonRecord;
import org.unidata.mdm.data.OriginKey;
import org.unidata.mdm.data.OriginRecord;

import static org.unidata.mdm.DataNamespace.DATA_API_NAMESPACE;


/**
 * 
 * Структура, описывающая детали события по изменению записи сущности. Всегда содержит исходную запись, которую затронуло событие, а также пересчитанную основную запись
 *             
 * 
 * <p>Java class for UpsertEventDetailsDef complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UpsertEventDetailsDef"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="originRecord" type="{http://data.mdm.unidata.org/}OriginRecord"/&gt;
 *         &lt;element name="etalonRecord" type="{http://data.mdm.unidata.org/}EtalonRecord"/&gt;
 *         &lt;element name="originKey" type="{http://data.mdm.unidata.org/}OriginKey"/&gt;
 *         &lt;element name="supplementaryKeys" type="{http://data.mdm.unidata.org/}OriginKey" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="upsertActionType" use="required" type="{http://api.mdm.unidata.org/}UpsertActionType" /&gt;
 *       &lt;attribute name="operationId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
public class UpsertEventDetailsDef implements Serializable {

    private final static long serialVersionUID = 12345L;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected OriginRecord originRecord;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected EtalonRecord etalonRecord;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected OriginKey originKey;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected List<OriginKey> supplementaryKeys;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected UpsertActionType upsertActionType;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected String operationId;

    /**
     * Gets the value of the originRecord property.
     * 
     * @return
     *     possible object is
     *     {@link OriginRecord }
     *     
     */
    public OriginRecord getOriginRecord() {
        return originRecord;
    }

    /**
     * Sets the value of the originRecord property.
     * 
     * @param value
     *     allowed object is
     *     {@link OriginRecord }
     *     
     */
    public void setOriginRecord(OriginRecord value) {
        this.originRecord = value;
    }

    /**
     * Gets the value of the etalonRecord property.
     * 
     * @return
     *     possible object is
     *     {@link EtalonRecord }
     *     
     */
    public EtalonRecord getEtalonRecord() {
        return etalonRecord;
    }

    /**
     * Sets the value of the etalonRecord property.
     * 
     * @param value
     *     allowed object is
     *     {@link EtalonRecord }
     *     
     */
    public void setEtalonRecord(EtalonRecord value) {
        this.etalonRecord = value;
    }

    /**
     * Gets the value of the originKey property.
     * 
     * @return
     *     possible object is
     *     {@link OriginKey }
     *     
     */
    public OriginKey getOriginKey() {
        return originKey;
    }

    /**
     * Sets the value of the originKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link OriginKey }
     *     
     */
    public void setOriginKey(OriginKey value) {
        this.originKey = value;
    }

    /**
     * Gets the value of the supplementaryKeys property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the supplementaryKeys property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSupplementaryKeys().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OriginKey }
     * 
     * 
     */
    public List<OriginKey> getSupplementaryKeys() {
        if (supplementaryKeys == null) {
            supplementaryKeys = new ArrayList<OriginKey>();
        }
        return this.supplementaryKeys;
    }

    /**
     * Gets the value of the upsertActionType property.
     * 
     * @return
     *     possible object is
     *     {@link UpsertActionType }
     *     
     */
    public UpsertActionType getUpsertActionType() {
        return upsertActionType;
    }

    /**
     * Sets the value of the upsertActionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpsertActionType }
     *     
     */
    public void setUpsertActionType(UpsertActionType value) {
        this.upsertActionType = value;
    }

    /**
     * Gets the value of the operationId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperationId() {
        return operationId;
    }

    /**
     * Sets the value of the operationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperationId(String value) {
        this.operationId = value;
    }

    public UpsertEventDetailsDef withOriginRecord(OriginRecord value) {
        setOriginRecord(value);
        return this;
    }

    public UpsertEventDetailsDef withEtalonRecord(EtalonRecord value) {
        setEtalonRecord(value);
        return this;
    }

    public UpsertEventDetailsDef withOriginKey(OriginKey value) {
        setOriginKey(value);
        return this;
    }

    public UpsertEventDetailsDef withSupplementaryKeys(OriginKey... values) {
        if (values!= null) {
            for (OriginKey value: values) {
                getSupplementaryKeys().add(value);
            }
        }
        return this;
    }

    public UpsertEventDetailsDef withSupplementaryKeys(Collection<OriginKey> values) {
        if (values!= null) {
            getSupplementaryKeys().addAll(values);
        }
        return this;
    }

    public UpsertEventDetailsDef withUpsertActionType(UpsertActionType value) {
        setUpsertActionType(value);
        return this;
    }

    public UpsertEventDetailsDef withOperationId(String value) {
        setOperationId(value);
        return this;
    }

}
