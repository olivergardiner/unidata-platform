package org.unidata.mdm.api;

import java.io.Serializable;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;


/**
 * 
 * Структура описывающая сессионный токен. Представляет собой произвольный набор символов. Используется для передачи ключа сессии между запросами.
 * В типовом сценарии использования, данный ключ возвращается к ответе на запрос 'RequestAuthenticate' и потом передаётся во все последующие запросы.
 *             
 * 
 * <p>Java class for SessionTokenDef complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SessionTokenDef"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="token" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
public class SessionTokenDef implements Serializable {

    private final static long serialVersionUID = 12345L;
    @JacksonXmlProperty(isAttribute = true)
    protected String token;

    /**
     * Gets the value of the token property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToken() {
        return token;
    }

    /**
     * Sets the value of the token property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToken(String value) {
        this.token = value;
    }

    public SessionTokenDef withToken(String value) {
        setToken(value);
        return this;
    }

}
