package org.unidata.mdm.api;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * <p>Java class for UpsertActionType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="UpsertActionType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Insert"/&gt;
 *     &lt;enumeration value="Update"/&gt;
 *     &lt;enumeration value="UpsertOrigin"/&gt;
 *     &lt;enumeration value="NoAction"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
public enum UpsertActionType {

    @JsonProperty("Insert")
    INSERT("Insert"),
    @JsonProperty("Update")
    UPDATE("Update"),
    @JsonProperty("UpsertOrigin")
    UPSERT_ORIGIN("UpsertOrigin"),
    @JsonProperty("NoAction")
    NO_ACTION("NoAction");

    private final String value;

    UpsertActionType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static UpsertActionType fromValue(String v) {
        for (UpsertActionType c: UpsertActionType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
