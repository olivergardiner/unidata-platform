package org.unidata.mdm.api;

import java.io.Serializable;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;


/**
 * 
 * Структура, описывающая условия поиска на конкретный атрибут сущности. Используется для формирования условий поиска сущности в запросе 'RequestSearch'.
 * Всегда состоит из имени атрибута, оператора сравнения и константы.
 *             
 * 
 * <p>Java class for SearchAtomDef complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchAtomDef"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://api.mdm.unidata.org/}SearchBaseDef"&gt;
 *       &lt;attribute name="attributeName" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="operator" use="required" type="{http://api.mdm.unidata.org/}CompareOperatorType" /&gt;
 *       &lt;attribute name="constant" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
public class SearchAtomDef extends SearchBaseDef implements Serializable {

    private final static long serialVersionUID = 12345L;
    @JacksonXmlProperty(isAttribute = true)
    protected String attributeName;
    @JacksonXmlProperty(isAttribute = true)
    protected CompareOperatorType operator;
    @JacksonXmlProperty(isAttribute = true)
    protected String constant;

    /**
     * Gets the value of the attributeName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttributeName() {
        return attributeName;
    }

    /**
     * Sets the value of the attributeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttributeName(String value) {
        this.attributeName = value;
    }

    /**
     * Gets the value of the operator property.
     * 
     * @return
     *     possible object is
     *     {@link CompareOperatorType }
     *     
     */
    public CompareOperatorType getOperator() {
        return operator;
    }

    /**
     * Sets the value of the operator property.
     * 
     * @param value
     *     allowed object is
     *     {@link CompareOperatorType }
     *     
     */
    public void setOperator(CompareOperatorType value) {
        this.operator = value;
    }

    /**
     * Gets the value of the constant property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConstant() {
        return constant;
    }

    /**
     * Sets the value of the constant property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConstant(String value) {
        this.constant = value;
    }

    public SearchAtomDef withAttributeName(String value) {
        setAttributeName(value);
        return this;
    }

    public SearchAtomDef withOperator(CompareOperatorType value) {
        setOperator(value);
        return this;
    }

    public SearchAtomDef withConstant(String value) {
        setConstant(value);
        return this;
    }

}
