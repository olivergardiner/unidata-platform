package org.unidata.mdm.api;

import java.io.Serializable;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import static org.unidata.mdm.DataNamespace.DATA_API_NAMESPACE;


/**
 * 
 *                 Объет мапинга, необходимый для уведомления системы о том , что запись классификатора принадлежит определенной ноде.
 *                 classifierName - имя классификатора, которым проклассифицированна запись.
 *                 classifierPointer - значение указателя.
 *                 pointerType - тип указателя.
 *             
 * 
 * <p>Java class for ClassifierPointerDef complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ClassifierPointerDef"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="pointerType" type="{http://api.mdm.unidata.org/}ClassifierPointerType"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="classifierName" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="classifierPointer" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
public class ClassifierPointerDef implements Serializable {

    private final static long serialVersionUID = 12345L;

    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected ClassifierPointerType pointerType;
    @JacksonXmlProperty(isAttribute = true)
    protected String classifierName;
    @JacksonXmlProperty(isAttribute = true)
    protected String classifierPointer;

    /**
     * Gets the value of the pointerType property.
     * 
     * @return
     *     possible object is
     *     {@link ClassifierPointerType }
     *     
     */
    public ClassifierPointerType getPointerType() {
        return pointerType;
    }

    /**
     * Sets the value of the pointerType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClassifierPointerType }
     *     
     */
    public void setPointerType(ClassifierPointerType value) {
        this.pointerType = value;
    }

    /**
     * Gets the value of the classifierName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClassifierName() {
        return classifierName;
    }

    /**
     * Sets the value of the classifierName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClassifierName(String value) {
        this.classifierName = value;
    }

    /**
     * Gets the value of the classifierPointer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClassifierPointer() {
        return classifierPointer;
    }

    /**
     * Sets the value of the classifierPointer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClassifierPointer(String value) {
        this.classifierPointer = value;
    }

    public ClassifierPointerDef withPointerType(ClassifierPointerType value) {
        setPointerType(value);
        return this;
    }

    public ClassifierPointerDef withClassifierName(String value) {
        setClassifierName(value);
        return this;
    }

    public ClassifierPointerDef withClassifierPointer(String value) {
        setClassifierPointer(value);
        return this;
    }

}
