package org.unidata.mdm.api;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.xml.datatype.XMLGregorianCalendar;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.unidata.mdm.data.EtalonKey;
import org.unidata.mdm.data.OriginKey;

import static org.unidata.mdm.DataNamespace.DATA_API_NAMESPACE;


/**
 * 
 * Запрос на вставку или модификацию основной или исходной записи сущности.
 * Общая часть запроса должна содержать данные для аутентификации пользователя (смотри элемент 'common' из структуры 'UnidataRequestBody').
 * 
 * 'upsert' означает комбинацию слов 'update' и 'insert'.
 * 
 * Все операции по модификации данных производятся над исходными записями сущностей из конкретной системы источника, включая служебную систему источник.
 * При этом основная запись сущности вычисляется заново как результат консолидации всех исходных записей.
 * 
 * Имеется возможность передать либо основную запись сущности (элемент 'etalonRecord'), либо исходную запись (элемент 'originRecord'). 
 * При передаче основной записи, платформа подберёт соответствующую исходную запись из служебной системы источника и
 * обеспечит выигрыш соответствующих значений атрибутов при консолидации. 
 * Любая из переданных записей содержит ключ, на основе значения которого принимается решение будет ли это операция вставки или изменения.
 * 
 * По умолчанию, к любой основной записи применяются все имеющиеся правила контроля качества данных. Вызывающая сторона может принудительно отказаться от применения правил, передав skipCleanse='true'
 * В случае операции изменения записи, есть возможность передать выборочный набор атрибутов. Все непереданные атрибуты остаются без изменений.
 * 
 * Поле range указывает границы действия версии данных. Отсутствующие значения означают бесконечность в прошлом или будущем или их комбинацию. Отсутствующий элемент означает бесконечность с обоих концов.
 *             
 * 
 * <p>Java class for RequestRelationsUpsert complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RequestRelationsUpsert"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://api.mdm.unidata.org/}UnidataAbstractRequest"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="originKey" type="{http://data.mdm.unidata.org/}OriginKey"/&gt;
 *           &lt;element name="etalonKey" type="{http://data.mdm.unidata.org/}EtalonKey"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="relations" type="{http://api.mdm.unidata.org/}UpsertRelationsDef"/&gt;
 *         &lt;element name="range" type="{http://api.mdm.unidata.org/}TimeIntervalDef" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="lastUpdateDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&gt;
 *       &lt;attribute name="skipCleanse" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="bypassExtensionPoints" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
public class RequestRelationsUpsert extends UnidataAbstractRequest implements Serializable {

    private final static long serialVersionUID = 12345L;

    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected OriginKey originKey;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected EtalonKey etalonKey;
    @JacksonXmlElementWrapper(localName = "relations", namespace = DATA_API_NAMESPACE)
    @JacksonXmlProperty(localName = "relation", namespace = DATA_API_NAMESPACE)
    protected List<UpsertRelationDef> relations;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected TimeIntervalDef range;
    @JacksonXmlProperty(isAttribute = true)
    protected XMLGregorianCalendar lastUpdateDate;
    @JacksonXmlProperty(isAttribute = true)
    protected Boolean skipCleanse;
    @JacksonXmlProperty(isAttribute = true)
    protected Boolean bypassExtensionPoints;

    /**
     * Gets the value of the originKey property.
     * 
     * @return
     *     possible object is
     *     {@link OriginKey }
     *     
     */
    public OriginKey getOriginKey() {
        return originKey;
    }

    /**
     * Sets the value of the originKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link OriginKey }
     *     
     */
    public void setOriginKey(OriginKey value) {
        this.originKey = value;
    }

    /**
     * Gets the value of the etalonKey property.
     * 
     * @return
     *     possible object is
     *     {@link EtalonKey }
     *     
     */
    public EtalonKey getEtalonKey() {
        return etalonKey;
    }

    /**
     * Sets the value of the etalonKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link EtalonKey }
     *     
     */
    public void setEtalonKey(EtalonKey value) {
        this.etalonKey = value;
    }

    /**
     * Gets the value of the range property.
     * 
     * @return
     *     possible object is
     *     {@link TimeIntervalDef }
     *     
     */
    public TimeIntervalDef getRange() {
        return range;
    }

    /**
     * Sets the value of the range property.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeIntervalDef }
     *     
     */
    public void setRange(TimeIntervalDef value) {
        this.range = value;
    }

    /**
     * Gets the value of the lastUpdateDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLastUpdateDate() {
        return lastUpdateDate;
    }

    /**
     * Sets the value of the lastUpdateDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLastUpdateDate(XMLGregorianCalendar value) {
        this.lastUpdateDate = value;
    }

    /**
     * Gets the value of the skipCleanse property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isSkipCleanse() {
        if (skipCleanse == null) {
            return false;
        } else {
            return skipCleanse;
        }
    }

    /**
     * Sets the value of the skipCleanse property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSkipCleanse(Boolean value) {
        this.skipCleanse = value;
    }

    /**
     * Gets the value of the bypassExtensionPoints property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isBypassExtensionPoints() {
        if (bypassExtensionPoints == null) {
            return false;
        } else {
            return bypassExtensionPoints;
        }
    }

    /**
     * Sets the value of the bypassExtensionPoints property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBypassExtensionPoints(Boolean value) {
        this.bypassExtensionPoints = value;
    }

    public List<UpsertRelationDef> getRelations() {
        if (relations == null) {
            relations = new ArrayList<UpsertRelationDef>();
        }
        return relations;
    }

    public void setRelations(List<UpsertRelationDef> relations) {
        this.relations = relations;
    }

    public RequestRelationsUpsert withOriginKey(OriginKey value) {
        setOriginKey(value);
        return this;
    }

    public RequestRelationsUpsert withEtalonKey(EtalonKey value) {
        setEtalonKey(value);
        return this;
    }

    public RequestRelationsUpsert withRange(TimeIntervalDef value) {
        setRange(value);
        return this;
    }

    public RequestRelationsUpsert withLastUpdateDate(XMLGregorianCalendar value) {
        setLastUpdateDate(value);
        return this;
    }

    public RequestRelationsUpsert withSkipCleanse(Boolean value) {
        setSkipCleanse(value);
        return this;
    }

    public RequestRelationsUpsert withBypassExtensionPoints(Boolean value) {
        setBypassExtensionPoints(value);
        return this;
    }

    public RequestRelationsUpsert withRelations(UpsertRelationDef... values) {
        if (values!= null) {
            for (UpsertRelationDef value: values) {
                getRelations().add(value);
            }
        }
        return this;
    }

    public RequestRelationsUpsert withRelations(Collection<UpsertRelationDef> values) {
        if (values!= null) {
            getRelations().addAll(values);
        }
        return this;
    }

    public RequestRelationsUpsert withRelations(List<UpsertRelationDef> relations) {
        setRelations(relations);
        return this;
    }

}
