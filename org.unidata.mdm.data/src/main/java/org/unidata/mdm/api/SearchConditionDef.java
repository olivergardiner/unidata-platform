package org.unidata.mdm.api;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import static org.unidata.mdm.DataNamespace.DATA_API_NAMESPACE;


/**
 * 
 * Структура, описывающая условия поиска сущностей. Используется для формирования условий поиска сущности в запросе 'RequestSearch'.
 * Является точкой входа при описании условий поиска - задаёт начальную группировку либо через логическое 'И', либо 'ИЛИ', либо как простой одиночный атом - условие
 *             
 * 
 * <p>Java class for SearchConditionDef complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchConditionDef"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="and" type="{http://api.mdm.unidata.org/}SearchAndDef"/&gt;
 *           &lt;element name="or" type="{http://api.mdm.unidata.org/}SearchOrDef"/&gt;
 *           &lt;element name="atom" type="{http://api.mdm.unidata.org/}SearchAtomDef"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
public class SearchConditionDef implements Serializable
{

    private final static long serialVersionUID = 12345L;
//    @XmlElements({
//        @XmlElement(name = "and", type = SearchAndDef.class),
//        @XmlElement(name = "or", type = SearchOrDef.class),
//        @XmlElement(name = "atom", type = SearchAtomDef.class)
//    })
    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.WRAPPER_OBJECT)
    @JsonSubTypes({
        @JsonSubTypes.Type(value = SearchAndDef.class, name = "and"),
        @JsonSubTypes.Type(value = SearchOrDef.class, name = "or"),
        @JsonSubTypes.Type(value = SearchAtomDef.class, name = "atom")
    })
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected SearchBaseDef expression;

    /**
     * Gets the value of the expression property.
     * 
     * @return
     *     possible object is
     *     {@link SearchAndDef }
     *     {@link SearchOrDef }
     *     {@link SearchAtomDef }
     *     
     */
    public SearchBaseDef getExpression() {
        return expression;
    }

    /**
     * Sets the value of the expression property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchAndDef }
     *     {@link SearchOrDef }
     *     {@link SearchAtomDef }
     *     
     */
    public void setExpression(SearchBaseDef value) {
        this.expression = value;
    }

    public SearchConditionDef withExpression(SearchBaseDef value) {
        setExpression(value);
        return this;
    }

}
