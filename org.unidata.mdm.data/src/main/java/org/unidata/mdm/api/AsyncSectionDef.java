package org.unidata.mdm.api;

import java.io.Serializable;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;


/**
 * 
 * Составная часть любого API запроса. Позволяет задать параметры асинхронного вызова.
 *             
 * 
 * <p>Java class for AsyncSectionDef complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AsyncSectionDef"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="useJMS" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="jmsReplyTo" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="jmsCorrelationId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
public class AsyncSectionDef implements Serializable {

    private final static long serialVersionUID = 12345L;

    @JacksonXmlProperty(isAttribute = true)
    protected Boolean useJMS;
    @JacksonXmlProperty(isAttribute = true)
    protected String jmsReplyTo;
    @JacksonXmlProperty(isAttribute = true)
    protected String jmsCorrelationId;

    /**
     * Gets the value of the useJMS property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isUseJMS() {
        if (useJMS == null) {
            return false;
        } else {
            return useJMS;
        }
    }

    /**
     * Sets the value of the useJMS property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseJMS(Boolean value) {
        this.useJMS = value;
    }

    /**
     * Gets the value of the jmsReplyTo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJmsReplyTo() {
        return jmsReplyTo;
    }

    /**
     * Sets the value of the jmsReplyTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJmsReplyTo(String value) {
        this.jmsReplyTo = value;
    }

    /**
     * Gets the value of the jmsCorrelationId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJmsCorrelationId() {
        return jmsCorrelationId;
    }

    /**
     * Sets the value of the jmsCorrelationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJmsCorrelationId(String value) {
        this.jmsCorrelationId = value;
    }

    public AsyncSectionDef withUseJMS(Boolean value) {
        setUseJMS(value);
        return this;
    }

    public AsyncSectionDef withJmsReplyTo(String value) {
        setJmsReplyTo(value);
        return this;
    }

    public AsyncSectionDef withJmsCorrelationId(String value) {
        setJmsCorrelationId(value);
        return this;
    }

}
