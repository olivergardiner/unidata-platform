package org.unidata.mdm.api;

import java.io.Serializable;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import static org.unidata.mdm.DataNamespace.DATA_API_NAMESPACE;


/**
 * 
 * Ответ на запрос на вставку или модификацию основной или исходной записи сущности ('RequestRelationsUpsert').
 * Общая часть ответа содержит код возврата, идентификатор логической операции и сообщения об ошибках (смотри элемент 'common' из структуры 'UnidataResponseBody').
 * В случае успешного исполнения всегда содержит два ключа, один идентифицирующий исходную запись, фактически изменённую, второй идентифицирующий основную запись сущности.
 * Помимо этого возращается тип действия, фактически выполненного платформой - 'action'.
 *             
 * 
 * <p>Java class for ResponseRelationsUpsert complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ResponseRelationsUpsert"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://api.mdm.unidata.org/}UnidataAbstractResponse"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="upsertCount" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="originAction" use="required" type="{http://api.mdm.unidata.org/}UpsertActionType" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
public class ResponseRelationsUpsert extends UnidataAbstractResponse implements Serializable {

    private final static long serialVersionUID = 12345L;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected int upsertCount;
    @JacksonXmlProperty(isAttribute = true)
    protected UpsertActionType originAction;

    /**
     * Gets the value of the upsertCount property.
     * 
     */
    public int getUpsertCount() {
        return upsertCount;
    }

    /**
     * Sets the value of the upsertCount property.
     * 
     */
    public void setUpsertCount(int value) {
        this.upsertCount = value;
    }

    /**
     * Gets the value of the originAction property.
     * 
     * @return
     *     possible object is
     *     {@link UpsertActionType }
     *     
     */
    public UpsertActionType getOriginAction() {
        return originAction;
    }

    /**
     * Sets the value of the originAction property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpsertActionType }
     *     
     */
    public void setOriginAction(UpsertActionType value) {
        this.originAction = value;
    }

    public ResponseRelationsUpsert withUpsertCount(int value) {
        setUpsertCount(value);
        return this;
    }

    public ResponseRelationsUpsert withOriginAction(UpsertActionType value) {
        setOriginAction(value);
        return this;
    }

}
