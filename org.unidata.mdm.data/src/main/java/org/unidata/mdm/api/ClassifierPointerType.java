package org.unidata.mdm.api;

/**
 * <p>Java class for ClassifierPointerType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ClassifierPointerType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="NODE_NAME"/&gt;
 *     &lt;enumeration value="NODE_CODE"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
public enum ClassifierPointerType {

    NODE_NAME,
    NODE_CODE;

    public String value() {
        return name();
    }

    public static ClassifierPointerType fromValue(String v) {
        return valueOf(v);
    }

}
