package org.unidata.mdm.api;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import static org.unidata.mdm.DataNamespace.DATA_API_NAMESPACE;


/**
 * 
 * Ответ на запрос на аутентификацию пользователя ('RequestAuthenticate').
 * Общая часть ответа содержит код возврата, идентификатор логической операции и сообщения об ошибках (смотри элемент 'common' из структуры 'UnidataResponseBody').
 * Содержит список ролей пользователя. Признак администратора. Также может содержать сессионный токен, в случае если запрос был сформирован в режиме doLogin='true'
 *             
 * 
 * <p>Java class for ResponseAuthenticate complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ResponseAuthenticate"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://api.mdm.unidata.org/}UnidataAbstractResponse"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="sessionToken" type="{http://api.mdm.unidata.org/}SessionTokenDef" minOccurs="0"/&gt;
 *         &lt;element name="role" type="{http://api.mdm.unidata.org/}RoleRefDef" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="isAdmin" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
public class ResponseAuthenticate extends UnidataAbstractResponse implements Serializable {

    private final static long serialVersionUID = 12345L;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected SessionTokenDef sessionToken;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected List<RoleRefDef> role;
    @JacksonXmlProperty(isAttribute = true)
    protected boolean isAdmin;

    /**
     * Gets the value of the sessionToken property.
     * 
     * @return
     *     possible object is
     *     {@link SessionTokenDef }
     *     
     */
    public SessionTokenDef getSessionToken() {
        return sessionToken;
    }

    /**
     * Sets the value of the sessionToken property.
     * 
     * @param value
     *     allowed object is
     *     {@link SessionTokenDef }
     *     
     */
    public void setSessionToken(SessionTokenDef value) {
        this.sessionToken = value;
    }

    /**
     * Gets the value of the role property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the role property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRole().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RoleRefDef }
     * 
     * 
     */
    public List<RoleRefDef> getRole() {
        if (role == null) {
            role = new ArrayList<RoleRefDef>();
        }
        return this.role;
    }

    /**
     * Gets the value of the isAdmin property.
     * 
     */
    public boolean isIsAdmin() {
        return isAdmin;
    }

    /**
     * Sets the value of the isAdmin property.
     * 
     */
    public void setIsAdmin(boolean value) {
        this.isAdmin = value;
    }

    public ResponseAuthenticate withSessionToken(SessionTokenDef value) {
        setSessionToken(value);
        return this;
    }

    public ResponseAuthenticate withRole(RoleRefDef... values) {
        if (values!= null) {
            for (RoleRefDef value: values) {
                getRole().add(value);
            }
        }
        return this;
    }

    public ResponseAuthenticate withRole(Collection<RoleRefDef> values) {
        if (values!= null) {
            getRole().addAll(values);
        }
        return this;
    }

    public ResponseAuthenticate withIsAdmin(boolean value) {
        setIsAdmin(value);
        return this;
    }

}
