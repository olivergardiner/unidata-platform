package org.unidata.mdm.api;

/**
 * <p>Java class for CompareOperatorType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CompareOperatorType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="EQUALS"/&gt;
 *     &lt;enumeration value="NOT_EQUALS"/&gt;
 *     &lt;enumeration value="GREATER"/&gt;
 *     &lt;enumeration value="GREATER_OR_EQUALS"/&gt;
 *     &lt;enumeration value="LESS"/&gt;
 *     &lt;enumeration value="LESS_OR_EQUALS"/&gt;
 *     &lt;enumeration value="LIKE"/&gt;
 *     &lt;enumeration value="FUZZY_EQUALS"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
public enum CompareOperatorType {

    EQUALS,
    NOT_EQUALS,
    GREATER,
    GREATER_OR_EQUALS,
    LESS,
    LESS_OR_EQUALS,
    LIKE,
    FUZZY_EQUALS;

    public String value() {
        return name();
    }

    public static CompareOperatorType fromValue(String v) {
        return valueOf(v);
    }

}
