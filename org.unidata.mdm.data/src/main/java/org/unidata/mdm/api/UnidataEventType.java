package org.unidata.mdm.api;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * <p>Java class for UnidataEventType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="UnidataEventType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Upsert"/&gt;
 *     &lt;enumeration value="Merge"/&gt;
 *     &lt;enumeration value="SoftDelete"/&gt;
 *     &lt;enumeration value="WipeDelete"/&gt;
 *     &lt;enumeration value="Restore"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
public enum UnidataEventType {

    @JsonProperty("Upsert")
    UPSERT("Upsert"),
    @JsonProperty("Merge")
    MERGE("Merge"),
    @JsonProperty("SoftDelete")
    SOFT_DELETE("SoftDelete"),
    @JsonProperty("WipeDelete")
    WIPE_DELETE("WipeDelete"),
    @JsonProperty("Restore")
    RESTORE("Restore");

    private final String value;

    UnidataEventType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static UnidataEventType fromValue(String v) {
        for (UnidataEventType c: UnidataEventType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
