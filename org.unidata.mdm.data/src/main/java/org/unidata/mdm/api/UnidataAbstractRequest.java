package org.unidata.mdm.api;

import java.io.Serializable;


/**
 * <p>Java class for UnidataAbstractRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UnidataAbstractRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
public class UnidataAbstractRequest implements Serializable {

    private final static long serialVersionUID = 12345L;

}
