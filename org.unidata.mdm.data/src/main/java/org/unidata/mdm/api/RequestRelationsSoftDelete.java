package org.unidata.mdm.api;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.unidata.mdm.data.EtalonKey;
import org.unidata.mdm.data.OriginKey;

import static org.unidata.mdm.DataNamespace.DATA_API_NAMESPACE;


/**
 * 
 * Запрос на логическое удаление связи основной записи, исходной записи или деактивация отдельного периода.
 * Общая часть запроса должна содержать данные для аутентификации пользователя (смотри элемент 'common' из структуры 'UnidataRequestBody').
 * Логическое удаление позволяет пометить связь либо исходной запись из конкретной системы источника, либо основной записи как логически удалённую.
 * При логическом удалении связи одной исходной записи из конкретной системы источника, связь на основная запись может продолжать оставаться активной, если в её составе есть другие активные исходные записи.
 * Тип удаления указывается в поле actionType.
 * При наличии прав запрос может содержать значение true в поле wipe. В этом случае запись будет ФИЗИЧЕСКИ и БЕЗВОЗВРАТНО удалена из хранилищи (hard delete).
 *             
 * 
 * <p>Java class for RequestRelationsSoftDelete complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RequestRelationsSoftDelete"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://api.mdm.unidata.org/}UnidataAbstractRequest"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="etalonKey" type="{http://data.mdm.unidata.org/}EtalonKey"/&gt;
 *           &lt;element name="originKey" type="{http://data.mdm.unidata.org/}OriginKey"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="relations" type="{http://api.mdm.unidata.org/}DeleteRelationsDef"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="actionType" use="required" type="{http://api.mdm.unidata.org/}SoftDeleteActionType" /&gt;
 *       &lt;attribute name="wipe" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
public class RequestRelationsSoftDelete extends UnidataAbstractRequest implements Serializable {

    private final static long serialVersionUID = 12345L;

    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected EtalonKey etalonKey;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected OriginKey originKey;
    @JacksonXmlElementWrapper(localName = "relations", namespace = DATA_API_NAMESPACE)
    @JacksonXmlProperty(localName = "relation", namespace = DATA_API_NAMESPACE)
    protected List<DeleteRelationDef> relations;
    @JacksonXmlProperty(isAttribute = true)
    protected SoftDeleteActionType actionType;
    @JacksonXmlProperty(isAttribute = true)
    protected Boolean wipe;

    /**
     * Gets the value of the etalonKey property.
     * 
     * @return
     *     possible object is
     *     {@link EtalonKey }
     *     
     */
    public EtalonKey getEtalonKey() {
        return etalonKey;
    }

    /**
     * Sets the value of the etalonKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link EtalonKey }
     *     
     */
    public void setEtalonKey(EtalonKey value) {
        this.etalonKey = value;
    }

    /**
     * Gets the value of the originKey property.
     * 
     * @return
     *     possible object is
     *     {@link OriginKey }
     *     
     */
    public OriginKey getOriginKey() {
        return originKey;
    }

    /**
     * Sets the value of the originKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link OriginKey }
     *     
     */
    public void setOriginKey(OriginKey value) {
        this.originKey = value;
    }

    /**
     * Gets the value of the actionType property.
     * 
     * @return
     *     possible object is
     *     {@link SoftDeleteActionType }
     *     
     */
    public SoftDeleteActionType getActionType() {
        return actionType;
    }

    /**
     * Sets the value of the actionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link SoftDeleteActionType }
     *     
     */
    public void setActionType(SoftDeleteActionType value) {
        this.actionType = value;
    }

    /**
     * Gets the value of the wipe property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isWipe() {
        return wipe;
    }

    /**
     * Sets the value of the wipe property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setWipe(Boolean value) {
        this.wipe = value;
    }

    public List<DeleteRelationDef> getRelations() {
        if (relations == null) {
            relations = new ArrayList<DeleteRelationDef>();
        }
        return relations;
    }

    public void setRelations(List<DeleteRelationDef> relations) {
        this.relations = relations;
    }

    public RequestRelationsSoftDelete withEtalonKey(EtalonKey value) {
        setEtalonKey(value);
        return this;
    }

    public RequestRelationsSoftDelete withOriginKey(OriginKey value) {
        setOriginKey(value);
        return this;
    }

    public RequestRelationsSoftDelete withActionType(SoftDeleteActionType value) {
        setActionType(value);
        return this;
    }

    public RequestRelationsSoftDelete withWipe(Boolean value) {
        setWipe(value);
        return this;
    }

    public RequestRelationsSoftDelete withRelations(DeleteRelationDef... values) {
        if (values!= null) {
            for (DeleteRelationDef value: values) {
                getRelations().add(value);
            }
        }
        return this;
    }

    public RequestRelationsSoftDelete withRelations(Collection<DeleteRelationDef> values) {
        if (values!= null) {
            getRelations().addAll(values);
        }
        return this;
    }

    public RequestRelationsSoftDelete withRelations(List<DeleteRelationDef> relations) {
        setRelations(relations);
        return this;
    }

}
