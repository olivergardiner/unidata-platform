package org.unidata.mdm.api;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import static org.unidata.mdm.DataNamespace.DATA_API_NAMESPACE;


/**
 * <p>Java class for Statistic complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Statistic"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="type" type="{http://api.mdm.unidata.org/}StatisticEnum"/&gt;
 *         &lt;element name="series" type="{http://api.mdm.unidata.org/}TimeSerie" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@JsonPropertyOrder({"type","series"})
public class Statistic implements Serializable {

    private final static long serialVersionUID = 12345L;

    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected StatisticEnum type;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected List<TimeSerie> series;

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link StatisticEnum }
     *     
     */
    public StatisticEnum getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatisticEnum }
     *     
     */
    public void setType(StatisticEnum value) {
        this.type = value;
    }

    /**
     * Gets the value of the series property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the series property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSeries().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TimeSerie }
     * 
     * 
     */
    public List<TimeSerie> getSeries() {
        if (series == null) {
            series = new ArrayList<TimeSerie>();
        }
        return this.series;
    }

    public Statistic withType(StatisticEnum value) {
        setType(value);
        return this;
    }

    public Statistic withSeries(TimeSerie... values) {
        if (values!= null) {
            for (TimeSerie value: values) {
                getSeries().add(value);
            }
        }
        return this;
    }

    public Statistic withSeries(Collection<TimeSerie> values) {
        if (values!= null) {
            getSeries().addAll(values);
        }
        return this;
    }

}
