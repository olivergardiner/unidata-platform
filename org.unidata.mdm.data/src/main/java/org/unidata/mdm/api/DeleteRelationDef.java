package org.unidata.mdm.api;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import static org.unidata.mdm.DataNamespace.DATA_API_NAMESPACE;


/**
 * 
 * Набор ключей to (правая сторона) одного определенного типа (origin или etalon) для одной определенной связи.
 *             
 * 
 * <p>Java class for DeleteRelationDef complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DeleteRelationDef"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="key" type="{http://api.mdm.unidata.org/}DeleteRelationRecordDef" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
public class DeleteRelationDef implements Serializable {

    private final static long serialVersionUID = 12345L;

    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected List<DeleteRelationRecordDef> keys;
    @JacksonXmlProperty(isAttribute = true)
    protected String name;

    /**
     * Gets the value of the keys property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the keys property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getKeys().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DeleteRelationRecordDef }
     * 
     * 
     */
    public List<DeleteRelationRecordDef> getKeys() {
        if (keys == null) {
            keys = new ArrayList<DeleteRelationRecordDef>();
        }
        return this.keys;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    public DeleteRelationDef withKeys(DeleteRelationRecordDef... values) {
        if (values!= null) {
            for (DeleteRelationRecordDef value: values) {
                getKeys().add(value);
            }
        }
        return this;
    }

    public DeleteRelationDef withKeys(Collection<DeleteRelationRecordDef> values) {
        if (values!= null) {
            getKeys().addAll(values);
        }
        return this;
    }

    public DeleteRelationDef withName(String value) {
        setName(value);
        return this;
    }

}
