package org.unidata.mdm.api;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * <p>Java class for ExitCodeType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ExitCodeType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Success"/&gt;
 *     &lt;enumeration value="Error"/&gt;
 *     &lt;enumeration value="Warning"/&gt;
 *     &lt;enumeration value="AuthenticationError"/&gt;
 *     &lt;enumeration value="AuthorizationError"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
public enum ExitCodeType {

    @JsonProperty("Success")
    SUCCESS("Success"),
    @JsonProperty("Error")
    ERROR("Error"),
    @JsonProperty("Warning")
    WARNING("Warning"),
    @JsonProperty("AuthenticationError")
    AUTHENTICATION_ERROR("AuthenticationError"),
    @JsonProperty("AuthorizationError")
    AUTHORIZATION_ERROR("AuthorizationError");

    private final String value;

    ExitCodeType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ExitCodeType fromValue(String v) {
        for (ExitCodeType c: ExitCodeType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
