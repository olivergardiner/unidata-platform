package org.unidata.mdm.api;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.unidata.mdm.data.SimpleAttribute;

import static org.unidata.mdm.DataNamespace.DATA_API_NAMESPACE;


/**
 * 
 * Запрос на исполнение функции очистки данных.
 * Общая часть запроса должна содержать данные для аутентификации пользователя (смотри элемент 'common' из структуры 'UnidataRequestBody').
 * Запрос обязательно содержит полное имя функции очистки данных, включающее имена групп разделённые точкой. Например 'Строковые.УбратьПробелы'
 * Помимо этого требуется указать значение всех 'входных' портов функции.
 * Ответ будет содержать значение всех 'выходных' портов функции.
 *             
 * 
 * <p>Java class for RequestCleanse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RequestCleanse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://api.mdm.unidata.org/}UnidataAbstractRequest"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="port" type="{http://data.mdm.unidata.org/}SimpleAttribute" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="cleanseName" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
public class RequestCleanse extends UnidataAbstractRequest implements Serializable {

    private final static long serialVersionUID = 12345L;

    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected List<SimpleAttribute> port;
    @JacksonXmlProperty(isAttribute = true)
    protected String cleanseName;

    /**
     * Gets the value of the port property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the port property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPort().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SimpleAttribute }
     * 
     * 
     */
    public List<SimpleAttribute> getPort() {
        if (port == null) {
            port = new ArrayList<SimpleAttribute>();
        }
        return this.port;
    }

    /**
     * Gets the value of the cleanseName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCleanseName() {
        return cleanseName;
    }

    /**
     * Sets the value of the cleanseName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCleanseName(String value) {
        this.cleanseName = value;
    }

    public RequestCleanse withPort(SimpleAttribute... values) {
        if (values!= null) {
            for (SimpleAttribute value: values) {
                getPort().add(value);
            }
        }
        return this;
    }

    public RequestCleanse withPort(Collection<SimpleAttribute> values) {
        if (values!= null) {
            getPort().addAll(values);
        }
        return this;
    }

    public RequestCleanse withCleanseName(String value) {
        setCleanseName(value);
        return this;
    }

}
