package org.unidata.mdm.api;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.unidata.mdm.data.DataQualityError;

import static org.unidata.mdm.DataNamespace.DATA_API_NAMESPACE;


/**
 * 
 * Ответ на запрос на получение списка ошибок основной сущности, созданных в результате применения правил контроля качества данных ('RequestGetDataQualityErrors').
 * Общая часть ответа содержит код возврата, идентификатор логической операции и сообщения об ошибках (смотри элемент 'common' из структуры 'UnidataResponseBody').
 * Содержит список ошибок качества данных
 *             
 * 
 * <p>Java class for ResponseGetDataQualityErrors complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ResponseGetDataQualityErrors"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://api.mdm.unidata.org/}UnidataAbstractResponse"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="dqError" type="{http://data.mdm.unidata.org/}DataQualityError" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
public class ResponseGetDataQualityErrors extends UnidataAbstractResponse implements Serializable {

    private final static long serialVersionUID = 12345L;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected List<DataQualityError> dqError;

    /**
     * Gets the value of the dqError property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dqError property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDqError().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataQualityError }
     * 
     * 
     */
    public List<DataQualityError> getDqError() {
        if (dqError == null) {
            dqError = new ArrayList<DataQualityError>();
        }
        return this.dqError;
    }

    public ResponseGetDataQualityErrors withDqError(DataQualityError... values) {
        if (values!= null) {
            for (DataQualityError value: values) {
                getDqError().add(value);
            }
        }
        return this;
    }

    public ResponseGetDataQualityErrors withDqError(Collection<DataQualityError> values) {
        if (values!= null) {
            getDqError().addAll(values);
        }
        return this;
    }

}
