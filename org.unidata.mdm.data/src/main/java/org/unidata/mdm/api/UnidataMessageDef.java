package org.unidata.mdm.api;

import java.io.Serializable;
import javax.xml.datatype.XMLGregorianCalendar;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import static org.unidata.mdm.DataNamespace.DATA_API_NAMESPACE;


/**
 * 
 * Структура, описывающая произвольное событие генерируемое платформой на изменение данных.
 *             
 * 
 * <p>Java class for UnidataMessageDef complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UnidataMessageDef"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="upsertEventDetails" type="{http://api.mdm.unidata.org/}UpsertEventDetailsDef"/&gt;
 *         &lt;element name="UpsertEventRelationDetailsDef" type="{http://api.mdm.unidata.org/}UpsertEventRelationDetailsDef"/&gt;
 *         &lt;element name="mergeEventDetails" type="{http://api.mdm.unidata.org/}MergeEventDetailsDef"/&gt;
 *         &lt;element name="softDeleteEventDetails" type="{http://api.mdm.unidata.org/}SoftDeleteEventDetailsDef"/&gt;
 *         &lt;element name="softDeleteRelationEventDetails" type="{http://api.mdm.unidata.org/}SoftDeleteRelationEventDetailsDef"/&gt;
 *         &lt;element name="restoreEventDetails" type="{http://api.mdm.unidata.org/}RestoreEventDetailsDef"/&gt;
 *         &lt;element name="wipeDeleteEventDetails" type="{http://api.mdm.unidata.org/}WipeDeleteEventDetailsDef"/&gt;
 *       &lt;/choice&gt;
 *       &lt;attribute name="eventType" use="required" type="{http://api.mdm.unidata.org/}UnidataEventType" /&gt;
 *       &lt;attribute name="eventDate" use="required" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&gt;
 *       &lt;attribute name="publishDate" use="required" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&gt;
 *       &lt;attribute name="operationId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
public class UnidataMessageDef implements Serializable {

    private final static long serialVersionUID = 12345L;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected UpsertEventDetailsDef upsertEventDetails;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected UpsertEventRelationDetailsDef upsertEventRelationDetailsDef;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected MergeEventDetailsDef mergeEventDetails;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected SoftDeleteEventDetailsDef softDeleteEventDetails;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected SoftDeleteRelationEventDetailsDef softDeleteRelationEventDetails;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected RestoreEventDetailsDef restoreEventDetails;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected WipeDeleteEventDetailsDef wipeDeleteEventDetails;
    @JacksonXmlProperty(isAttribute = true)
    protected UnidataEventType eventType;
    @JacksonXmlProperty(isAttribute = true)
    protected XMLGregorianCalendar eventDate;
    @JacksonXmlProperty(isAttribute = true)
    protected XMLGregorianCalendar publishDate;
    @JacksonXmlProperty(isAttribute = true)
    protected String operationId;

    /**
     * Gets the value of the upsertEventDetails property.
     * 
     * @return
     *     possible object is
     *     {@link UpsertEventDetailsDef }
     *     
     */
    public UpsertEventDetailsDef getUpsertEventDetails() {
        return upsertEventDetails;
    }

    /**
     * Sets the value of the upsertEventDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpsertEventDetailsDef }
     *     
     */
    public void setUpsertEventDetails(UpsertEventDetailsDef value) {
        this.upsertEventDetails = value;
    }

    /**
     * Gets the value of the upsertEventRelationDetailsDef property.
     * 
     * @return
     *     possible object is
     *     {@link UpsertEventRelationDetailsDef }
     *     
     */
    public UpsertEventRelationDetailsDef getUpsertEventRelationDetailsDef() {
        return upsertEventRelationDetailsDef;
    }

    /**
     * Sets the value of the upsertEventRelationDetailsDef property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpsertEventRelationDetailsDef }
     *     
     */
    public void setUpsertEventRelationDetailsDef(UpsertEventRelationDetailsDef value) {
        this.upsertEventRelationDetailsDef = value;
    }

    /**
     * Gets the value of the mergeEventDetails property.
     * 
     * @return
     *     possible object is
     *     {@link MergeEventDetailsDef }
     *     
     */
    public MergeEventDetailsDef getMergeEventDetails() {
        return mergeEventDetails;
    }

    /**
     * Sets the value of the mergeEventDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link MergeEventDetailsDef }
     *     
     */
    public void setMergeEventDetails(MergeEventDetailsDef value) {
        this.mergeEventDetails = value;
    }

    /**
     * Gets the value of the softDeleteEventDetails property.
     * 
     * @return
     *     possible object is
     *     {@link SoftDeleteEventDetailsDef }
     *     
     */
    public SoftDeleteEventDetailsDef getSoftDeleteEventDetails() {
        return softDeleteEventDetails;
    }

    /**
     * Sets the value of the softDeleteEventDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link SoftDeleteEventDetailsDef }
     *     
     */
    public void setSoftDeleteEventDetails(SoftDeleteEventDetailsDef value) {
        this.softDeleteEventDetails = value;
    }

    /**
     * Gets the value of the softDeleteRelationEventDetails property.
     * 
     * @return
     *     possible object is
     *     {@link SoftDeleteRelationEventDetailsDef }
     *     
     */
    public SoftDeleteRelationEventDetailsDef getSoftDeleteRelationEventDetails() {
        return softDeleteRelationEventDetails;
    }

    /**
     * Sets the value of the softDeleteRelationEventDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link SoftDeleteRelationEventDetailsDef }
     *     
     */
    public void setSoftDeleteRelationEventDetails(SoftDeleteRelationEventDetailsDef value) {
        this.softDeleteRelationEventDetails = value;
    }

    /**
     * Gets the value of the restoreEventDetails property.
     * 
     * @return
     *     possible object is
     *     {@link RestoreEventDetailsDef }
     *     
     */
    public RestoreEventDetailsDef getRestoreEventDetails() {
        return restoreEventDetails;
    }

    /**
     * Sets the value of the restoreEventDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link RestoreEventDetailsDef }
     *     
     */
    public void setRestoreEventDetails(RestoreEventDetailsDef value) {
        this.restoreEventDetails = value;
    }

    /**
     * Gets the value of the wipeDeleteEventDetails property.
     * 
     * @return
     *     possible object is
     *     {@link WipeDeleteEventDetailsDef }
     *     
     */
    public WipeDeleteEventDetailsDef getWipeDeleteEventDetails() {
        return wipeDeleteEventDetails;
    }

    /**
     * Sets the value of the wipeDeleteEventDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link WipeDeleteEventDetailsDef }
     *     
     */
    public void setWipeDeleteEventDetails(WipeDeleteEventDetailsDef value) {
        this.wipeDeleteEventDetails = value;
    }

    /**
     * Gets the value of the eventType property.
     * 
     * @return
     *     possible object is
     *     {@link UnidataEventType }
     *     
     */
    public UnidataEventType getEventType() {
        return eventType;
    }

    /**
     * Sets the value of the eventType property.
     * 
     * @param value
     *     allowed object is
     *     {@link UnidataEventType }
     *     
     */
    public void setEventType(UnidataEventType value) {
        this.eventType = value;
    }

    /**
     * Gets the value of the eventDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEventDate() {
        return eventDate;
    }

    /**
     * Sets the value of the eventDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEventDate(XMLGregorianCalendar value) {
        this.eventDate = value;
    }

    /**
     * Gets the value of the publishDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPublishDate() {
        return publishDate;
    }

    /**
     * Sets the value of the publishDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPublishDate(XMLGregorianCalendar value) {
        this.publishDate = value;
    }

    /**
     * Gets the value of the operationId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperationId() {
        return operationId;
    }

    /**
     * Sets the value of the operationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperationId(String value) {
        this.operationId = value;
    }

    public UnidataMessageDef withUpsertEventDetails(UpsertEventDetailsDef value) {
        setUpsertEventDetails(value);
        return this;
    }

    public UnidataMessageDef withUpsertEventRelationDetailsDef(UpsertEventRelationDetailsDef value) {
        setUpsertEventRelationDetailsDef(value);
        return this;
    }

    public UnidataMessageDef withMergeEventDetails(MergeEventDetailsDef value) {
        setMergeEventDetails(value);
        return this;
    }

    public UnidataMessageDef withSoftDeleteEventDetails(SoftDeleteEventDetailsDef value) {
        setSoftDeleteEventDetails(value);
        return this;
    }

    public UnidataMessageDef withSoftDeleteRelationEventDetails(SoftDeleteRelationEventDetailsDef value) {
        setSoftDeleteRelationEventDetails(value);
        return this;
    }

    public UnidataMessageDef withRestoreEventDetails(RestoreEventDetailsDef value) {
        setRestoreEventDetails(value);
        return this;
    }

    public UnidataMessageDef withWipeDeleteEventDetails(WipeDeleteEventDetailsDef value) {
        setWipeDeleteEventDetails(value);
        return this;
    }

    public UnidataMessageDef withEventType(UnidataEventType value) {
        setEventType(value);
        return this;
    }

    public UnidataMessageDef withEventDate(XMLGregorianCalendar value) {
        setEventDate(value);
        return this;
    }

    public UnidataMessageDef withPublishDate(XMLGregorianCalendar value) {
        setPublishDate(value);
        return this;
    }

    public UnidataMessageDef withOperationId(String value) {
        setOperationId(value);
        return this;
    }

}
