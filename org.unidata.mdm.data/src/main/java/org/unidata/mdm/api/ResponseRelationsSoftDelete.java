package org.unidata.mdm.api;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.unidata.mdm.data.EtalonKey;
import org.unidata.mdm.data.OriginKey;

import static org.unidata.mdm.DataNamespace.DATA_API_NAMESPACE;


/**
 * 
 * Ответ на запрос на логическое удаление основной или исходной записи сущности ('RequestSoftDelete').
 * Общая часть ответа содержит код возврата, идентификатор логической операции и сообщения об ошибках (смотри элемент 'common' из структуры 'UnidataResponseBody').
 * 
 * В случае успешного исполнения содержит список ключей логически удалённых записей, а также соответствующих исходных записей.
 *             
 * 
 * <p>Java class for ResponseRelationsSoftDelete complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ResponseRelationsSoftDelete"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://api.mdm.unidata.org/}UnidataAbstractResponse"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="etalonKeys" type="{http://data.mdm.unidata.org/}EtalonKey" maxOccurs="unbounded"/&gt;
 *         &lt;element name="originKeys" type="{http://data.mdm.unidata.org/}OriginKey" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@JsonPropertyOrder({"etalonKeys","originKeys"})
public class ResponseRelationsSoftDelete extends UnidataAbstractResponse implements Serializable {

    private final static long serialVersionUID = 12345L;

    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected List<EtalonKey> etalonKeys;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected List<OriginKey> originKeys;

    /**
     * Gets the value of the etalonKeys property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the etalonKeys property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEtalonKeys().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EtalonKey }
     * 
     * 
     */
    public List<EtalonKey> getEtalonKeys() {
        if (etalonKeys == null) {
            etalonKeys = new ArrayList<EtalonKey>();
        }
        return this.etalonKeys;
    }

    /**
     * Gets the value of the originKeys property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the originKeys property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOriginKeys().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OriginKey }
     * 
     * 
     */
    public List<OriginKey> getOriginKeys() {
        if (originKeys == null) {
            originKeys = new ArrayList<OriginKey>();
        }
        return this.originKeys;
    }

    public ResponseRelationsSoftDelete withEtalonKeys(EtalonKey... values) {
        if (values!= null) {
            for (EtalonKey value: values) {
                getEtalonKeys().add(value);
            }
        }
        return this;
    }

    public ResponseRelationsSoftDelete withEtalonKeys(Collection<EtalonKey> values) {
        if (values!= null) {
            getEtalonKeys().addAll(values);
        }
        return this;
    }

    public ResponseRelationsSoftDelete withOriginKeys(OriginKey... values) {
        if (values!= null) {
            for (OriginKey value: values) {
                getOriginKeys().add(value);
            }
        }
        return this;
    }

    public ResponseRelationsSoftDelete withOriginKeys(Collection<OriginKey> values) {
        if (values!= null) {
            getOriginKeys().addAll(values);
        }
        return this;
    }

}
