package org.unidata.mdm.api;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import static org.unidata.mdm.DataNamespace.DATA_API_NAMESPACE;


/**
 * 
 * Структура любого API ответа. Состоит из
 * - обязательной секции 'common', содержащий стандартный набор общих данных, таких как код ошибки, итд
 * - одного из конкретных ответов
 *             
 * 
 * <p>Java class for UnidataResponseBody complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UnidataResponseBody"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="common" type="{http://api.mdm.unidata.org/}CommonResponseDef"/&gt;
 *         &lt;choice minOccurs="0"&gt;
 *           &lt;element name="responseAuthenticate" type="{http://api.mdm.unidata.org/}ResponseAuthenticate"/&gt;
 *           &lt;element name="responseGetLookupValues" type="{http://api.mdm.unidata.org/}ResponseGetLookupValues"/&gt;
 *           &lt;element name="responseCleanse" type="{http://api.mdm.unidata.org/}ResponseCleanse"/&gt;
 *           &lt;element name="responseUpsert" type="{http://api.mdm.unidata.org/}ResponseUpsert"/&gt;
 *           &lt;element name="responseRelationsUpsert" type="{http://api.mdm.unidata.org/}ResponseRelationsUpsert"/&gt;
 *           &lt;element name="responseMerge" type="{http://api.mdm.unidata.org/}ResponseMerge"/&gt;
 *           &lt;element name="responseJoin" type="{http://api.mdm.unidata.org/}ResponseJoin"/&gt;
 *           &lt;element name="responseSoftDelete" type="{http://api.mdm.unidata.org/}ResponseSoftDelete"/&gt;
 *           &lt;element name="responseRelationsSoftDelete" type="{http://api.mdm.unidata.org/}ResponseRelationsSoftDelete"/&gt;
 *           &lt;element name="responseGet" type="{http://api.mdm.unidata.org/}ResponseGet"/&gt;
 *           &lt;element name="responseRelationsGet" type="{http://api.mdm.unidata.org/}ResponseRelationsGet"/&gt;
 *           &lt;element name="responseSearch" type="{http://api.mdm.unidata.org/}ResponseSearch"/&gt;
 *           &lt;element name="responseGetDataQualityErrors" type="{http://api.mdm.unidata.org/}ResponseGetDataQualityErrors"/&gt;
 *           &lt;element name="responseInfoGet" type="{http://api.mdm.unidata.org/}ResponseInfoGet"/&gt;
 *           &lt;element name="responseBulkUpsert" type="{http://api.mdm.unidata.org/}ResponseBulkUpsert"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@JacksonXmlRootElement(localName = "apiResponseBody", namespace = DATA_API_NAMESPACE)
@JsonPropertyOrder({"common","responseAuthenticate","responseGetLookupValues","responseCleanse","responseUpsert",
    "responseRelationsUpsert","responseMerge","responseJoin","responseSoftDelete","responseRelationsSoftDelete",
    "responseGet","responseRelationsGet","responseSearch","responseGetDataQualityErrors","responseInfoGet","responseBulkUpsert"})
public class UnidataResponseBody implements Serializable {

    private final static long serialVersionUID = 12345L;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected CommonResponseDef common;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected ResponseAuthenticate responseAuthenticate;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected ResponseGetLookupValues responseGetLookupValues;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected ResponseCleanse responseCleanse;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected ResponseUpsert responseUpsert;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected ResponseRelationsUpsert responseRelationsUpsert;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected ResponseMerge responseMerge;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected ResponseJoin responseJoin;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected ResponseSoftDelete responseSoftDelete;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected ResponseRelationsSoftDelete responseRelationsSoftDelete;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected ResponseGet responseGet;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected ResponseRelationsGet responseRelationsGet;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected ResponseSearch responseSearch;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected ResponseGetDataQualityErrors responseGetDataQualityErrors;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected ResponseInfoGet responseInfoGet;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected ResponseBulkUpsert responseBulkUpsert;

    /**
     * Gets the value of the common property.
     * 
     * @return
     *     possible object is
     *     {@link CommonResponseDef }
     *     
     */
    public CommonResponseDef getCommon() {
        return common;
    }

    /**
     * Sets the value of the common property.
     * 
     * @param value
     *     allowed object is
     *     {@link CommonResponseDef }
     *     
     */
    public void setCommon(CommonResponseDef value) {
        this.common = value;
    }

    /**
     * Gets the value of the responseAuthenticate property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseAuthenticate }
     *     
     */
    public ResponseAuthenticate getResponseAuthenticate() {
        return responseAuthenticate;
    }

    /**
     * Sets the value of the responseAuthenticate property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseAuthenticate }
     *     
     */
    public void setResponseAuthenticate(ResponseAuthenticate value) {
        this.responseAuthenticate = value;
    }

    /**
     * Gets the value of the responseGetLookupValues property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseGetLookupValues }
     *     
     */
    public ResponseGetLookupValues getResponseGetLookupValues() {
        return responseGetLookupValues;
    }

    /**
     * Sets the value of the responseGetLookupValues property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseGetLookupValues }
     *     
     */
    public void setResponseGetLookupValues(ResponseGetLookupValues value) {
        this.responseGetLookupValues = value;
    }

    /**
     * Gets the value of the responseCleanse property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseCleanse }
     *     
     */
    public ResponseCleanse getResponseCleanse() {
        return responseCleanse;
    }

    /**
     * Sets the value of the responseCleanse property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseCleanse }
     *     
     */
    public void setResponseCleanse(ResponseCleanse value) {
        this.responseCleanse = value;
    }

    /**
     * Gets the value of the responseUpsert property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseUpsert }
     *     
     */
    public ResponseUpsert getResponseUpsert() {
        return responseUpsert;
    }

    /**
     * Sets the value of the responseUpsert property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseUpsert }
     *     
     */
    public void setResponseUpsert(ResponseUpsert value) {
        this.responseUpsert = value;
    }

    /**
     * Gets the value of the responseRelationsUpsert property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseRelationsUpsert }
     *     
     */
    public ResponseRelationsUpsert getResponseRelationsUpsert() {
        return responseRelationsUpsert;
    }

    /**
     * Sets the value of the responseRelationsUpsert property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseRelationsUpsert }
     *     
     */
    public void setResponseRelationsUpsert(ResponseRelationsUpsert value) {
        this.responseRelationsUpsert = value;
    }

    /**
     * Gets the value of the responseMerge property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseMerge }
     *     
     */
    public ResponseMerge getResponseMerge() {
        return responseMerge;
    }

    /**
     * Sets the value of the responseMerge property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseMerge }
     *     
     */
    public void setResponseMerge(ResponseMerge value) {
        this.responseMerge = value;
    }

    /**
     * Gets the value of the responseJoin property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseJoin }
     *     
     */
    public ResponseJoin getResponseJoin() {
        return responseJoin;
    }

    /**
     * Sets the value of the responseJoin property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseJoin }
     *     
     */
    public void setResponseJoin(ResponseJoin value) {
        this.responseJoin = value;
    }

    /**
     * Gets the value of the responseSoftDelete property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseSoftDelete }
     *     
     */
    public ResponseSoftDelete getResponseSoftDelete() {
        return responseSoftDelete;
    }

    /**
     * Sets the value of the responseSoftDelete property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseSoftDelete }
     *     
     */
    public void setResponseSoftDelete(ResponseSoftDelete value) {
        this.responseSoftDelete = value;
    }

    /**
     * Gets the value of the responseRelationsSoftDelete property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseRelationsSoftDelete }
     *     
     */
    public ResponseRelationsSoftDelete getResponseRelationsSoftDelete() {
        return responseRelationsSoftDelete;
    }

    /**
     * Sets the value of the responseRelationsSoftDelete property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseRelationsSoftDelete }
     *     
     */
    public void setResponseRelationsSoftDelete(ResponseRelationsSoftDelete value) {
        this.responseRelationsSoftDelete = value;
    }

    /**
     * Gets the value of the responseGet property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseGet }
     *     
     */
    public ResponseGet getResponseGet() {
        return responseGet;
    }

    /**
     * Sets the value of the responseGet property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseGet }
     *     
     */
    public void setResponseGet(ResponseGet value) {
        this.responseGet = value;
    }

    /**
     * Gets the value of the responseRelationsGet property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseRelationsGet }
     *     
     */
    public ResponseRelationsGet getResponseRelationsGet() {
        return responseRelationsGet;
    }

    /**
     * Sets the value of the responseRelationsGet property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseRelationsGet }
     *     
     */
    public void setResponseRelationsGet(ResponseRelationsGet value) {
        this.responseRelationsGet = value;
    }

    /**
     * Gets the value of the responseSearch property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseSearch }
     *     
     */
    public ResponseSearch getResponseSearch() {
        return responseSearch;
    }

    /**
     * Sets the value of the responseSearch property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseSearch }
     *     
     */
    public void setResponseSearch(ResponseSearch value) {
        this.responseSearch = value;
    }

    /**
     * Gets the value of the responseGetDataQualityErrors property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseGetDataQualityErrors }
     *     
     */
    public ResponseGetDataQualityErrors getResponseGetDataQualityErrors() {
        return responseGetDataQualityErrors;
    }

    /**
     * Sets the value of the responseGetDataQualityErrors property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseGetDataQualityErrors }
     *     
     */
    public void setResponseGetDataQualityErrors(ResponseGetDataQualityErrors value) {
        this.responseGetDataQualityErrors = value;
    }

    /**
     * Gets the value of the responseInfoGet property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseInfoGet }
     *     
     */
    public ResponseInfoGet getResponseInfoGet() {
        return responseInfoGet;
    }

    /**
     * Sets the value of the responseInfoGet property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseInfoGet }
     *     
     */
    public void setResponseInfoGet(ResponseInfoGet value) {
        this.responseInfoGet = value;
    }

    /**
     * Gets the value of the responseBulkUpsert property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseBulkUpsert }
     *     
     */
    public ResponseBulkUpsert getResponseBulkUpsert() {
        return responseBulkUpsert;
    }

    /**
     * Sets the value of the responseBulkUpsert property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseBulkUpsert }
     *     
     */
    public void setResponseBulkUpsert(ResponseBulkUpsert value) {
        this.responseBulkUpsert = value;
    }

    public UnidataResponseBody withCommon(CommonResponseDef value) {
        setCommon(value);
        return this;
    }

    public UnidataResponseBody withResponseAuthenticate(ResponseAuthenticate value) {
        setResponseAuthenticate(value);
        return this;
    }

    public UnidataResponseBody withResponseGetLookupValues(ResponseGetLookupValues value) {
        setResponseGetLookupValues(value);
        return this;
    }

    public UnidataResponseBody withResponseCleanse(ResponseCleanse value) {
        setResponseCleanse(value);
        return this;
    }

    public UnidataResponseBody withResponseUpsert(ResponseUpsert value) {
        setResponseUpsert(value);
        return this;
    }

    public UnidataResponseBody withResponseRelationsUpsert(ResponseRelationsUpsert value) {
        setResponseRelationsUpsert(value);
        return this;
    }

    public UnidataResponseBody withResponseMerge(ResponseMerge value) {
        setResponseMerge(value);
        return this;
    }

    public UnidataResponseBody withResponseJoin(ResponseJoin value) {
        setResponseJoin(value);
        return this;
    }

    public UnidataResponseBody withResponseSoftDelete(ResponseSoftDelete value) {
        setResponseSoftDelete(value);
        return this;
    }

    public UnidataResponseBody withResponseRelationsSoftDelete(ResponseRelationsSoftDelete value) {
        setResponseRelationsSoftDelete(value);
        return this;
    }

    public UnidataResponseBody withResponseGet(ResponseGet value) {
        setResponseGet(value);
        return this;
    }

    public UnidataResponseBody withResponseRelationsGet(ResponseRelationsGet value) {
        setResponseRelationsGet(value);
        return this;
    }

    public UnidataResponseBody withResponseSearch(ResponseSearch value) {
        setResponseSearch(value);
        return this;
    }

    public UnidataResponseBody withResponseGetDataQualityErrors(ResponseGetDataQualityErrors value) {
        setResponseGetDataQualityErrors(value);
        return this;
    }

    public UnidataResponseBody withResponseInfoGet(ResponseInfoGet value) {
        setResponseInfoGet(value);
        return this;
    }

    public UnidataResponseBody withResponseBulkUpsert(ResponseBulkUpsert value) {
        setResponseBulkUpsert(value);
        return this;
    }

}
