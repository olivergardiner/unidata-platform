package org.unidata.mdm.api;

import java.io.Serializable;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import static org.unidata.mdm.DataNamespace.DATA_API_NAMESPACE;


/**
 * 
 * Общая часть любого API ответа:
 * - Всегда содержит идентификатор логической операции, либо изначально переданный в запросе, либо авто-сгенерированный платформой.
 * - Всегда содержит 'exitCode', который должен использоваться вызывающей стороной для анализа ошибок
 * - Может содержать несколько элементов с сообщениями. В случае 'exitCode' отличного от 'Success', будет как минимум один такой элемент
 *             
 * 
 * <p>Java class for CommonResponseDef complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CommonResponseDef"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice minOccurs="0"&gt;
 *         &lt;element name="message" type="{http://api.mdm.unidata.org/}ExecutionMessageDef"/&gt;
 *       &lt;/choice&gt;
 *       &lt;attribute name="version" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="platform" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="operationId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="exitCode" use="required" type="{http://api.mdm.unidata.org/}ExitCodeType" /&gt;
 *       &lt;attribute name="processingTime" use="required" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
public class CommonResponseDef implements Serializable {

    private final static long serialVersionUID = 12345L;

    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected ExecutionMessageDef message;
    @JacksonXmlProperty(isAttribute = true)
    protected String version;
    @JacksonXmlProperty(isAttribute = true)
    protected String platform;
    @JacksonXmlProperty(isAttribute = true)
    protected String operationId;
    @JacksonXmlProperty(isAttribute = true)
    protected ExitCodeType exitCode;
    @JacksonXmlProperty(isAttribute = true)
    protected int processingTime;

    /**
     * Gets the value of the message property.
     * 
     * @return
     *     possible object is
     *     {@link ExecutionMessageDef }
     *     
     */
    public ExecutionMessageDef getMessage() {
        return message;
    }

    /**
     * Sets the value of the message property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExecutionMessageDef }
     *     
     */
    public void setMessage(ExecutionMessageDef value) {
        this.message = value;
    }

    /**
     * Gets the value of the version property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * Gets the value of the platform property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlatform() {
        return platform;
    }

    /**
     * Sets the value of the platform property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlatform(String value) {
        this.platform = value;
    }

    /**
     * Gets the value of the operationId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperationId() {
        return operationId;
    }

    /**
     * Sets the value of the operationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperationId(String value) {
        this.operationId = value;
    }

    /**
     * Gets the value of the exitCode property.
     * 
     * @return
     *     possible object is
     *     {@link ExitCodeType }
     *     
     */
    public ExitCodeType getExitCode() {
        return exitCode;
    }

    /**
     * Sets the value of the exitCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExitCodeType }
     *     
     */
    public void setExitCode(ExitCodeType value) {
        this.exitCode = value;
    }

    /**
     * Gets the value of the processingTime property.
     * 
     */
    public int getProcessingTime() {
        return processingTime;
    }

    /**
     * Sets the value of the processingTime property.
     * 
     */
    public void setProcessingTime(int value) {
        this.processingTime = value;
    }

    public CommonResponseDef withMessage(ExecutionMessageDef value) {
        setMessage(value);
        return this;
    }

    public CommonResponseDef withVersion(String value) {
        setVersion(value);
        return this;
    }

    public CommonResponseDef withPlatform(String value) {
        setPlatform(value);
        return this;
    }

    public CommonResponseDef withOperationId(String value) {
        setOperationId(value);
        return this;
    }

    public CommonResponseDef withExitCode(ExitCodeType value) {
        setExitCode(value);
        return this;
    }

    public CommonResponseDef withProcessingTime(int value) {
        setProcessingTime(value);
        return this;
    }

}
