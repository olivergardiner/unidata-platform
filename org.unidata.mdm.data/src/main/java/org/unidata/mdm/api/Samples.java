package org.unidata.mdm.api;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import org.unidata.mdm.data.EtalonKey;
import org.unidata.mdm.data.OriginKey;

import static org.unidata.mdm.DataNamespace.DATA_API_NAMESPACE;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice maxOccurs="unbounded"&gt;
 *         &lt;element name="sample" type="{http://api.mdm.unidata.org/}SampleDef"/&gt;
 *         &lt;element name="sampleJMSMessage" type="{http://api.mdm.unidata.org/}UnidataMessageDef"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@JacksonXmlRootElement(localName = "samples", namespace = DATA_API_NAMESPACE)
public class Samples implements Serializable
{

    private final static long serialVersionUID = 12345L;
//    @XmlElements({
//        @XmlElement(name = "sample", type = SampleDef.class),
//        @XmlElement(name = "sampleJMSMessage", type = UnidataMessageDef.class)
//    })
    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.WRAPPER_OBJECT)
    @JsonSubTypes({
        @JsonSubTypes.Type(value = SampleDef.class, name = "sample"),
        @JsonSubTypes.Type(value = UnidataMessageDef.class, name = "sampleJMSMessage")
    })
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected List<Serializable> sampleOrSampleJMSMessage;

    /**
     * Gets the value of the sampleOrSampleJMSMessage property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sampleOrSampleJMSMessage property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSampleOrSampleJMSMessage().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SampleDef }
     * {@link UnidataMessageDef }
     * 
     * 
     */
    public List<Serializable> getSampleOrSampleJMSMessage() {
        if (sampleOrSampleJMSMessage == null) {
            sampleOrSampleJMSMessage = new ArrayList<Serializable>();
        }
        return this.sampleOrSampleJMSMessage;
    }

    public Samples withSampleOrSampleJMSMessage(Serializable... values) {
        if (values!= null) {
            for (Serializable value: values) {
                getSampleOrSampleJMSMessage().add(value);
            }
        }
        return this;
    }

    public Samples withSampleOrSampleJMSMessage(Collection<Serializable> values) {
        if (values!= null) {
            getSampleOrSampleJMSMessage().addAll(values);
        }
        return this;
    }

}
