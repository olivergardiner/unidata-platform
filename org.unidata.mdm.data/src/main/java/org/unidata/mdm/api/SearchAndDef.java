package org.unidata.mdm.api;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import static org.unidata.mdm.DataNamespace.DATA_API_NAMESPACE;


/**
 * 
 * And element.
 *             
 * 
 * <p>Java class for SearchAndDef complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchAndDef"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://api.mdm.unidata.org/}SearchBaseDef"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice maxOccurs="unbounded"&gt;
 *           &lt;element name="and" type="{http://api.mdm.unidata.org/}SearchAndDef"/&gt;
 *           &lt;element name="or" type="{http://api.mdm.unidata.org/}SearchOrDef"/&gt;
 *           &lt;element name="atom" type="{http://api.mdm.unidata.org/}SearchAtomDef"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
public class SearchAndDef extends SearchBaseDef implements Serializable {

    private final static long serialVersionUID = 12345L;
//    @XmlElements({
//        @XmlElement(name = "and", type = SearchAndDef.class),
//        @XmlElement(name = "or", type = SearchOrDef.class),
//        @XmlElement(name = "atom", type = SearchAtomDef.class)
//    }
    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.WRAPPER_OBJECT)
    @JsonSubTypes({
        @JsonSubTypes.Type(value = SearchAndDef.class, name = "and"),
        @JsonSubTypes.Type(value = SearchOrDef.class, name = "or"),
        @JsonSubTypes.Type(value = SearchAtomDef.class, name = "atom")
    })
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected List<SearchBaseDef> expressions;

    /**
     * Gets the value of the expressions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the expressions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExpressions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchAndDef }
     * {@link SearchOrDef }
     * {@link SearchAtomDef }
     * 
     * 
     */
    public List<SearchBaseDef> getExpressions() {
        if (expressions == null) {
            expressions = new ArrayList<SearchBaseDef>();
        }
        return this.expressions;
    }

    public SearchAndDef withExpressions(SearchBaseDef... values) {
        if (values!= null) {
            for (SearchBaseDef value: values) {
                getExpressions().add(value);
            }
        }
        return this;
    }

    public SearchAndDef withExpressions(Collection<SearchBaseDef> values) {
        if (values!= null) {
            getExpressions().addAll(values);
        }
        return this;
    }

}
