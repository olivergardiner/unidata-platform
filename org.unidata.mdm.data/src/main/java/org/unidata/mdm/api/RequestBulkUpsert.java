package org.unidata.mdm.api;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import static org.unidata.mdm.DataNamespace.DATA_API_NAMESPACE;


/**
 * 
 *                 Запрос на множественную вставку или модификацию основной или исходной записи сущности.
 *                 Документацию о единичной вставке в ('RequestUpsert')
 *            
 * 
 * <p>Java class for RequestBulkUpsert complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RequestBulkUpsert"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://api.mdm.unidata.org/}UnidataAbstractRequest"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="upsertRecordRequests" type="{http://api.mdm.unidata.org/}RequestUpsert" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
public class RequestBulkUpsert extends UnidataAbstractRequest implements Serializable {

    private final static long serialVersionUID = 12345L;

    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected List<RequestUpsert> upsertRecordRequests;

    /**
     * Gets the value of the upsertRecordRequests property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the upsertRecordRequests property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUpsertRecordRequests().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RequestUpsert }
     * 
     * 
     */
    public List<RequestUpsert> getUpsertRecordRequests() {
        if (upsertRecordRequests == null) {
            upsertRecordRequests = new ArrayList<RequestUpsert>();
        }
        return this.upsertRecordRequests;
    }

    public RequestBulkUpsert withUpsertRecordRequests(RequestUpsert... values) {
        if (values!= null) {
            for (RequestUpsert value: values) {
                getUpsertRecordRequests().add(value);
            }
        }
        return this;
    }

    public RequestBulkUpsert withUpsertRecordRequests(Collection<RequestUpsert> values) {
        if (values!= null) {
            getUpsertRecordRequests().addAll(values);
        }
        return this;
    }

}
