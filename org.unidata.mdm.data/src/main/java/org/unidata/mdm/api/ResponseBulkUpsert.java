package org.unidata.mdm.api;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import static org.unidata.mdm.DataNamespace.DATA_API_NAMESPACE;


/**
 * 
 *                 Ответ на запрос на множественную вставку или модификацию основной или исходной записи сущности ('BulkRequestUpsert').
 *                 Документацию об ответе при единичной вставке в ('ResponseUpsert')
 *             
 * 
 * <p>Java class for ResponseBulkUpsert complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ResponseBulkUpsert"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://api.mdm.unidata.org/}UnidataAbstractResponse"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="upsertRecordResponses" type="{http://api.mdm.unidata.org/}ResponseUpsert" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
public class ResponseBulkUpsert extends UnidataAbstractResponse implements Serializable {

    private final static long serialVersionUID = 12345L;

    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected List<ResponseUpsert> upsertRecordResponses;

    /**
     * Gets the value of the upsertRecordResponses property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the upsertRecordResponses property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUpsertRecordResponses().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ResponseUpsert }
     * 
     * 
     */
    public List<ResponseUpsert> getUpsertRecordResponses() {
        if (upsertRecordResponses == null) {
            upsertRecordResponses = new ArrayList<ResponseUpsert>();
        }
        return this.upsertRecordResponses;
    }

    public ResponseBulkUpsert withUpsertRecordResponses(ResponseUpsert... values) {
        if (values!= null) {
            for (ResponseUpsert value: values) {
                getUpsertRecordResponses().add(value);
            }
        }
        return this;
    }

    public ResponseBulkUpsert withUpsertRecordResponses(Collection<ResponseUpsert> values) {
        if (values!= null) {
            getUpsertRecordResponses().addAll(values);
        }
        return this;
    }

}
