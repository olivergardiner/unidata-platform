package org.unidata.mdm.api;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.unidata.mdm.data.EtalonKey;
import org.unidata.mdm.data.OriginKey;

import static org.unidata.mdm.DataNamespace.DATA_API_NAMESPACE;


/**
 * 
 * Структура, описывающая детали события по консолидации записей сущности. Всегда основную запись, в которую произошла консолидация, а также ключи всех проигравших записей
 *             
 * 
 * <p>Java class for MergeEventDetailsDef complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MergeEventDetailsDef"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="winningEtalonKey" type="{http://data.mdm.unidata.org/}EtalonKey"/&gt;
 *         &lt;element name="looserEtalonKey" type="{http://data.mdm.unidata.org/}EtalonKey" maxOccurs="unbounded"/&gt;
 *         &lt;element name="supplementaryKeys" type="{http://data.mdm.unidata.org/}OriginKey" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="operationId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
public class MergeEventDetailsDef implements Serializable {

    private final static long serialVersionUID = 12345L;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected EtalonKey winningEtalonKey;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected List<EtalonKey> looserEtalonKey;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected List<OriginKey> supplementaryKeys;
    @JacksonXmlProperty(isAttribute = true)
    protected String operationId;

    /**
     * Gets the value of the winningEtalonKey property.
     * 
     * @return
     *     possible object is
     *     {@link EtalonKey }
     *     
     */
    public EtalonKey getWinningEtalonKey() {
        return winningEtalonKey;
    }

    /**
     * Sets the value of the winningEtalonKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link EtalonKey }
     *     
     */
    public void setWinningEtalonKey(EtalonKey value) {
        this.winningEtalonKey = value;
    }

    /**
     * Gets the value of the looserEtalonKey property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the looserEtalonKey property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLooserEtalonKey().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EtalonKey }
     * 
     * 
     */
    public List<EtalonKey> getLooserEtalonKey() {
        if (looserEtalonKey == null) {
            looserEtalonKey = new ArrayList<EtalonKey>();
        }
        return this.looserEtalonKey;
    }

    /**
     * Gets the value of the supplementaryKeys property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the supplementaryKeys property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSupplementaryKeys().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OriginKey }
     * 
     * 
     */
    public List<OriginKey> getSupplementaryKeys() {
        if (supplementaryKeys == null) {
            supplementaryKeys = new ArrayList<OriginKey>();
        }
        return this.supplementaryKeys;
    }

    /**
     * Gets the value of the operationId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperationId() {
        return operationId;
    }

    /**
     * Sets the value of the operationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperationId(String value) {
        this.operationId = value;
    }

    public MergeEventDetailsDef withWinningEtalonKey(EtalonKey value) {
        setWinningEtalonKey(value);
        return this;
    }

    public MergeEventDetailsDef withLooserEtalonKey(EtalonKey... values) {
        if (values!= null) {
            for (EtalonKey value: values) {
                getLooserEtalonKey().add(value);
            }
        }
        return this;
    }

    public MergeEventDetailsDef withLooserEtalonKey(Collection<EtalonKey> values) {
        if (values!= null) {
            getLooserEtalonKey().addAll(values);
        }
        return this;
    }

    public MergeEventDetailsDef withSupplementaryKeys(OriginKey... values) {
        if (values!= null) {
            for (OriginKey value: values) {
                getSupplementaryKeys().add(value);
            }
        }
        return this;
    }

    public MergeEventDetailsDef withSupplementaryKeys(Collection<OriginKey> values) {
        if (values!= null) {
            getSupplementaryKeys().addAll(values);
        }
        return this;
    }

    public MergeEventDetailsDef withOperationId(String value) {
        setOperationId(value);
        return this;
    }

}
