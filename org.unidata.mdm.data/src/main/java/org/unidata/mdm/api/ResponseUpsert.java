package org.unidata.mdm.api;

import java.io.Serializable;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.unidata.mdm.data.EtalonKey;
import org.unidata.mdm.data.OriginKey;

import static org.unidata.mdm.DataNamespace.DATA_API_NAMESPACE;


/**
 * 
 *                 Ответ на запрос на вставку или модификацию основной или исходной записи сущности ('RequestUpsert').
 *                 Общая часть ответа содержит код возврата, идентификатор логической операции и сообщения об ошибках (смотри элемент 'common' из структуры 'UnidataResponseBody').
 *                 В случае успешного исполнения всегда содержит два ключа, один идентифицирующий исходную запись, фактически изменённую, второй идентифицирующий основную запись сущности.
 *                 Помимо этого возращается тип действия, фактически выполненного платформой - 'action'.
 *             
 * 
 * <p>Java class for ResponseUpsert complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ResponseUpsert"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://api.mdm.unidata.org/}UnidataAbstractResponse"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="originKey" type="{http://data.mdm.unidata.org/}OriginKey"/&gt;
 *         &lt;element name="etalonKey" type="{http://data.mdm.unidata.org/}EtalonKey"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="originAction" use="required" type="{http://api.mdm.unidata.org/}UpsertActionType" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
public class ResponseUpsert extends UnidataAbstractResponse implements Serializable {

    private final static long serialVersionUID = 12345L;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected OriginKey originKey;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected EtalonKey etalonKey;
    @JacksonXmlProperty(isAttribute = true)
    protected UpsertActionType originAction;

    /**
     * Gets the value of the originKey property.
     * 
     * @return
     *     possible object is
     *     {@link OriginKey }
     *     
     */
    public OriginKey getOriginKey() {
        return originKey;
    }

    /**
     * Sets the value of the originKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link OriginKey }
     *     
     */
    public void setOriginKey(OriginKey value) {
        this.originKey = value;
    }

    /**
     * Gets the value of the etalonKey property.
     * 
     * @return
     *     possible object is
     *     {@link EtalonKey }
     *     
     */
    public EtalonKey getEtalonKey() {
        return etalonKey;
    }

    /**
     * Sets the value of the etalonKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link EtalonKey }
     *     
     */
    public void setEtalonKey(EtalonKey value) {
        this.etalonKey = value;
    }

    /**
     * Gets the value of the originAction property.
     * 
     * @return
     *     possible object is
     *     {@link UpsertActionType }
     *     
     */
    public UpsertActionType getOriginAction() {
        return originAction;
    }

    /**
     * Sets the value of the originAction property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpsertActionType }
     *     
     */
    public void setOriginAction(UpsertActionType value) {
        this.originAction = value;
    }

    public ResponseUpsert withOriginKey(OriginKey value) {
        setOriginKey(value);
        return this;
    }

    public ResponseUpsert withEtalonKey(EtalonKey value) {
        setEtalonKey(value);
        return this;
    }

    public ResponseUpsert withOriginAction(UpsertActionType value) {
        setOriginAction(value);
        return this;
    }

}
