package org.unidata.mdm.api;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.unidata.mdm.data.DataQualityError;

import static org.unidata.mdm.DataNamespace.DATA_API_NAMESPACE;


/**
 * 
 * Структура любого сообщение возвращаемого вместе с ответом на запрос. Всегда содержит текст сообщения в поле 'messageText'. Также может содержать структуры,
 * описывающие ошибку, включая детальные сообщения от сервера
 *             
 * 
 * <p>Java class for ExecutionMessageDef complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ExecutionMessageDef"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="stackTrace" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="error" type="{http://api.mdm.unidata.org/}ExecutionErrorDef" minOccurs="0"/&gt;
 *         &lt;element name="dqErrors" type="{http://data.mdm.unidata.org/}DataQualityError" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/choice&gt;
 *       &lt;attribute name="messageText" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@JsonPropertyOrder({"stackTrace","error","dqErrors"})
public class ExecutionMessageDef implements Serializable {

    private final static long serialVersionUID = 12345L;

    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected String stackTrace;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected ExecutionErrorDef error;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected List<DataQualityError> dqErrors;
    @JacksonXmlProperty(isAttribute = true)
    protected String messageText;

    /**
     * Gets the value of the stackTrace property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStackTrace() {
        return stackTrace;
    }

    /**
     * Sets the value of the stackTrace property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStackTrace(String value) {
        this.stackTrace = value;
    }

    /**
     * Gets the value of the error property.
     * 
     * @return
     *     possible object is
     *     {@link ExecutionErrorDef }
     *     
     */
    public ExecutionErrorDef getError() {
        return error;
    }

    /**
     * Sets the value of the error property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExecutionErrorDef }
     *     
     */
    public void setError(ExecutionErrorDef value) {
        this.error = value;
    }

    /**
     * Gets the value of the dqErrors property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dqErrors property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDqErrors().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataQualityError }
     * 
     * 
     */
    public List<DataQualityError> getDqErrors() {
        if (dqErrors == null) {
            dqErrors = new ArrayList<DataQualityError>();
        }
        return this.dqErrors;
    }

    /**
     * Gets the value of the messageText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageText() {
        return messageText;
    }

    /**
     * Sets the value of the messageText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageText(String value) {
        this.messageText = value;
    }

    public ExecutionMessageDef withStackTrace(String value) {
        setStackTrace(value);
        return this;
    }

    public ExecutionMessageDef withError(ExecutionErrorDef value) {
        setError(value);
        return this;
    }

    public ExecutionMessageDef withDqErrors(DataQualityError... values) {
        if (values!= null) {
            for (DataQualityError value: values) {
                getDqErrors().add(value);
            }
        }
        return this;
    }

    public ExecutionMessageDef withDqErrors(Collection<DataQualityError> values) {
        if (values!= null) {
            getDqErrors().addAll(values);
        }
        return this;
    }

    public ExecutionMessageDef withMessageText(String value) {
        setMessageText(value);
        return this;
    }

}
