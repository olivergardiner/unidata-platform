package org.unidata.mdm.api;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.unidata.mdm.data.EtalonKey;
import org.unidata.mdm.data.EtalonRecord;
import org.unidata.mdm.data.OriginKey;

import static org.unidata.mdm.DataNamespace.DATA_API_NAMESPACE;


/**
 * 
 *                 Структура, описывающая детали события по удалению записи сущности. Всегда содержит ключи удаленных записей.
 *             
 * 
 * <p>Java class for SoftDeleteEventDetailsDef complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SoftDeleteEventDetailsDef"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="etalonKey" type="{http://data.mdm.unidata.org/}EtalonKey"/&gt;
 *         &lt;element name="originKey" type="{http://data.mdm.unidata.org/}OriginKey" minOccurs="0"/&gt;
 *         &lt;element name="etalonRecord" type="{http://data.mdm.unidata.org/}EtalonRecord" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="supplementaryKeys" type="{http://data.mdm.unidata.org/}OriginKey" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="SoftDeleteActionType" use="required" type="{http://api.mdm.unidata.org/}SoftDeleteActionType" /&gt;
 *       &lt;attribute name="operationId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
public class SoftDeleteEventDetailsDef implements Serializable {

    private final static long serialVersionUID = 12345L;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected EtalonKey etalonKey;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected OriginKey originKey;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected List<EtalonRecord> etalonRecord;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected List<OriginKey> supplementaryKeys;
    @JacksonXmlProperty(isAttribute = true)
    protected SoftDeleteActionType softDeleteActionType;
    @JacksonXmlProperty(isAttribute = true)
    protected String operationId;

    /**
     * Gets the value of the etalonKey property.
     * 
     * @return
     *     possible object is
     *     {@link EtalonKey }
     *     
     */
    public EtalonKey getEtalonKey() {
        return etalonKey;
    }

    /**
     * Sets the value of the etalonKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link EtalonKey }
     *     
     */
    public void setEtalonKey(EtalonKey value) {
        this.etalonKey = value;
    }

    /**
     * Gets the value of the originKey property.
     * 
     * @return
     *     possible object is
     *     {@link OriginKey }
     *     
     */
    public OriginKey getOriginKey() {
        return originKey;
    }

    /**
     * Sets the value of the originKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link OriginKey }
     *     
     */
    public void setOriginKey(OriginKey value) {
        this.originKey = value;
    }

    /**
     * Gets the value of the etalonRecord property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the etalonRecord property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEtalonRecord().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EtalonRecord }
     * 
     * 
     */
    public List<EtalonRecord> getEtalonRecord() {
        if (etalonRecord == null) {
            etalonRecord = new ArrayList<EtalonRecord>();
        }
        return this.etalonRecord;
    }

    /**
     * Gets the value of the supplementaryKeys property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the supplementaryKeys property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSupplementaryKeys().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OriginKey }
     * 
     * 
     */
    public List<OriginKey> getSupplementaryKeys() {
        if (supplementaryKeys == null) {
            supplementaryKeys = new ArrayList<OriginKey>();
        }
        return this.supplementaryKeys;
    }

    /**
     * Gets the value of the softDeleteActionType property.
     * 
     * @return
     *     possible object is
     *     {@link SoftDeleteActionType }
     *     
     */
    public SoftDeleteActionType getSoftDeleteActionType() {
        return softDeleteActionType;
    }

    /**
     * Sets the value of the softDeleteActionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link SoftDeleteActionType }
     *     
     */
    public void setSoftDeleteActionType(SoftDeleteActionType value) {
        this.softDeleteActionType = value;
    }

    /**
     * Gets the value of the operationId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperationId() {
        return operationId;
    }

    /**
     * Sets the value of the operationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperationId(String value) {
        this.operationId = value;
    }

    public SoftDeleteEventDetailsDef withEtalonKey(EtalonKey value) {
        setEtalonKey(value);
        return this;
    }

    public SoftDeleteEventDetailsDef withOriginKey(OriginKey value) {
        setOriginKey(value);
        return this;
    }

    public SoftDeleteEventDetailsDef withEtalonRecord(EtalonRecord... values) {
        if (values!= null) {
            for (EtalonRecord value: values) {
                getEtalonRecord().add(value);
            }
        }
        return this;
    }

    public SoftDeleteEventDetailsDef withEtalonRecord(Collection<EtalonRecord> values) {
        if (values!= null) {
            getEtalonRecord().addAll(values);
        }
        return this;
    }

    public SoftDeleteEventDetailsDef withSupplementaryKeys(OriginKey... values) {
        if (values!= null) {
            for (OriginKey value: values) {
                getSupplementaryKeys().add(value);
            }
        }
        return this;
    }

    public SoftDeleteEventDetailsDef withSupplementaryKeys(Collection<OriginKey> values) {
        if (values!= null) {
            getSupplementaryKeys().addAll(values);
        }
        return this;
    }

    public SoftDeleteEventDetailsDef withSoftDeleteActionType(SoftDeleteActionType value) {
        setSoftDeleteActionType(value);
        return this;
    }

    public SoftDeleteEventDetailsDef withOperationId(String value) {
        setOperationId(value);
        return this;
    }

}
