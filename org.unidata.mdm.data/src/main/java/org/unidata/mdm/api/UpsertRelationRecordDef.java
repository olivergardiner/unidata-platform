package org.unidata.mdm.api;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.unidata.mdm.data.IntegralRecord;
import org.unidata.mdm.data.RelationBase;
import org.unidata.mdm.data.RelationTo;

import static org.unidata.mdm.DataNamespace.DATA_API_NAMESPACE;


/**
 * Контейнер для одиночных связей.
 *
 *
 * <p>Java class for UpsertRelationRecordDef complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="UpsertRelationRecordDef"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="integralEntity" type="{http://data.mdm.unidata.org/}IntegralRecord"/&gt;
 *           &lt;element name="relationTo" type="{http://data.mdm.unidata.org/}RelationTo"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="range" type="{http://api.mdm.unidata.org/}TimeIntervalDef"/&gt;
 *         &lt;element name="referenceAliasKey" type="{http://api.mdm.unidata.org/}ReferenceAliasKey"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@JsonPropertyOrder({"record", "range", "referenceAliasKey"})
public class UpsertRelationRecordDef implements Serializable {

    private final static long serialVersionUID = 12345L;

    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.WRAPPER_OBJECT)
    @JsonSubTypes({
        @JsonSubTypes.Type(value = IntegralRecord.class, name = "integralEntity"),
        @JsonSubTypes.Type(value = RelationTo.class, name = "relationTo"),
    })
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected RelationBase record;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected TimeIntervalDef range;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected ReferenceAliasKey referenceAliasKey;

    /**
     * Gets the value of the record property.
     *
     * @return possible object is
     * {@link IntegralRecord }
     * {@link RelationTo }
     */
    public RelationBase getRecord() {
        return record;
    }

    /**
     * Sets the value of the record property.
     *
     * @param value allowed object is
     * {@link IntegralRecord }
     * {@link RelationTo }
     */
    public void setRecord(RelationBase value) {
        this.record = value;
    }

    /**
     * Gets the value of the range property.
     *
     * @return possible object is
     * {@link TimeIntervalDef }
     */
    public TimeIntervalDef getRange() {
        return range;
    }

    /**
     * Sets the value of the range property.
     *
     * @param value allowed object is
     * {@link TimeIntervalDef }
     */
    public void setRange(TimeIntervalDef value) {
        this.range = value;
    }

    /**
     * Gets the value of the referenceAliasKey property.
     *
     * @return possible object is
     * {@link ReferenceAliasKey }
     */
    public ReferenceAliasKey getReferenceAliasKey() {
        return referenceAliasKey;
    }

    /**
     * Sets the value of the referenceAliasKey property.
     *
     * @param value allowed object is
     * {@link ReferenceAliasKey }
     */
    public void setReferenceAliasKey(ReferenceAliasKey value) {
        this.referenceAliasKey = value;
    }

    public UpsertRelationRecordDef withRecord(RelationBase value) {
        setRecord(value);
        return this;
    }

    public UpsertRelationRecordDef withRange(TimeIntervalDef value) {
        setRange(value);
        return this;
    }

    public UpsertRelationRecordDef withReferenceAliasKey(ReferenceAliasKey value) {
        setReferenceAliasKey(value);
        return this;
    }

}
