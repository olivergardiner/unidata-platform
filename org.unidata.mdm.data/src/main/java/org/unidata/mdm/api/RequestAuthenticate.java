package org.unidata.mdm.api;

import java.io.Serializable;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;


/**
 * 
 * Запрос на аутентификацию пользователя.
 * Общая часть запроса должна содержать данные для аутентификации пользователя (смотри элемент 'common' из структуры 'UnidataRequestBody').
 * Есть возможность идентифицировать пользователя двумя способами. Первый всегда по имени пользователя и паролю (элемент 'credentials' общей секции).
 * Второй способ передать сессионый токен, если данная сессия всё ещё активна, то сервер 'обновит' сессию и вернёт ответ как будто были переданы правильные имя и пароль пользователя.
 * По умолчанию сервер всегда создаёт новую сессию. Если нужно только проверить имя пользователя и пароль или получить список ролей пользователя, то можно использовать режим doLogin='false'. При этом новая сессия не создаётся.
 * Ответ сервера, при успешной аутентификации, всегда содержит список ролей пользователя. Также, в случае создании новой сессии, ответ содержит сессионный токен, который может быть использован для исполнения последующих запросов.
 *             
 * 
 * <p>Java class for RequestAuthenticate complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RequestAuthenticate"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://api.mdm.unidata.org/}UnidataAbstractRequest"&gt;
 *       &lt;attribute name="doLogin" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
public class RequestAuthenticate extends UnidataAbstractRequest implements Serializable {

    private final static long serialVersionUID = 12345L;

    @JacksonXmlProperty(isAttribute = true)
    protected boolean doLogin;

    /**
     * Gets the value of the doLogin property.
     * 
     */
    public boolean isDoLogin() {
        return doLogin;
    }

    /**
     * Sets the value of the doLogin property.
     * 
     */
    public void setDoLogin(boolean value) {
        this.doLogin = value;
    }

    public RequestAuthenticate withDoLogin(boolean value) {
        setDoLogin(value);
        return this;
    }

}
