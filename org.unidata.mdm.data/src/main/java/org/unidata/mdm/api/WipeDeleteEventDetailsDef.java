package org.unidata.mdm.api;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.unidata.mdm.data.EtalonKey;
import org.unidata.mdm.data.EtalonRecord;
import org.unidata.mdm.data.OriginKey;

import static org.unidata.mdm.DataNamespace.DATA_API_NAMESPACE;


/**
 * 
 *                 Структура, описывающая детали события по физическому удалению записи сущности. Всегда содержит ключи удаленных записей.
 *             
 * 
 * <p>Java class for WipeDeleteEventDetailsDef complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WipeDeleteEventDetailsDef"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="etalonKey" type="{http://data.mdm.unidata.org/}EtalonKey"/&gt;
 *         &lt;element name="originKey" type="{http://data.mdm.unidata.org/}OriginKey" minOccurs="0"/&gt;
 *         &lt;element name="etalonRecord" type="{http://data.mdm.unidata.org/}EtalonRecord" minOccurs="0"/&gt;
 *         &lt;element name="supplementaryKeys" type="{http://data.mdm.unidata.org/}OriginKey" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="operationId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
public class WipeDeleteEventDetailsDef implements Serializable {

    private final static long serialVersionUID = 12345L;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected EtalonKey etalonKey;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected OriginKey originKey;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected EtalonRecord etalonRecord;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected List<OriginKey> supplementaryKeys;
    @JacksonXmlProperty(isAttribute = true)
    protected String operationId;

    /**
     * Gets the value of the etalonKey property.
     * 
     * @return
     *     possible object is
     *     {@link EtalonKey }
     *     
     */
    public EtalonKey getEtalonKey() {
        return etalonKey;
    }

    /**
     * Sets the value of the etalonKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link EtalonKey }
     *     
     */
    public void setEtalonKey(EtalonKey value) {
        this.etalonKey = value;
    }

    /**
     * Gets the value of the originKey property.
     * 
     * @return
     *     possible object is
     *     {@link OriginKey }
     *     
     */
    public OriginKey getOriginKey() {
        return originKey;
    }

    /**
     * Sets the value of the originKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link OriginKey }
     *     
     */
    public void setOriginKey(OriginKey value) {
        this.originKey = value;
    }

    /**
     * Gets the value of the etalonRecord property.
     * 
     * @return
     *     possible object is
     *     {@link EtalonRecord }
     *     
     */
    public EtalonRecord getEtalonRecord() {
        return etalonRecord;
    }

    /**
     * Sets the value of the etalonRecord property.
     * 
     * @param value
     *     allowed object is
     *     {@link EtalonRecord }
     *     
     */
    public void setEtalonRecord(EtalonRecord value) {
        this.etalonRecord = value;
    }

    /**
     * Gets the value of the supplementaryKeys property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the supplementaryKeys property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSupplementaryKeys().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OriginKey }
     * 
     * 
     */
    public List<OriginKey> getSupplementaryKeys() {
        if (supplementaryKeys == null) {
            supplementaryKeys = new ArrayList<OriginKey>();
        }
        return this.supplementaryKeys;
    }

    /**
     * Gets the value of the operationId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperationId() {
        return operationId;
    }

    /**
     * Sets the value of the operationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperationId(String value) {
        this.operationId = value;
    }

    public WipeDeleteEventDetailsDef withEtalonKey(EtalonKey value) {
        setEtalonKey(value);
        return this;
    }

    public WipeDeleteEventDetailsDef withOriginKey(OriginKey value) {
        setOriginKey(value);
        return this;
    }

    public WipeDeleteEventDetailsDef withEtalonRecord(EtalonRecord value) {
        setEtalonRecord(value);
        return this;
    }

    public WipeDeleteEventDetailsDef withSupplementaryKeys(OriginKey... values) {
        if (values!= null) {
            for (OriginKey value: values) {
                getSupplementaryKeys().add(value);
            }
        }
        return this;
    }

    public WipeDeleteEventDetailsDef withSupplementaryKeys(Collection<OriginKey> values) {
        if (values!= null) {
            getSupplementaryKeys().addAll(values);
        }
        return this;
    }

    public WipeDeleteEventDetailsDef withOperationId(String value) {
        setOperationId(value);
        return this;
    }

}
