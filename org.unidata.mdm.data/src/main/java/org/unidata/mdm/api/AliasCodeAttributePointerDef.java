package org.unidata.mdm.api;

import java.io.Serializable;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;


/**
 * 
 *                 Объет мапинга, необходимый для уведомления системы о том , что запись ссылается на альтернативный кодовый атрибут в справочнике.
 *                 'recordAttributeName' - имя атрибута ссылающегося на справочник. (если это вложенный атрибут, то имя должно разделяться точками)
 *                 'aliasCodeAttributeName' - имя альтернативного кодового атрибута справочника.
 *             
 * 
 * <p>Java class for AliasCodeAttributePointerDef complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AliasCodeAttributePointerDef"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="aliasCodeAttributeName" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="recordAttributeName" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 * 
 */
public class AliasCodeAttributePointerDef implements Serializable {

    private final static long serialVersionUID = 12345L;

    @JacksonXmlProperty(isAttribute = true)
    protected String aliasCodeAttributeName;
    @JacksonXmlProperty(isAttribute = true)
    protected String recordAttributeName;

    /**
     * Gets the value of the aliasCodeAttributeName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAliasCodeAttributeName() {
        return aliasCodeAttributeName;
    }

    /**
     * Sets the value of the aliasCodeAttributeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAliasCodeAttributeName(String value) {
        this.aliasCodeAttributeName = value;
    }

    /**
     * Gets the value of the recordAttributeName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecordAttributeName() {
        return recordAttributeName;
    }

    /**
     * Sets the value of the recordAttributeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecordAttributeName(String value) {
        this.recordAttributeName = value;
    }

    public AliasCodeAttributePointerDef withAliasCodeAttributeName(String value) {
        setAliasCodeAttributeName(value);
        return this;
    }

    public AliasCodeAttributePointerDef withRecordAttributeName(String value) {
        setRecordAttributeName(value);
        return this;
    }

}
