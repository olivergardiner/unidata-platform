package org.unidata.mdm.api;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.unidata.mdm.data.EtalonKey;
import org.unidata.mdm.data.OriginKey;

import static org.unidata.mdm.DataNamespace.DATA_API_NAMESPACE;


/**
 * 
 * Ответ на запрос на вставку или модификацию основной или исходной записи сущности ('RequestUpsert').
 * Общая часть ответа содержит код возврата, идентификатор логической операции и сообщения об ошибках (смотри элемент 'common' из структуры 'UnidataResponseBody').
 * В случае успешного исполнения всегда содержит два ключа, один идентифицирующий исходную запись, фактически изменённую, второй идентифицирующий основную запись сущности.
 * Помимо этого возращается тип действия, фактически выполненного платформой - 'action'.
 *             
 * 
 * <p>Java class for ResponseUpsertList complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ResponseUpsertList"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://api.mdm.unidata.org/}UnidataAbstractResponse"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="originKeys" type="{http://data.mdm.unidata.org/}OriginKey" maxOccurs="unbounded"/&gt;
 *           &lt;element name="etalonKeys" type="{http://data.mdm.unidata.org/}EtalonKey" maxOccurs="unbounded"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="originActions" type="{http://api.mdm.unidata.org/}UpsertActionType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@JsonPropertyOrder({"originKeys","etalonKeys","originActions"})
public class ResponseUpsertList extends UnidataAbstractResponse implements Serializable {

    private final static long serialVersionUID = 12345L;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected List<OriginKey> originKeys;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected List<EtalonKey> etalonKeys;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected List<UpsertActionType> originActions;

    /**
     * Gets the value of the originKeys property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the originKeys property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOriginKeys().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OriginKey }
     * 
     * 
     */
    public List<OriginKey> getOriginKeys() {
        if (originKeys == null) {
            originKeys = new ArrayList<OriginKey>();
        }
        return this.originKeys;
    }

    /**
     * Gets the value of the etalonKeys property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the etalonKeys property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEtalonKeys().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EtalonKey }
     * 
     * 
     */
    public List<EtalonKey> getEtalonKeys() {
        if (etalonKeys == null) {
            etalonKeys = new ArrayList<EtalonKey>();
        }
        return this.etalonKeys;
    }

    /**
     * Gets the value of the originActions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the originActions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOriginActions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UpsertActionType }
     * 
     * 
     */
    public List<UpsertActionType> getOriginActions() {
        if (originActions == null) {
            originActions = new ArrayList<UpsertActionType>();
        }
        return this.originActions;
    }

    public ResponseUpsertList withOriginKeys(OriginKey... values) {
        if (values!= null) {
            for (OriginKey value: values) {
                getOriginKeys().add(value);
            }
        }
        return this;
    }

    public ResponseUpsertList withOriginKeys(Collection<OriginKey> values) {
        if (values!= null) {
            getOriginKeys().addAll(values);
        }
        return this;
    }

    public ResponseUpsertList withEtalonKeys(EtalonKey... values) {
        if (values!= null) {
            for (EtalonKey value: values) {
                getEtalonKeys().add(value);
            }
        }
        return this;
    }

    public ResponseUpsertList withEtalonKeys(Collection<EtalonKey> values) {
        if (values!= null) {
            getEtalonKeys().addAll(values);
        }
        return this;
    }

    public ResponseUpsertList withOriginActions(UpsertActionType... values) {
        if (values!= null) {
            for (UpsertActionType value: values) {
                getOriginActions().add(value);
            }
        }
        return this;
    }

    public ResponseUpsertList withOriginActions(Collection<UpsertActionType> values) {
        if (values!= null) {
            getOriginActions().addAll(values);
        }
        return this;
    }

}
