package org.unidata.mdm.api;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * <p>Java class for RestoreActionType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RestoreActionType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="RestoreEtalon"/&gt;
 *     &lt;enumeration value="RestoreOrigin"/&gt;
 *     &lt;enumeration value="RestoreEtalonPeriod"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
public enum RestoreActionType {

    @JsonProperty("RestoreEtalon")
    RESTORE_ETALON("RestoreEtalon"),
    @JsonProperty("RestoreOrigin")
    RESTORE_ORIGIN("RestoreOrigin"),
    @JsonProperty("RestoreEtalonPeriod")
    RESTORE_ETALON_PERIOD("RestoreEtalonPeriod");
    private final String value;

    RestoreActionType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RestoreActionType fromValue(String v) {
        for (RestoreActionType c: RestoreActionType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
