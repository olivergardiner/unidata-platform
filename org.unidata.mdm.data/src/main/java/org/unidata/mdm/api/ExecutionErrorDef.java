package org.unidata.mdm.api;

import java.io.Serializable;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;


/**
 * 
 * Детальное описание ошибки исполнения запроса - включает код ошибки, сообщение для пользователя, а также техническое сообщение для службы поддержки
 *             
 * 
 * <p>Java class for ExecutionErrorDef complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ExecutionErrorDef"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="errorCode" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="userMessage" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="internalMessage" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
public class ExecutionErrorDef implements Serializable {

    private final static long serialVersionUID = 12345L;

    @JacksonXmlProperty(isAttribute = true)
    protected String errorCode;
    @JacksonXmlProperty(isAttribute = true)
    protected String userMessage;
    @JacksonXmlProperty(isAttribute = true)
    protected String internalMessage;

    /**
     * Gets the value of the errorCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorCode() {
        return errorCode;
    }

    /**
     * Sets the value of the errorCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorCode(String value) {
        this.errorCode = value;
    }

    /**
     * Gets the value of the userMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserMessage() {
        return userMessage;
    }

    /**
     * Sets the value of the userMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserMessage(String value) {
        this.userMessage = value;
    }

    /**
     * Gets the value of the internalMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInternalMessage() {
        return internalMessage;
    }

    /**
     * Sets the value of the internalMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInternalMessage(String value) {
        this.internalMessage = value;
    }

    public ExecutionErrorDef withErrorCode(String value) {
        setErrorCode(value);
        return this;
    }

    public ExecutionErrorDef withUserMessage(String value) {
        setUserMessage(value);
        return this;
    }

    public ExecutionErrorDef withInternalMessage(String value) {
        setInternalMessage(value);
        return this;
    }

}
