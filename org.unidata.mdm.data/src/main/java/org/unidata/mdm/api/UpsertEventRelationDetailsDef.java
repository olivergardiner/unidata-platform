package org.unidata.mdm.api;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import org.unidata.mdm.data.EtalonKey;
import org.unidata.mdm.data.IntegralRecord;
import org.unidata.mdm.data.OriginKey;
import org.unidata.mdm.data.RelationBase;
import org.unidata.mdm.data.RelationTo;

import static org.unidata.mdm.DataNamespace.DATA_API_NAMESPACE;


/**
 * 
 *                 Структура, описывающая детали события по добавлению объекта связи к записи.
 *             
 * 
 * <p>Java class for UpsertEventRelationDetailsDef complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UpsertEventRelationDetailsDef"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="integralEntity" type="{http://data.mdm.unidata.org/}IntegralRecord"/&gt;
 *           &lt;element name="relationTo" type="{http://data.mdm.unidata.org/}RelationTo"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="fromOriginKey" type="{http://data.mdm.unidata.org/}OriginKey" minOccurs="0"/&gt;
 *         &lt;element name="fromEtalonKey" type="{http://data.mdm.unidata.org/}EtalonKey"/&gt;
 *         &lt;element name="toOriginKey" type="{http://data.mdm.unidata.org/}OriginKey" minOccurs="0"/&gt;
 *         &lt;element name="toEtalonKey" type="{http://data.mdm.unidata.org/}EtalonKey"/&gt;
 *         &lt;element name="relationType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="upsertActionType" use="required" type="{http://api.mdm.unidata.org/}UpsertActionType" /&gt;
 *       &lt;attribute name="operationId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@JacksonXmlRootElement(namespace = DATA_API_NAMESPACE)
public class UpsertEventRelationDetailsDef implements Serializable {

    private final static long serialVersionUID = 12345L;

    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.WRAPPER_OBJECT)
    @JsonSubTypes({
        @JsonSubTypes.Type(value = IntegralRecord.class, name = "integralEntity"),
        @JsonSubTypes.Type(value = RelationTo.class, name = "relationTo"),
    })
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected RelationBase record;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected OriginKey fromOriginKey;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected EtalonKey fromEtalonKey;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected OriginKey toOriginKey;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected EtalonKey toEtalonKey;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected String relationType;
    @JacksonXmlProperty(isAttribute = true)
    protected UpsertActionType upsertActionType;
    @JacksonXmlProperty(isAttribute = true)
    protected String operationId;

    /**
     * Gets the value of the record property.
     * 
     * @return
     *     possible object is
     *     {@link IntegralRecord }
     *     {@link RelationTo }
     *     
     */
    public RelationBase getRecord() {
        return record;
    }

    /**
     * Sets the value of the record property.
     * 
     * @param value
     *     allowed object is
     *     {@link IntegralRecord }
     *     {@link RelationTo }
     *     
     */
    public void setRecord(RelationBase value) {
        this.record = value;
    }

    /**
     * Gets the value of the fromOriginKey property.
     * 
     * @return
     *     possible object is
     *     {@link OriginKey }
     *     
     */
    public OriginKey getFromOriginKey() {
        return fromOriginKey;
    }

    /**
     * Sets the value of the fromOriginKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link OriginKey }
     *     
     */
    public void setFromOriginKey(OriginKey value) {
        this.fromOriginKey = value;
    }

    /**
     * Gets the value of the fromEtalonKey property.
     * 
     * @return
     *     possible object is
     *     {@link EtalonKey }
     *     
     */
    public EtalonKey getFromEtalonKey() {
        return fromEtalonKey;
    }

    /**
     * Sets the value of the fromEtalonKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link EtalonKey }
     *     
     */
    public void setFromEtalonKey(EtalonKey value) {
        this.fromEtalonKey = value;
    }

    /**
     * Gets the value of the toOriginKey property.
     * 
     * @return
     *     possible object is
     *     {@link OriginKey }
     *     
     */
    public OriginKey getToOriginKey() {
        return toOriginKey;
    }

    /**
     * Sets the value of the toOriginKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link OriginKey }
     *     
     */
    public void setToOriginKey(OriginKey value) {
        this.toOriginKey = value;
    }

    /**
     * Gets the value of the toEtalonKey property.
     * 
     * @return
     *     possible object is
     *     {@link EtalonKey }
     *     
     */
    public EtalonKey getToEtalonKey() {
        return toEtalonKey;
    }

    /**
     * Sets the value of the toEtalonKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link EtalonKey }
     *     
     */
    public void setToEtalonKey(EtalonKey value) {
        this.toEtalonKey = value;
    }

    /**
     * Gets the value of the relationType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelationType() {
        return relationType;
    }

    /**
     * Sets the value of the relationType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelationType(String value) {
        this.relationType = value;
    }

    /**
     * Gets the value of the upsertActionType property.
     * 
     * @return
     *     possible object is
     *     {@link UpsertActionType }
     *     
     */
    public UpsertActionType getUpsertActionType() {
        return upsertActionType;
    }

    /**
     * Sets the value of the upsertActionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpsertActionType }
     *     
     */
    public void setUpsertActionType(UpsertActionType value) {
        this.upsertActionType = value;
    }

    /**
     * Gets the value of the operationId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperationId() {
        return operationId;
    }

    /**
     * Sets the value of the operationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperationId(String value) {
        this.operationId = value;
    }

    public UpsertEventRelationDetailsDef withRecord(RelationBase value) {
        setRecord(value);
        return this;
    }

    public UpsertEventRelationDetailsDef withFromOriginKey(OriginKey value) {
        setFromOriginKey(value);
        return this;
    }

    public UpsertEventRelationDetailsDef withFromEtalonKey(EtalonKey value) {
        setFromEtalonKey(value);
        return this;
    }

    public UpsertEventRelationDetailsDef withToOriginKey(OriginKey value) {
        setToOriginKey(value);
        return this;
    }

    public UpsertEventRelationDetailsDef withToEtalonKey(EtalonKey value) {
        setToEtalonKey(value);
        return this;
    }

    public UpsertEventRelationDetailsDef withRelationType(String value) {
        setRelationType(value);
        return this;
    }

    public UpsertEventRelationDetailsDef withUpsertActionType(UpsertActionType value) {
        setUpsertActionType(value);
        return this;
    }

    public UpsertEventRelationDetailsDef withOperationId(String value) {
        setOperationId(value);
        return this;
    }

}
