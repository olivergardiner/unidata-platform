package org.unidata.mdm.api;

import java.io.Serializable;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.unidata.mdm.data.EntityRelations;

import static org.unidata.mdm.DataNamespace.DATA_API_NAMESPACE;


/**
 * 
 * Ответ на запрос на получение основных и исходных записей связей для конкретной сущности.
 *             
 * 
 * <p>Java class for ResponseRelationsGet complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ResponseRelationsGet"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://api.mdm.unidata.org/}UnidataAbstractResponse"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="relations" type="{http://data.mdm.unidata.org/}EntityRelations"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
public class ResponseRelationsGet extends UnidataAbstractResponse implements Serializable {

    private final static long serialVersionUID = 12345L;

    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected EntityRelations relations;

    /**
     * Gets the value of the relations property.
     * 
     * @return
     *     possible object is
     *     {@link EntityRelations }
     *     
     */
    public EntityRelations getRelations() {
        return relations;
    }

    /**
     * Sets the value of the relations property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntityRelations }
     *     
     */
    public void setRelations(EntityRelations value) {
        this.relations = value;
    }

    public ResponseRelationsGet withRelations(EntityRelations value) {
        setRelations(value);
        return this;
    }

}
