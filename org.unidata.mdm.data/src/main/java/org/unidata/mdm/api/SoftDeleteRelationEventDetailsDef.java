package org.unidata.mdm.api;

import java.io.Serializable;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.unidata.mdm.data.EtalonKey;
import org.unidata.mdm.data.OriginKey;

import static org.unidata.mdm.DataNamespace.DATA_API_NAMESPACE;


/**
 * 
 *                 Структура, описывающая детали события по удалению связи.
 *             
 * 
 * <p>Java class for SoftDeleteRelationEventDetailsDef complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SoftDeleteRelationEventDetailsDef"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="fromOriginKey" type="{http://data.mdm.unidata.org/}OriginKey" minOccurs="0"/&gt;
 *         &lt;element name="fromEtalonKey" type="{http://data.mdm.unidata.org/}EtalonKey"/&gt;
 *         &lt;element name="toOriginKey" type="{http://data.mdm.unidata.org/}OriginKey" minOccurs="0"/&gt;
 *         &lt;element name="toEtalonKey" type="{http://data.mdm.unidata.org/}EtalonKey"/&gt;
 *         &lt;element name="relationType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="SoftDeleteActionType" use="required" type="{http://api.mdm.unidata.org/}SoftDeleteActionType" /&gt;
 *       &lt;attribute name="operationId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
public class SoftDeleteRelationEventDetailsDef implements Serializable {

    private final static long serialVersionUID = 12345L;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected OriginKey fromOriginKey;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected EtalonKey fromEtalonKey;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected OriginKey toOriginKey;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected EtalonKey toEtalonKey;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected String relationType;
    @JacksonXmlProperty(isAttribute = true)
    protected SoftDeleteActionType softDeleteActionType;
    @JacksonXmlProperty(isAttribute = true)
    protected String operationId;

    /**
     * Gets the value of the fromOriginKey property.
     * 
     * @return
     *     possible object is
     *     {@link OriginKey }
     *     
     */
    public OriginKey getFromOriginKey() {
        return fromOriginKey;
    }

    /**
     * Sets the value of the fromOriginKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link OriginKey }
     *     
     */
    public void setFromOriginKey(OriginKey value) {
        this.fromOriginKey = value;
    }

    /**
     * Gets the value of the fromEtalonKey property.
     * 
     * @return
     *     possible object is
     *     {@link EtalonKey }
     *     
     */
    public EtalonKey getFromEtalonKey() {
        return fromEtalonKey;
    }

    /**
     * Sets the value of the fromEtalonKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link EtalonKey }
     *     
     */
    public void setFromEtalonKey(EtalonKey value) {
        this.fromEtalonKey = value;
    }

    /**
     * Gets the value of the toOriginKey property.
     * 
     * @return
     *     possible object is
     *     {@link OriginKey }
     *     
     */
    public OriginKey getToOriginKey() {
        return toOriginKey;
    }

    /**
     * Sets the value of the toOriginKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link OriginKey }
     *     
     */
    public void setToOriginKey(OriginKey value) {
        this.toOriginKey = value;
    }

    /**
     * Gets the value of the toEtalonKey property.
     * 
     * @return
     *     possible object is
     *     {@link EtalonKey }
     *     
     */
    public EtalonKey getToEtalonKey() {
        return toEtalonKey;
    }

    /**
     * Sets the value of the toEtalonKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link EtalonKey }
     *     
     */
    public void setToEtalonKey(EtalonKey value) {
        this.toEtalonKey = value;
    }

    /**
     * Gets the value of the relationType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelationType() {
        return relationType;
    }

    /**
     * Sets the value of the relationType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelationType(String value) {
        this.relationType = value;
    }

    /**
     * Gets the value of the softDeleteActionType property.
     * 
     * @return
     *     possible object is
     *     {@link SoftDeleteActionType }
     *     
     */
    public SoftDeleteActionType getSoftDeleteActionType() {
        return softDeleteActionType;
    }

    /**
     * Sets the value of the softDeleteActionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link SoftDeleteActionType }
     *     
     */
    public void setSoftDeleteActionType(SoftDeleteActionType value) {
        this.softDeleteActionType = value;
    }

    /**
     * Gets the value of the operationId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperationId() {
        return operationId;
    }

    /**
     * Sets the value of the operationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperationId(String value) {
        this.operationId = value;
    }

    public SoftDeleteRelationEventDetailsDef withFromOriginKey(OriginKey value) {
        setFromOriginKey(value);
        return this;
    }

    public SoftDeleteRelationEventDetailsDef withFromEtalonKey(EtalonKey value) {
        setFromEtalonKey(value);
        return this;
    }

    public SoftDeleteRelationEventDetailsDef withToOriginKey(OriginKey value) {
        setToOriginKey(value);
        return this;
    }

    public SoftDeleteRelationEventDetailsDef withToEtalonKey(EtalonKey value) {
        setToEtalonKey(value);
        return this;
    }

    public SoftDeleteRelationEventDetailsDef withRelationType(String value) {
        setRelationType(value);
        return this;
    }

    public SoftDeleteRelationEventDetailsDef withSoftDeleteActionType(SoftDeleteActionType value) {
        setSoftDeleteActionType(value);
        return this;
    }

    public SoftDeleteRelationEventDetailsDef withOperationId(String value) {
        setOperationId(value);
        return this;
    }

}
