package org.unidata.mdm.api;

import java.io.Serializable;
import java.math.BigInteger;
import javax.xml.datatype.XMLGregorianCalendar;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import static org.unidata.mdm.DataNamespace.DATA_API_NAMESPACE;


/**
 * 
 * Запрос на поиск основных записей сущности.
 * Общая часть запроса должна содержать данные для аутентификации пользователя (смотри элемент 'common' из структуры 'UnidataRequestBody').
 * Вызывающая сторона должна задать условия поиска и сортировки ('searchCondition' и 'sortCondition'), а также параметры постраничного вывода результатов
 *             
 * 
 * <p>Java class for RequestSearch complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RequestSearch"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://api.mdm.unidata.org/}UnidataAbstractRequest"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="searchCondition" type="{http://api.mdm.unidata.org/}SearchConditionDef"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="entityName" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="returnCount" type="{http://www.w3.org/2001/XMLSchema}boolean" default="true" /&gt;
 *       &lt;attribute name="doCountOnly" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="pageSize" type="{http://www.w3.org/2001/XMLSchema}integer" default="10" /&gt;
 *       &lt;attribute name="pageNumber" type="{http://www.w3.org/2001/XMLSchema}integer" default="0" /&gt;
 *       &lt;attribute name="asOf" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
public class RequestSearch extends UnidataAbstractRequest implements Serializable {

    private final static long serialVersionUID = 12345L;

    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected SearchConditionDef searchCondition;
    @JacksonXmlProperty(isAttribute = true)
    protected String entityName;
    @JacksonXmlProperty(isAttribute = true)
    protected Boolean returnCount;
    @JacksonXmlProperty(isAttribute = true)
    protected Boolean doCountOnly;
    @JacksonXmlProperty(isAttribute = true)
    protected BigInteger pageSize;
    @JacksonXmlProperty(isAttribute = true)
    protected BigInteger pageNumber;
    @JacksonXmlProperty(isAttribute = true)
    protected XMLGregorianCalendar asOf;

    /**
     * Gets the value of the searchCondition property.
     * 
     * @return
     *     possible object is
     *     {@link SearchConditionDef }
     *     
     */
    public SearchConditionDef getSearchCondition() {
        return searchCondition;
    }

    /**
     * Sets the value of the searchCondition property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchConditionDef }
     *     
     */
    public void setSearchCondition(SearchConditionDef value) {
        this.searchCondition = value;
    }

    /**
     * Gets the value of the entityName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityName() {
        return entityName;
    }

    /**
     * Sets the value of the entityName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityName(String value) {
        this.entityName = value;
    }

    /**
     * Gets the value of the returnCount property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isReturnCount() {
        if (returnCount == null) {
            return true;
        } else {
            return returnCount;
        }
    }

    /**
     * Sets the value of the returnCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReturnCount(Boolean value) {
        this.returnCount = value;
    }

    /**
     * Gets the value of the doCountOnly property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isDoCountOnly() {
        if (doCountOnly == null) {
            return false;
        } else {
            return doCountOnly;
        }
    }

    /**
     * Sets the value of the doCountOnly property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDoCountOnly(Boolean value) {
        this.doCountOnly = value;
    }

    /**
     * Gets the value of the pageSize property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPageSize() {
        if (pageSize == null) {
            return new BigInteger("10");
        } else {
            return pageSize;
        }
    }

    /**
     * Sets the value of the pageSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPageSize(BigInteger value) {
        this.pageSize = value;
    }

    /**
     * Gets the value of the pageNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPageNumber() {
        if (pageNumber == null) {
            return new BigInteger("0");
        } else {
            return pageNumber;
        }
    }

    /**
     * Sets the value of the pageNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPageNumber(BigInteger value) {
        this.pageNumber = value;
    }

    /**
     * Gets the value of the asOf property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAsOf() {
        return asOf;
    }

    /**
     * Sets the value of the asOf property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAsOf(XMLGregorianCalendar value) {
        this.asOf = value;
    }

    public RequestSearch withSearchCondition(SearchConditionDef value) {
        setSearchCondition(value);
        return this;
    }

    public RequestSearch withEntityName(String value) {
        setEntityName(value);
        return this;
    }

    public RequestSearch withReturnCount(Boolean value) {
        setReturnCount(value);
        return this;
    }

    public RequestSearch withDoCountOnly(Boolean value) {
        setDoCountOnly(value);
        return this;
    }

    public RequestSearch withPageSize(BigInteger value) {
        setPageSize(value);
        return this;
    }

    public RequestSearch withPageNumber(BigInteger value) {
        setPageNumber(value);
        return this;
    }

    public RequestSearch withAsOf(XMLGregorianCalendar value) {
        setAsOf(value);
        return this;
    }

}
