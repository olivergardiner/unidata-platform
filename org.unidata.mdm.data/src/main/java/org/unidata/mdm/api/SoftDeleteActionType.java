package org.unidata.mdm.api;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * <p>Java class for SoftDeleteActionType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SoftDeleteActionType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="SoftDeleteEtalon"/&gt;
 *     &lt;enumeration value="SoftDeleteOrigin"/&gt;
 *     &lt;enumeration value="SoftDeleteEtalonPeriod"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
public enum SoftDeleteActionType {

    @JsonProperty("SoftDeleteEtalon")
    SOFT_DELETE_ETALON("SoftDeleteEtalon"),
    @JsonProperty("SoftDeleteOrigin")
    SOFT_DELETE_ORIGIN("SoftDeleteOrigin"),
    @JsonProperty("SoftDeleteEtalonPeriod")
    SOFT_DELETE_ETALON_PERIOD("SoftDeleteEtalonPeriod");

    private final String value;

    SoftDeleteActionType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SoftDeleteActionType fromValue(String v) {
        for (SoftDeleteActionType c: SoftDeleteActionType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
