package org.unidata.mdm.api;

import java.io.Serializable;


/**
 * 
 * Ответ на Запрос на добавление внешнего ID к cуществующему etalonId.
 * Ответ не содержит никаких специфических полей. Вызывающая сторона должна ориентироваться на код возврата из общей секции.
 *             
 * 
 * <p>Java class for ResponseJoin complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ResponseJoin"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://api.mdm.unidata.org/}UnidataAbstractResponse"&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
public class ResponseJoin extends UnidataAbstractResponse implements Serializable {

    private final static long serialVersionUID = 12345L;

}
