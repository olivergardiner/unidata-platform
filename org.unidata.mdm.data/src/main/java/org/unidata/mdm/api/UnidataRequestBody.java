package org.unidata.mdm.api;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import static org.unidata.mdm.DataNamespace.DATA_API_NAMESPACE;


/**
 * 
 * Структура любого API запроса. Состоит из
 * - обязательной секции 'common', содержащий стандартный набор общих параметров, таких как идентификатор хранилища, безопасность, итд
 * - одного из конкретных запросов
 *             
 * 
 * <p>Java class for UnidataRequestBody complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UnidataRequestBody"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="common" type="{http://api.mdm.unidata.org/}CommonSectionDef"/&gt;
 *         &lt;choice&gt;
 *           &lt;element name="requestAuthenticate" type="{http://api.mdm.unidata.org/}RequestAuthenticate"/&gt;
 *           &lt;element name="requestGetLookupValues" type="{http://api.mdm.unidata.org/}RequestGetLookupValues"/&gt;
 *           &lt;element name="requestCleanse" type="{http://api.mdm.unidata.org/}RequestCleanse"/&gt;
 *           &lt;element name="requestUpsert" type="{http://api.mdm.unidata.org/}RequestUpsert"/&gt;
 *           &lt;element name="requestRelationsUpsert" type="{http://api.mdm.unidata.org/}RequestRelationsUpsert"/&gt;
 *           &lt;element name="requestMerge" type="{http://api.mdm.unidata.org/}RequestMerge"/&gt;
 *           &lt;element name="requestJoin" type="{http://api.mdm.unidata.org/}RequestJoin"/&gt;
 *           &lt;element name="requestSoftDelete" type="{http://api.mdm.unidata.org/}RequestSoftDelete"/&gt;
 *           &lt;element name="requestRelationsSoftDelete" type="{http://api.mdm.unidata.org/}RequestRelationsSoftDelete"/&gt;
 *           &lt;element name="requestGet" type="{http://api.mdm.unidata.org/}RequestGet"/&gt;
 *           &lt;element name="requestRelationsGet" type="{http://api.mdm.unidata.org/}RequestRelationsGet"/&gt;
 *           &lt;element name="requestSearch" type="{http://api.mdm.unidata.org/}RequestSearch"/&gt;
 *           &lt;element name="requestGetDataQualityErrors" type="{http://api.mdm.unidata.org/}RequestGetDataQualityErrors"/&gt;
 *           &lt;element name="requestInfoGet" type="{http://api.mdm.unidata.org/}RequestInfoGet"/&gt;
 *           &lt;element name="requestBulkUpsert" type="{http://api.mdm.unidata.org/}RequestBulkUpsert"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@JacksonXmlRootElement(localName = "apiRequestBody", namespace = DATA_API_NAMESPACE)
@JsonPropertyOrder({"common","requestAuthenticate","requestGetLookupValues","requestCleanse","requestUpsert","requestRelationsUpsert",
    "requestMerge","requestJoin","requestSoftDelete","requestRelationsSoftDelete","requestGet","requestRelationsGet","requestSearch",
    "requestGetDataQualityErrors","requestInfoGet","requestBulkUpsert"})
public class UnidataRequestBody implements Serializable {

    private final static long serialVersionUID = 12345L;

    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected CommonSectionDef common;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected RequestAuthenticate requestAuthenticate;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected RequestGetLookupValues requestGetLookupValues;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected RequestCleanse requestCleanse;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected RequestUpsert requestUpsert;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected RequestRelationsUpsert requestRelationsUpsert;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected RequestMerge requestMerge;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected RequestJoin requestJoin;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected RequestSoftDelete requestSoftDelete;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected RequestRelationsSoftDelete requestRelationsSoftDelete;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected RequestGet requestGet;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected RequestRelationsGet requestRelationsGet;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected RequestSearch requestSearch;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected RequestGetDataQualityErrors requestGetDataQualityErrors;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected RequestInfoGet requestInfoGet;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected RequestBulkUpsert requestBulkUpsert;

    /**
     * Gets the value of the common property.
     * 
     * @return
     *     possible object is
     *     {@link CommonSectionDef }
     *     
     */
    public CommonSectionDef getCommon() {
        return common;
    }

    /**
     * Sets the value of the common property.
     * 
     * @param value
     *     allowed object is
     *     {@link CommonSectionDef }
     *     
     */
    public void setCommon(CommonSectionDef value) {
        this.common = value;
    }

    /**
     * Gets the value of the requestAuthenticate property.
     * 
     * @return
     *     possible object is
     *     {@link RequestAuthenticate }
     *     
     */
    public RequestAuthenticate getRequestAuthenticate() {
        return requestAuthenticate;
    }

    /**
     * Sets the value of the requestAuthenticate property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestAuthenticate }
     *     
     */
    public void setRequestAuthenticate(RequestAuthenticate value) {
        this.requestAuthenticate = value;
    }

    /**
     * Gets the value of the requestGetLookupValues property.
     * 
     * @return
     *     possible object is
     *     {@link RequestGetLookupValues }
     *     
     */
    public RequestGetLookupValues getRequestGetLookupValues() {
        return requestGetLookupValues;
    }

    /**
     * Sets the value of the requestGetLookupValues property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestGetLookupValues }
     *     
     */
    public void setRequestGetLookupValues(RequestGetLookupValues value) {
        this.requestGetLookupValues = value;
    }

    /**
     * Gets the value of the requestCleanse property.
     * 
     * @return
     *     possible object is
     *     {@link RequestCleanse }
     *     
     */
    public RequestCleanse getRequestCleanse() {
        return requestCleanse;
    }

    /**
     * Sets the value of the requestCleanse property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestCleanse }
     *     
     */
    public void setRequestCleanse(RequestCleanse value) {
        this.requestCleanse = value;
    }

    /**
     * Gets the value of the requestUpsert property.
     * 
     * @return
     *     possible object is
     *     {@link RequestUpsert }
     *     
     */
    public RequestUpsert getRequestUpsert() {
        return requestUpsert;
    }

    /**
     * Sets the value of the requestUpsert property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestUpsert }
     *     
     */
    public void setRequestUpsert(RequestUpsert value) {
        this.requestUpsert = value;
    }

    /**
     * Gets the value of the requestRelationsUpsert property.
     * 
     * @return
     *     possible object is
     *     {@link RequestRelationsUpsert }
     *     
     */
    public RequestRelationsUpsert getRequestRelationsUpsert() {
        return requestRelationsUpsert;
    }

    /**
     * Sets the value of the requestRelationsUpsert property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestRelationsUpsert }
     *     
     */
    public void setRequestRelationsUpsert(RequestRelationsUpsert value) {
        this.requestRelationsUpsert = value;
    }

    /**
     * Gets the value of the requestMerge property.
     * 
     * @return
     *     possible object is
     *     {@link RequestMerge }
     *     
     */
    public RequestMerge getRequestMerge() {
        return requestMerge;
    }

    /**
     * Sets the value of the requestMerge property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestMerge }
     *     
     */
    public void setRequestMerge(RequestMerge value) {
        this.requestMerge = value;
    }

    /**
     * Gets the value of the requestJoin property.
     * 
     * @return
     *     possible object is
     *     {@link RequestJoin }
     *     
     */
    public RequestJoin getRequestJoin() {
        return requestJoin;
    }

    /**
     * Sets the value of the requestJoin property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestJoin }
     *     
     */
    public void setRequestJoin(RequestJoin value) {
        this.requestJoin = value;
    }

    /**
     * Gets the value of the requestSoftDelete property.
     * 
     * @return
     *     possible object is
     *     {@link RequestSoftDelete }
     *     
     */
    public RequestSoftDelete getRequestSoftDelete() {
        return requestSoftDelete;
    }

    /**
     * Sets the value of the requestSoftDelete property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestSoftDelete }
     *     
     */
    public void setRequestSoftDelete(RequestSoftDelete value) {
        this.requestSoftDelete = value;
    }

    /**
     * Gets the value of the requestRelationsSoftDelete property.
     * 
     * @return
     *     possible object is
     *     {@link RequestRelationsSoftDelete }
     *     
     */
    public RequestRelationsSoftDelete getRequestRelationsSoftDelete() {
        return requestRelationsSoftDelete;
    }

    /**
     * Sets the value of the requestRelationsSoftDelete property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestRelationsSoftDelete }
     *     
     */
    public void setRequestRelationsSoftDelete(RequestRelationsSoftDelete value) {
        this.requestRelationsSoftDelete = value;
    }

    /**
     * Gets the value of the requestGet property.
     * 
     * @return
     *     possible object is
     *     {@link RequestGet }
     *     
     */
    public RequestGet getRequestGet() {
        return requestGet;
    }

    /**
     * Sets the value of the requestGet property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestGet }
     *     
     */
    public void setRequestGet(RequestGet value) {
        this.requestGet = value;
    }

    /**
     * Gets the value of the requestRelationsGet property.
     * 
     * @return
     *     possible object is
     *     {@link RequestRelationsGet }
     *     
     */
    public RequestRelationsGet getRequestRelationsGet() {
        return requestRelationsGet;
    }

    /**
     * Sets the value of the requestRelationsGet property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestRelationsGet }
     *     
     */
    public void setRequestRelationsGet(RequestRelationsGet value) {
        this.requestRelationsGet = value;
    }

    /**
     * Gets the value of the requestSearch property.
     * 
     * @return
     *     possible object is
     *     {@link RequestSearch }
     *     
     */
    public RequestSearch getRequestSearch() {
        return requestSearch;
    }

    /**
     * Sets the value of the requestSearch property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestSearch }
     *     
     */
    public void setRequestSearch(RequestSearch value) {
        this.requestSearch = value;
    }

    /**
     * Gets the value of the requestGetDataQualityErrors property.
     * 
     * @return
     *     possible object is
     *     {@link RequestGetDataQualityErrors }
     *     
     */
    public RequestGetDataQualityErrors getRequestGetDataQualityErrors() {
        return requestGetDataQualityErrors;
    }

    /**
     * Sets the value of the requestGetDataQualityErrors property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestGetDataQualityErrors }
     *     
     */
    public void setRequestGetDataQualityErrors(RequestGetDataQualityErrors value) {
        this.requestGetDataQualityErrors = value;
    }

    /**
     * Gets the value of the requestInfoGet property.
     * 
     * @return
     *     possible object is
     *     {@link RequestInfoGet }
     *     
     */
    public RequestInfoGet getRequestInfoGet() {
        return requestInfoGet;
    }

    /**
     * Sets the value of the requestInfoGet property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestInfoGet }
     *     
     */
    public void setRequestInfoGet(RequestInfoGet value) {
        this.requestInfoGet = value;
    }

    /**
     * Gets the value of the requestBulkUpsert property.
     * 
     * @return
     *     possible object is
     *     {@link RequestBulkUpsert }
     *     
     */
    public RequestBulkUpsert getRequestBulkUpsert() {
        return requestBulkUpsert;
    }

    /**
     * Sets the value of the requestBulkUpsert property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestBulkUpsert }
     *     
     */
    public void setRequestBulkUpsert(RequestBulkUpsert value) {
        this.requestBulkUpsert = value;
    }

    public UnidataRequestBody withCommon(CommonSectionDef value) {
        setCommon(value);
        return this;
    }

    public UnidataRequestBody withRequestAuthenticate(RequestAuthenticate value) {
        setRequestAuthenticate(value);
        return this;
    }

    public UnidataRequestBody withRequestGetLookupValues(RequestGetLookupValues value) {
        setRequestGetLookupValues(value);
        return this;
    }

    public UnidataRequestBody withRequestCleanse(RequestCleanse value) {
        setRequestCleanse(value);
        return this;
    }

    public UnidataRequestBody withRequestUpsert(RequestUpsert value) {
        setRequestUpsert(value);
        return this;
    }

    public UnidataRequestBody withRequestRelationsUpsert(RequestRelationsUpsert value) {
        setRequestRelationsUpsert(value);
        return this;
    }

    public UnidataRequestBody withRequestMerge(RequestMerge value) {
        setRequestMerge(value);
        return this;
    }

    public UnidataRequestBody withRequestJoin(RequestJoin value) {
        setRequestJoin(value);
        return this;
    }

    public UnidataRequestBody withRequestSoftDelete(RequestSoftDelete value) {
        setRequestSoftDelete(value);
        return this;
    }

    public UnidataRequestBody withRequestRelationsSoftDelete(RequestRelationsSoftDelete value) {
        setRequestRelationsSoftDelete(value);
        return this;
    }

    public UnidataRequestBody withRequestGet(RequestGet value) {
        setRequestGet(value);
        return this;
    }

    public UnidataRequestBody withRequestRelationsGet(RequestRelationsGet value) {
        setRequestRelationsGet(value);
        return this;
    }

    public UnidataRequestBody withRequestSearch(RequestSearch value) {
        setRequestSearch(value);
        return this;
    }

    public UnidataRequestBody withRequestGetDataQualityErrors(RequestGetDataQualityErrors value) {
        setRequestGetDataQualityErrors(value);
        return this;
    }

    public UnidataRequestBody withRequestInfoGet(RequestInfoGet value) {
        setRequestInfoGet(value);
        return this;
    }

    public UnidataRequestBody withRequestBulkUpsert(RequestBulkUpsert value) {
        setRequestBulkUpsert(value);
        return this;
    }

}
