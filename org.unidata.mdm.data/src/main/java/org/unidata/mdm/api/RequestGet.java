package org.unidata.mdm.api;

import java.io.Serializable;
import javax.xml.datatype.XMLGregorianCalendar;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.unidata.mdm.data.EtalonKey;
import org.unidata.mdm.data.OriginKey;

import static org.unidata.mdm.DataNamespace.DATA_API_NAMESPACE;


/**
 * 
 * Запрос на получение основных и исходных записей для конкретной сущности, а также исторических деталей.
 * Общая часть запроса должна содержать данные для аутентификации пользователя (смотри элемент 'common' из структуры 'UnidataRequestBody').
 * Запрос всегда содержит имя сущности, ключ идентифицирующий либо основную либо исходную запись, а также ряд параметров, указывающих что именно нужно вернуть.
 * По умолчанию запрос всегда возвращает текущее значение основной записи, но вызывающая сторона может указать значение даты ('forDate'), на которое нужно вернуть основную запись. Данный функционал полезен при работе с временными диапазонами.
 *             
 * 
 * <p>Java class for RequestGet complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RequestGet"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://api.mdm.unidata.org/}UnidataAbstractRequest"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="etalonKey" type="{http://data.mdm.unidata.org/}EtalonKey"/&gt;
 *           &lt;element name="originKey" type="{http://data.mdm.unidata.org/}OriginKey"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="entityName" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="asOf" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&gt;
 *       &lt;attribute name="originsAsOf" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="etalonHistory" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="softDeleted" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
public class RequestGet extends UnidataAbstractRequest implements Serializable {

    private final static long serialVersionUID = 12345L;

    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected EtalonKey etalonKey;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected OriginKey originKey;
    @JacksonXmlProperty(isAttribute = true)
    protected String entityName;
    @JacksonXmlProperty(isAttribute = true)
    protected XMLGregorianCalendar asOf;
    @JacksonXmlProperty(isAttribute = true)
    protected boolean originsAsOf;
    @JacksonXmlProperty(isAttribute = true)
    protected boolean etalonHistory;
    @JacksonXmlProperty(isAttribute = true)
    protected boolean softDeleted;

    /**
     * Gets the value of the etalonKey property.
     * 
     * @return
     *     possible object is
     *     {@link EtalonKey }
     *     
     */
    public EtalonKey getEtalonKey() {
        return etalonKey;
    }

    /**
     * Sets the value of the etalonKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link EtalonKey }
     *     
     */
    public void setEtalonKey(EtalonKey value) {
        this.etalonKey = value;
    }

    /**
     * Gets the value of the originKey property.
     * 
     * @return
     *     possible object is
     *     {@link OriginKey }
     *     
     */
    public OriginKey getOriginKey() {
        return originKey;
    }

    /**
     * Sets the value of the originKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link OriginKey }
     *     
     */
    public void setOriginKey(OriginKey value) {
        this.originKey = value;
    }

    /**
     * Gets the value of the entityName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityName() {
        return entityName;
    }

    /**
     * Sets the value of the entityName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityName(String value) {
        this.entityName = value;
    }

    /**
     * Gets the value of the asOf property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAsOf() {
        return asOf;
    }

    /**
     * Sets the value of the asOf property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAsOf(XMLGregorianCalendar value) {
        this.asOf = value;
    }

    /**
     * Gets the value of the originsAsOf property.
     * 
     */
    public boolean isOriginsAsOf() {
        return originsAsOf;
    }

    /**
     * Sets the value of the originsAsOf property.
     * 
     */
    public void setOriginsAsOf(boolean value) {
        this.originsAsOf = value;
    }

    /**
     * Gets the value of the etalonHistory property.
     * 
     */
    public boolean isEtalonHistory() {
        return etalonHistory;
    }

    /**
     * Sets the value of the etalonHistory property.
     * 
     */
    public void setEtalonHistory(boolean value) {
        this.etalonHistory = value;
    }

    /**
     * Gets the value of the softDeleted property.
     * 
     */
    public boolean isSoftDeleted() {
        return softDeleted;
    }

    /**
     * Sets the value of the softDeleted property.
     * 
     */
    public void setSoftDeleted(boolean value) {
        this.softDeleted = value;
    }

    public RequestGet withEtalonKey(EtalonKey value) {
        setEtalonKey(value);
        return this;
    }

    public RequestGet withOriginKey(OriginKey value) {
        setOriginKey(value);
        return this;
    }

    public RequestGet withEntityName(String value) {
        setEntityName(value);
        return this;
    }

    public RequestGet withAsOf(XMLGregorianCalendar value) {
        setAsOf(value);
        return this;
    }

    public RequestGet withOriginsAsOf(boolean value) {
        setOriginsAsOf(value);
        return this;
    }

    public RequestGet withEtalonHistory(boolean value) {
        setEtalonHistory(value);
        return this;
    }

    public RequestGet withSoftDeleted(boolean value) {
        setSoftDeleted(value);
        return this;
    }

}
