package org.unidata.mdm.api;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import static org.unidata.mdm.DataNamespace.DATA_API_NAMESPACE;


/**
 * 
 * Запрос на получение деталей справочника.
 * Общая часть запроса должна содержать данные для аутентификации пользователя (смотри элемент 'common' из структуры 'UnidataRequestBody').
 * Можно запрашивать как все записи справочника, так и конкретную запись идентифицированную значением кода.
 * Для всех справочников всегда определён кодовый атрибут, содержащий уникальные идентификаторы, например трёхбуквенный код странны для справочника стран
 *             
 * 
 * <p>Java class for RequestGetLookupValues complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RequestGetLookupValues"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://api.mdm.unidata.org/}UnidataAbstractRequest"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="lookupEntity" type="{http://api.mdm.unidata.org/}LookupEntityRefDef" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
public class RequestGetLookupValues extends UnidataAbstractRequest implements Serializable {

    private final static long serialVersionUID = 12345L;

    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected List<LookupEntityRefDef> lookupEntity;

    /**
     * Gets the value of the lookupEntity property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the lookupEntity property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLookupEntity().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LookupEntityRefDef }
     * 
     * 
     */
    public List<LookupEntityRefDef> getLookupEntity() {
        if (lookupEntity == null) {
            lookupEntity = new ArrayList<LookupEntityRefDef>();
        }
        return this.lookupEntity;
    }

    public RequestGetLookupValues withLookupEntity(LookupEntityRefDef... values) {
        if (values!= null) {
            for (LookupEntityRefDef value: values) {
                getLookupEntity().add(value);
            }
        }
        return this;
    }

    public RequestGetLookupValues withLookupEntity(Collection<LookupEntityRefDef> values) {
        if (values!= null) {
            getLookupEntity().addAll(values);
        }
        return this;
    }

}
