package org.unidata.mdm.api;

import java.io.Serializable;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;


/**
 * 
 *                 Объет мапинга, необходимый для уведомления системы о том , что связь ссылается на альтернативный ключ в реестре.
 *                 Используется в только с RelationTo объектами.
 *             
 * 
 * <p>Java class for ReferenceAliasKey complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReferenceAliasKey"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="value" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="entityAttributeName" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
public class ReferenceAliasKey implements Serializable {

    private final static long serialVersionUID = 12345L;
    @JacksonXmlProperty(isAttribute = true)
    protected String value;
    @JacksonXmlProperty(isAttribute = true)
    protected String entityAttributeName;

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets the value of the entityAttributeName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityAttributeName() {
        return entityAttributeName;
    }

    /**
     * Sets the value of the entityAttributeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityAttributeName(String value) {
        this.entityAttributeName = value;
    }

    public ReferenceAliasKey withValue(String value) {
        setValue(value);
        return this;
    }

    public ReferenceAliasKey withEntityAttributeName(String value) {
        setEntityAttributeName(value);
        return this;
    }

}
