package org.unidata.mdm.api;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import static org.unidata.mdm.DataNamespace.DATA_API_NAMESPACE;


/**
 * 
 * Общая часть любого API запроса. Содержащий данные для аутентификации пользователя. Есть возможность работать в двух режимах.
 * Первый всегда передавать имя пользователя и пароль (элемент 'credentials'). При этом серверная сторона будет всегда создавать новую сессию со всеми вытекающими издержками.
 * Второй способ передавать сессионый токен, который может быть получен после успешного исполнения сервиса RequestAuthenticate в режиме doLogin='true'. Это рекомендованный подход, когда вызывающая сторона заведомо знает о необходимости выполнить последовательность API запросов
 *             
 * 
 * <p>Java class for SecuritySectionDef complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SecuritySectionDef"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="credentials" type="{http://api.mdm.unidata.org/}CredentialsDef"/&gt;
 *         &lt;element name="sessionToken" type="{http://api.mdm.unidata.org/}SessionTokenDef"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@JsonPropertyOrder({"credentials","sessionToken"})
public class SecuritySectionDef implements Serializable
{

    private final static long serialVersionUID = 12345L;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected CredentialsDef credentials;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected SessionTokenDef sessionToken;

    /**
     * Gets the value of the credentials property.
     * 
     * @return
     *     possible object is
     *     {@link CredentialsDef }
     *     
     */
    public CredentialsDef getCredentials() {
        return credentials;
    }

    /**
     * Sets the value of the credentials property.
     * 
     * @param value
     *     allowed object is
     *     {@link CredentialsDef }
     *     
     */
    public void setCredentials(CredentialsDef value) {
        this.credentials = value;
    }

    /**
     * Gets the value of the sessionToken property.
     * 
     * @return
     *     possible object is
     *     {@link SessionTokenDef }
     *     
     */
    public SessionTokenDef getSessionToken() {
        return sessionToken;
    }

    /**
     * Sets the value of the sessionToken property.
     * 
     * @param value
     *     allowed object is
     *     {@link SessionTokenDef }
     *     
     */
    public void setSessionToken(SessionTokenDef value) {
        this.sessionToken = value;
    }

    public SecuritySectionDef withCredentials(CredentialsDef value) {
        setCredentials(value);
        return this;
    }

    public SecuritySectionDef withSessionToken(SessionTokenDef value) {
        setSessionToken(value);
        return this;
    }

}
