package org.unidata.mdm.api;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.unidata.mdm.data.EtalonKey;
import org.unidata.mdm.data.EtalonRecord;
import org.unidata.mdm.data.OriginKey;

import static org.unidata.mdm.DataNamespace.DATA_API_NAMESPACE;


/**
 * 
 *                 Структура, описывающая детали события по востановлению записи сущности. Всегда содержит ключ востановленной записи и ее название.
 *             
 * 
 * <p>Java class for RestoreEventDetailsDef complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RestoreEventDetailsDef"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="etalonKey" type="{http://data.mdm.unidata.org/}EtalonKey"/&gt;
 *         &lt;element name="etalonRecord" type="{http://data.mdm.unidata.org/}EtalonRecord" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="supplementaryKeys" type="{http://data.mdm.unidata.org/}OriginKey" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="RestoreActionType" use="required" type="{http://api.mdm.unidata.org/}RestoreActionType" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
public class RestoreEventDetailsDef implements Serializable {

    private final static long serialVersionUID = 12345L;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected EtalonKey etalonKey;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected List<EtalonRecord> etalonRecord;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected List<OriginKey> supplementaryKeys;
    @JacksonXmlProperty(isAttribute = true)
    protected RestoreActionType restoreActionType;

    /**
     * Gets the value of the etalonKey property.
     * 
     * @return
     *     possible object is
     *     {@link EtalonKey }
     *     
     */
    public EtalonKey getEtalonKey() {
        return etalonKey;
    }

    /**
     * Sets the value of the etalonKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link EtalonKey }
     *     
     */
    public void setEtalonKey(EtalonKey value) {
        this.etalonKey = value;
    }

    /**
     * Gets the value of the etalonRecord property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the etalonRecord property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEtalonRecord().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EtalonRecord }
     * 
     * 
     */
    public List<EtalonRecord> getEtalonRecord() {
        if (etalonRecord == null) {
            etalonRecord = new ArrayList<EtalonRecord>();
        }
        return this.etalonRecord;
    }

    /**
     * Gets the value of the supplementaryKeys property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the supplementaryKeys property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSupplementaryKeys().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OriginKey }
     * 
     * 
     */
    public List<OriginKey> getSupplementaryKeys() {
        if (supplementaryKeys == null) {
            supplementaryKeys = new ArrayList<OriginKey>();
        }
        return this.supplementaryKeys;
    }

    /**
     * Gets the value of the restoreActionType property.
     * 
     * @return
     *     possible object is
     *     {@link RestoreActionType }
     *     
     */
    public RestoreActionType getRestoreActionType() {
        return restoreActionType;
    }

    /**
     * Sets the value of the restoreActionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link RestoreActionType }
     *     
     */
    public void setRestoreActionType(RestoreActionType value) {
        this.restoreActionType = value;
    }

    public RestoreEventDetailsDef withEtalonKey(EtalonKey value) {
        setEtalonKey(value);
        return this;
    }

    public RestoreEventDetailsDef withEtalonRecord(EtalonRecord... values) {
        if (values!= null) {
            for (EtalonRecord value: values) {
                getEtalonRecord().add(value);
            }
        }
        return this;
    }

    public RestoreEventDetailsDef withEtalonRecord(Collection<EtalonRecord> values) {
        if (values!= null) {
            getEtalonRecord().addAll(values);
        }
        return this;
    }

    public RestoreEventDetailsDef withSupplementaryKeys(OriginKey... values) {
        if (values!= null) {
            for (OriginKey value: values) {
                getSupplementaryKeys().add(value);
            }
        }
        return this;
    }

    public RestoreEventDetailsDef withSupplementaryKeys(Collection<OriginKey> values) {
        if (values!= null) {
            getSupplementaryKeys().addAll(values);
        }
        return this;
    }

    public RestoreEventDetailsDef withRestoreActionType(RestoreActionType value) {
        setRestoreActionType(value);
        return this;
    }

}
