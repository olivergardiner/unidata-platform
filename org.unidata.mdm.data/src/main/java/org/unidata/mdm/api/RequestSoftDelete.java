package org.unidata.mdm.api;

import java.io.Serializable;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.unidata.mdm.data.EtalonKey;
import org.unidata.mdm.data.OriginKey;

import static org.unidata.mdm.DataNamespace.DATA_API_NAMESPACE;


/**
 * 
 *                 Запрос на логическое удаление основной записи сущности, исходной записи сущности или ее отдельного
 *                 периода.
 *                 Общая часть запроса должна содержать данные для аутентификации пользователя (смотри элемент 'common' из
 *                 структуры 'UnidataRequestBody').
 *                 Логическое удаление позволяет пометить либо исходную запись из конкретной системы источника, либо
 *                 основную запись, как логически удалённую, при этом такие записи выпадают из обычных операции, таких как
 *                 поиск.
 *                 При логическом удалении одной исходной записи из конкретной системы источника, основная запись может
 *                 продолжать оставаться активной, если в её составе есть другие активные исходные записи.
 * 
 *                 Поле range указывает границы действия НЕАКТИВНОЙ (удаленной) версии данных для указанного источника,
 *                 которая будет создана в результате запроса, если речь идет о деактивации периода.
 *                 Отсутствующие значения означают бесконечность в прошлом или будущем или их комбинацию.
 *                 Тип удаления указывается в поле actionType.
 *                 При наличии прав запрос может содержать значение true в поле wipe. В этом случае запись будет ФИЗИЧЕСКИ
 *                 и БЕЗВОЗВРАТНО удалена из хранилищи (hard delete).
 *             
 * 
 * <p>Java class for RequestSoftDelete complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RequestSoftDelete"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://api.mdm.unidata.org/}UnidataAbstractRequest"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="etalonKey" type="{http://data.mdm.unidata.org/}EtalonKey"/&gt;
 *           &lt;element name="originKey" type="{http://data.mdm.unidata.org/}OriginKey"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="range" type="{http://api.mdm.unidata.org/}TimeIntervalDef" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="actionType" use="required" type="{http://api.mdm.unidata.org/}SoftDeleteActionType" /&gt;
 *       &lt;attribute name="wipe" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
public class RequestSoftDelete extends UnidataAbstractRequest implements Serializable {

    private final static long serialVersionUID = 12345L;

    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected EtalonKey etalonKey;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected OriginKey originKey;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected TimeIntervalDef range;
    @JacksonXmlProperty(isAttribute = true)
    protected SoftDeleteActionType actionType;
    @JacksonXmlProperty(isAttribute = true)
    protected Boolean wipe;

    /**
     * Gets the value of the etalonKey property.
     * 
     * @return
     *     possible object is
     *     {@link EtalonKey }
     *     
     */
    public EtalonKey getEtalonKey() {
        return etalonKey;
    }

    /**
     * Sets the value of the etalonKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link EtalonKey }
     *     
     */
    public void setEtalonKey(EtalonKey value) {
        this.etalonKey = value;
    }

    /**
     * Gets the value of the originKey property.
     * 
     * @return
     *     possible object is
     *     {@link OriginKey }
     *     
     */
    public OriginKey getOriginKey() {
        return originKey;
    }

    /**
     * Sets the value of the originKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link OriginKey }
     *     
     */
    public void setOriginKey(OriginKey value) {
        this.originKey = value;
    }

    /**
     * Gets the value of the range property.
     * 
     * @return
     *     possible object is
     *     {@link TimeIntervalDef }
     *     
     */
    public TimeIntervalDef getRange() {
        return range;
    }

    /**
     * Sets the value of the range property.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeIntervalDef }
     *     
     */
    public void setRange(TimeIntervalDef value) {
        this.range = value;
    }

    /**
     * Gets the value of the actionType property.
     * 
     * @return
     *     possible object is
     *     {@link SoftDeleteActionType }
     *     
     */
    public SoftDeleteActionType getActionType() {
        return actionType;
    }

    /**
     * Sets the value of the actionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link SoftDeleteActionType }
     *     
     */
    public void setActionType(SoftDeleteActionType value) {
        this.actionType = value;
    }

    /**
     * Gets the value of the wipe property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isWipe() {
        return wipe;
    }

    /**
     * Sets the value of the wipe property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setWipe(Boolean value) {
        this.wipe = value;
    }

    public RequestSoftDelete withEtalonKey(EtalonKey value) {
        setEtalonKey(value);
        return this;
    }

    public RequestSoftDelete withOriginKey(OriginKey value) {
        setOriginKey(value);
        return this;
    }

    public RequestSoftDelete withRange(TimeIntervalDef value) {
        setRange(value);
        return this;
    }

    public RequestSoftDelete withActionType(SoftDeleteActionType value) {
        setActionType(value);
        return this;
    }

    public RequestSoftDelete withWipe(Boolean value) {
        setWipe(value);
        return this;
    }

}
