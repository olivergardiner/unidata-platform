package org.unidata.mdm.api;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.unidata.mdm.data.SimpleAttribute;

import static org.unidata.mdm.DataNamespace.DATA_API_NAMESPACE;


/**
 * 
 * Ответ на запрос на исполнение функции очистки данных ('RequestCleanse').
 * Общая часть ответа содержит код возврата, идентификатор логической операции и сообщения об ошибках (смотри элемент 'common' из структуры 'UnidataResponseBody').
 * Ответ содержит значение всех 'выходных' портов функции.
 *             
 * 
 * <p>Java class for ResponseCleanse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ResponseCleanse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://api.mdm.unidata.org/}UnidataAbstractResponse"&gt;
 *       &lt;choice maxOccurs="unbounded"&gt;
 *         &lt;element name="port" type="{http://data.mdm.unidata.org/}SimpleAttribute" maxOccurs="unbounded"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
public class ResponseCleanse extends UnidataAbstractResponse implements Serializable {

    private final static long serialVersionUID = 12345L;

    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected List<SimpleAttribute> port;

    /**
     * Gets the value of the port property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the port property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPort().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SimpleAttribute }
     * 
     * 
     */
    public List<SimpleAttribute> getPort() {
        if (port == null) {
            port = new ArrayList<SimpleAttribute>();
        }
        return this.port;
    }

    public ResponseCleanse withPort(SimpleAttribute... values) {
        if (values!= null) {
            for (SimpleAttribute value: values) {
                getPort().add(value);
            }
        }
        return this;
    }

    public ResponseCleanse withPort(Collection<SimpleAttribute> values) {
        if (values!= null) {
            getPort().addAll(values);
        }
        return this;
    }

}
