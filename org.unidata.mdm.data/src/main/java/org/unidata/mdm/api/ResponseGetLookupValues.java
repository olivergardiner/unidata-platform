package org.unidata.mdm.api;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.unidata.mdm.data.EtalonRecord;

import static org.unidata.mdm.DataNamespace.DATA_API_NAMESPACE;


/**
 * 
 * Ответ на запрос на получение деталей справочника ('RequestGetLookupValues').
 * Общая часть ответа содержит код возврата, идентификатор логической операции и сообщения об ошибках (смотри элемент 'common' из структуры 'UnidataResponseBody').
 * Содержит список либо всех записей справочника, либо конкретных если в запросе было указано значение кода
 *             
 * 
 * <p>Java class for ResponseGetLookupValues complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ResponseGetLookupValues"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://api.mdm.unidata.org/}UnidataAbstractResponse"&gt;
 *       &lt;choice maxOccurs="unbounded" minOccurs="0"&gt;
 *         &lt;element name="etalonRecord" type="{http://data.mdm.unidata.org/}EtalonRecord"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
public class ResponseGetLookupValues extends UnidataAbstractResponse implements Serializable {

    private final static long serialVersionUID = 12345L;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected List<EtalonRecord> etalonRecord;

    /**
     * Gets the value of the etalonRecord property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the etalonRecord property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEtalonRecord().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EtalonRecord }
     * 
     * 
     */
    public List<EtalonRecord> getEtalonRecord() {
        if (etalonRecord == null) {
            etalonRecord = new ArrayList<EtalonRecord>();
        }
        return this.etalonRecord;
    }

    public ResponseGetLookupValues withEtalonRecord(EtalonRecord... values) {
        if (values!= null) {
            for (EtalonRecord value: values) {
                getEtalonRecord().add(value);
            }
        }
        return this;
    }

    public ResponseGetLookupValues withEtalonRecord(Collection<EtalonRecord> values) {
        if (values!= null) {
            getEtalonRecord().addAll(values);
        }
        return this;
    }

}
