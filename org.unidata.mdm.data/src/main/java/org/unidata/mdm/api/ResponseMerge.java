package org.unidata.mdm.api;

import java.io.Serializable;


/**
 * 
 * Ответ на запрос на консолидацию нескольких основных записей сущности ('RequestMerge').
 * Общая часть ответа содержит код возврата, идентификатор логической операции и сообщения об ошибках (смотри элемент 'common' из структуры 'UnidataResponseBody').
 * Ответ не содержит никаких специфических параметров. Вызывающая сторона должна ориентироваться на код возврата из общей секции.
 *             
 * 
 * <p>Java class for ResponseMerge complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ResponseMerge"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://api.mdm.unidata.org/}UnidataAbstractResponse"&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
public class ResponseMerge extends UnidataAbstractResponse implements Serializable {

    private final static long serialVersionUID = 12345L;

}
