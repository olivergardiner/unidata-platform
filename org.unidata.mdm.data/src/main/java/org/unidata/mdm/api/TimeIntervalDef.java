package org.unidata.mdm.api;

import java.io.Serializable;
import javax.xml.datatype.XMLGregorianCalendar;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;


/**
 * 
 * Структура, описывающая временной интервал.
 *             
 * 
 * <p>Java class for TimeIntervalDef complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TimeIntervalDef"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="rangeFrom" use="required" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&gt;
 *       &lt;attribute name="rangeTo" use="required" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
public class TimeIntervalDef implements Serializable {

    private final static long serialVersionUID = 12345L;
    @JacksonXmlProperty(isAttribute = true)
    protected XMLGregorianCalendar rangeFrom;
    @JacksonXmlProperty(isAttribute = true)
    protected XMLGregorianCalendar rangeTo;

    /**
     * Gets the value of the rangeFrom property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRangeFrom() {
        return rangeFrom;
    }

    /**
     * Sets the value of the rangeFrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRangeFrom(XMLGregorianCalendar value) {
        this.rangeFrom = value;
    }

    /**
     * Gets the value of the rangeTo property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRangeTo() {
        return rangeTo;
    }

    /**
     * Sets the value of the rangeTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRangeTo(XMLGregorianCalendar value) {
        this.rangeTo = value;
    }

    public TimeIntervalDef withRangeFrom(XMLGregorianCalendar value) {
        setRangeFrom(value);
        return this;
    }

    public TimeIntervalDef withRangeTo(XMLGregorianCalendar value) {
        setRangeTo(value);
        return this;
    }

}
