package org.unidata.mdm.api;

import java.io.Serializable;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import static org.unidata.mdm.DataNamespace.DATA_API_NAMESPACE;


/**
 * 
 * Общая часть любого API запроса. Состоит из двух основных частей - параметры безопасности и параметры асинхронного вызова.
 * Помимо этого, содержит обязательный параметр 'storageId', идентифицирующий логическое хранилище.
 * Также, есть возможность предоставить опциональный параметр - идентификатор логической операции. Если идентификатор не предоставлен,
 * то платформа автоматически сгенерирует новое уникальное значение. Идентификатор логических операций используется для аудита, а также для контроля композитных операций
 *             
 * 
 * <p>Java class for CommonSectionDef complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CommonSectionDef"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="security" type="{http://api.mdm.unidata.org/}SecuritySectionDef"/&gt;
 *         &lt;element name="asyncOptions" type="{http://api.mdm.unidata.org/}AsyncSectionDef" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="version" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="storageId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="operationId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
public class CommonSectionDef implements Serializable {

    private final static long serialVersionUID = 12345L;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected SecuritySectionDef security;
    @JacksonXmlProperty(namespace = DATA_API_NAMESPACE)
    protected AsyncSectionDef asyncOptions;
    @JacksonXmlProperty(isAttribute = true)
    protected String version;
    @JacksonXmlProperty(isAttribute = true)
    protected String storageId;
    @JacksonXmlProperty(isAttribute = true)
    protected String operationId;

    /**
     * Gets the value of the security property.
     * 
     * @return
     *     possible object is
     *     {@link SecuritySectionDef }
     *     
     */
    public SecuritySectionDef getSecurity() {
        return security;
    }

    /**
     * Sets the value of the security property.
     * 
     * @param value
     *     allowed object is
     *     {@link SecuritySectionDef }
     *     
     */
    public void setSecurity(SecuritySectionDef value) {
        this.security = value;
    }

    /**
     * Gets the value of the asyncOptions property.
     * 
     * @return
     *     possible object is
     *     {@link AsyncSectionDef }
     *     
     */
    public AsyncSectionDef getAsyncOptions() {
        return asyncOptions;
    }

    /**
     * Sets the value of the asyncOptions property.
     * 
     * @param value
     *     allowed object is
     *     {@link AsyncSectionDef }
     *     
     */
    public void setAsyncOptions(AsyncSectionDef value) {
        this.asyncOptions = value;
    }

    /**
     * Gets the value of the version property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * Gets the value of the storageId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStorageId() {
        return storageId;
    }

    /**
     * Sets the value of the storageId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStorageId(String value) {
        this.storageId = value;
    }

    /**
     * Gets the value of the operationId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperationId() {
        return operationId;
    }

    /**
     * Sets the value of the operationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperationId(String value) {
        this.operationId = value;
    }

    public CommonSectionDef withSecurity(SecuritySectionDef value) {
        setSecurity(value);
        return this;
    }

    public CommonSectionDef withAsyncOptions(AsyncSectionDef value) {
        setAsyncOptions(value);
        return this;
    }

    public CommonSectionDef withVersion(String value) {
        setVersion(value);
        return this;
    }

    public CommonSectionDef withStorageId(String value) {
        setStorageId(value);
        return this;
    }

    public CommonSectionDef withOperationId(String value) {
        setOperationId(value);
        return this;
    }

}
