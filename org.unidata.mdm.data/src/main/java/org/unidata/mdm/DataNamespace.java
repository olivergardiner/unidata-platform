package org.unidata.mdm;

/**
 *  Data module namespace
 *
 * @author Alexandr Serov
 * @since 03.08.2020
 **/
public interface DataNamespace {

    String DATA_API_NAMESPACE = "http://api.mdm.unidata.org/";

    String DATA_NAMESPACE = "http://data.mdm.unidata.org/";

}
