/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 * 
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.data.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.unidata.mdm.data.EtalonRecord;
import org.unidata.mdm.data.EtalonRecordFull;
import org.unidata.mdm.data.OriginRecord;
import org.unidata.mdm.data.RelationTo;
import org.unidata.mdm.data.exception.DataExceptionIds;
import org.unidata.mdm.system.exception.PlatformFailureException;
import org.unidata.mdm.system.util.AbstractJaxbUtils;

/**
 * JAXb stuff, related to base data.
 * @author Mikhail Mikhailov on Oct 18, 2019
 */
public final class DataJaxbUtils extends AbstractJaxbUtils {
    /**
     * Logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(DataJaxbUtils.class);
    /**
     * Data root package.
     */
    private static final String DATA_ROOT_PACKAGE = "org.unidata.mdm.data";
    /**
     * API root package.
     */
    private static final String API_ROOT_PACKAGE = "org.unidata.mdm.api";
    /**
     * XSD dateTime date format.
     */
    public static final String XSD_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ssX";

    /**
     * Data object factory.
     */
    private static final org.unidata.mdm.data.ObjectFactory DATA_OBJECT_FACTORY
            = new org.unidata.mdm.data.ObjectFactory();
    /**
     * API object factory.
     */
    private static final org.unidata.mdm.api.ObjectFactory API_OBJECT_FACTORY
            = new org.unidata.mdm.api.ObjectFactory();

    /**
     * Constructor.
     */
    private DataJaxbUtils() {
        super();
    }

    /**
     * @return the dataObjectFactory
     */
    public static org.unidata.mdm.data.ObjectFactory getDataObjectFactory() {
        return DATA_OBJECT_FACTORY;
    }
    /**
     * @return the apiObjectFactory
     */
    public static org.unidata.mdm.api.ObjectFactory getApiObjectFactory() {
        return API_OBJECT_FACTORY;
    }
    /**
     * Unmarshals a {@link EtalonRecord}.
     *
     * @param s string
     * @return golden record or null
     */
    public static OriginRecord unmarshalOriginRecord(String s) {
        try {
            return unmarshalObject(OriginRecord.class, s);
        } catch (Exception je) {
            final String message = "Cannot unmarshall origin record from [{}]";
            LOGGER.warn(message, s, je);
            throw new PlatformFailureException(message, je, DataExceptionIds.EX_DATA_CANNOT_UNMARSHAL_ORIGIN, s);
        }
    }

    /**
     * Unmarshals a {@link RelationTo}.
     *
     * @param s string
     * @return relation element or null
     */
    public static RelationTo unmarshalRelationTo(String s) {
        try {
            return unmarshalObject(RelationTo.class, s);
        } catch (Exception je) {
            final String message = "Cannot unmarshall relation from [{}]";
            LOGGER.warn(message, s, je);
            throw new PlatformFailureException(message, je, DataExceptionIds.EX_DATA_CANNOT_UNMARSHAL_RELATION, s);
        }
    }
    /**
     * Marshal a {@link EtalonRecord}.
     *
     * @param record the record
     * @return string
     */
    public static String marshalEtalonRecord(EtalonRecord record) {
        try {
            return marshalObject(record);
        } catch (Exception je) {
            final String message = "Cannot marshall etalon record from [{}]";
            LOGGER.warn(message, record, je);
            throw new PlatformFailureException(message, je, DataExceptionIds.EX_DATA_CANNOT_MARSHAL_ETALON, record);
        }
    }

    /**
     * Marshal a {@link EtalonRecordFull}.
     *
     * @param record the record
     * @return string
     */
    public static String marshalEtalonRecordFull(EtalonRecordFull record) {
        try {
            return marshalObject(record);
        } catch (Exception je) {
            final String message = "Cannot marshall etalon record from [{}]";
            LOGGER.warn(message, record, je);
            throw new PlatformFailureException(message, je, DataExceptionIds.EX_DATA_CANNOT_MARSHAL_ETALON, record);
        }
    }

    /**
     * Marshal a {@link OriginRecord}.
     *
     * @param record the record
     * @return string
     */
    public static String marshalOriginRecord(OriginRecord record) {
        try {
            return marshalObject(record);
        } catch (Exception je) {
            final String message = "Cannot marshall origin record from [{}]";
            LOGGER.warn(message, record, je);
            throw new PlatformFailureException(message, je, DataExceptionIds.EX_DATA_CANNOT_MARSHAL_ORIGIN, record);
        }
    }

    /**
     * Marshal a {@link RelationTo}.
     *
     * @param relationTo relation to record
     * @return string
     */
    public static String marshalRelationTo(RelationTo relationTo) {
        try {
            return marshalObject(relationTo);
        } catch (Exception je) {
            final String message = "Cannot marshall relation from [{}]";
            LOGGER.warn(message, relationTo, je);
            throw new PlatformFailureException(message, je, DataExceptionIds.EX_DATA_CANNOT_MARSHAL_RELATION, relationTo);
        }
    }
}
