package org.unidata.mdm.data;

import java.io.Serializable;
import javax.xml.datatype.XMLGregorianCalendar;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import static org.unidata.mdm.DataNamespace.DATA_NAMESPACE;


/**
 * 
 * Базовая структура, описывающая значение атрибута сущности. Всегда содержит только одно значение в зависимости от типа данных
 *             
 * 
 * <p>Java class for AbstractSingleValueAttribute complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AbstractSingleValueAttribute"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://data.mdm.unidata.org/}AbstractAttribute"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="blobValue" type="{http://data.mdm.unidata.org/}BlobValue"/&gt;
 *           &lt;element name="clobValue" type="{http://data.mdm.unidata.org/}ClobValue"/&gt;
 *           &lt;element name="intValue" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *           &lt;element name="dateValue" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *           &lt;element name="timeValue" type="{http://www.w3.org/2001/XMLSchema}time"/&gt;
 *           &lt;element name="timestampValue" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *           &lt;element name="stringValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *           &lt;element name="numberValue" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *           &lt;element name="boolValue" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *           &lt;element name="measuredValue" type="{http://data.mdm.unidata.org/}MeasuredValue"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="displayValue" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="linkEtalonId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
public class AbstractSingleValueAttribute extends AbstractAttribute implements Serializable {

    private final static long serialVersionUID = 12345L;
    @JacksonXmlProperty(namespace = DATA_NAMESPACE)
    protected BlobValue blobValue;
    @JacksonXmlProperty(namespace = DATA_NAMESPACE)
    protected ClobValue clobValue;
    @JacksonXmlProperty(namespace = DATA_NAMESPACE)
    protected Long intValue;
    @JacksonXmlProperty(namespace = DATA_NAMESPACE)
    protected XMLGregorianCalendar dateValue;
    @JacksonXmlProperty(namespace = DATA_NAMESPACE)
    protected XMLGregorianCalendar timeValue;
    @JacksonXmlProperty(namespace = DATA_NAMESPACE)
    protected XMLGregorianCalendar timestampValue;
    @JacksonXmlProperty(namespace = DATA_NAMESPACE)
    protected String stringValue;
    @JacksonXmlProperty(namespace = DATA_NAMESPACE)
    protected Double numberValue;
    @JacksonXmlProperty(namespace = DATA_NAMESPACE)
    protected Boolean boolValue;
    @JacksonXmlProperty(namespace = DATA_NAMESPACE)
    protected MeasuredValue measuredValue;
    @JacksonXmlProperty(isAttribute = true)
    protected String displayValue;
    @JacksonXmlProperty(isAttribute = true)
    protected String linkEtalonId;

    /**
     * Gets the value of the blobValue property.
     */
    public BlobValue getBlobValue() {
        return blobValue;
    }

    /**
     * Sets the value of the blobValue property.
     */
    public void setBlobValue(BlobValue value) {
        this.blobValue = value;
    }

    /**
     * Gets the value of the clobValue property.
     */
    public ClobValue getClobValue() {
        return clobValue;
    }

    /**
     * Sets the value of the clobValue property.
     */
    public void setClobValue(ClobValue value) {
        this.clobValue = value;
    }

    /**
     * Gets the value of the intValue property.
     */
    public Long getIntValue() {
        return intValue;
    }

    /**
     * Sets the value of the intValue property.
     */
    public void setIntValue(Long value) {
        this.intValue = value;
    }

    /**
     * Gets the value of the dateValue property.
     */
    public XMLGregorianCalendar getDateValue() {
        return dateValue;
    }

    /**
     * Sets the value of the dateValue property.
     */
    public void setDateValue(XMLGregorianCalendar value) {
        this.dateValue = value;
    }

    /**
     * Gets the value of the timeValue property.
     */
    public XMLGregorianCalendar getTimeValue() {
        return timeValue;
    }

    /**
     * Sets the value of the timeValue property.
     */
    public void setTimeValue(XMLGregorianCalendar value) {
        this.timeValue = value;
    }

    /**
     * Gets the value of the timestampValue property.
     */
    public XMLGregorianCalendar getTimestampValue() {
        return timestampValue;
    }

    /**
     * Sets the value of the timestampValue property.
     */
    public void setTimestampValue(XMLGregorianCalendar value) {
        this.timestampValue = value;
    }

    /**
     * Gets the value of the stringValue property.
     */
    public String getStringValue() {
        return stringValue;
    }

    /**
     * Sets the value of the stringValue property.
     */
    public void setStringValue(String value) {
        this.stringValue = value;
    }

    /**
     * Gets the value of the numberValue property.
     */
    public Double getNumberValue() {
        return numberValue;
    }

    /**
     * Sets the value of the numberValue property.
     */
    public void setNumberValue(Double value) {
        this.numberValue = value;
    }

    /**
     * Gets the value of the boolValue property.
     */
    public Boolean getBoolValue() {
        return boolValue;
    }

    /**
     * Sets the value of the boolValue property.
     */
    public void setBoolValue(Boolean value) {
        this.boolValue = value;
    }

    /**
     * Gets the value of the measuredValue property.
     */
    public MeasuredValue getMeasuredValue() {
        return measuredValue;
    }

    /**
     * Sets the value of the measuredValue property.
     */
    public void setMeasuredValue(MeasuredValue value) {
        this.measuredValue = value;
    }

    /**
     * Gets the value of the displayValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDisplayValue() {
        return displayValue;
    }

    /**
     * Sets the value of the displayValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDisplayValue(String value) {
        this.displayValue = value;
    }

    /**
     * Gets the value of the linkEtalonId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLinkEtalonId() {
        return linkEtalonId;
    }

    /**
     * Sets the value of the linkEtalonId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLinkEtalonId(String value) {
        this.linkEtalonId = value;
    }

    public AbstractSingleValueAttribute withBlobValue(BlobValue value) {
        setBlobValue(value);
        return this;
    }

    public AbstractSingleValueAttribute withClobValue(ClobValue value) {
        setClobValue(value);
        return this;
    }

    public AbstractSingleValueAttribute withIntValue(Long value) {
        setIntValue(value);
        return this;
    }

    public AbstractSingleValueAttribute withDateValue(XMLGregorianCalendar value) {
        setDateValue(value);
        return this;
    }

    public AbstractSingleValueAttribute withTimeValue(XMLGregorianCalendar value) {
        setTimeValue(value);
        return this;
    }

    public AbstractSingleValueAttribute withTimestampValue(XMLGregorianCalendar value) {
        setTimestampValue(value);
        return this;
    }

    public AbstractSingleValueAttribute withStringValue(String value) {
        setStringValue(value);
        return this;
    }

    public AbstractSingleValueAttribute withNumberValue(Double value) {
        setNumberValue(value);
        return this;
    }

    public AbstractSingleValueAttribute withBoolValue(Boolean value) {
        setBoolValue(value);
        return this;
    }

    public AbstractSingleValueAttribute withMeasuredValue(MeasuredValue value) {
        setMeasuredValue(value);
        return this;
    }

    public AbstractSingleValueAttribute withDisplayValue(String value) {
        setDisplayValue(value);
        return this;
    }

    public AbstractSingleValueAttribute withLinkEtalonId(String value) {
        setLinkEtalonId(value);
        return this;
    }

    @Override
    public AbstractSingleValueAttribute withName(String value) {
        setName(value);
        return this;
    }

}
