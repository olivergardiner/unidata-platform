package org.unidata.mdm.data;

/**
 * <p>Java class for DataShift.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="DataShift"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="PRISTINE"/&gt;
 *     &lt;enumeration value="REVISED"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
public enum DataShift {

    PRISTINE,
    REVISED;

    public String value() {
        return name();
    }

    public static DataShift fromValue(String v) {
        return valueOf(v);
    }

}
