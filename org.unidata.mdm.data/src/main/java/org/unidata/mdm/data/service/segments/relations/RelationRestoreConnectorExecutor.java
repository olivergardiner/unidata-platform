/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.data.service.segments.relations;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.type.data.RecordStatus;
import org.unidata.mdm.core.type.timeline.Timeline;
import org.unidata.mdm.data.context.AbstractRelationIdentityContext;
import org.unidata.mdm.data.context.ReadWriteDataContext;
import org.unidata.mdm.data.context.RecordIdentityContext;
import org.unidata.mdm.data.context.RelationRestoreContext;
import org.unidata.mdm.data.context.RestoreFromRelationRequestContext;
import org.unidata.mdm.data.context.RestoreRelationsRequestContext;
import org.unidata.mdm.data.context.RestoreToRelationRequestContext;
import org.unidata.mdm.data.dao.RelationsDAO;
import org.unidata.mdm.data.dto.RelationStateDTO;
import org.unidata.mdm.data.dto.RestoreRelationDTO;
import org.unidata.mdm.data.dto.RestoreRelationsDTO;
import org.unidata.mdm.data.exception.DataExceptionIds;
import org.unidata.mdm.data.exception.DataProcessingException;
import org.unidata.mdm.data.module.DataModule;
import org.unidata.mdm.data.service.impl.CommonRelationsComponent;
import org.unidata.mdm.data.type.data.OriginRelation;
import org.unidata.mdm.data.type.data.RelationType;
import org.unidata.mdm.data.type.keys.RecordKeys;
import org.unidata.mdm.data.type.keys.RelationKeys;
import org.unidata.mdm.meta.type.model.entities.Relation;
import org.unidata.mdm.meta.service.MetaModelService;
import org.unidata.mdm.meta.type.RelativeDirection;
import org.unidata.mdm.system.context.CommonRequestContext;
import org.unidata.mdm.system.service.ExecutionService;
import org.unidata.mdm.system.type.pipeline.Connector;
import org.unidata.mdm.system.type.pipeline.Pipeline;
import org.unidata.mdm.system.type.pipeline.PipelineInput;
import org.unidata.mdm.system.type.pipeline.fragment.InputFragmentContainer;
import org.unidata.mdm.system.type.runtime.MeasurementPoint;

/**
 * @author Mikhail Mikhailov on Dec 4, 2019
 */
@Component(RelationRestoreConnectorExecutor.SEGMENT_ID)
public class RelationRestoreConnectorExecutor extends Connector<PipelineInput, RestoreRelationsDTO> {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = DataModule.MODULE_ID + "[RELATIONS_RESTORE_CONNECTOR]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = DataModule.MODULE_ID + ".relations.restore.connector.description";
    /**
     * Logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(RelationRestoreConnectorExecutor.class);
    /**
     * The execution service.
     */
    @Autowired
    private ExecutionService executionService;
    /**
     * The MMS instance.
     */
    @Autowired
    private MetaModelService metaModelService;
    /**
     * The CRC II.
     */
    @Autowired
    private CommonRelationsComponent commonRelationsComponent;
    /**
     * Relations vistory DAO.
     */
    @Autowired
    private RelationsDAO relationsDao;
    /**
     * Constructor.
     */
    public RelationRestoreConnectorExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public RestoreRelationsDTO connect(PipelineInput ctx) {

        InputFragmentContainer target = (InputFragmentContainer) ctx;
        RestoreRelationsRequestContext payload = target.fragment(RestoreRelationsRequestContext.FRAGMENT_ID);
        if (Objects.isNull(payload)) {
            return null;
        }

        if (ctx instanceof RecordIdentityContext) {
            payload.keys(((RecordIdentityContext) ctx).keys());
        }

        if (ctx instanceof ReadWriteDataContext) {
            payload.timestamp(((ReadWriteDataContext<?>) ctx).timestamp());
        }

        payload.setOperationId(((CommonRequestContext) ctx).getOperationId());
        return execute(payload, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RestoreRelationsDTO connect(PipelineInput ctx, Pipeline p) {

        InputFragmentContainer target = (InputFragmentContainer) ctx;
        RestoreRelationsRequestContext payload = target.fragment(RestoreRelationsRequestContext.FRAGMENT_ID);
        if (Objects.isNull(payload)) {
            return null;
        }

        if (ctx instanceof RecordIdentityContext) {
            payload.keys(((RecordIdentityContext) ctx).keys());
        }

        if (ctx instanceof ReadWriteDataContext) {
            payload.timestamp(((ReadWriteDataContext<?>) ctx).timestamp());
        }

        payload.setOperationId(((CommonRequestContext) ctx).getOperationId());
        return execute(payload, p);
    }
    /**
     * Does the actual context processing.
     * @param ctx the context
     * @param p the pipeline
     * @return result
     */
    public RestoreRelationsDTO execute(@Nonnull RestoreRelationsRequestContext ctx, @Nullable Pipeline p) {

        MeasurementPoint.start();
        try {

            // 1. First of all check record keys (the main keys interface is the master key)
            commonRelationsComponent.ensureAndGetFromRecordKeys(ctx);

            // 3. Check input. Return on no input, what is not a crime
            Map<String, List<RestoreFromRelationRequestContext>> from = ensureFromInput(ctx);
            Map<String, List<RestoreToRelationRequestContext>> to = ensureToInput(ctx);
            if (MapUtils.isEmpty(from) && MapUtils.isEmpty(to)) {
                return null;
            }

            Map<String, List<AbstractRelationIdentityContext>> input = new HashMap<>();
            from.entrySet().forEach(entry -> input.computeIfAbsent(entry.getKey(), k -> new ArrayList<AbstractRelationIdentityContext>())
                           .addAll(entry.getValue()));
            to.entrySet().forEach(entry -> input.computeIfAbsent(entry.getKey(), k -> new ArrayList<AbstractRelationIdentityContext>())
                         .addAll(entry.getValue()));

            // 4. Merge input and execute
            if (Objects.isNull(ctx.timestamp())) {
                ctx.timestamp(new Date());
            }

            return new RestoreRelationsDTO(executeRestore(input, p, ctx));
        } finally {
            MeasurementPoint.stop();
        }
    }

    protected Map<String, List<RestoreFromRelationRequestContext>> ensureFromInput(RestoreRelationsRequestContext ctx) {

        if (MapUtils.isNotEmpty(ctx.getRelationsFrom())) {
            return ctx.getRelationsFrom();
        } else if (CollectionUtils.isEmpty(ctx.getFromRelationNames()) && !ctx.isApplyToAll()) {
            return Collections.emptyMap();
        }

        Map<String, List<UUID>> relationEtalonIds = Stream.of(ctx.keys())
                .map(keys -> relationsDao.loadMappedRelationEtalonIds(
                        UUID.fromString(keys.getEtalonKey().getId()),
                            ctx.getFromRelationNames(),
                            RelativeDirection.FROM))
                .filter(MapUtils::isNotEmpty)
                .map(Map::entrySet)
                .flatMap(Collection::stream)
                .collect(Collectors.groupingBy(Entry::getKey,
                         Collectors.flatMapping(entry -> entry.getValue().stream(), Collectors.toList())));

        if (MapUtils.isEmpty(relationEtalonIds)) {
            return Collections.emptyMap();
        }

        Map<String, List<RestoreFromRelationRequestContext>> result = new HashMap<>(relationEtalonIds.size());
        relationEtalonIds.forEach((k, v) ->
            result.put(k, v.stream()
                    .map(id ->
                        RestoreFromRelationRequestContext.builder()
                            .relationEtalonKey(id.toString())
                            .operationId(ctx.getOperationId())
                            .periodRestore(ctx.isPeriodRestore())
                            .validFrom(ctx.getValidFrom())
                            .validTo(ctx.getValidTo())
                            .batchOperation(ctx.isBatchOperation())
                            .forDate(ctx.getForDate())
                            .lastUpdate(ctx.getLastUpdate())
                            .build())
                    .collect(Collectors.toList())));

        // 2. Filter only those, which can be restored
        if (MapUtils.isNotEmpty(result)) {
            return result.values().stream()
                .flatMap(Collection::stream)
                .map(i -> ensureInputItem(i, ctx))
                .filter(Objects::nonNull)
                .map(i -> (RestoreFromRelationRequestContext) i)
                .collect(Collectors.groupingBy(i -> i.relationKeys().getRelationName(), Collectors.toList()));
        }

        return result;
    }

    protected Map<String, List<RestoreToRelationRequestContext>> ensureToInput(RestoreRelationsRequestContext ctx) {

        if (MapUtils.isNotEmpty(ctx.getRelationsTo())) {
            return ctx.getRelationsTo();
        } else if (CollectionUtils.isEmpty(ctx.getToRelationNames()) && !ctx.isApplyToAll()) {
            return Collections.emptyMap();
        }

        Map<String, List<UUID>> relationEtalonIds = Stream.of(ctx.keys())
                .map(keys -> relationsDao.loadMappedRelationEtalonIds(
                        UUID.fromString(keys.getEtalonKey().getId()),
                            ctx.getToRelationNames(),
                            RelativeDirection.TO))
                .filter(MapUtils::isNotEmpty)
                .map(Map::entrySet)
                .flatMap(Collection::stream)
                .collect(Collectors.groupingBy(Entry::getKey,
                         Collectors.flatMapping(entry -> entry.getValue().stream(), Collectors.toList())));

        if (MapUtils.isEmpty(relationEtalonIds)) {
            return Collections.emptyMap();
        }

        Map<String, List<RestoreToRelationRequestContext>> result = new HashMap<>(relationEtalonIds.size());
        relationEtalonIds.forEach((k, v) ->
            result.put(k, v.stream()
                    .map(id ->
                        RestoreToRelationRequestContext.builder()
                            .relationEtalonKey(id.toString())
                            .operationId(ctx.getOperationId())
                            .periodRestore(ctx.isPeriodRestore())
                            .validFrom(ctx.getValidFrom())
                            .validTo(ctx.getValidTo())
                            .batchOperation(ctx.isBatchOperation())
                            .forDate(ctx.getForDate())
                            .lastUpdate(ctx.getLastUpdate())
                            .build())
                    .collect(Collectors.toList())));

        // 2. Filter only those, which can be restored
        if (MapUtils.isNotEmpty(result)) {
            return result.values().stream()
                .flatMap(Collection::stream)
                .map(i -> ensureInputItem(i, ctx))
                .filter(Objects::nonNull)
                .map(i -> (RestoreToRelationRequestContext) i)
                .collect(Collectors.groupingBy(i -> i.relationKeys().getRelationName(), Collectors.toList()));
        }

        return result;
    }

    @Nullable
    protected RelationRestoreContext ensureInputItem(@Nonnull RelationRestoreContext i, @Nonnull RestoreRelationsRequestContext ctx) {

        Timeline<OriginRelation> current = commonRelationsComponent.ensureAndGetRelationTimeline(i);
        RelationKeys keys = current.getKeys();

        // Period
        if (i.isPeriodRestore()) {

            // Unable to restore this inactive/merged period.
            if (keys.getEtalonKey().getStatus() != RecordStatus.ACTIVE) {
                return null;
            }

            current = current.reduceBy(i.getValidFrom(), i.getValidTo());

            // Dates frame does not select at least one period.
            if (current.isEmpty()) {
                return null;
            }
        // Record
        } else {
            // Unable to restore this already active record.
            if (keys.getEtalonKey().getStatus() != RecordStatus.INACTIVE) {
                return null;
            }
        }

        i.currentTimeline(current);
        i.keys(current.getKeys());
        return i;
    }

    @SuppressWarnings("unchecked")
    protected Map<RelationStateDTO, List<RestoreRelationDTO>> executeRestore(
            Map<String, List<AbstractRelationIdentityContext>> input,
            Pipeline p,
            RestoreRelationsRequestContext ctx) {

        // 0. Set up vars
        RecordKeys keys = ctx.keys();
        Date timestamp = ctx.timestamp();

        // 1. Execute
        Map<RelationStateDTO, List<RestoreRelationDTO>> result = new HashMap<>();
        for (Entry<String, List<AbstractRelationIdentityContext>> entry : input.entrySet()) {

            if (CollectionUtils.isEmpty(entry.getValue())) {
                continue;
            }

            // 2.1 Check rel's existance. Fail if not found
            final Relation relation = ensureRelationDefinition(entry.getKey());

            // 2.2 Set up content and run single gets
            final String resolvedName = relation.getName();
            final RelationType resolvedType = RelationType.fromModelType(relation.getRelType());

            RelationStateDTO state = new RelationStateDTO(resolvedName, resolvedType);
            List<RestoreRelationDTO> collected = new ArrayList<>(entry.getValue().size());
            for (AbstractRelationIdentityContext mCtx : entry.getValue()) {

                if (mCtx.getDirection() == RelativeDirection.FROM) {
                    ((RestoreFromRelationRequestContext) mCtx).fromKeys(keys);
                } else {
                    ((RestoreToRelationRequestContext) mCtx).toKeys(keys);
                }

                ((ReadWriteDataContext<OriginRelation>) mCtx).timestamp(timestamp);
                mCtx.relationName(resolvedName);
                mCtx.relationType(resolvedType);
                mCtx.setOperationId(ctx.getOperationId());

                RestoreRelationDTO interim;
                if (Objects.isNull(p)) {
                    interim = executionService.execute((PipelineInput) mCtx);
                } else {
                    interim = executionService.execute(p, (PipelineInput) mCtx);
                }

                if (Objects.nonNull(interim)) {
                    collected.add(interim);
                }
            }

            result.put(state, collected);
        }

        return result;
    }

    protected Relation ensureRelationDefinition(String name) {

        Relation relation = metaModelService.getRelationById(name);
        if (relation == null) {
            final String message = "Relation [{}] not found. Stopping.";
            LOGGER.warn(message, name);
            throw new DataProcessingException(message, DataExceptionIds.EX_DATA_RELATIONS_GET_RELATION_NOT_FOUND, name);
        }

        return relation;
    }
}
