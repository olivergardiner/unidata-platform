/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.data.service.segments.records;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.type.data.RecordStatus;
import org.unidata.mdm.core.util.SecurityUtils;
import org.unidata.mdm.data.context.MergeRequestContext;
import org.unidata.mdm.data.module.DataModule;
import org.unidata.mdm.data.po.data.RecordEtalonPO;
import org.unidata.mdm.data.po.data.RecordOriginRemapPO;
import org.unidata.mdm.data.po.keys.RecordExternalKeysPO;
import org.unidata.mdm.data.service.RecordChangeSetProcessor;
import org.unidata.mdm.data.type.apply.RecordMergeChangeSet;
import org.unidata.mdm.data.type.keys.RecordKeys;
import org.unidata.mdm.system.type.pipeline.Point;
import org.unidata.mdm.system.type.pipeline.Start;
import org.unidata.mdm.system.type.runtime.MeasurementPoint;

/**
 * @author Mikhail Mikhailov on Nov 20, 2019
 * Former origin component code.
 */
@Component(RecordMergePersistenceExecutor.SEGMENT_ID)
public class RecordMergePersistenceExecutor extends Point<MergeRequestContext> {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = DataModule.MODULE_ID + "[RECORD_MERGE_PERSISTENCE]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = DataModule.MODULE_ID + ".record.merge.persistence.description";
    /**
     * The set processor.
     */
    @Autowired
    private RecordChangeSetProcessor recordChangeSetProcessor;
    /**
     * Constructor.
     */
    public RecordMergePersistenceExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void point(MergeRequestContext ctx) {

        MeasurementPoint.start();
        try {

            // 1. Prepare set
            prepareChangeSet(ctx);

            // 2. Apply changes
            applyChangeSet(ctx);

        } finally {
            MeasurementPoint.stop();
        }
    }

    private void prepareChangeSet(MergeRequestContext ctx) {

        Date ts = ctx.timestamp();
        String updatedBy = SecurityUtils.getCurrentUserName();
        RecordMergeChangeSet set = ctx.changeSet();

        RecordKeys masterKey = ctx.keys();
        List<RecordKeys> duplicatesKeys = ctx.duplicateKeys();

        // 2. Write objects to log
        duplicatesKeys.forEach(duplicate -> {

            RecordEtalonPO po = new RecordEtalonPO();
            po.setId(duplicate.getEtalonKey().getId());
            po.setShard(duplicate.getShard());
            po.setStatus(RecordStatus.MERGED);
            po.setUpdateDate(ts);
            po.setUpdatedBy(updatedBy);
            po.setOperationId(ctx.getOperationId());
            po.setName(duplicate.getEntityName());

            set.getRecordEtalonMergePOs().add(po);

            duplicate.getSupplementaryKeys().forEach(oKey -> {

                RecordOriginRemapPO orpo = new RecordOriginRemapPO();
                orpo.setId(oKey.getId());
                orpo.setInitialOwner(oKey.getInitialOwner());
                orpo.setExternalId(oKey.getExternalId(), oKey.getEntityName(), oKey.getSourceSystem());
                orpo.setCreateDate(oKey.getCreateDate());
                orpo.setCreatedBy(oKey.getCreatedBy());
                orpo.setUpdateDate(ts);
                orpo.setUpdatedBy(updatedBy);
                orpo.setStatus(oKey.getStatus());
                orpo.setEnrichment(oKey.isEnrichment());
                orpo.setEtalonId(duplicate.getEtalonKey().getId());
                orpo.setShard(duplicate.getShard()); // Old shard
                orpo.setNewEtalonId(UUID.fromString(masterKey.getEtalonKey().getId()));
                orpo.setNewShard(masterKey.getShard()); // <- Possibly new shard


                RecordExternalKeysPO ekpo = new RecordExternalKeysPO();
                ekpo.setExternalId(oKey.toExternalId());
                ekpo.setEtalonId(UUID.fromString(masterKey.getEtalonKey().getId()));

                set.getRecordOriginRemapPOs().add(orpo);
                set.getRecordExternalKeysUpdatePOs().add(ekpo);
            });
        });

        RecordEtalonPO etalonWinner = new RecordEtalonPO();

        etalonWinner.setName(masterKey.getEntityName());
        etalonWinner.setId(masterKey.getEtalonKey().getId());

        set.setEtalonRecordWinnerPO(etalonWinner);
    }

    private void applyChangeSet(MergeRequestContext ctx) {

        // Will be applied later in batched fashion.
        if (ctx.isBatchOperation()) {
            return;
        }

        RecordMergeChangeSet set = ctx.changeSet();
        recordChangeSetProcessor.apply(set);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?> start) {
        return MergeRequestContext.class.isAssignableFrom(start.getInputTypeClass());
    }
}
