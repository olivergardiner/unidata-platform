package org.unidata.mdm.data;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * <p>Java class for DataQualityStatusType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="DataQualityStatusType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="new"/&gt;
 *     &lt;enumeration value="resolved"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
public enum DataQualityStatusType {

    @JsonProperty("new")
    NEW("new"),
    @JsonProperty("resolved")
    RESOLVED("resolved");

    private final String value;

    DataQualityStatusType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DataQualityStatusType fromValue(String v) {
        for (DataQualityStatusType c: DataQualityStatusType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
