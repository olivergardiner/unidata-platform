package org.unidata.mdm.data;

import java.io.Serializable;
import java.util.Collection;

import org.unidata.mdm.data.serialization.jaxb.CodeAttributeImpl;


/**
 * 
 * Кодовый атрибут.
 *             
 * 
 * <p>Java class for CodeAttribute complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CodeAttribute"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://data.mdm.unidata.org/}AbstractCodeAttribute"&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
public class CodeAttribute extends CodeAttributeImpl implements Serializable {

    private final static long serialVersionUID = 12345L;

    @Override
    public CodeAttribute withIntValue(Long value) {
        setIntValue(value);
        return this;
    }

    @Override
    public CodeAttribute withStringValue(String value) {
        setStringValue(value);
        return this;
    }

    @Override
    public CodeAttribute withSupplementaryIntValues(Long... values) {
        if (values!= null) {
            for (Long value: values) {
                getSupplementaryIntValues().add(value);
            }
        }
        return this;
    }

    @Override
    public CodeAttribute withSupplementaryIntValues(Collection<Long> values) {
        if (values!= null) {
            getSupplementaryIntValues().addAll(values);
        }
        return this;
    }

    @Override
    public CodeAttribute withSupplementaryStringValues(String... values) {
        if (values!= null) {
            for (String value: values) {
                getSupplementaryStringValues().add(value);
            }
        }
        return this;
    }

    @Override
    public CodeAttribute withSupplementaryStringValues(Collection<String> values) {
        if (values!= null) {
            getSupplementaryStringValues().addAll(values);
        }
        return this;
    }

    @Override
    public CodeAttribute withName(String value) {
        setName(value);
        return this;
    }

}
