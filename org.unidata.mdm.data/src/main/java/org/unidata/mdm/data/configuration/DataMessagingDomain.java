package org.unidata.mdm.data.configuration;

import org.unidata.mdm.data.module.DataModule;
import org.unidata.mdm.data.type.messaging.DataTypes;
import org.unidata.mdm.system.type.messaging.DomainType;
import org.unidata.mdm.system.util.ResourceUtils;

/**
 * Data messaging domain(s).
 * @author Mikhail Mikhailov on Jul 15, 2020
 */
public class DataMessagingDomain {
    /**
     * Constructor.
     */
    private DataMessagingDomain() {
        super();
    }
    /**
     * Domain name.
     */
    public static final String NAME = "data";
    /**
     * The sole data domain.
     */
    public static final DomainType DOMAIN =
            DomainType.ofLocalized(NAME, ResourceUtils.asString("classpath:/routes/data.xml"))
                .withDescription(DataModule.MODULE_ID + ".messaging.domain.description")
                .withMessageTypes(DataTypes.TYPES);
}
