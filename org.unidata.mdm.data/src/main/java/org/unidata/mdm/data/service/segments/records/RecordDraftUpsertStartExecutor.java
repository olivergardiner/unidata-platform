/*
 *
 *  * Unidata Platform
 *  * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *  *
 *  * Commercial License
 *  * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *  *
 *  * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 *  * For clarification or additional options, please contact: info@unidata-platform.com
 *  * -------
 *  * Disclaimer:
 *  * -------
 *  * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 *  * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 *  * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 *  * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 *
 */

package org.unidata.mdm.data.service.segments.records;

import java.util.Objects;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.type.data.OperationType;
import org.unidata.mdm.data.exception.DataExceptionIds;
import org.unidata.mdm.data.exception.DataProcessingException;
import org.unidata.mdm.data.module.DataModule;
import org.unidata.mdm.data.service.impl.CommonRecordsComponent;
import org.unidata.mdm.data.type.draft.DataDraftConstants;
import org.unidata.mdm.data.type.keys.RecordEtalonKey;
import org.unidata.mdm.data.type.keys.RecordKeys;
import org.unidata.mdm.draft.context.DraftUpsertContext;
import org.unidata.mdm.draft.type.Draft;
import org.unidata.mdm.meta.service.MetaModelService;
import org.unidata.mdm.system.type.pipeline.Start;
import org.unidata.mdm.system.type.variables.Variables;
import org.unidata.mdm.system.util.IdUtils;

/**
 * @author Alexey Tsarapkin
 */
@Component(RecordDraftUpsertStartExecutor.SEGMENT_ID)
public class RecordDraftUpsertStartExecutor extends Start<DraftUpsertContext> {

    public static final String SEGMENT_ID = DataModule.MODULE_ID + "[RECORD_DRAFT_UPSERT_START]";

    private static final String SEGMENT_DESCRIPTION = DataModule.MODULE_ID + ".record.draft.upsert.start.description";

    @Autowired
    private CommonRecordsComponent commonRecordsComponent;

    @Autowired
    private MetaModelService metaModelService;

    public RecordDraftUpsertStartExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION, DraftUpsertContext.class);
    }

    @Override
    public void start(@Nonnull DraftUpsertContext ctx) {
        setup(ctx);
    }

    @Nullable
    @Override
    public String subject(DraftUpsertContext ctx) {

        setup(ctx);

        Draft draft = ctx.currentDraft();
        Variables variables = draft.getProperties();
        return variables.valueGet(DataDraftConstants.ENTITY_NAME);
    }

    protected void setup(DraftUpsertContext ctx) {

        if (ctx.setUp()) {
            return;
        }

        // 1. Init variables for new drafts
        setupVariables(ctx);

        ctx.setUp(true);
    }

    protected void setupVariables(DraftUpsertContext ctx) {

        Draft draft = ctx.currentDraft();

        // Exisitng draft has properties already set
        if (draft.isExisting()) {
            return;
        }

        Variables variables = new Variables();
        String subjectId = draft.getSubjectId();

        // New record
        if (Objects.isNull(subjectId)) {

            // Check input and set vars
            String entityName = ensureEntityName(ctx);
            String externalId = ensureExternalId(ctx);
            String sourceSystem = ensureSourceSystem(ctx);

            subjectId = IdUtils.v1String();

            variables
                .add(DataDraftConstants.IS_NEW_RECORD, Boolean.TRUE)
                .add(DataDraftConstants.ETALON_ID, subjectId)
                .add(DataDraftConstants.ENTITY_NAME, entityName)
                .add(DataDraftConstants.EXTERNAL_ID, externalId)
                .add(DataDraftConstants.SOURCE_SYSTEM, sourceSystem);

        // Exisitng record
        } else {

            RecordKeys keys = commonRecordsComponent.identify(RecordEtalonKey.builder()
                    .id(subjectId)
                    .build());

            if (Objects.isNull(keys)) {
                throw new DataProcessingException("Existing data record not found by subject ID [{}].",
                        DataExceptionIds.EX_DATA_RECORD_DRAFT_UNKNOWN_SUBJECT_ID, subjectId);
            }

            variables
                .add(DataDraftConstants.IS_NEW_RECORD, Boolean.FALSE)
                .add(DataDraftConstants.ETALON_ID, subjectId)
                .add(DataDraftConstants.ENTITY_NAME, keys.getEntityName())
                .add(DataDraftConstants.EXTERNAL_ID, keys.getOriginKey().getExternalId())
                .add(DataDraftConstants.SOURCE_SYSTEM, keys.getOriginKey().getSourceSystem());
        }

        String operationType = ctx.getParameter(DataDraftConstants.OPERATION_TYPE);
        variables.add(DataDraftConstants.OPERATION_TYPE, Objects.isNull(operationType) ? OperationType.DIRECT.name() : operationType);

        draft.setProperties(variables);
    }


    protected String ensureSourceSystem(DraftUpsertContext ctx) {

        String sourceSystem = ctx.getParameter(DataDraftConstants.SOURCE_SYSTEM);
        if (Objects.nonNull(sourceSystem) && Objects.isNull(metaModelService.getSourceSystemById(sourceSystem))) {

            throw new DataProcessingException("Source system [{}] for new draft record is unknown.",
                    DataExceptionIds.EX_DATA_RECORD_DRAFT_SOURCE_SYSTEM_UNKNOWN, sourceSystem);
        }

        return sourceSystem == null ? metaModelService.getAdminSourceSystem().getName() : sourceSystem;
    }

    protected String ensureExternalId(DraftUpsertContext ctx) {

        String sourceSystem = ctx.getParameter(DataDraftConstants.SOURCE_SYSTEM);
        String externalId = ctx.getParameter(DataDraftConstants.EXTERNAL_ID);

        if ((Objects.nonNull(sourceSystem) && Objects.isNull(externalId))
         || (Objects.isNull(sourceSystem) && Objects.nonNull(externalId))) {

            throw new DataProcessingException("Either external id or source system not supplied for new draft record.",
                    DataExceptionIds.EX_DATA_RECORD_DRAFT_INCOMPLETE_EXTERNAL_ID);
        }

        return externalId == null ? IdUtils.v1String() : externalId;
    }

    protected String ensureEntityName(DraftUpsertContext ctx) {

        String entityName = ctx.getParameter(DataDraftConstants.ENTITY_NAME);
        if (Objects.isNull(entityName) || Objects.isNull(metaModelService.getEntityModelElementById(entityName))) {
            throw new DataProcessingException("No or unknown entity name supplied for new draft record.",
                    DataExceptionIds.EX_DATA_RECORD_DRAFT_NO_ENTITY_NAME);
        }

        return entityName;
    }
}
