    package org.unidata.mdm.data;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * <p>Java class for ValueDataType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ValueDataType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Date"/&gt;
 *     &lt;enumeration value="Time"/&gt;
 *     &lt;enumeration value="Timestamp"/&gt;
 *     &lt;enumeration value="String"/&gt;
 *     &lt;enumeration value="Integer"/&gt;
 *     &lt;enumeration value="Number"/&gt;
 *     &lt;enumeration value="Boolean"/&gt;
 *     &lt;enumeration value="Blob"/&gt;
 *     &lt;enumeration value="Clob"/&gt;
 *     &lt;enumeration value="Measured"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
public enum ValueDataType {

    @JsonProperty("Date")
    DATE("Date"),
    @JsonProperty("Time")
    TIME("Time"),
    @JsonProperty("Timestamp")
    TIMESTAMP("Timestamp"),
    @JsonProperty("String")
    STRING("String"),
    @JsonProperty("Integer")
    INTEGER("Integer"),
    @JsonProperty("Number")
    NUMBER("Number"),
    @JsonProperty("Boolean")
    BOOLEAN("Boolean"),
    @JsonProperty("Blob")
    BLOB("Blob"),
    @JsonProperty("Clob")
    CLOB("Clob"),
    @JsonProperty("Measured")
    MEASURED("Measured");

    private final String value;

    ValueDataType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ValueDataType fromValue(String v) {
        for (ValueDataType c: ValueDataType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
