/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.data.service.segments.records;

import org.springframework.stereotype.Component;
import org.unidata.mdm.data.context.MergeRequestContext;
import org.unidata.mdm.data.dto.MergeRecordsDTO;
import org.unidata.mdm.data.module.DataModule;
import org.unidata.mdm.system.type.pipeline.Finish;
import org.unidata.mdm.system.type.pipeline.Start;
import org.unidata.mdm.system.type.runtime.MeasurementPoint;

/**
 * @author Mikhail Mikhailov on Nov 21, 2019
 */
@Component(RecordMergeFinishExecutor.SEGMENT_ID)
public class RecordMergeFinishExecutor extends Finish<MergeRequestContext, MergeRecordsDTO> {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = DataModule.MODULE_ID + "[RECORD_MERGE_FINISH]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = DataModule.MODULE_ID + ".record.merge.finish.description";
    /**
     * Constructor.
     */
    public RecordMergeFinishExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION, MergeRecordsDTO.class);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public MergeRecordsDTO finish(MergeRequestContext ctx) {

        MeasurementPoint.start();
        try {
            // Not much here so far.
            return new MergeRecordsDTO(ctx.keys(), ctx.duplicateKeys());
        } finally {
            MeasurementPoint.stop();
        }
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?> start) {
        return MergeRequestContext.class.isAssignableFrom(start.getInputTypeClass());
    }
}
