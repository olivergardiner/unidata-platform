package org.unidata.mdm.data.type.messaging;

import org.unidata.mdm.core.type.messaging.CoreSubsystems;
import org.unidata.mdm.data.module.DataModule;
import org.unidata.mdm.system.type.messaging.MessageType;

/**
 * @author Mikhail Mikhailov on Jul 15, 2020
 */
public class DataTypes {
    /**
     * Constructor.
     */
    private DataTypes() {
        super();
    }
    /**
     * Record upsert audit message.
     */
    public static final MessageType RECORD_UPSERT = new MessageType("record-upsert")
            .withSubsystem(CoreSubsystems.AUDIT_SUBSYSTEM)
            .withDescription(DataModule.MODULE_ID + ".messaging.record-upsert")
            .withHeaders(DataHeaders.CONTEXT);
    /**
     * Record delete audit message.
     */
    public static final MessageType RECORD_DELETE = new MessageType("record-delete")
            .withSubsystem(CoreSubsystems.AUDIT_SUBSYSTEM)
            .withDescription(DataModule.MODULE_ID + ".messaging.record-delete")
            .withHeaders(DataHeaders.CONTEXT);
    /**
     * Record get audit message.
     */
    public static final MessageType RECORD_GET = new MessageType("record-get")
            .withSubsystem(CoreSubsystems.AUDIT_SUBSYSTEM)
            .withDescription(DataModule.MODULE_ID + ".messaging.record-get")
            .withHeaders(DataHeaders.CONTEXT);

    public static final MessageType[] TYPES = {
            RECORD_UPSERT,
            RECORD_DELETE,
            RECORD_GET
    };
}
