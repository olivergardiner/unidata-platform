/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.data.service.segments.relations;

import org.springframework.stereotype.Component;
import org.unidata.mdm.data.context.RelationMergeContext;
import org.unidata.mdm.data.dto.MergeRelationDTO;
import org.unidata.mdm.data.module.DataModule;
import org.unidata.mdm.system.type.pipeline.Finish;
import org.unidata.mdm.system.type.pipeline.Start;

/**
 * @author Mikhail Mikhailov on Nov 24, 2019
 */
@Component(RelationMergeFinishExecutor.SEGMENT_ID)
public class RelationMergeFinishExecutor extends Finish<RelationMergeContext, MergeRelationDTO> {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = DataModule.MODULE_ID + "[RELATION_MERGE_FINISH]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = DataModule.MODULE_ID + ".relation.merge.finish.description";
    /**
     * Constructor.
     */
    public RelationMergeFinishExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION, MergeRelationDTO.class);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public MergeRelationDTO finish(RelationMergeContext iCtx) {
        return new MergeRelationDTO(iCtx.masterKeys(), iCtx.relationKeys());
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?> start) {
        return RelationMergeContext.class.isAssignableFrom(start.getInputTypeClass());
    }
}
