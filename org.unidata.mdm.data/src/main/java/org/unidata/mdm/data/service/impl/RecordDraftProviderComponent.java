package org.unidata.mdm.data.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.serialization.CoreSerializer;
import org.unidata.mdm.core.type.formless.BundlesArray;
import org.unidata.mdm.data.service.segments.records.RecordDraftGetStartExecutor;
import org.unidata.mdm.data.service.segments.records.RecordDraftPublishStartExecutor;
import org.unidata.mdm.data.service.segments.records.RecordDraftUpsertStartExecutor;
import org.unidata.mdm.draft.service.DraftService;
import org.unidata.mdm.draft.type.DraftOperation;
import org.unidata.mdm.draft.type.DraftProvider;
import org.unidata.mdm.system.service.TextService;

/**
 * @author Mikhail Mikhailov on Sep 25, 2020
 */
@Component
public class RecordDraftProviderComponent implements DraftProvider<BundlesArray> {
    /**
     * This draft provider id.
     */
    private static final String ID = "record";
    /**
     * This draft provider description.
     */
    private static final String DESCRIPTION = "app.data.draft.record.provider.description";
    /**
     * The TS.
     */
    @Autowired
    private TextService textService;
    /**
     * Constructor.
     */
    @Autowired
    public RecordDraftProviderComponent(DraftService draftService) {
        super();
        draftService.register(this);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getId() {
        return ID;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getDescription() {
        return textService.getText(DESCRIPTION);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getPipelineId(DraftOperation operation) {

        switch (operation) {
        case GET_DATA:
            return RecordDraftGetStartExecutor.SEGMENT_ID;
        case UPSERT_DATA:
            return RecordDraftUpsertStartExecutor.SEGMENT_ID;
        case PUBLISH_DATA:
            return RecordDraftPublishStartExecutor.SEGMENT_ID;
        }

        return null;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public BundlesArray fromBytes(byte[] data) {
        return CoreSerializer.bundlesArrayFromProtostuff(data);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public byte[] toBytes(BundlesArray data) {
        return CoreSerializer.bundlesArrayToProtostuff(data);
    }
}
