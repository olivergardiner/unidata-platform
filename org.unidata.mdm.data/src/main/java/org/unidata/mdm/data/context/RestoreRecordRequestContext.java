/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.data.context;

import java.util.Date;

import org.unidata.mdm.core.context.DataRecordContext;
import org.unidata.mdm.core.context.MutableValidityRangeContext;
import org.unidata.mdm.core.type.data.DataRecord;
import org.unidata.mdm.core.type.keys.ExternalId;
import org.unidata.mdm.data.service.segments.records.RecordRestoreStartExecutor;
import org.unidata.mdm.data.type.data.OriginRecord;
import org.unidata.mdm.system.context.SetupAwareContext;

/**
 * @author Mikhail Mikhailov
 * Record upsert.
 */
public class RestoreRecordRequestContext
    extends AbstractRecordIdentityContext
    implements
        DataRecordContext,
        ExternalIdResettingContext,
        MutableValidityRangeContext,
        ReadWriteTimelineContext<OriginRecord>,
        ReadWriteDataContext<OriginRecord>,
        OperationTypeContext,
        AccessRightContext,
        BatchAwareContext,
        SetupAwareContext {

    /**
     * Generated SVUID.
     */
    private static final long serialVersionUID = 6651928422821780602L;
    /**
     * Golden record.
     */
    private final DataRecord record;
    /**
     * Last update date to use (optional).
     */
    private final Date lastUpdate;
    /**
     * For a particular date (as of).
     */
    private final Date forDate;
    /**
     * Set range from.
     */
    private Date validFrom;
    /**
     * Set range to.
     */
    private Date validTo;
    /**
     * The box key.
     */
    protected String boxKey;
    /**
     * Constructor.
     */
    protected RestoreRecordRequestContext(RestoreRecordRequestContextBuilder b) {
        super(b);

        this.record = b.record;
        this.forDate = b.forDate;
        this.lastUpdate = b.lastUpdate;
        this.validFrom = b.validFrom;
        this.validTo = b.validTo;

        flags.set(DataContextFlags.FLAG_IS_PERIOD_RESTORE, b.restorePeriod);
        flags.set(DataContextFlags.FLAG_BATCH_OPERATION, b.batchOperation);
        flags.set(DataContextFlags.FLAG_IS_MODIFIED, b.modified);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getStartTypeId() {
        return RecordRestoreStartExecutor.SEGMENT_ID;
    }

    /**
     * @return the record
     */
    @Override
    public DataRecord getRecord() {
        return record;
    }

    /**
     * @return the forDate
     */
    public Date getForDate() {
        return forDate;
    }

    /**
     * @return the lastUpdate
     */
    public Date getLastUpdate() {
        return lastUpdate;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setExternalId(String externalId) {
        this.externalId = this.externalId != null
                ? ExternalId.of(externalId, this.externalId.getEntityName(), this.externalId.getSourceSystem())
                : null;
    }
    /**
     * @return define that is period restore request.
     */
    public boolean isPeriodRestore() {
        return flags.get(DataContextFlags.FLAG_IS_PERIOD_RESTORE);
    }
    /**
     * Record modified hint.
     * @return true, if modified, false otherwise
     */
    public boolean isModified() {
        return flags.get(DataContextFlags.FLAG_IS_MODIFIED);
    }
    /**
     * @return the validFrom
     */
    @Override
    public Date getValidFrom() {
        return validFrom;
    }

    /**
     * @return the validTo
     */
    @Override
    public Date getValidTo() {
        return validTo;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setValidFrom(Date from) {
        this.validFrom = from;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setValidTo(Date to) {
        this.validTo = to;
    }

    /**
     * Builder shortcut.
     * @return builder
     */
    public static RestoreRecordRequestContextBuilder builder() {
        return new RestoreRecordRequestContextBuilder();
    }

    /**
     * Re-packaging builder shorthand.
     * @return builder
     */
    public static RestoreRecordRequestContextBuilder builder(RestoreRecordRequestContext other) {
        return new RestoreRecordRequestContextBuilder(other);
    }

    /**
     * @author Mikhail Mikhailov
     * Context builder.
     */
    public static class RestoreRecordRequestContextBuilder extends AbstractRecordIdentityContextBuilder<RestoreRecordRequestContextBuilder> {
        /**
         * The data record.
         */
        private DataRecord record;
        /**
         * The TL point.
         */
        private Date forDate;
        /**
         * Last update date to use (optional).
         */
        private Date lastUpdate;
        /**
         * Set range from.
         */
        private Date validFrom;
        /**
         * Set range to.
         */
        private Date validTo;
        /**
         * define that is restore period request.
         */
        private boolean restorePeriod;
        /**
         * Tells the callee, whether the record was modified.
         */
        private boolean modified;
        /**
         * This context is participating in a batch upsert. Collect artifacts instead of upserting immediately.
         */
        private boolean batchOperation;
        /**
         * Constructor.
         */
        protected RestoreRecordRequestContextBuilder() {
            super();
        }
        /**
         * Constructor.
         */
        protected RestoreRecordRequestContextBuilder(RestoreRecordRequestContext other) {
            super(other);
            this.record = other.record;
            this.lastUpdate = other.lastUpdate;
            this.validFrom = other.validFrom;
            this.validTo = other.validTo;
            this.restorePeriod = other.flags.get(DataContextFlags.FLAG_IS_PERIOD_RESTORE);
            this.batchOperation = other.flags.get(DataContextFlags.FLAG_BATCH_OPERATION);
            this.modified = other.getFlag(DataContextFlags.FLAG_IS_MODIFIED);
        }

        /**
         * @param record the golden record to set
         */
        public RestoreRecordRequestContextBuilder record(DataRecord record) {
            this.record = record;
            return this;
        }
        /**
         * @param lastUpdate the last update to set
         */
        public RestoreRecordRequestContextBuilder lastUpdate(Date lastUpdate) {
            this.lastUpdate = lastUpdate;
            return this;
        }
        /**
         * @param forDate the forDate to set
         */
        public RestoreRecordRequestContextBuilder forDate(Date forDate) {
            this.forDate = forDate;
            return this;
        }
        /**
         * @param batchUpsert the flag
         * @return self
         */
        public RestoreRecordRequestContextBuilder batchOperation(boolean batchUpsert) {
            this.batchOperation = batchUpsert;
            return this;
        }
        /**
         * @param validFrom the range from to set
         */
        public RestoreRecordRequestContextBuilder validFrom(Date validFrom) {
            this.validFrom = validFrom;
            return this;
        }
        /**
         * @param validTo the range to to set
         */
        public RestoreRecordRequestContextBuilder validTo(Date validTo) {
            this.validTo = validTo;
            return this;
        }
        /**
         * define that that is a period restore request.
         * @param periodRestore
         * @return self
         */
        public RestoreRecordRequestContextBuilder periodRestore(boolean periodRestore) {
            this.restorePeriod = periodRestore;
            return this;
        }
        /**
         * Tells the callee, whether the record was modified.
         * @param modified the flag
         * @return self
         */
        public RestoreRecordRequestContextBuilder modified(boolean modified) {
            this.modified = modified;
            return this;
        }
        /**
         * @return  get valid from
         */
        public Date getValidFrom() {
            return validFrom;
        }
        /**
         * @return  get valid to
         */
        public Date getValidTo() {
            return validTo;
        }
        /**
         * @return  get record
         */
        public DataRecord getRecord() {
            return record;
        }
        /**
         * Builds a context.
         * @return a new context
         */
        @Override
        public RestoreRecordRequestContext build() {
            return new RestoreRecordRequestContext(this);
        }
    }
}
