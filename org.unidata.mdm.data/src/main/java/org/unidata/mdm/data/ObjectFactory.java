package org.unidata.mdm.data;

import org.unidata.mdm.data.serialization.jaxb.ArrayAttributeImpl;
import org.unidata.mdm.data.serialization.jaxb.CodeAttributeImpl;
import org.unidata.mdm.data.serialization.jaxb.SingleValueAttributeImpl;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.unidata.mdm.data package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
public class ObjectFactory {

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.unidata.mdm.data
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link EtalonRecord }
     * 
     */
    public EtalonRecord createEtalonRecord() {
        return new EtalonRecord();
    }

    /**
     * Create an instance of {@link OriginRecord }
     * 
     */
    public OriginRecord createOriginRecord() {
        return new OriginRecord();
    }

    /**
     * Create an instance of {@link RelationTo }
     * 
     */
    public RelationTo createRelationTo() {
        return new RelationTo();
    }

    /**
     * Create an instance of {@link EtalonRecordFull }
     * 
     */
    public EtalonRecordFull createEtalonRecordFull() {
        return new EtalonRecordFull();
    }

    /**
     * Create an instance of {@link BaseValue }
     * 
     */
    public BaseValue createBaseValue() {
        return new BaseValue();
    }

    /**
     * Create an instance of {@link BlobValue }
     * 
     */
    public BlobValue createBlobValue() {
        return new BlobValue();
    }

    /**
     * Create an instance of {@link ClobValue }
     * 
     */
    public ClobValue createClobValue() {
        return new ClobValue();
    }

    /**
     * Create an instance of {@link MeasuredValue }
     * 
     */
    public MeasuredValue createMeasuredValue() {
        return new MeasuredValue();
    }

    /**
     * Create an instance of {@link AbstractAttribute }
     * 
     */
    public AbstractAttribute createAbstractAttribute() {
        return new AbstractAttribute();
    }

    /**
     * Create an instance of {@link AbstractSingleValueAttribute }
     * 
     */
    public AbstractSingleValueAttribute createAbstractSingleValueAttribute() {
        return new SingleValueAttributeImpl();
    }

    /**
     * Create an instance of {@link SimpleAttribute }
     * 
     */
    public SimpleAttribute createSimpleAttribute() {
        return new SimpleAttribute();
    }

    /**
     * Create an instance of {@link AbstractArrayAttribute }
     * 
     */
    public AbstractArrayAttribute createAbstractArrayAttribute() {
        return new ArrayAttributeImpl();
    }

    /**
     * Create an instance of {@link ArrayAttribute }
     * 
     */
    public ArrayAttribute createArrayAttribute() {
        return new ArrayAttribute();
    }

    /**
     * Create an instance of {@link AbstractCodeAttribute }
     * 
     */
    public AbstractCodeAttribute createAbstractCodeAttribute() {
        return new CodeAttributeImpl();
    }

    /**
     * Create an instance of {@link CodeAttribute }
     * 
     */
    public CodeAttribute createCodeAttribute() {
        return new CodeAttribute();
    }

    /**
     * Create an instance of {@link ComplexAttribute }
     * 
     */
    public ComplexAttribute createComplexAttribute() {
        return new ComplexAttribute();
    }

    /**
     * Create an instance of {@link EtalonKey }
     * 
     */
    public EtalonKey createEtalonKey() {
        return new EtalonKey();
    }

    /**
     * Create an instance of {@link OriginKey }
     * 
     */
    public OriginKey createOriginKey() {
        return new OriginKey();
    }

    /**
     * Create an instance of {@link NestedRecord }
     * 
     */
    public NestedRecord createNestedRecord() {
        return new NestedRecord();
    }

    /**
     * Create an instance of {@link RelationBase }
     * 
     */
    public RelationBase createRelationBase() {
        return new RelationBase();
    }

    /**
     * Create an instance of {@link IntegralRecord }
     * 
     */
    public IntegralRecord createIntegralRecord() {
        return new IntegralRecord();
    }

    /**
     * Create an instance of {@link RelationToInfoSection }
     * 
     */
    public RelationToInfoSection createRelationToInfoSection() {
        return new RelationToInfoSection();
    }

    /**
     * Create an instance of {@link EntityRelations }
     * 
     */
    public EntityRelations createEntityRelations() {
        return new EntityRelations();
    }

    /**
     * Create an instance of {@link EtalonRecordInfoSection }
     * 
     */
    public EtalonRecordInfoSection createEtalonRecordInfoSection() {
        return new EtalonRecordInfoSection();
    }

    /**
     * Create an instance of {@link OriginRecordInfoSection }
     * 
     */
    public OriginRecordInfoSection createOriginRecordInfoSection() {
        return new OriginRecordInfoSection();
    }

    /**
     * Create an instance of {@link DataQualityError }
     * 
     */
    public DataQualityError createDataQualityError() {
        return new DataQualityError();
    }

    /**
     * Create an instance of {@link ExternalSourceId }
     * 
     */
    public ExternalSourceId createExternalSourceId() {
        return new ExternalSourceId();
    }


}
