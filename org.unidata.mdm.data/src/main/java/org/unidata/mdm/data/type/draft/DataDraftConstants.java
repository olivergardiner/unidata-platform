package org.unidata.mdm.data.type.draft;

import org.unidata.mdm.core.type.calculables.ModificationBox;

/**
 * @author Mikhail Mikhailov on Sep 18, 2020
 */
public class DataDraftConstants {
    /**
     * Constructor.
     */
    private DataDraftConstants() {
        super();
    }
    /**
     * All exisiting periods are marked as this.
     * This allows {@link ModificationBox#toCalculationBase()} and {@link ModificationBox#toModifications()} to work as expected.
     */
    public static final int BASE_DRAFT_REVISION = 1;
    /**
     * New draft periods marked as this (see above).
     */
    public static final int USER_DRAFT_REVISION = 0;
    /**
     * New record mark.
     */
    public static final String IS_NEW_RECORD = "IS_NEW_RECORD";
    /**
     * Data bundles (period records), added by user will have this set to 'true'.
     */
    public static final String IS_USER_PERIOD = "IS_USER_PERIOD";
    /**
     * New etalon ID.
     */
    public static final String ETALON_ID = "ETALON_ID";
    /**
     * The name of the entity, being upserted.
     */
    public static final String ENTITY_NAME = "ENTITY_NAME";
    /**
     * The source system.
     */
    public static final String SOURCE_SYSTEM = "SOURCE_SYSTEM";
    /**
     * External id.
     */
    public static final String EXTERNAL_ID = "EXTERNAL_ID";
    /**
     * Valid from.
     */
    public static final String VALID_FROM = "VALID_FROM";
    /**
     * Valid to.
     */
    public static final String VALID_TO = "VALID_TO";
    /**
     * As of.
     */
    public static final String AS_OF = "AS_OF";
    /**
     * Optype.
     */
    public static final String OPERATION_TYPE = "OPERATION_TYPE";
    /**
     * Upsert status.
     */
    public static final String PERIOD_STATUS = "PERIOD_STATUS";
    /**
     * Timestamp.
     */
    public static final String CREATE_TIMESTAMP = "CREATE_TIMESTAMP";
    /**
     * Created by.
     */
    public static final String CREATED_BY = "CREATED_BY";
    /**
     * The data operation.
     */
    public static final String DRAFT_OPERATION = "DRAFT_OPERATION";
}
