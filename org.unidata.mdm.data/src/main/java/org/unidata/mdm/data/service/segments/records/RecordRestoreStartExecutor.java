/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.data.service.segments.records;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.collections4.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.type.calculables.CalculableHolder;
import org.unidata.mdm.core.type.data.Attribute;
import org.unidata.mdm.core.type.data.DataRecord;
import org.unidata.mdm.core.type.data.RecordStatus;
import org.unidata.mdm.core.type.model.AttributeModelElement;
import org.unidata.mdm.core.type.model.EntityModelElement;
import org.unidata.mdm.core.type.timeline.TimeInterval;
import org.unidata.mdm.core.type.timeline.Timeline;
import org.unidata.mdm.data.context.GetRecordTimelineRequestContext;
import org.unidata.mdm.data.context.RecordIdentityContextSupport;
import org.unidata.mdm.data.context.RestoreRecordRequestContext;
import org.unidata.mdm.data.exception.DataConsistencyException;
import org.unidata.mdm.data.exception.DataExceptionIds;
import org.unidata.mdm.data.exception.DataProcessingException;
import org.unidata.mdm.data.module.DataModule;
import org.unidata.mdm.data.service.impl.CommonRecordsComponent;
import org.unidata.mdm.data.service.impl.RecordValidationComponent;
import org.unidata.mdm.data.service.segments.AttributesAutogenerationSupport;
import org.unidata.mdm.data.service.segments.ExternalIdAutogenerationSupport;
import org.unidata.mdm.data.type.apply.RecordRestoreChangeSet;
import org.unidata.mdm.data.type.data.OriginRecord;
import org.unidata.mdm.data.type.keys.RecordKeys;
import org.unidata.mdm.meta.service.MetaModelService;
import org.unidata.mdm.system.exception.ValidationResult;
import org.unidata.mdm.system.type.pipeline.Start;
import org.unidata.mdm.system.type.runtime.MeasurementPoint;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

/**
 * @author Mikhail Mikhailov
 *         Old 'ensure before' part of the ORC.
 */
@Component(RecordRestoreStartExecutor.SEGMENT_ID)
public class RecordRestoreStartExecutor
    extends Start<RestoreRecordRequestContext>
    implements
        RecordIdentityContextSupport,
        AttributesAutogenerationSupport,
        ExternalIdAutogenerationSupport {
    /**
     * This logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(RecordRestoreStartExecutor.class);
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = DataModule.MODULE_ID + "[RECORD_RESTORE_START]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = DataModule.MODULE_ID + ".record.restore.start.description";
    /**
     * Common functionality.
     */
    @Autowired
    private CommonRecordsComponent commonRecordsComponent;
    /**
     * Meta model service.
     */
    @Autowired
    private MetaModelService metaModelService;
    /**
     * The VC.
     */
    @Autowired
    private RecordValidationComponent recordValidationComponent;
    /**
     * Constructor.
     */
    public RecordRestoreStartExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION, RestoreRecordRequestContext.class);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void start(RestoreRecordRequestContext ctx) {
        MeasurementPoint.start();
        try {
            setup(ctx);
        } finally {
            MeasurementPoint.stop();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String subject(RestoreRecordRequestContext ctx) {
        MeasurementPoint.start();
        try {
            setup(ctx);
            RecordKeys keys = ctx.keys();
            return keys.getEntityName();
        } finally {
            MeasurementPoint.stop();
        }
    }

    protected void setup(RestoreRecordRequestContext ctx) {

        if (ctx.setUp()) {
            return;
        }

        MeasurementPoint.start();
        try {
            // 1. Load current timeline and keys
            setupTimeline(ctx);

            // 2. Run autogeneration.
            setupAutogeneration(ctx);

            // 3. Verify
            setupVerify(ctx);

            // 5. Setup other fields
            setupFields(ctx);

            ctx.setUp(true);
        } finally {
            MeasurementPoint.stop();
        }
    }

    protected void setupAutogeneration(RestoreRecordRequestContext ctx) {

        // 0. Stop on valid keys
        if (ctx.isValidRecordKey()) {
            return;
        }

        // 1. Select model element
        String entityName = selectEntityName(ctx);
        if (Objects.isNull(entityName)) {
            return;
        }

        EntityModelElement info = metaModelService.getEntityModelElementById(entityName);

        // 2. Check code attributes generation
        setupAttributesAutogeneration(info, ctx);

        // 3. Check for "ext id autogeneration" rules turned on and apply them
        setupExternalIdAutogeneration(info, ctx);
    }
    /**
     * Loads previous version of the data and merges the input into it.
     *
     * @param ctx current context
     * @param current current timeline
     * @return merged record
     */
    protected void setupMergeWithPreviousVersion(RestoreRecordRequestContext ctx, RecordKeys keys, Timeline<OriginRecord> current) {

        if (Objects.isNull(keys) || Objects.isNull(keys.getOriginKey())) {
            return;
        }

        Date recordFrom = ctx.getValidFrom();
        Date recordTo = ctx.getValidTo();
        OriginRecord prevOrigin = null;
        TimeInterval<OriginRecord> selected = current.selectAsOf(nonNull(recordFrom) ? recordFrom : recordTo);
        if (Objects.nonNull(selected)) {
            for (CalculableHolder<OriginRecord> ch : selected) {
                if (ch.toBoxKey().equals(keys.getOriginKey().toBoxKey())) {
                    prevOrigin = ch.getValue();
                    break;
                }
            }
        }

        if (isNull(prevOrigin)) {
            return;
        }

        // 1st level only
        DataRecord upsert = ctx.getRecord();
        for (Attribute attr : prevOrigin.getAllAttributes()) {
            if (upsert.getAttribute(attr.getName()) != null) {
                continue;
            }
            upsert.addAttribute(attr);
        }
    }

    protected void setupFields(RestoreRecordRequestContext ctx) {

        Date ts = ctx.localTimestamp();
        if (ts == null) {
            ts = new Date(System.currentTimeMillis());
        }

        ctx.timestamp(ts);

        if (Objects.isNull(ctx.changeSet())) {
            ctx.changeSet(new RecordRestoreChangeSet());
        }
    }

    protected void setupTimeline(RestoreRecordRequestContext uCtx) {

        MeasurementPoint.start();
        try {

            RecordKeys keys = uCtx.keys();
            GetRecordTimelineRequestContext tlCtx = GetRecordTimelineRequestContext.builder(uCtx)
                    .fetchData(true)
                    .build();

            tlCtx.keys(keys);

            Timeline<OriginRecord> current = commonRecordsComponent.loadTimeline(tlCtx);
            if (uCtx.isPeriodRestore()) {
                current = current.reduceBy(uCtx.getValidFrom(), uCtx.getValidTo());
            }

            uCtx.currentTimeline(current);
            uCtx.keys(current.getKeys());

        } finally {
            MeasurementPoint.stop();
        }
    }

    protected void setupVerify(RestoreRecordRequestContext ctx) {

        RecordKeys keys = ctx.keys();

        // 1. Check supplied keys validity.
        if (keys == null) {
            final String message = "Record can not be identified by supplied keys. Restore rejected.";
            LOGGER.warn(message, ctx);
            throw new DataProcessingException(message, DataExceptionIds.EX_DATA_RESTORE_INVALID_KEYS);
        }

        // 2. Origin is inactive, discard updates
        if (!ctx.isPeriodRestore() && keys.getEtalonKey().getStatus() != RecordStatus.INACTIVE) {
            final String message = "Etalon [ID: {}] is active. Restore rejected.";
            LOGGER.warn(message, keys.getEtalonKey().getId());
            throw new DataProcessingException(message, DataExceptionIds.EX_DATA_RESTORE_ETALON_ACTIVE, keys.getEtalonKey().getId());
        }

        // 2. Origin is inactive, discard updates
        if (ctx.isPeriodRestore() && keys.getEtalonKey().getStatus() != RecordStatus.ACTIVE) {
            final String message = "Etalon [ID: {}] is inactive. Period restore rejected.";
            LOGGER.warn(message, keys.getEtalonKey().getId());
            throw new DataProcessingException(message, DataExceptionIds.EX_DATA_RESTORE_PERIOD_INACTIVE, keys.getEtalonKey().getId());
        }

        // 3. Check ability to restore record.
        Collection<DataRecord> check;
        Timeline<OriginRecord> current = ctx.currentTimeline();
        if (ctx.isPeriodRestore()) {

            List<TimeInterval<OriginRecord>> selection = current.selectBy(ctx.getValidFrom(), ctx.getValidTo());
            if (selection.isEmpty()) {
                throw new DataProcessingException("Cannot restore period. No intervals exist for from [{}] and to [{}].",
                        DataExceptionIds.EX_DATA_RESTORE_EMPTY_PERIOD, ctx.getValidFrom(), ctx.getValidTo());
            }

            check = new ArrayList<>(selection.size());
            for (TimeInterval<OriginRecord> i : selection) {
                check.add(i.getCalculationResult());
            }

        } else {
            if (ctx.hasData()) {
                check = Collections.singletonList(ctx.getRecord());
            } else {
                TimeInterval<OriginRecord> i = current.selectAsOf(ctx.getForDate());
                if (Objects.isNull(i)) {
                    throw new DataProcessingException("Cannot restore record. No data exists for asOf date [{}].",
                            DataExceptionIds.EX_DATA_RESTORE_EMPTY_RECORD, ctx.getForDate());
                }

                check = Collections.singletonList(i.getCalculationResult());
            }
        }

        Map<AttributeModelElement, List<Object>> collected = check.stream()
            .map(r -> recordValidationComponent.checkMissedLookupValues(keys, r))
            .flatMap(m -> m.entrySet().stream())
            .collect(Collectors.groupingBy(Entry::getKey, Collectors.flatMapping(entry -> entry.getValue().stream(), Collectors.toList())));

        if (MapUtils.isNotEmpty(collected)) {

            List<ValidationResult> v = collected.entrySet().stream()
                .map(entry -> new ValidationResult(
                        "Attribute [{}] references missing lookup code values [{}].",
                        "attribute.references.missing.code.values", entry.getKey().getName(), entry.getValue()))
                .collect(Collectors.toList());

            throw new DataConsistencyException("Some attributes reference missing code values.",
                    DataExceptionIds.EX_DATA_RESTORE_MISSING_CODE_REFERENCED, v);
        }
    }
}
