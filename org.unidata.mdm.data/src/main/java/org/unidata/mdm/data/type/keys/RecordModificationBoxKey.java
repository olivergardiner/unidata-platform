package org.unidata.mdm.data.type.keys;

import org.unidata.mdm.core.type.calculables.ModificationBoxKey;

/**
 * Specific record keys markr interface.
 * @author Mikhail Mikhailov on Feb 15, 2020
 */
public interface RecordModificationBoxKey extends ModificationBoxKey {
    /**
     * Creates box key.
     * @param sourceSystem the source system
     * @param externalId the external id
     * @return key
     */
    static String toRecordBoxKey(String sourceSystem, String externalId) {
        return String.join("|", sourceSystem, externalId);
    }
}
