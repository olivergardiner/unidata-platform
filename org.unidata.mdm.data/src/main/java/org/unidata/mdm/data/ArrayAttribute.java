package org.unidata.mdm.data;

import java.io.Serializable;
import java.util.Collection;
import javax.xml.datatype.XMLGregorianCalendar;

import org.unidata.mdm.data.serialization.jaxb.ArrayAttributeImpl;


/**
 * 
 * Массив.
 *             
 * 
 * <p>Java class for ArrayAttribute complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayAttribute"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://data.mdm.unidata.org/}AbstractArrayAttribute"&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
public class ArrayAttribute extends ArrayAttributeImpl implements Serializable {

    private final static long serialVersionUID = 12345L;

    @Override
    public ArrayAttribute withIntValue(Long... values) {
        if (values!= null) {
            for (Long value: values) {
                getIntValue().add(value);
            }
        }
        return this;
    }

    @Override
    public ArrayAttribute withIntValue(Collection<Long> values) {
        if (values!= null) {
            getIntValue().addAll(values);
        }
        return this;
    }

    @Override
    public ArrayAttribute withDateValue(XMLGregorianCalendar... values) {
        if (values!= null) {
            for (XMLGregorianCalendar value: values) {
                getDateValue().add(value);
            }
        }
        return this;
    }

    @Override
    public ArrayAttribute withDateValue(Collection<XMLGregorianCalendar> values) {
        if (values!= null) {
            getDateValue().addAll(values);
        }
        return this;
    }

    @Override
    public ArrayAttribute withTimeValue(XMLGregorianCalendar... values) {
        if (values!= null) {
            for (XMLGregorianCalendar value: values) {
                getTimeValue().add(value);
            }
        }
        return this;
    }

    @Override
    public ArrayAttribute withTimeValue(Collection<XMLGregorianCalendar> values) {
        if (values!= null) {
            getTimeValue().addAll(values);
        }
        return this;
    }

    @Override
    public ArrayAttribute withTimestampValue(XMLGregorianCalendar... values) {
        if (values!= null) {
            for (XMLGregorianCalendar value: values) {
                getTimestampValue().add(value);
            }
        }
        return this;
    }

    @Override
    public ArrayAttribute withTimestampValue(Collection<XMLGregorianCalendar> values) {
        if (values!= null) {
            getTimestampValue().addAll(values);
        }
        return this;
    }

    @Override
    public ArrayAttribute withStringValue(String... values) {
        if (values!= null) {
            for (String value: values) {
                getStringValue().add(value);
            }
        }
        return this;
    }

    @Override
    public ArrayAttribute withStringValue(Collection<String> values) {
        if (values!= null) {
            getStringValue().addAll(values);
        }
        return this;
    }

    @Override
    public ArrayAttribute withNumberValue(Double... values) {
        if (values!= null) {
            for (Double value: values) {
                getNumberValue().add(value);
            }
        }
        return this;
    }

    @Override
    public ArrayAttribute withNumberValue(Collection<Double> values) {
        if (values!= null) {
            getNumberValue().addAll(values);
        }
        return this;
    }

    @Override
    public ArrayAttribute withName(String value) {
        setName(value);
        return this;
    }

}
