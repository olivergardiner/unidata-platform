/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.data.service.segments.records;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.exception.PlatformSecurityException;
import org.unidata.mdm.core.type.security.Right;
import org.unidata.mdm.core.util.SecurityUtils;
import org.unidata.mdm.data.context.AccessRightContext;
import org.unidata.mdm.data.context.RecordIdentityContext;
import org.unidata.mdm.data.context.RecordIdentityContextSupport;
import org.unidata.mdm.data.exception.DataExceptionIds;
import org.unidata.mdm.data.module.DataModule;
import org.unidata.mdm.data.type.keys.RecordKeys;
import org.unidata.mdm.system.type.pipeline.PipelineInput;
import org.unidata.mdm.system.type.pipeline.Point;
import org.unidata.mdm.system.type.pipeline.Start;

/**
 * @author Mikhail Mikhailov
 * The former GET SEC executor.
 */
@Component(RecordReadAccessExecutor.SEGMENT_ID)
public class RecordReadAccessExecutor extends Point<PipelineInput> implements RecordIdentityContextSupport {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = DataModule.MODULE_ID + "[RECORD_READ_ACCESS]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = DataModule.MODULE_ID + ".record.read.security.description";

    /**
     * Logger.
     */
    private static final Logger LOGGER
        = LoggerFactory.getLogger(RecordReadAccessExecutor.class);

    /**
     * Constructor.
     */
    public RecordReadAccessExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void point(PipelineInput ctx) {

        RecordIdentityContext iCtx = (RecordIdentityContext) ctx;
        AccessRightContext aCtx = (AccessRightContext) ctx;

        Right rights = SecurityUtils.getRightsForResourceWithDefault(selectEntityName(iCtx));
        if (!rights.isRead()) {

            RecordKeys keys = iCtx.keys();
            final String message = "The user '{}' has no or unsufficient read rights for resource '{}'. Read denied.";
            LOGGER.info(message, SecurityUtils.getCurrentUserName(), keys.getEntityName());
            throw new PlatformSecurityException(message,
                    DataExceptionIds.EX_DATA_READ_NO_RIGHTS,
                    SecurityUtils.getCurrentUserName(), keys.getEntityName());
        }

        aCtx.accessRight(rights);
    }

    @Override
    public boolean supports(Start<?> start) {
        return RecordIdentityContext.class.isAssignableFrom(start.getInputTypeClass())
            && AccessRightContext.class.isAssignableFrom(start.getInputTypeClass());
    }
}
