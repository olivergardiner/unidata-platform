package org.unidata.mdm.data;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * <p>Java class for ArrayDataType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ArrayDataType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Date"/&gt;
 *     &lt;enumeration value="Time"/&gt;
 *     &lt;enumeration value="Timestamp"/&gt;
 *     &lt;enumeration value="String"/&gt;
 *     &lt;enumeration value="Integer"/&gt;
 *     &lt;enumeration value="Number"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
public enum ArrayDataType {

    @JsonProperty("Date")
    DATE("Date"),
    @JsonProperty("Time")
    TIME("Time"),
    @JsonProperty("Timestamp")
    TIMESTAMP("Timestamp"),
    @JsonProperty("String")
    STRING("String"),
    @JsonProperty("Integer")
    INTEGER("Integer"),
    @JsonProperty("Number")
    NUMBER("Number");
    private final String value;

    ArrayDataType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ArrayDataType fromValue(String v) {
        for (ArrayDataType c: ArrayDataType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
