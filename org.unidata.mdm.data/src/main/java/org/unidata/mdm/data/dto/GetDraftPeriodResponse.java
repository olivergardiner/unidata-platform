package org.unidata.mdm.data.dto;

import org.unidata.mdm.core.dto.ResourceSpecificRightDTO;
import org.unidata.mdm.core.type.data.impl.SimpleAttributesDiff;
import org.unidata.mdm.data.type.data.EtalonRecord;
import org.unidata.mdm.draft.type.DraftPayloadResponse;

/**
 * @author Mikhail Mikhailov on Sep 24, 2020
 */
public class GetDraftPeriodResponse implements DraftPayloadResponse {
    /**
     * The record.
     */
    private EtalonRecord record;
    /**
     * Rights.
     */
    private ResourceSpecificRightDTO rights;
    /**
     * Diff to record's current approved state.
     */
    private SimpleAttributesDiff diffToCurrent;
    /**
     * Diff to a previous or particular draft revision state.
     */
    private SimpleAttributesDiff diffToDraft;
    /**
     * Constructor.
     */
    public GetDraftPeriodResponse() {
        super();
    }
    /**
     * @return the record
     */
    public EtalonRecord getRecord() {
        return record;
    }
    /**
     * @param record the record to set
     */
    public void setRecord(EtalonRecord record) {
        this.record = record;
    }
    /**
     * @return the rights
     */
    public ResourceSpecificRightDTO getRights() {
        return rights;
    }
    /**
     * @param rights the rights to set
     */
    public void setRights(ResourceSpecificRightDTO rights) {
        this.rights = rights;
    }
    /**
     * @return the diffToCurrent
     */
    public SimpleAttributesDiff getDiffToCurrent() {
        return diffToCurrent;
    }
    /**
     * @param diffToCurrent the diffToCurrent to set
     */
    public void setDiffToCurrent(SimpleAttributesDiff diffToCurrent) {
        this.diffToCurrent = diffToCurrent;
    }
    /**
     * @return the diffToDraft
     */
    public SimpleAttributesDiff getDiffToDraft() {
        return diffToDraft;
    }
    /**
     * @param diffToDraft the diffToDraft to set
     */
    public void setDiffToDraft(SimpleAttributesDiff diffToDraft) {
        this.diffToDraft = diffToDraft;
    }
}
