/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 * 
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.data.serialization.jaxb;

import java.time.LocalDateTime;
import java.util.Objects;
import javax.xml.datatype.XMLGregorianCalendar;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.unidata.mdm.data.AbstractSingleValueAttribute;
import org.unidata.mdm.data.BlobValue;
import org.unidata.mdm.data.ClobValue;
import org.unidata.mdm.data.MeasuredValue;
import org.unidata.mdm.data.ValueDataType;

/**
 * @author Mikhail Mikhailov
 *         Simple attribute value custom implementation.
 */
@SuppressWarnings("serial")
public class SingleValueAttributeImpl extends AbstractSingleValueAttribute {

    /**
     * Value data type.
     */
    @JsonIgnore
    private ValueDataType type;

    /**
     * Constructor.
     */
    public SingleValueAttributeImpl() {
        super();
    }

    /**
     * @return the type
     */
    public ValueDataType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    protected void setType(ValueDataType type) {
        this.type = type;
    }

    @Override
    public void setBlobValue(BlobValue value) {
        super.setBlobValue(value);
        this.type = ValueDataType.BLOB;
    }

    @Override
    public void setClobValue(ClobValue value) {
        super.setClobValue(value);
        this.type = ValueDataType.CLOB;
    }

    @Override
    public void setIntValue(Long value) {
        super.setIntValue(value);
        this.type = ValueDataType.INTEGER;
    }

    @Override
    public void setDateValue(XMLGregorianCalendar value) {
        super.setDateValue(value);
        this.type = ValueDataType.DATE;
    }

    @Override
    public void setTimeValue(XMLGregorianCalendar value) {
        super.setTimeValue(value);
        this.type = ValueDataType.TIME;
    }

    @Override
    public void setTimestampValue(XMLGregorianCalendar value) {
        super.setTimestampValue(value);
        this.type = ValueDataType.TIMESTAMP;
    }

    @Override
    public void setStringValue(String value) {
        super.setStringValue(value);
        this.type = ValueDataType.STRING;
    }

    @Override
    public void setNumberValue(Double value) {
        super.setNumberValue(value);
        this.type = ValueDataType.NUMBER;
    }

    @Override
    public void setBoolValue(Boolean value) {
        super.setBoolValue(value);
        this.type = ValueDataType.BOOLEAN;
    }

    @Override
    public void setMeasuredValue(MeasuredValue value) {
        super.setMeasuredValue(value);
        this.type = ValueDataType.MEASURED;
    }

    /**
     * Supports MF fluent interface for {@link BlobValue} manually.
     * @param value the value
     *
     * @return self
     */
    public SingleValueAttributeImpl withBlobValue(BlobValue value) {
        setBlobValue(value);
        return this;
    }

    /**
     * Supports MF fluent interface for {@link MeasuredValue} manually.
     * @param value the value
     *
     * @return self
     */
    public SingleValueAttributeImpl withMeasuredValue(MeasuredValue value) {
        setMeasuredValue(value);
        return this;
    }

    /**
     * Supports MF fluent interface for {@link ClobValue} manually.
     * @param value the value
     *
     * @return self
     */
    public SingleValueAttributeImpl withClobValue(ClobValue value) {
        setClobValue(value);
        return this;
    }

    /**
     * Supports MF fluent interface for {@link Long} manually.
     * @param value the value
     *
     * @return self
     */
    public SingleValueAttributeImpl withIntValue(Long value) {
        setIntValue(value);
        return this;
    }

    /**
     * Supports MF fluent interface for date values manually.
     * @param value the value
     *
     * @return self
     */
    public SingleValueAttributeImpl withDateValue(XMLGregorianCalendar value) {
        setDateValue(value);
        return this;
    }

    /**
     * Supports MF fluent interface for time values manually.
     * @param value the value
     *
     * @return self
     */
    public SingleValueAttributeImpl withTimeValue(XMLGregorianCalendar value) {
        setTimeValue(value);
        return this;
    }

    /**
     * Supports MF fluent interface for timestamp values manually.
     * @param value the value
     *
     * @return self
     */
    public SingleValueAttributeImpl withTimestampValue(XMLGregorianCalendar value) {
        setTimestampValue(value);
        return this;
    }

    /**
     * Supports MF fluent interface for string values manually.
     * @param value the value
     *
     * @return self
     */
    public SingleValueAttributeImpl withStringValue(String value) {
        setStringValue(value);
        return this;
    }

    /**
     * Supports MF fluent interface for {@link Double} values manually.
     * @param value the value
     *
     * @return self
     */
    public SingleValueAttributeImpl withNumberValue(Double value) {
        setNumberValue(value);
        return this;
    }

    /**
     * Supports MF fluent interface for {@link Boolean} values manually.
     * @param value the value
     *
     * @return self
     */
    public SingleValueAttributeImpl withBoolValue(Boolean value) {
        setBoolValue(value);
        return this;
    }

    /**
     * Gets value of the attribute.
     *
     * @return the value
     */
    @SuppressWarnings("unchecked")
    public <T> T getValue() {
        if (type != null) {
            switch (type) {
                case STRING:
                    return (T) stringValue;
                case INTEGER:
                    return (T) intValue;
                case NUMBER:
                    return (T) numberValue;
                case BOOLEAN:
                    return (T) boolValue;
                case DATE:
                    return (T) dateValue;
                case TIME:
                    return (T) timeValue;
                case TIMESTAMP:
                    return (T) timestampValue;
                case BLOB:
                    return (T) blobValue;
                case CLOB:
                    return (T) clobValue;
                case MEASURED:
                    return (T) measuredValue;
            }
        }

        return null;
    }

    /**
     * @see Object#hashCode()
     * TODO re-write this crap asap. Introduce solid value identity system instead.
     */
    @Override
    public int hashCode() {

        if (type == ValueDataType.BLOB) {
            BlobValue bv = getValue();
            return Objects.hash(type,
                    bv != null ? bv.getFileName() : null,
                    bv != null ? bv.getSize() : null,
                    bv != null ? bv.getMimeType() : null,
                    bv != null ? bv.getId() : null);
        }

        if (type == ValueDataType.CLOB) {
            ClobValue cv = getValue();
            return Objects.hash(type,
                    cv != null ? cv.getFileName() : null,
                    cv != null ? cv.getSize() : null,
                    cv != null ? cv.getMimeType() : null,
                    cv != null ? cv.getId() : null);
        }

        if (type == ValueDataType.MEASURED) {
            MeasuredValue mv = getValue();
            return Objects.hash(type,
                    mv == null ? null : mv.getValue(),
                    mv == null ? null : mv.getMeasurementValueId(),
                    mv == null ? null : mv.getMeasurementUnitId());
        }

        if (type == ValueDataType.DATE || type == ValueDataType.TIME || type == ValueDataType.TIMESTAMP) {
            XMLGregorianCalendar xgc = getValue();
            return Objects.hash(type, xgc != null
                    ? LocalDateTime.ofInstant(xgc.toGregorianCalendar().toInstant(), java.time.ZoneId.systemDefault())
                    : null);
        }

        return Objects.hash(type, getValue());
    }

    /**
     * @see Object#equals(Object)
     */
    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }

        if (obj == null) {
            return false;
        }

        if (!getClass().isInstance(obj)) {
            return false;
        }

        SingleValueAttributeImpl other = (SingleValueAttributeImpl) obj;
        if (type != other.type) {
            return false;
        }

        return Objects.equals(getValue(), other.getValue());
    }
}
