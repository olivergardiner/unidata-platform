package org.unidata.mdm.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import static org.unidata.mdm.DataNamespace.DATA_NAMESPACE;


/**
 * 
 * Рекурсивная структура, описывающая запись произвольной вложенной сущности. Состоит из произвольного количества простых атрибутов, а также сложных атрибутов, обеспечивающих рекурсивного погружение.
 *             
 * 
 * <p>Java class for NestedRecord complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="NestedRecord"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="simpleAttribute" type="{http://data.mdm.unidata.org/}SimpleAttribute" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="codeAttribute" type="{http://data.mdm.unidata.org/}CodeAttribute" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="arrayAttribute" type="{http://data.mdm.unidata.org/}ArrayAttribute" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="complexAttribute" type="{http://data.mdm.unidata.org/}ComplexAttribute" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@JsonPropertyOrder({"id","simpleAttributes","codeAttributes","arrayAttributes","complexAttributes"})
public class NestedRecord implements Serializable {

    private final static long serialVersionUID = 12345L;

    @JacksonXmlProperty(namespace = DATA_NAMESPACE)
    protected String id;
    @JacksonXmlProperty(localName = "simpleAttribute", namespace = DATA_NAMESPACE)
    protected List<SimpleAttribute> simpleAttributes;
    @JacksonXmlProperty(localName = "codeAttribute", namespace = DATA_NAMESPACE)
    protected List<CodeAttribute> codeAttributes;
    @JacksonXmlProperty(localName = "arrayAttribute", namespace = DATA_NAMESPACE)
    protected List<ArrayAttribute> arrayAttributes;
    @JacksonXmlProperty(localName = "complexAttribute", namespace = DATA_NAMESPACE)
    protected List<ComplexAttribute> complexAttributes;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the simpleAttributes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the simpleAttributes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSimpleAttributes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SimpleAttribute }
     * 
     * 
     */
    public List<SimpleAttribute> getSimpleAttributes() {
        if (simpleAttributes == null) {
            simpleAttributes = new ArrayList<SimpleAttribute>();
        }
        return this.simpleAttributes;
    }

    /**
     * Gets the value of the codeAttributes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the codeAttributes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCodeAttributes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CodeAttribute }
     * 
     * 
     */
    public List<CodeAttribute> getCodeAttributes() {
        if (codeAttributes == null) {
            codeAttributes = new ArrayList<CodeAttribute>();
        }
        return this.codeAttributes;
    }

    /**
     * Gets the value of the arrayAttributes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the arrayAttributes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getArrayAttributes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ArrayAttribute }
     * 
     * 
     */
    public List<ArrayAttribute> getArrayAttributes() {
        if (arrayAttributes == null) {
            arrayAttributes = new ArrayList<ArrayAttribute>();
        }
        return this.arrayAttributes;
    }

    /**
     * Gets the value of the complexAttributes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the complexAttributes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getComplexAttributes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ComplexAttribute }
     * 
     * 
     */
    public List<ComplexAttribute> getComplexAttributes() {
        if (complexAttributes == null) {
            complexAttributes = new ArrayList<ComplexAttribute>();
        }
        return this.complexAttributes;
    }

    public NestedRecord withId(String value) {
        setId(value);
        return this;
    }

    public NestedRecord withSimpleAttributes(SimpleAttribute... values) {
        if (values!= null) {
            for (SimpleAttribute value: values) {
                getSimpleAttributes().add(value);
            }
        }
        return this;
    }

    public NestedRecord withSimpleAttributes(Collection<SimpleAttribute> values) {
        if (values!= null) {
            getSimpleAttributes().addAll(values);
        }
        return this;
    }

    public NestedRecord withCodeAttributes(CodeAttribute... values) {
        if (values!= null) {
            for (CodeAttribute value: values) {
                getCodeAttributes().add(value);
            }
        }
        return this;
    }

    public NestedRecord withCodeAttributes(Collection<CodeAttribute> values) {
        if (values!= null) {
            getCodeAttributes().addAll(values);
        }
        return this;
    }

    public NestedRecord withArrayAttributes(ArrayAttribute... values) {
        if (values!= null) {
            for (ArrayAttribute value: values) {
                getArrayAttributes().add(value);
            }
        }
        return this;
    }

    public NestedRecord withArrayAttributes(Collection<ArrayAttribute> values) {
        if (values!= null) {
            getArrayAttributes().addAll(values);
        }
        return this;
    }

    public NestedRecord withComplexAttributes(ComplexAttribute... values) {
        if (values!= null) {
            for (ComplexAttribute value: values) {
                getComplexAttributes().add(value);
            }
        }
        return this;
    }

    public NestedRecord withComplexAttributes(Collection<ComplexAttribute> values) {
        if (values!= null) {
            getComplexAttributes().addAll(values);
        }
        return this;
    }

}
