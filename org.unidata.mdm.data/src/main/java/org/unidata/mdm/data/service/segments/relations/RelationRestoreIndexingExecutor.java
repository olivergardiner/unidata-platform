/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.data.service.segments.relations;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;
import org.unidata.mdm.core.type.timeline.TimeInterval;
import org.unidata.mdm.core.type.timeline.Timeline;
import org.unidata.mdm.data.context.RelationRestoreContext;
import org.unidata.mdm.data.convert.RelationIndexingConverter;
import org.unidata.mdm.data.module.DataModule;
import org.unidata.mdm.data.type.apply.RelationRestoreChangeSet;
import org.unidata.mdm.data.type.data.EtalonRelation;
import org.unidata.mdm.data.type.data.OriginRelation;
import org.unidata.mdm.data.type.keys.RelationKeys;
import org.unidata.mdm.meta.type.search.RelationFromIndexId;
import org.unidata.mdm.meta.type.search.RelationToIndexId;
import org.unidata.mdm.search.configuration.SearchConfigurationConstants;
import org.unidata.mdm.search.context.IndexRequestContext;
import org.unidata.mdm.search.type.id.ManagedIndexId;
import org.unidata.mdm.search.type.indexing.Indexing;
import org.unidata.mdm.system.type.annotation.ConfigurationRef;
import org.unidata.mdm.system.type.configuration.ConfigurationValue;
import org.unidata.mdm.system.type.pipeline.Point;
import org.unidata.mdm.system.type.pipeline.Start;
import org.unidata.mdm.system.type.runtime.MeasurementPoint;

/**
 * Executor responsible for modifying relations have an alias key.
 */
@Component(RelationRestoreIndexingExecutor.SEGMENT_ID)
public class RelationRestoreIndexingExecutor extends Point<RelationRestoreContext> {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = DataModule.MODULE_ID + "[RELATION_RESTORE_INDEXING]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = DataModule.MODULE_ID + ".relation.restore.indexing.description";
    /**
     * Delay for async audit operations.
     */
    @ConfigurationRef(SearchConfigurationConstants.PROPERTY_REFRESH_IMMEDIATE)
    private ConfigurationValue<Boolean> refreshImmediate;
    /**
     * Constructor.
     */
    public RelationRestoreIndexingExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void point(RelationRestoreContext ctx) {
        MeasurementPoint.start();
        try {
            final RelationKeys keys = ctx.relationKeys();
            final RelationRestoreChangeSet cs = ctx.changeSet();
            final Timeline<OriginRelation> current = ctx.currentTimeline();
            final Timeline<OriginRelation> next = ctx.nextTimeline();

            IndexRequestContext irc = IndexRequestContext.builder()
                    .drop(true)
                    .entity(keys.getFromEntityName())
                    .delete(collectDeletes(current))
                    .index(collectUpdates(next))
                    .refresh(!ctx.isBatchOperation() && refreshImmediate.getValue())
                    .build();

            cs.getIndexRequestContexts().add(irc);
        } finally {
            MeasurementPoint.stop();
        }
    }

    protected Collection<ManagedIndexId> collectDeletes(Timeline<OriginRelation> current) {

        if (current.isEmpty()) {
            return Collections.emptyList();
        }

        final RelationKeys keys = current.getKeys();
        final List<ManagedIndexId> deletes = new ArrayList<>();
        current.forEach(interval -> {
            deletes.add(
                    RelationFromIndexId.of(
                            keys.getFromEntityName(),
                            keys.getRelationName(),
                            keys.getEtalonKey().getFrom().getId(),
                            keys.getEtalonKey().getTo().getId(),
                            interval.getPeriodId()));
            deletes.add(
                    RelationToIndexId.of(
                            keys.getToEntityName(),
                            keys.getRelationName(),
                            keys.getEtalonKey().getFrom().getId(),
                            keys.getEtalonKey().getTo().getId(),
                            interval.getPeriodId()));
        });

        return deletes;
    }

    protected Collection<Indexing> collectUpdates(Timeline<OriginRelation> next) {
        return RelationIndexingConverter.convert(next.getKeys(), next.stream()
                .filter(Objects::nonNull)
                .map(TimeInterval::<EtalonRelation>getCalculationResult)
                .collect(Collectors.toList()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?> start) {
        return RelationRestoreContext.class.isAssignableFrom(start.getInputTypeClass());
    }
}
