/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.data.type.data;

import java.util.Objects;

import org.unidata.mdm.meta.type.model.entities.RelType;

/**
 * @author Mikhail Mikhailov
 * Relation type.
 * This is a duplication of the RelType enum, just to have the type value unrelated.
 */
public enum RelationType {
    /**
     * Reference relation.
     */
    REFERENCES,
    /**
     * Containment relation.
     */
    CONTAINS,
    /**
     * Many to many relation.
     */
    MANY_TO_MANY;
    /**
     * Convenient value method.
     * @return value / name.
     */
    public String value() {
        return name();
    }
    /**
     * Covenient from value wmethod.
     * @param v value / name
     * @return parsed value
     */
    public static RelationType fromValue(String v) {
        return valueOf(v);
    }

    public static RelationType fromModelType(RelType v) {

        if (Objects.nonNull(v)) {
            switch (v) {
            case CONTAINS:
                return RelationType.CONTAINS;
            case MANY_TO_MANY:
                return RelationType.MANY_TO_MANY;
            case REFERENCES:
                return REFERENCES;
            default:
                break;
            }
        }

        return null;
    }
}
