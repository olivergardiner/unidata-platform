package org.unidata.mdm.data;

import java.io.Serializable;
import java.util.Collection;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import static org.unidata.mdm.DataNamespace.DATA_NAMESPACE;


/**
 * 
 * Структура полностью описывающая основную запись сущности. Является логическим расширением структуры 'NestedRecord'. Всегда содержит
 * - имя сущности, такое как 'Банк', 'Сотдрудник' итд.
 * - набор простых и сложных атрибутов
 * - значение ключа идентифицирующего основную запись
 * - список связей данной сущности
 *             
 * 
 * <p>Java class for EtalonRecord complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EtalonRecord"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://data.mdm.unidata.org/}NestedRecord"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="etalonKey" type="{http://data.mdm.unidata.org/}EtalonKey"/&gt;
 *         &lt;element name="relations" type="{http://data.mdm.unidata.org/}EntityRelations" minOccurs="0"/&gt;
 *         &lt;element name="infoSection" type="{http://data.mdm.unidata.org/}EtalonRecordInfoSection" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@JacksonXmlRootElement(localName = "etalonRecord", namespace = DATA_NAMESPACE)
@JsonPropertyOrder({"etalonKey","relations","infoSection"})
public class EtalonRecord extends NestedRecord implements Serializable {

    private final static long serialVersionUID = 12345L;

    @JacksonXmlProperty(namespace = DATA_NAMESPACE)
    protected EtalonKey etalonKey;
    @JacksonXmlProperty(namespace = DATA_NAMESPACE)
    protected EntityRelations relations;
    @JacksonXmlProperty(namespace = DATA_NAMESPACE)
    protected EtalonRecordInfoSection infoSection;

    /**
     * Gets the value of the etalonKey property.
     * 
     * @return
     *     possible object is
     *     {@link EtalonKey }
     *     
     */
    public EtalonKey getEtalonKey() {
        return etalonKey;
    }

    /**
     * Sets the value of the etalonKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link EtalonKey }
     *     
     */
    public void setEtalonKey(EtalonKey value) {
        this.etalonKey = value;
    }

    /**
     * Gets the value of the relations property.
     * 
     * @return
     *     possible object is
     *     {@link EntityRelations }
     *     
     */
    public EntityRelations getRelations() {
        return relations;
    }

    /**
     * Sets the value of the relations property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntityRelations }
     *     
     */
    public void setRelations(EntityRelations value) {
        this.relations = value;
    }

    /**
     * Gets the value of the infoSection property.
     * 
     * @return
     *     possible object is
     *     {@link EtalonRecordInfoSection }
     *     
     */
    public EtalonRecordInfoSection getInfoSection() {
        return infoSection;
    }

    /**
     * Sets the value of the infoSection property.
     * 
     * @param value
     *     allowed object is
     *     {@link EtalonRecordInfoSection }
     *     
     */
    public void setInfoSection(EtalonRecordInfoSection value) {
        this.infoSection = value;
    }

    public EtalonRecord withEtalonKey(EtalonKey value) {
        setEtalonKey(value);
        return this;
    }

    public EtalonRecord withRelations(EntityRelations value) {
        setRelations(value);
        return this;
    }

    public EtalonRecord withInfoSection(EtalonRecordInfoSection value) {
        setInfoSection(value);
        return this;
    }

    @Override
    public EtalonRecord withId(String value) {
        setId(value);
        return this;
    }

    @Override
    public EtalonRecord withSimpleAttributes(SimpleAttribute... values) {
        if (values!= null) {
            for (SimpleAttribute value: values) {
                getSimpleAttributes().add(value);
            }
        }
        return this;
    }

    @Override
    public EtalonRecord withSimpleAttributes(Collection<SimpleAttribute> values) {
        if (values!= null) {
            getSimpleAttributes().addAll(values);
        }
        return this;
    }

    @Override
    public EtalonRecord withCodeAttributes(CodeAttribute... values) {
        if (values!= null) {
            for (CodeAttribute value: values) {
                getCodeAttributes().add(value);
            }
        }
        return this;
    }

    @Override
    public EtalonRecord withCodeAttributes(Collection<CodeAttribute> values) {
        if (values!= null) {
            getCodeAttributes().addAll(values);
        }
        return this;
    }

    @Override
    public EtalonRecord withArrayAttributes(ArrayAttribute... values) {
        if (values!= null) {
            for (ArrayAttribute value: values) {
                getArrayAttributes().add(value);
            }
        }
        return this;
    }

    @Override
    public EtalonRecord withArrayAttributes(Collection<ArrayAttribute> values) {
        if (values!= null) {
            getArrayAttributes().addAll(values);
        }
        return this;
    }

    @Override
    public EtalonRecord withComplexAttributes(ComplexAttribute... values) {
        if (values!= null) {
            for (ComplexAttribute value: values) {
                getComplexAttributes().add(value);
            }
        }
        return this;
    }

    @Override
    public EtalonRecord withComplexAttributes(Collection<ComplexAttribute> values) {
        if (values!= null) {
            getComplexAttributes().addAll(values);
        }
        return this;
    }

}
