package org.unidata.mdm.data;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;


/**
 * <p>Java class for RelationBase complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RelationBase"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="relName" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
//@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.WRAPPER_OBJECT)
//@JsonSubTypes({
//    @JsonSubTypes.Type(value = IntegralRecord.class, name = "integralEntity"),
//    @JsonSubTypes.Type(value = RelationTo.class, name = "relationTo"),
//})
public class RelationBase implements Serializable {

    private final static long serialVersionUID = 12345L;

    @JacksonXmlProperty(isAttribute = true)
    protected String relName;

    /**
     * Gets the value of the relName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelName() {
        return relName;
    }

    /**
     * Sets the value of the relName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelName(String value) {
        this.relName = value;
    }

    public RelationBase withRelName(String value) {
        setRelName(value);
        return this;
    }

}
