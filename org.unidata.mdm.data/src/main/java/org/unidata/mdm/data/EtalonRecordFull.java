package org.unidata.mdm.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import static org.unidata.mdm.DataNamespace.DATA_NAMESPACE;


/**
 * <p>Java class for EtalonRecordFull complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EtalonRecordFull"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="etalonRecord" type="{http://data.mdm.unidata.org/}EtalonRecord"/&gt;
 *         &lt;element name="relationTo" type="{http://data.mdm.unidata.org/}RelationTo" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="integralRecord" type="{http://data.mdm.unidata.org/}IntegralRecord" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@JacksonXmlRootElement(localName = "etalonRecordFull", namespace = DATA_NAMESPACE)
@JsonPropertyOrder({"etalonRecord","relationTo","integralRecord"})
public class EtalonRecordFull implements Serializable {

    private final static long serialVersionUID = 12345L;

    @JacksonXmlProperty(namespace = DATA_NAMESPACE)
    protected EtalonRecord etalonRecord;
    @JacksonXmlProperty(namespace = DATA_NAMESPACE)
    protected List<RelationTo> relationTo;
    @JacksonXmlProperty(namespace = DATA_NAMESPACE)
    protected List<IntegralRecord> integralRecord;

    /**
     * Gets the value of the etalonRecord property.
     * 
     * @return
     *     possible object is
     *     {@link EtalonRecord }
     *     
     */
    public EtalonRecord getEtalonRecord() {
        return etalonRecord;
    }

    /**
     * Sets the value of the etalonRecord property.
     * 
     * @param value
     *     allowed object is
     *     {@link EtalonRecord }
     *     
     */
    public void setEtalonRecord(EtalonRecord value) {
        this.etalonRecord = value;
    }

    /**
     * Gets the value of the relationTo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the relationTo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRelationTo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RelationTo }
     * 
     * 
     */
    public List<RelationTo> getRelationTo() {
        if (relationTo == null) {
            relationTo = new ArrayList<RelationTo>();
        }
        return this.relationTo;
    }

    /**
     * Gets the value of the integralRecord property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the integralRecord property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIntegralRecord().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IntegralRecord }
     * 
     * 
     */
    public List<IntegralRecord> getIntegralRecord() {
        if (integralRecord == null) {
            integralRecord = new ArrayList<IntegralRecord>();
        }
        return this.integralRecord;
    }

    public EtalonRecordFull withEtalonRecord(EtalonRecord value) {
        setEtalonRecord(value);
        return this;
    }

    public EtalonRecordFull withRelationTo(RelationTo... values) {
        if (values!= null) {
            for (RelationTo value: values) {
                getRelationTo().add(value);
            }
        }
        return this;
    }

    public EtalonRecordFull withRelationTo(Collection<RelationTo> values) {
        if (values!= null) {
            getRelationTo().addAll(values);
        }
        return this;
    }

    public EtalonRecordFull withIntegralRecord(IntegralRecord... values) {
        if (values!= null) {
            for (IntegralRecord value: values) {
                getIntegralRecord().add(value);
            }
        }
        return this;
    }

    public EtalonRecordFull withIntegralRecord(Collection<IntegralRecord> values) {
        if (values!= null) {
            getIntegralRecord().addAll(values);
        }
        return this;
    }

}
