/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.data.service.segments.relations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.data.context.RelationRestoreContext;
import org.unidata.mdm.data.context.RestoreRecordRequestContext;
import org.unidata.mdm.data.module.DataModule;
import org.unidata.mdm.data.service.segments.ContainmentRelationSupport;
import org.unidata.mdm.data.type.data.RelationType;
import org.unidata.mdm.data.type.keys.RelationKeys;
import org.unidata.mdm.system.context.CommonRequestContext;
import org.unidata.mdm.system.service.ExecutionService;
import org.unidata.mdm.system.type.pipeline.Point;
import org.unidata.mdm.system.type.pipeline.Start;
import org.unidata.mdm.system.type.runtime.MeasurementPoint;

/**
 * Executor responsible for restoring containment.
 */
@Component(RelationRestoreContainmentExecutor.SEGMENT_ID)
public class RelationRestoreContainmentExecutor extends Point<RelationRestoreContext> implements ContainmentRelationSupport {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = DataModule.MODULE_ID + "[RELATION_RESTORE_CONTAINMENT]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = DataModule.MODULE_ID + ".relation.restore.containment.description";
    /**
     * The ES.
     */
    @Autowired
    private ExecutionService executionService;
    /**
     * Constructor.
     */
    public RelationRestoreContainmentExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void point(RelationRestoreContext ctx) {

        MeasurementPoint.start();
        try {

            RelationKeys keys = ctx.relationKeys();
            if (keys.getRelationType() != RelationType.CONTAINS) {
                return;
            }

            RestoreRecordRequestContext dCtx = RestoreRecordRequestContext.builder()
                    .etalonKey(keys.getEtalonKey().getTo().getId())
                    .validFrom(ctx.getValidFrom())
                    .validTo(ctx.getValidTo())
                    .periodRestore(ctx.isPeriodRestore())
                    .forDate(ctx.getForDate())
                    .lastUpdate(ctx.getLastUpdate())
                    .operationId(((CommonRequestContext) ctx).getOperationId())
                    .batchOperation(ctx.isBatchOperation())
                    .build();

            dCtx.timestamp(ctx.timestamp());

            executionService.execute(dCtx);

            ctx.containmentContext(dCtx);
            ctx.currentTimeline(mirrorTimeline(keys, dCtx.currentTimeline()));
        } finally {
            MeasurementPoint.stop();
        }
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?> start) {
        return RelationRestoreContext.class.isAssignableFrom(start.getInputTypeClass());
    }
}
