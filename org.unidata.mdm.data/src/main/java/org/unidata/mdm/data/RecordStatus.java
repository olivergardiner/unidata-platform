package org.unidata.mdm.data;

/**
 * <p>Java class for RecordStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RecordStatus"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="ACTIVE"/&gt;
 *     &lt;enumeration value="INACTIVE"/&gt;
 *     &lt;enumeration value="MERGED"/&gt;
 *     &lt;enumeration value="PENDING"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
public enum RecordStatus {

    ACTIVE,
    INACTIVE,
    MERGED,
    PENDING;

    public String value() {
        return name();
    }

    public static RecordStatus fromValue(String v) {
        return valueOf(v);
    }

}
