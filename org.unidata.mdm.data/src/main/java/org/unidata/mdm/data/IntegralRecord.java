package org.unidata.mdm.data;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import static org.unidata.mdm.DataNamespace.DATA_NAMESPACE;


/**
 * 
 * Структура, описывающая неотделимую сущность. Используется для представления записи в композитных объектах. Состоит из обязательного имени связи, а также либо основной, либо исходной записи сущности.
 * Примером такой связанной сущности может служить информация о банковском счёте, неотделимой от сущности 'сотрудник'
 *             
 * 
 * <p>Java class for IntegralRecord complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IntegralRecord"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://data.mdm.unidata.org/}RelationBase"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="etalonRecord" type="{http://data.mdm.unidata.org/}EtalonRecord"/&gt;
 *         &lt;element name="originRecord" type="{http://data.mdm.unidata.org/}OriginRecord"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@JsonPropertyOrder({"etalonRecord","originRecord"})
public class IntegralRecord extends RelationBase implements Serializable {

    private final static long serialVersionUID = 12345L;

    @JacksonXmlProperty(namespace = DATA_NAMESPACE)
    protected EtalonRecord etalonRecord;
    @JacksonXmlProperty(namespace = DATA_NAMESPACE)
    protected OriginRecord originRecord;

    /**
     * Gets the value of the etalonRecord property.
     * 
     * @return
     *     possible object is
     *     {@link EtalonRecord }
     *     
     */
    public EtalonRecord getEtalonRecord() {
        return etalonRecord;
    }

    /**
     * Sets the value of the etalonRecord property.
     * 
     * @param value
     *     allowed object is
     *     {@link EtalonRecord }
     *     
     */
    public void setEtalonRecord(EtalonRecord value) {
        this.etalonRecord = value;
    }

    /**
     * Gets the value of the originRecord property.
     * 
     * @return
     *     possible object is
     *     {@link OriginRecord }
     *     
     */
    public OriginRecord getOriginRecord() {
        return originRecord;
    }

    /**
     * Sets the value of the originRecord property.
     * 
     * @param value
     *     allowed object is
     *     {@link OriginRecord }
     *     
     */
    public void setOriginRecord(OriginRecord value) {
        this.originRecord = value;
    }

    public IntegralRecord withEtalonRecord(EtalonRecord value) {
        setEtalonRecord(value);
        return this;
    }

    public IntegralRecord withOriginRecord(OriginRecord value) {
        setOriginRecord(value);
        return this;
    }

    @Override
    public IntegralRecord withRelName(String value) {
        setRelName(value);
        return this;
    }

}
