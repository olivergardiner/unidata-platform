/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.data.context;

import java.util.Date;
import java.util.Objects;

import org.apache.commons.lang3.tuple.Pair;
import org.unidata.mdm.meta.type.RelativeDirection;

/**
 * @author Mikhail Mikhailov
 * Gets relations
 */
public class GetRelationTimelineRequestContext
    extends AbstractRelationIdentityContext
    implements RelationFromIdentityContext, RelationToIdentityContext {
    /**
     * Generated SVUID.
     */
    private static final long serialVersionUID = -5874979941996090899L;
    /**
     * For a particular date (as of).
     */
    private final Date forDate;
    /**
     * For a particular date range.
     */
    private final Pair<Date, Date> forDatesFrame;
    /**
     * Has updates (new versions) after this date.
     */
    private final Date forUpdatesAfter;
    /**
     * Last update date to cut off versions.
     */
    private final Date forLastUpdate;
    /**
     * Operation id.
     */
    private final String forOperationId;
    /**
     * Constructor.
     */
    protected GetRelationTimelineRequestContext(GetRelationTimelineRequestContextBuilder b) {
        super(b);
        this.forDate = b.forDate;
        this.forDatesFrame = b.forDatesFrame;
        this.forUpdatesAfter = b.forUpdatesAfter;
        this.forLastUpdate = b.forLastUpdate;
        this.forOperationId = b.forOperationId;

        flags.set(DataContextFlags.FLAG_INCLUDE_DRAFTS, b.includeDrafts);
        flags.set(DataContextFlags.FLAG_FETCH_KEYS, b.fetchKeys);
        flags.set(DataContextFlags.FLAG_FETCH_TIMELINE_DATA, b.fetchData);
        flags.set(DataContextFlags.FLAG_SKIP_TIMELINE_CALCULATIONS, b.skipCalculations);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public RelativeDirection getDirection() {

        if (Objects.nonNull(fromKeys())) {
            return RelativeDirection.FROM;
        }

        return Objects.nonNull(toKeys()) ? RelativeDirection.TO : null;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String toBoxKey() {

        String boxKey = RelationFromIdentityContext.super.toBoxKey();
        if (Objects.isNull(boxKey)) {
            return RelationToIdentityContext.super.toBoxKey();
        }

        return boxKey;
    }
    /**
     * @return the forDate
     */
    public Date getForDate() {
        return forDate;
    }
    /**
     * @return the dates frame
     */
    public Pair<Date, Date> getForDatesFrame() {
        return forDatesFrame;
    }
    /**
     * @return the updatesAfter
     */
    public Date getForUpdatesAfter() {
        return forUpdatesAfter;
    }
    /**
     * @return the lastUpdate
     */
    public Date getForLastUpdate() {
        return forLastUpdate;
    }
    /**
     * @return the forOperationId
     */
    public String getForOperationId() {
        return forOperationId;
    }
    /**
     * @return the unpublishedState
     */
    public boolean isIncludeDrafts() {
        return flags.get(DataContextFlags.FLAG_INCLUDE_DRAFTS);
    }
    /**
     * @return the tasks
     */
    public boolean isFetchKeys() {
        return flags.get(DataContextFlags.FLAG_FETCH_KEYS);
    }
    /**
     * @return the fetchTimelineData
     */
    public boolean isFetchData() {
        return flags.get(DataContextFlags.FLAG_FETCH_TIMELINE_DATA);
    }
    /**
     * @return the skipCalculations
     */
    public boolean isSkipCalculations() {
        return flags.get(DataContextFlags.FLAG_SKIP_TIMELINE_CALCULATIONS);
    }
    /**
     * Default empty builder.
     * @return builder
     */
    public static GetRelationTimelineRequestContextBuilder builder(){
        return new GetRelationTimelineRequestContextBuilder();
    }
    /**
    * Copy builder.
    * @return builder
    */
   public static GetRelationTimelineRequestContextBuilder builder(GetRelationRequestContext other) {

       GetRelationTimelineRequestContextBuilder b = new GetRelationTimelineRequestContextBuilder();
       b.relationEtalonKey = other.getRelationEtalonKey();
       b.relationOriginKey = other.getRelationOriginKey();
       b.entityName = other.getEntityName();
       b.etalonKey = other.getEtalonKey();
       b.externalId = other.getExternalId();
       b.sourceSystem = other.getSourceSystem();
       b.originKey = other.getOriginKey();
       b.shard = other.getShard();
       b.relationLsn = other.getLsn();
       b.fetchData = other.isFetchTimelineData();
       b.forDate = other.getForDate();
       b.forLastUpdate = other.getForLastUpdate();
       b.forUpdatesAfter = other.getForUpdatesAfter();
       b.forDatesFrame = other.getForDatesFrame();
       b.forOperationId = other.getOperationId();
       b.includeDrafts = other.isIncludeDrafts();

       return b;
   }

    /**
     * @author Mikhail Mikhailov
     * Context builder.
     */
    public static class GetRelationTimelineRequestContextBuilder
        extends AbstractRelationIdentityContextBuilder<GetRelationTimelineRequestContextBuilder> {
        /**
         * For a particular date (as of).
         */
        protected Date forDate;
        /**
         * For a particular date range (left <-> right).
         */
        protected Pair<Date, Date> forDatesFrame;
        /**
         * Has updates (new versions) after this date.
         */
        protected Date forUpdatesAfter;
        /**
         * Last update date to cut off versions.
         */
        protected Date forLastUpdate;
        /**
         * Operation id.
         */
        protected String forOperationId;
        /**
         * Show draft version.
         */
        protected boolean includeDrafts;
        /**
         * Return keys.
         */
        protected boolean fetchKeys = true;
        /**
         * Return timeline with data.
         */
        protected boolean fetchData;
        /**
         * Skip etalon, activity, operation type calculations.
         * Return raw timeline.
         */
        protected boolean skipCalculations;
        /**
         * Constructor.
         */
        protected GetRelationTimelineRequestContextBuilder() {
            super();
        }
        /**
         * @param forDate the forDate to set
         */
        public GetRelationTimelineRequestContextBuilder forDate(Date forDate) {
            this.forDate = forDate;
            return self();
        }
        /**
         * @param forDatesFrame the date frame to set
         */
        public GetRelationTimelineRequestContextBuilder forDatesFrame(Pair<Date, Date> forDatesFrame) {
            this.forDatesFrame = forDatesFrame;
            return self();
        }
        /**
         * Sets last update date to the context.
         * @param lastUpdate the date
         * @return self
         */
        public GetRelationTimelineRequestContextBuilder forLastUpdate(Date lastUpdate) {
            this.forLastUpdate = lastUpdate;
            return self();
        }
        /**
         * @param updatesAfter the updatesAfter to set
         */
        public GetRelationTimelineRequestContextBuilder forUpdatesAfter(Date updatesAfter) {
            this.forUpdatesAfter = updatesAfter;
            return self();
        }
        /**
         * @param forOperationId the forOperationId to set
         */
        public GetRelationTimelineRequestContextBuilder forOperationId(String forOperationId) {
            this.forOperationId = forOperationId;
            return self();
        }
        /**
         * Request tasks additionally. Show draft version.
         */
        public GetRelationTimelineRequestContextBuilder includeDrafts(boolean includeDrafts) {
            this.includeDrafts = includeDrafts;
            return self();
        }
        /**
         * @param keys the keys to set
         */
        public GetRelationTimelineRequestContextBuilder fetchKeys(boolean keys) {
            this.fetchKeys = keys;
            return self();
        }
        /**
         * @param fetchTimelineData the fetchTimelineData to set
         */
        public GetRelationTimelineRequestContextBuilder fetchData(boolean fetchTimelineData) {
            this.fetchData = fetchTimelineData;
            return self();
        }
        /**
         * @param skipCalculations the skipCalculations to set
         */
        public GetRelationTimelineRequestContextBuilder skipCalculations(boolean skipCalculations) {
            this.skipCalculations = skipCalculations;
            return self();
        }
        /**
         * Builds a context.
         * @return a new context
         */
        @Override
        public GetRelationTimelineRequestContext build() {
            return new GetRelationTimelineRequestContext(this);
        }
    }
}
