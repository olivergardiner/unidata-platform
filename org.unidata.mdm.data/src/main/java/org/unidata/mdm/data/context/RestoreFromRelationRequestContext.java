package org.unidata.mdm.data.context;

import java.util.Date;

import org.unidata.mdm.data.service.segments.relations.RelationRestoreStartExecutor;
import org.unidata.mdm.data.type.keys.RecordKeys;

/**
 * @author Mikhail Mikhailov on May 3, 2020
 */
public class RestoreFromRelationRequestContext
    extends AbstractRelationIdentityContext
    implements
        RelationFromIdentityContext,
        RelationRestoreContext {
    /**
     * Set range from.
     */
    private Date validFrom;
    /**
     * Set range to.
     */
    private Date validTo;
    /**
     * Last update date to use (optional).
     */
    private final Date lastUpdate;
    /**
     * For a particular date (as of).
     */
    private final Date forDate;
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = -5647147792960534162L;
    /**
     * Constructor.
     * @param b the builder
     */
    private RestoreFromRelationRequestContext(RestoreFromRelationRequestContextBuilder b) {
        super(b);
        this.fromKeys(b.fromKeys);
        this.forDate = b.forDate;
        this.lastUpdate = b.lastUpdate;
        this.validFrom = b.validFrom;
        this.validTo = b.validTo;

        flags.set(DataContextFlags.FLAG_IS_PERIOD_RESTORE, b.restorePeriod);
        flags.set(DataContextFlags.FLAG_BATCH_OPERATION, b.batchOperation);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getStartTypeId() {
        return RelationRestoreStartExecutor.SEGMENT_ID;
    }
    /**
     * @return the forDate
     */
    @Override
    public Date getForDate() {
        return forDate;
    }
    /**
     * @return the lastUpdate
     */
    @Override
    public Date getLastUpdate() {
        return lastUpdate;
    }
    /**
     * @return the validFrom
     */
    @Override
    public Date getValidFrom() {
        return validFrom;
    }
    /**
     * @return the validTo
     */
    @Override
    public Date getValidTo() {
        return validTo;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void setValidFrom(Date from) {
        this.validFrom = from;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void setValidTo(Date to) {
        this.validTo = to;
    }
    /**
     * The builder.
     * @return builder
     */
    public static RestoreFromRelationRequestContextBuilder builder() {
        return new RestoreFromRelationRequestContextBuilder();
    }
    /**
     * The builder.
     * @author Mikhail Mikhailov on May 3, 2020
     */
    public static class RestoreFromRelationRequestContextBuilder extends AbstractRelationIdentityContextBuilder<RestoreFromRelationRequestContextBuilder> {
        /**
         * The from side keys.
         */
        private RecordKeys fromKeys;
        /**
         * Set range from.
         */
        private Date validFrom;
        /**
         * Set range to.
         */
        private Date validTo;
        /**
         * The TL point.
         */
        private Date forDate;
        /**
         * Last update date to use (optional).
         */
        private Date lastUpdate;
        /**
         * define that is restore period request.
         */
        private boolean restorePeriod;
        /**
         * This context is participating in a batch upsert. Collect artifacts instead of upserting immediately.
         */
        private boolean batchOperation;
        /**
         * Constructor.
         */
        private RestoreFromRelationRequestContextBuilder() {
            super();
        }
        /**
         * Sets the from keys.
         * Relevant for a single rel restore only.
         * In all other cases happens automatically.
         * @param keys of the from side.
         * @return self
         */
        public RestoreFromRelationRequestContextBuilder fromKeys(RecordKeys keys) {
            this.fromKeys = keys;
            return self();
        }
        /**
         * @param validFrom the range from to set
         */
        public RestoreFromRelationRequestContextBuilder validFrom(Date validFrom) {
            this.validFrom = validFrom;
            return this;
        }
        /**
         * @param validTo the range to to set
         */
        public RestoreFromRelationRequestContextBuilder validTo(Date validTo) {
            this.validTo = validTo;
            return this;
        }
        /**
         * @param lastUpdate the last update to set
         */
        public RestoreFromRelationRequestContextBuilder lastUpdate(Date lastUpdate) {
            this.lastUpdate = lastUpdate;
            return this;
        }
        /**
         * @param forDate the forDate to set
         */
        public RestoreFromRelationRequestContextBuilder forDate(Date forDate) {
            this.forDate = forDate;
            return this;
        }
        /**
         * @param batchUpsert the flag
         * @return self
         */
        public RestoreFromRelationRequestContextBuilder batchOperation(boolean batchUpsert) {
            this.batchOperation = batchUpsert;
            return this;
        }
        /**
         * define that that is a period restore request.
         * @param periodRestore
         * @return self
         */
        public RestoreFromRelationRequestContextBuilder periodRestore(boolean periodRestore) {
            this.restorePeriod = periodRestore;
            return this;
        }
        /**
         * {@inheritDoc}
         */
        @Override
        public RestoreFromRelationRequestContext build() {
            return new RestoreFromRelationRequestContext(this);
        }
    }
}
