package org.unidata.mdm.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import static org.unidata.mdm.DataNamespace.DATA_NAMESPACE;


/**
 * Базовая структура, описывающая кодовый атрибут справочника. Состоит из обязательного имени, кодового значения и массива значений в зависимости от типа данных
 *
 *
 * <p>Java class for AbstractCodeAttribute complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="AbstractCodeAttribute"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://data.mdm.unidata.org/}AbstractAttribute"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="intValue" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *           &lt;element name="stringValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;/choice&gt;
 *         &lt;choice&gt;
 *           &lt;element name="supplementaryIntValues" type="{http://www.w3.org/2001/XMLSchema}long" maxOccurs="unbounded" minOccurs="0"/&gt;
 *           &lt;element name="supplementaryStringValues" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@JsonPropertyOrder({"intValue", "stringValue", "supplementaryIntValues", "supplementaryStringValues"})
public class AbstractCodeAttribute extends AbstractAttribute implements Serializable {

    private final static long serialVersionUID = 12345L;

    @JacksonXmlProperty(namespace = DATA_NAMESPACE)
    protected Long intValue;
    @JacksonXmlProperty(namespace = DATA_NAMESPACE)
    protected String stringValue;
    @JacksonXmlProperty(namespace = DATA_NAMESPACE)
    protected List<Long> supplementaryIntValues;
    @JacksonXmlProperty(namespace = DATA_NAMESPACE)
    protected List<String> supplementaryStringValues;

    /**
     * Gets the value of the intValue property.
     *
     * @return possible object is
     * {@link Long }
     */
    public Long getIntValue() {
        return intValue;
    }

    /**
     * Sets the value of the intValue property.
     *
     * @param value allowed object is
     * {@link Long }
     */
    public void setIntValue(Long value) {
        this.intValue = value;
    }

    /**
     * Gets the value of the stringValue property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getStringValue() {
        return stringValue;
    }

    /**
     * Sets the value of the stringValue property.
     *
     * @param value allowed object is
     * {@link String }
     */
    public void setStringValue(String value) {
        this.stringValue = value;
    }

    /**
     * Gets the value of the supplementaryIntValues property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the supplementaryIntValues property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSupplementaryIntValues().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Long }
     */
    public List<Long> getSupplementaryIntValues() {
        if (supplementaryIntValues == null) {
            supplementaryIntValues = new ArrayList<Long>();
        }
        return this.supplementaryIntValues;
    }

    /**
     * Gets the value of the supplementaryStringValues property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the supplementaryStringValues property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSupplementaryStringValues().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     */
    public List<String> getSupplementaryStringValues() {
        if (supplementaryStringValues == null) {
            supplementaryStringValues = new ArrayList<String>();
        }
        return this.supplementaryStringValues;
    }

    public AbstractCodeAttribute withIntValue(Long value) {
        setIntValue(value);
        return this;
    }

    public AbstractCodeAttribute withStringValue(String value) {
        setStringValue(value);
        return this;
    }

    public AbstractCodeAttribute withSupplementaryIntValues(Long... values) {
        if (values != null) {
            for (Long value : values) {
                getSupplementaryIntValues().add(value);
            }
        }
        return this;
    }

    public AbstractCodeAttribute withSupplementaryIntValues(Collection<Long> values) {
        if (values != null) {
            getSupplementaryIntValues().addAll(values);
        }
        return this;
    }

    public AbstractCodeAttribute withSupplementaryStringValues(String... values) {
        if (values != null) {
            for (String value : values) {
                getSupplementaryStringValues().add(value);
            }
        }
        return this;
    }

    public AbstractCodeAttribute withSupplementaryStringValues(Collection<String> values) {
        if (values != null) {
            getSupplementaryStringValues().addAll(values);
        }
        return this;
    }

    @Override
    public AbstractCodeAttribute withName(String value) {
        setName(value);
        return this;
    }

}
