/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.data.service;

import org.unidata.mdm.data.type.apply.batch.impl.RecordDeleteBatchSetAccumulator;
import org.unidata.mdm.data.type.apply.batch.impl.RecordMergeBatchSetAccumulator;
import org.unidata.mdm.data.type.apply.batch.impl.RecordRestoreBatchSetAccumulator;
import org.unidata.mdm.data.type.apply.batch.impl.RecordUpsertBatchSetAccumulator;

/**
 * @author Mikhail Mikhailov
 * The batch application processor interface.
 */
public interface RecordBatchSetProcessor extends RecordChangeSetProcessor {
    /**
     * Applies upsert batch set to DB and index.
     * @param bsa batch set accumulator for processing
     */
    void apply(RecordUpsertBatchSetAccumulator bsa);
    /**
     * Applies delete batch set to DB and index.
     * @param bsa batch set accumulator for processing
     */
    void apply(RecordDeleteBatchSetAccumulator bsa);
    /**
     * Applies merge batch set to DB and index.
     * @param bsa batch set accumulator for processing
     */
    void apply(RecordMergeBatchSetAccumulator bsa);
    /**
     * Applies restore batch set to DB and index.
     * @param bsa the batch set accumulator to process
     */
    void apply(RecordRestoreBatchSetAccumulator bsa);
}
