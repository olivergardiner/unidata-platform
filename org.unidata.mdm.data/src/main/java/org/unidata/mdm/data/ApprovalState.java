package org.unidata.mdm.data;

/**
 * <p>Java class for ApprovalState.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ApprovalState"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="PENDING"/&gt;
 *     &lt;enumeration value="APPROVED"/&gt;
 *     &lt;enumeration value="DECLINED"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
public enum ApprovalState {

    PENDING,
    APPROVED,
    DECLINED;

    public String value() {
        return name();
    }

    public static ApprovalState fromValue(String v) {
        return valueOf(v);
    }

}
