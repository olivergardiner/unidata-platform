package org.unidata.mdm.data.type.draft;

/**
 * @author Mikhail Mikhailov on Sep 24, 2020
 * Operations, that are supported by the data module via draft UPSERT call.
 */
public enum DraftDataUpsertOperation {
    /**
     * Upsert period data (new or existing record).
     */
    UPSERT_PERIOD,
    /**
     * Re-activate particular period.
     */
    RESTORE_PERIOD,
    /**
     * Inactivate particular period.
     */
    DELETE_PERIOD,
    /**
     * Inactivate the whole record.
     */
    DELETE_RECORD
}
