/*
 *
 *  * Unidata Platform
 *  * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *  *
 *  * Commercial License
 *  * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *  *
 *  * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 *  * For clarification or additional options, please contact: info@unidata-platform.com
 *  * -------
 *  * Disclaimer:
 *  * -------
 *  * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 *  * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 *  * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 *  * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 *
 */

package org.unidata.mdm.data.service.segments.records;

import java.time.Instant;
import java.util.Collection;
import java.util.Date;
import java.util.ListIterator;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.type.calculables.CalculableHolder;
import org.unidata.mdm.core.type.data.DataRecord;
import org.unidata.mdm.core.type.data.OperationType;
import org.unidata.mdm.core.type.data.RecordStatus;
import org.unidata.mdm.core.type.formless.BundlesArray;
import org.unidata.mdm.core.type.formless.DataBundle;
import org.unidata.mdm.core.type.model.EntityModelElement;
import org.unidata.mdm.core.type.timeline.MutableTimeInterval;
import org.unidata.mdm.core.type.timeline.TimeInterval;
import org.unidata.mdm.core.type.timeline.Timeline;
import org.unidata.mdm.core.util.SecurityUtils;
import org.unidata.mdm.data.context.GetRecordTimelineRequestContext;
import org.unidata.mdm.data.context.UpsertRequestContext;
import org.unidata.mdm.data.module.DataModule;
import org.unidata.mdm.data.service.impl.CommonRecordsComponent;
import org.unidata.mdm.data.service.segments.RecordDraftTimelineSupport;
import org.unidata.mdm.data.service.segments.ValidityRangeCheckSupport;
import org.unidata.mdm.data.type.data.EtalonRecord;
import org.unidata.mdm.data.type.data.OriginRecord;
import org.unidata.mdm.data.type.draft.DataDraftConstants;
import org.unidata.mdm.data.type.draft.DraftDataUpsertOperation;
import org.unidata.mdm.draft.context.DraftUpsertContext;
import org.unidata.mdm.draft.service.DraftService;
import org.unidata.mdm.draft.type.Draft;
import org.unidata.mdm.draft.type.Edition;
import org.unidata.mdm.meta.service.MetaModelService;
import org.unidata.mdm.system.type.pipeline.Point;
import org.unidata.mdm.system.type.pipeline.Start;
import org.unidata.mdm.system.type.support.IdentityHashSet;
import org.unidata.mdm.system.type.variables.Variables;

/**
 * @author Alexey Tsarapkin
 */
@Component(RecordDraftUpsertProcessExecutor.SEGMENT_ID)
public class RecordDraftUpsertProcessExecutor extends Point<DraftUpsertContext>
    implements ValidityRangeCheckSupport<UpsertRequestContext>, RecordDraftTimelineSupport {
    /**
     * This segment id.
     */
    public static final String SEGMENT_ID = DataModule.MODULE_ID + "[RECORD_DRAFT_UPSERT_PROCESS]";
    /**
     * This segment description.
     */
    private static final String SEGMENT_DESCRIPTION = DataModule.MODULE_ID + ".record.draft.upsert.process.description";
    /**
     * The draft service.
     */
    @Autowired
    private DraftService draftService;
    /**
     * Common records component.
     */
    @Autowired
    private CommonRecordsComponent commonRecordsComponent;

    @Autowired
    private MetaModelService metaModelService;
    /**
     * Constructor.
     */
    public RecordDraftUpsertProcessExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void point(DraftUpsertContext ctx) {

        // 1. The stuff runs only, if some data were supplied
        if (!ctx.hasPayload()) {
            return;
        }

        Draft draft = ctx.currentDraft();

        // 2. Process user input and prepare this bundle
        DataBundle bundle = setupBundle(draft, ctx);

        // 3. Load last edition
        Edition current = null;
        if (draft.isExisting()) {
            current = draftService.current(draft.getDraftId(), true);
        }

        // 4. Take data snapshot for new drafts, if no editions exist
        BundlesArray bundles = null;
        if (Objects.isNull(current)) {
            bundles = setupState(draft);
        } else {
            bundles = current.getContent();
        }

        // 5. Create bundles array, if needed
        if (Objects.isNull(bundles)) {
            bundles = new BundlesArray();
        }

        // 6. Add the payload to collection
        bundles.add(bundle);

        Edition next = new Edition();
        next.setContent(bundles);
        next.setCreateDate(new Date(System.currentTimeMillis()));
        next.setCreatedBy(SecurityUtils.getCurrentUserName());

        // 7. Cleanse the bundle - remove invisible periods and save it to edition
        cleanseBundles(draft, next);

        ctx.currentEdition(next);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?> start) {
        return DraftUpsertContext.class.isAssignableFrom(start.getInputTypeClass());
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public CommonRecordsComponent getCommonRecordsComponent() {
        return commonRecordsComponent;
    }

    protected BundlesArray setupState(Draft draft) {

        boolean isNew = draft.getProperties().<Boolean>valueGet(DataDraftConstants.IS_NEW_RECORD);
        if (isNew) {
            return null;
        }

        String etalonId = draft.getProperties().valueGet(DataDraftConstants.ETALON_ID);

        Timeline<OriginRecord> timeline = commonRecordsComponent.loadTimeline(GetRecordTimelineRequestContext.builder()
                .etalonKey(etalonId)
                .fetchData(true)
                .build());

        if (timeline.isEmpty()) {
            return null;
        }

        BundlesArray result = new BundlesArray();
        for (TimeInterval<OriginRecord> i : timeline) {

            if (!i.isActive()) {
                continue;
            }

            EtalonRecord etalon = i.getCalculationResult();
            if (Objects.nonNull(etalon)) {

                Instant from = etalon.getInfoSection().getValidFrom() == null
                        ? null
                        : etalon.getInfoSection().getValidFrom().toInstant();

                Instant to = etalon.getInfoSection().getValidTo() == null
                        ? null
                        : etalon.getInfoSection().getValidTo().toInstant();

                result.add(new DataBundle()
                        .withRecord(etalon)
                        .withVariables(new Variables()
                                .add(DataDraftConstants.VALID_FROM, from)
                                .add(DataDraftConstants.VALID_TO, to)
                                .add(DataDraftConstants.IS_USER_PERIOD, Boolean.FALSE)
                                .add(DataDraftConstants.OPERATION_TYPE, etalon.getInfoSection().getOperationType())
                                .add(DataDraftConstants.PERIOD_STATUS, etalon.getInfoSection().getStatus())
                                .add(DataDraftConstants.CREATE_TIMESTAMP, Instant.EPOCH)
                                .add(DataDraftConstants.CREATED_BY, etalon.getInfoSection().getCreatedBy())));
            }
        }

        return result;
    }

    protected DataBundle setupBundle(Draft draft, DraftUpsertContext ctx) {

        Variables variables = draft.getProperties();

        // 1. Dates boundary
        String entityName = variables.valueGet(DataDraftConstants.ENTITY_NAME);
        EntityModelElement ew = metaModelService.getEntityModelElementById(entityName);

        Date suppliedFrom = ctx.getParameter(DataDraftConstants.VALID_FROM);
        Date suppliedTo = ctx.getParameter(DataDraftConstants.VALID_TO);

        Date from = ensureFrom(suppliedFrom, ew.getValidityStart());
        Date to = ensureTo(suppliedTo, ew.getValidityEnd());

        ensurePeriodOverlapping(from, to);

        // 2. Variables
        DraftDataUpsertOperation operation = ctx.getParameter(DataDraftConstants.DRAFT_OPERATION);
        RecordStatus status = operation == DraftDataUpsertOperation.UPSERT_PERIOD || operation == DraftDataUpsertOperation.RESTORE_PERIOD
                ? RecordStatus.ACTIVE
                : RecordStatus.INACTIVE;
        OperationType optype = ctx.isParameterSet(DataDraftConstants.OPERATION_TYPE)
                ? OperationType.valueOf(ctx.getParameter(DataDraftConstants.OPERATION_TYPE))
                : OperationType.DIRECT;

        Variables v = new Variables()
            .add(DataDraftConstants.VALID_FROM, from == null ? null : from.toInstant())
            .add(DataDraftConstants.VALID_TO, to == null ? null : to.toInstant())
            .add(DataDraftConstants.IS_USER_PERIOD, Boolean.TRUE)
            .add(DataDraftConstants.OPERATION_TYPE, optype)
            .add(DataDraftConstants.PERIOD_STATUS, status)
            .add(DataDraftConstants.CREATE_TIMESTAMP, Instant.now())
            .add(DataDraftConstants.CREATED_BY, SecurityUtils.getCurrentUserName());

        // 3. Record
        DataRecord record = ctx.getPayload();

        // 4. Return
        return new DataBundle()
                .withRecord(record)
                .withVariables(v);
    }

    protected void cleanseBundles(Draft draft, Edition edition) {

        BundlesArray bundles = edition.getContent();
        if (Objects.isNull(bundles) || bundles.size() <= 1) {
            return;
        }

        Timeline<OriginRecord> timeline = timeline(draft, edition);
        Set<CalculableHolder<OriginRecord>> unique = timeline.stream()
            .map(ti -> (MutableTimeInterval<OriginRecord>) ti)
            .map(MutableTimeInterval::toCalculables)
            .flatMap(Collection::stream)
            .collect(Collectors.toCollection(IdentityHashSet::new));

        ListIterator<DataBundle> li = bundles.listIterator();
        while (li.hasNext()) {

            DataBundle dataBundle = li.next();
            Instant ts = dataBundle.getVariables().valueGet(DataDraftConstants.CREATE_TIMESTAMP);
            Date timestamp = new Date(ts.toEpochMilli());

            boolean isVisible = unique.stream().anyMatch(ch -> timestamp.getTime() == ch.getLastUpdate().getTime());
            if (!isVisible) {
                li.remove();
            }
        }
    }
}
