package org.unidata.mdm.data.service.impl;

import org.springframework.stereotype.Component;
import org.unidata.mdm.core.type.data.DataRecord;

/**
 * @author Mikhail Mikhailov on Jun 3, 2020
 */
@Component("relationValidationComponent")
public class RelationValidationComponent extends AbstractValidationComponent {
    /**
     * Constructor.
     */
    public RelationValidationComponent() {
        super();
    }
    /**
     * Check specifically a relation record.
     * @param record the relation record
     * @param id the relation name
     */
    public void checkRelationDataRecord(DataRecord record, String id) {
        checkDataRecord(record, id);
    }
}
