package org.unidata.mdm.data;

import java.io.Serializable;
import java.util.Collection;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import static org.unidata.mdm.DataNamespace.DATA_NAMESPACE;


/**
 * 
 * Структура полностю аналогичная 'EtalonRecord' с той лишь разницей, что описывает исходную запись сущности. Также является логическим расширением структуры 'NestedRecord'. Всегда содержит
 * - имя сущности, такое как 'Банк', 'Сотдрудник' итд.
 * - набор простых и сложных атрибутов
 * - значение ключа идентифицирующего исходную запись
 * - список связей данной сущности
 *             
 * 
 * <p>Java class for OriginRecord complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OriginRecord"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://data.mdm.unidata.org/}NestedRecord"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="originKey" type="{http://data.mdm.unidata.org/}OriginKey"/&gt;
 *         &lt;element name="relations" type="{http://data.mdm.unidata.org/}EntityRelations" minOccurs="0"/&gt;
 *         &lt;element name="infoSection" type="{http://data.mdm.unidata.org/}OriginRecordInfoSection" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@JacksonXmlRootElement(localName = "originRecord", namespace = DATA_NAMESPACE)
@JsonPropertyOrder({"originKey","relations","infoSection"})
public class OriginRecord extends NestedRecord implements Serializable {

    private final static long serialVersionUID = 12345L;

    @JacksonXmlProperty(namespace = DATA_NAMESPACE)
    protected OriginKey originKey;
    @JacksonXmlProperty(namespace = DATA_NAMESPACE)
    protected EntityRelations relations;
    @JacksonXmlProperty(namespace = DATA_NAMESPACE)
    protected OriginRecordInfoSection infoSection;

    /**
     * Gets the value of the originKey property.
     * 
     * @return
     *     possible object is
     *     {@link OriginKey }
     *     
     */
    public OriginKey getOriginKey() {
        return originKey;
    }

    /**
     * Sets the value of the originKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link OriginKey }
     *     
     */
    public void setOriginKey(OriginKey value) {
        this.originKey = value;
    }

    /**
     * Gets the value of the relations property.
     * 
     * @return
     *     possible object is
     *     {@link EntityRelations }
     *     
     */
    public EntityRelations getRelations() {
        return relations;
    }

    /**
     * Sets the value of the relations property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntityRelations }
     *     
     */
    public void setRelations(EntityRelations value) {
        this.relations = value;
    }

    /**
     * Gets the value of the infoSection property.
     * 
     * @return
     *     possible object is
     *     {@link OriginRecordInfoSection }
     *     
     */
    public OriginRecordInfoSection getInfoSection() {
        return infoSection;
    }

    /**
     * Sets the value of the infoSection property.
     * 
     * @param value
     *     allowed object is
     *     {@link OriginRecordInfoSection }
     *     
     */
    public void setInfoSection(OriginRecordInfoSection value) {
        this.infoSection = value;
    }

    public OriginRecord withOriginKey(OriginKey value) {
        setOriginKey(value);
        return this;
    }

    public OriginRecord withRelations(EntityRelations value) {
        setRelations(value);
        return this;
    }

    public OriginRecord withInfoSection(OriginRecordInfoSection value) {
        setInfoSection(value);
        return this;
    }

    @Override
    public OriginRecord withId(String value) {
        setId(value);
        return this;
    }

    @Override
    public OriginRecord withSimpleAttributes(SimpleAttribute... values) {
        if (values!= null) {
            for (SimpleAttribute value: values) {
                getSimpleAttributes().add(value);
            }
        }
        return this;
    }

    @Override
    public OriginRecord withSimpleAttributes(Collection<SimpleAttribute> values) {
        if (values!= null) {
            getSimpleAttributes().addAll(values);
        }
        return this;
    }

    @Override
    public OriginRecord withCodeAttributes(CodeAttribute... values) {
        if (values!= null) {
            for (CodeAttribute value: values) {
                getCodeAttributes().add(value);
            }
        }
        return this;
    }

    @Override
    public OriginRecord withCodeAttributes(Collection<CodeAttribute> values) {
        if (values!= null) {
            getCodeAttributes().addAll(values);
        }
        return this;
    }

    @Override
    public OriginRecord withArrayAttributes(ArrayAttribute... values) {
        if (values!= null) {
            for (ArrayAttribute value: values) {
                getArrayAttributes().add(value);
            }
        }
        return this;
    }

    @Override
    public OriginRecord withArrayAttributes(Collection<ArrayAttribute> values) {
        if (values!= null) {
            getArrayAttributes().addAll(values);
        }
        return this;
    }

    @Override
    public OriginRecord withComplexAttributes(ComplexAttribute... values) {
        if (values!= null) {
            for (ComplexAttribute value: values) {
                getComplexAttributes().add(value);
            }
        }
        return this;
    }

    @Override
    public OriginRecord withComplexAttributes(Collection<ComplexAttribute> values) {
        if (values!= null) {
            getComplexAttributes().addAll(values);
        }
        return this;
    }

}
