package org.unidata.mdm.data;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * <p>Java class for CodeDataType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CodeDataType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="String"/&gt;
 *     &lt;enumeration value="Integer"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
public enum CodeDataType {

    @JsonProperty("String")
    STRING("String"),
    @JsonProperty("Integer")
    INTEGER("Integer");
    private final String value;

    CodeDataType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CodeDataType fromValue(String v) {
        for (CodeDataType c: CodeDataType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
