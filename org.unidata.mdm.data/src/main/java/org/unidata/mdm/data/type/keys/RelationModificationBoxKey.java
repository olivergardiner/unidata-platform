package org.unidata.mdm.data.type.keys;

import org.apache.commons.lang3.StringUtils;
import org.unidata.mdm.core.type.calculables.ModificationBoxKey;

/**
 * Modbox key for relations.
 * @author Mikhail Mikhailov on Feb 15, 2020
 */
public interface RelationModificationBoxKey extends ModificationBoxKey {
    /**
     * Relation box building fn.
     * @return key
     */
    static String toRelationBoxKey(String fromExternalId, String toExternalId, String sourceSystem) {
        return String.join("|", fromExternalId != null ? fromExternalId : StringUtils.EMPTY, sourceSystem, toExternalId);
    }
}
