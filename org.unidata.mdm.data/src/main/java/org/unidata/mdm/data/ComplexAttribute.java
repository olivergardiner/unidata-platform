package org.unidata.mdm.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import static org.unidata.mdm.DataNamespace.DATA_NAMESPACE;


/**
 * 
 * Базовая структура, описывающая сложный атрибут сущности. Состоит из обязательного имени и неограниченного набора вложенных сущностей
 *             
 * 
 * <p>Java class for ComplexAttribute complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ComplexAttribute"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://data.mdm.unidata.org/}AbstractAttribute"&gt;
 *       &lt;choice maxOccurs="unbounded" minOccurs="0"&gt;
 *         &lt;element name="nestedRecord" type="{http://data.mdm.unidata.org/}NestedRecord"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
public class ComplexAttribute extends AbstractAttribute implements Serializable {

    private final static long serialVersionUID = 12345L;

    @JacksonXmlProperty(namespace = DATA_NAMESPACE)
    protected List<NestedRecord> nestedRecord;

    /**
     * Gets the value of the nestedRecord property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the nestedRecord property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNestedRecord().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NestedRecord }
     * 
     * 
     */
    public List<NestedRecord> getNestedRecord() {
        if (nestedRecord == null) {
            nestedRecord = new ArrayList<NestedRecord>();
        }
        return this.nestedRecord;
    }

    public ComplexAttribute withNestedRecord(NestedRecord... values) {
        if (values!= null) {
            for (NestedRecord value: values) {
                getNestedRecord().add(value);
            }
        }
        return this;
    }

    public ComplexAttribute withNestedRecord(Collection<NestedRecord> values) {
        if (values!= null) {
            getNestedRecord().addAll(values);
        }
        return this;
    }

    @Override
    public ComplexAttribute withName(String value) {
        setName(value);
        return this;
    }

}
