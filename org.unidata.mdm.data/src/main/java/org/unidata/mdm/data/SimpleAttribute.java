package org.unidata.mdm.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.xml.datatype.XMLGregorianCalendar;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.unidata.mdm.data.serialization.jaxb.SingleValueAttributeImpl;

import static org.unidata.mdm.DataNamespace.DATA_NAMESPACE;


/**
 * 
 * Базовая структура, описывающая простой атрибут сущности. Состоит из обязательного имени и только одного значения в зависимости от типа данных
 *             
 * 
 * <p>Java class for SimpleAttribute complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SimpleAttribute"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://data.mdm.unidata.org/}AbstractSingleValueAttribute"&gt;
 *       &lt;sequence maxOccurs="unbounded" minOccurs="0"&gt;
 *         &lt;element name="externalSourceIds" type="{http://data.mdm.unidata.org/}ExternalSourceId"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
public class SimpleAttribute extends SingleValueAttributeImpl implements Serializable {

    private final static long serialVersionUID = 12345L;
    @JacksonXmlProperty(namespace = DATA_NAMESPACE)
    protected List<ExternalSourceId> externalSourceIds;

    /**
     * Gets the value of the externalSourceIds property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the externalSourceIds property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExternalSourceIds().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ExternalSourceId }
     * 
     * 
     */
    public List<ExternalSourceId> getExternalSourceIds() {
        if (externalSourceIds == null) {
            externalSourceIds = new ArrayList<ExternalSourceId>();
        }
        return this.externalSourceIds;
    }

    public SimpleAttribute withExternalSourceIds(ExternalSourceId... values) {
        if (values!= null) {
            for (ExternalSourceId value: values) {
                getExternalSourceIds().add(value);
            }
        }
        return this;
    }

    public SimpleAttribute withExternalSourceIds(Collection<ExternalSourceId> values) {
        if (values!= null) {
            getExternalSourceIds().addAll(values);
        }
        return this;
    }

    @Override
    public SimpleAttribute withBlobValue(BlobValue value) {
        setBlobValue(value);
        return this;
    }

    @Override
    public SimpleAttribute withClobValue(ClobValue value) {
        setClobValue(value);
        return this;
    }

    @Override
    public SimpleAttribute withIntValue(Long value) {
        setIntValue(value);
        return this;
    }

    @Override
    public SimpleAttribute withDateValue(XMLGregorianCalendar value) {
        setDateValue(value);
        return this;
    }

    @Override
    public SimpleAttribute withTimeValue(XMLGregorianCalendar value) {
        setTimeValue(value);
        return this;
    }

    @Override
    public SimpleAttribute withTimestampValue(XMLGregorianCalendar value) {
        setTimestampValue(value);
        return this;
    }

    @Override
    public SimpleAttribute withStringValue(String value) {
        setStringValue(value);
        return this;
    }

    @Override
    public SimpleAttribute withNumberValue(Double value) {
        setNumberValue(value);
        return this;
    }

    @Override
    public SimpleAttribute withBoolValue(Boolean value) {
        setBoolValue(value);
        return this;
    }

    @Override
    public SimpleAttribute withMeasuredValue(MeasuredValue value) {
        setMeasuredValue(value);
        return this;
    }

    @Override
    public SimpleAttribute withDisplayValue(String value) {
        setDisplayValue(value);
        return this;
    }

    @Override
    public SimpleAttribute withLinkEtalonId(String value) {
        setLinkEtalonId(value);
        return this;
    }

    @Override
    public SimpleAttribute withName(String value) {
        setName(value);
        return this;
    }

}
