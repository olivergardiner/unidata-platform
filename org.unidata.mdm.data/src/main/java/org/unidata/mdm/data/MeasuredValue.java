package org.unidata.mdm.data;

import java.io.Serializable;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import static org.unidata.mdm.DataNamespace.DATA_NAMESPACE;


/**
 * <p>Java class for MeasuredValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MeasuredValue"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://data.mdm.unidata.org/}BaseValue"&gt;
 *       &lt;attribute name="value" type="{http://www.w3.org/2001/XMLSchema}double" /&gt;
 *       &lt;attribute name="measurementValueId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="measurementUnitId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@JacksonXmlRootElement(localName = "measuredValue", namespace = DATA_NAMESPACE)
public class MeasuredValue extends BaseValue implements Serializable {

    private final static long serialVersionUID = 12345L;
    @JacksonXmlProperty(isAttribute = true)
    protected Double value;
    @JacksonXmlProperty(isAttribute = true)
    protected String measurementValueId;
    @JacksonXmlProperty(isAttribute = true)
    protected String measurementUnitId;

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setValue(Double value) {
        this.value = value;
    }

    /**
     * Gets the value of the measurementValueId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMeasurementValueId() {
        return measurementValueId;
    }

    /**
     * Sets the value of the measurementValueId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMeasurementValueId(String value) {
        this.measurementValueId = value;
    }

    /**
     * Gets the value of the measurementUnitId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMeasurementUnitId() {
        return measurementUnitId;
    }

    /**
     * Sets the value of the measurementUnitId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMeasurementUnitId(String value) {
        this.measurementUnitId = value;
    }

    public MeasuredValue withValue(Double value) {
        setValue(value);
        return this;
    }

    public MeasuredValue withMeasurementValueId(String value) {
        setMeasurementValueId(value);
        return this;
    }

    public MeasuredValue withMeasurementUnitId(String value) {
        setMeasurementUnitId(value);
        return this;
    }

}
