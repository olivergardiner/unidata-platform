package org.unidata.mdm.data.type.draft;

/**
 * @author Mikhail Mikhailov on Sep 24, 2020
 * Operations, that are supported by the data module via draft GET call.
 */
public enum DraftDataGetOperation {
    /**
     * Gets the timeline view.
     */
    GET_TIMELINE,
    /**
     * Gets period view.
     */
    GET_PERIOD
}
