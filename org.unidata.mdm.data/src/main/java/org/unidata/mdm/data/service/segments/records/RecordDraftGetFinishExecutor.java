/*
 *
 *  * Unidata Platform
 *  * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *  *
 *  * Commercial License
 *  * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *  *
 *  * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 *  * For clarification or additional options, please contact: info@unidata-platform.com
 *  * -------
 *  * Disclaimer:
 *  * -------
 *  * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 *  * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 *  * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 *  * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 *
 */

package org.unidata.mdm.data.service.segments.records;

import java.util.Date;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.dto.ResourceSpecificRightDTO;
import org.unidata.mdm.core.type.security.Right;
import org.unidata.mdm.core.type.timeline.TimeInterval;
import org.unidata.mdm.core.type.timeline.Timeline;
import org.unidata.mdm.core.util.SecurityUtils;
import org.unidata.mdm.data.dto.GetDraftPeriodResponse;
import org.unidata.mdm.data.dto.GetDraftTimelineResponse;
import org.unidata.mdm.data.module.DataModule;
import org.unidata.mdm.data.service.impl.CommonRecordsComponent;
import org.unidata.mdm.data.service.impl.RecordComposerComponent;
import org.unidata.mdm.data.service.segments.RecordDraftTimelineSupport;
import org.unidata.mdm.data.type.data.OriginRecord;
import org.unidata.mdm.data.type.draft.DataDraftConstants;
import org.unidata.mdm.data.type.draft.DraftDataGetOperation;
import org.unidata.mdm.draft.context.DraftGetContext;
import org.unidata.mdm.draft.dto.DraftGetResult;
import org.unidata.mdm.draft.type.Draft;
import org.unidata.mdm.draft.type.DraftPayloadResponse;
import org.unidata.mdm.draft.type.Edition;
import org.unidata.mdm.system.type.pipeline.Finish;
import org.unidata.mdm.system.type.pipeline.Start;
import org.unidata.mdm.system.type.variables.Variables;

/**
 * @author Alexey Tsarapkin
 */
@Component(RecordDraftGetFinishExecutor.SEGMENT_ID)
public class RecordDraftGetFinishExecutor extends Finish<DraftGetContext, DraftGetResult> implements RecordDraftTimelineSupport {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = DataModule.MODULE_ID + "[RECORD_DRAFT_GET_FINISH]";
    /**
     * This segment description.
     */
    private static final String SEGMENT_DESCRIPTION = DataModule.MODULE_ID + ".record.draft.get.finish.description";
    /**
     * CRC.
     */
    @Autowired
    private CommonRecordsComponent commonRecordsComponent;
    /**
     * RCC.
     */
    @Autowired
    private RecordComposerComponent recordComposerComponent;
    /**
     * Constructor.
     */
    public RecordDraftGetFinishExecutor(){
        super(SEGMENT_ID, SEGMENT_DESCRIPTION, DraftGetResult.class);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public DraftGetResult finish(DraftGetContext ctx) {

        DraftGetResult result = new DraftGetResult();

        result.setDraft(ctx.currentDraft());
        result.setPayload(payload(ctx));

        return result;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?> start) {
        return DraftGetContext.class.isAssignableFrom(start.getInputTypeClass());
    }

    private DraftPayloadResponse payload(DraftGetContext ctx) {

        Draft draft = ctx.currentDraft();
        Edition edition = ctx.currentEdition();
        Variables variables = draft.getProperties();
        Right rights = SecurityUtils.getRightsForResourceWithDefault(variables.valueGet(DataDraftConstants.ENTITY_NAME));
        Timeline<OriginRecord> timeline = timeline(draft, edition);

        if (Objects.nonNull(timeline)) {

            DraftDataGetOperation operation = ctx.getParameter(DataDraftConstants.DRAFT_OPERATION);
            if (operation == DraftDataGetOperation.GET_TIMELINE) {

                GetDraftTimelineResponse response = new GetDraftTimelineResponse();
                response.setTimeline(timeline);
                response.setRights(new ResourceSpecificRightDTO(rights));

                return response;
            } else if (operation == DraftDataGetOperation.GET_PERIOD) {

                Date asOf = ctx.getParameter(DataDraftConstants.AS_OF);
                TimeInterval<OriginRecord> period = timeline.selectAsOf(asOf);

                GetDraftPeriodResponse response = new GetDraftPeriodResponse();

                if (Objects.nonNull(period)) {

                    recordComposerComponent.toEtalon(timeline.getKeys(), period);
                    response.setRecord(period.getCalculationResult());
                }

                response.setRights(new ResourceSpecificRightDTO(rights));

                return response;
            }
        }

        return null;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public CommonRecordsComponent getCommonRecordsComponent() {
        return commonRecordsComponent;
    }
}
