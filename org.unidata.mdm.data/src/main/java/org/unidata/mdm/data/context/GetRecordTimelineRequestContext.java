/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 *
 */
package org.unidata.mdm.data.context;

import java.util.Date;

import org.apache.commons.lang3.tuple.Pair;

/**
 * @author Mikhail Mikhailov
 * Fetch timeline for a record.
 */
public class GetRecordTimelineRequestContext
    extends AbstractRecordIdentityContext {
    /**
     * Generated SVUID.
     */
    private static final long serialVersionUID = -6826317454436803507L;
    /**
     * For a particular date (as of).
     */
    private final Date forDate;
    /**
     * For a particular date range.
     */
    private final Pair<Date, Date> forDatesFrame;
    /**
     * Has updates (new versions) after this date.
     */
    private final Date forUpdatesAfter;
    /**
     * Last update date to cut off versions.
     */
    private final Date forLastUpdate;
    /**
     * Operation id.
     */
    private final String forOperationId;
    /**
     * Constructor.
     */
    protected GetRecordTimelineRequestContext(GetRecordTimelineRequestContextBuilder b) {
        super(b);
        this.forDate = b.forDate;
        this.forDatesFrame = b.forDatesFrame;
        this.forUpdatesAfter = b.forUpdatesAfter;
        this.forLastUpdate = b.forLastUpdate;
        this.forOperationId = b.forOperationId;

        // Flags
        flags.set(DataContextFlags.FLAG_INCLUDE_DRAFTS, b.includeDrafts);
        flags.set(DataContextFlags.FLAG_FETCH_KEYS, b.fetchKeys);
        flags.set(DataContextFlags.FLAG_FETCH_TIMELINE_DATA, b.fetchData);
        flags.set(DataContextFlags.FLAG_SKIP_TIMELINE_CALCULATIONS, b.skipCalculations);
    }
    /**
     * @return the forDate
     */
    public Date getForDate() {
        return forDate;
    }
    /**
     * @return the dates frame
     */
    public Pair<Date, Date> getForDatesFrame() {
        return forDatesFrame;
    }
    /**
     * @return the updatesAfter
     */
    public Date getForUpdatesAfter() {
        return forUpdatesAfter;
    }
    /**
     * @return the lastUpdate
     */
    public Date getForLastUpdate() {
        return forLastUpdate;
    }
    /**
     * @return the forOperationId
     */
    public String getForOperationId() {
        return forOperationId;
    }
    /**
     * @return the unpublishedState
     */
    public boolean isIncludeDrafts() {
        return flags.get(DataContextFlags.FLAG_INCLUDE_DRAFTS);
    }
    /**
     * @return the tasks
     */
    public boolean isFetchKeys() {
        return flags.get(DataContextFlags.FLAG_FETCH_KEYS);
    }
    /**
     * @return the fetchTimelineData
     */
    public boolean isFetchData() {
        return flags.get(DataContextFlags.FLAG_FETCH_TIMELINE_DATA);
    }
    /**
     * @return the skipCalculations
     */
    public boolean isSkipCalculations() {
        return flags.get(DataContextFlags.FLAG_SKIP_TIMELINE_CALCULATIONS);
    }
    /**
     * Builder shorthand.
     * @return builder
     */
    public static GetRecordTimelineRequestContextBuilder builder() {
        return new GetRecordTimelineRequestContextBuilder();
    }
    /**
     * Builder shorthand.
     * @return builder
     */
    public static GetRecordTimelineRequestContextBuilder builder(AbstractRecordIdentityContext other) {

        GetRecordTimelineRequestContextBuilder b = new GetRecordTimelineRequestContextBuilder(other);
        if (other instanceof GetRequestContext) {
            GetRequestContext gOther = (GetRequestContext) other;
            b.forDate = gOther.getForDate();
            b.fetchData = gOther.isFetchTimelineData();
            b.includeDrafts = gOther.isIncludeDrafts();
            b.forLastUpdate = gOther.getForLastUpdate();
            b.forOperationId = gOther.getForOperationId();
            b.forUpdatesAfter = gOther.getUpdatesAfter();
        } else if (other instanceof UpsertRequestContext) {
            UpsertRequestContext gOther = (UpsertRequestContext) other;
            b.fetchData = true;
            b.includeDrafts = gOther.isIncludeDraftVersions();
        }

        return b;
    }
    /**
     * @author Mikhail Mikhailov
     * Context builder.
     */
    public static class GetRecordTimelineRequestContextBuilder
        extends AbstractRecordIdentityContextBuilder<GetRecordTimelineRequestContextBuilder> {
        /**
         * For a particular date (as of).
         */
        protected Date forDate;
        /**
         * For a particular date range (left <-> right).
         */
        protected Pair<Date, Date> forDatesFrame;
        /**
         * Has updates (new versions) after this date.
         */
        protected Date forUpdatesAfter;
        /**
         * Last update date to cut off versions.
         */
        protected Date forLastUpdate;
        /**
         * Operation id.
         */
        protected String forOperationId;
        /**
         * View unpublished state or not.
         */
        protected boolean includeDrafts;
        /**
         * Return keys.
         */
        protected boolean fetchKeys = true;
        /**
         * Return timeline data.
         */
        protected boolean fetchData;
        /**
         * Skip etalon, activity, operation type calculations.
         * Return raw timeline.
         */
        protected boolean skipCalculations;
        /**
         * Constructor.
         */
        protected GetRecordTimelineRequestContextBuilder() {
            super();
        }
        /**
         * Copy constructor.
         * @param other the other identity
         */
        protected GetRecordTimelineRequestContextBuilder(AbstractRecordIdentityContext other) {
            super(other);
        }
        /**
         * @param forDate the forDate to set
         */
        public GetRecordTimelineRequestContextBuilder forDate(Date forDate) {
            this.forDate = forDate;
            return self();
        }
        /**
         * @param forDatesFrame the date frame to set
         */
        public GetRecordTimelineRequestContextBuilder forDatesFrame(Pair<Date, Date> forDatesFrame) {
            this.forDatesFrame = forDatesFrame;
            return self();
        }
        /**
         * @param updatesAfter the updatesAfter to set
         */
        public GetRecordTimelineRequestContextBuilder forUpdatesAfter(Date updatesAfter) {
            this.forUpdatesAfter = updatesAfter;
            return self();
        }
        /**
         * @param forOperationId the forOperationId to set
         */
        public GetRecordTimelineRequestContextBuilder forOperationId(String forOperationId) {
            this.forOperationId = forOperationId;
            return self();
        }
        /**
         * Sets last update date to the context.
         * @param lastUpdate the date
         * @return self
         */
        public GetRecordTimelineRequestContextBuilder forLastUpdate(Date lastUpdate) {
            this.forLastUpdate = lastUpdate;
            return self();
        }
        /**
         * Request unpublished state of a record or not.
         * @param includeDrafts requested state
         * @return self
         */
        public GetRecordTimelineRequestContextBuilder includeDrafts(boolean includeDrafts) {
            this.includeDrafts = includeDrafts;
            return self();
        }
        /**
         * @param keys the keys to set
         */
        public GetRecordTimelineRequestContextBuilder fetchKeys(boolean keys) {
            this.fetchKeys = keys;
            return self();
        }
        /**
         * @param fetchData the fetchData to set
         */
        public GetRecordTimelineRequestContextBuilder fetchData(boolean fetchData) {
            this.fetchData = fetchData;
            return self();
        }
        /**
         * @param skipCalculations the skipCalculations to set
         */
        public GetRecordTimelineRequestContextBuilder skipCalculations(boolean skipCalculations) {
            this.skipCalculations = skipCalculations;
            return self();
        }
        /**
         * Builds a context.
         * @return a new context
         */
        @Override
        public GetRecordTimelineRequestContext build() {
            return new GetRecordTimelineRequestContext(this);
        }
    }
}
