/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.data.service.segments.relations;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.springframework.stereotype.Component;
import org.unidata.mdm.core.type.timeline.TimeInterval;
import org.unidata.mdm.core.type.timeline.Timeline;
import org.unidata.mdm.data.context.RelationMergeContext;
import org.unidata.mdm.data.convert.RelationIndexingConverter;
import org.unidata.mdm.data.module.DataModule;
import org.unidata.mdm.data.type.apply.RelationMergeChangeSet;
import org.unidata.mdm.data.type.data.EtalonRelation;
import org.unidata.mdm.data.type.data.OriginRelation;
import org.unidata.mdm.data.type.keys.RelationKeys;
import org.unidata.mdm.data.type.merge.MergeRelationMasterState;
import org.unidata.mdm.meta.type.RelativeDirection;
import org.unidata.mdm.meta.type.search.RelationFromIndexId;
import org.unidata.mdm.meta.type.search.RelationToIndexId;
import org.unidata.mdm.search.configuration.SearchConfigurationConstants;
import org.unidata.mdm.search.context.IndexRequestContext;
import org.unidata.mdm.search.type.id.ManagedIndexId;
import org.unidata.mdm.system.type.annotation.ConfigurationRef;
import org.unidata.mdm.system.type.configuration.ConfigurationValue;
import org.unidata.mdm.system.type.pipeline.Point;
import org.unidata.mdm.system.type.pipeline.Start;
import org.unidata.mdm.system.type.runtime.MeasurementPoint;

/**
 * Executor responsible for modifying relations have an alias key.
 */
@Component(RelationMergeIndexingExecutor.SEGMENT_ID)
public class RelationMergeIndexingExecutor extends Point<RelationMergeContext> {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = DataModule.MODULE_ID + "[RELATION_MERGE_INDEXING]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = DataModule.MODULE_ID + ".relation.merge.indexing.description";
    /**
     * Delay for async audit operations.
     */
    @ConfigurationRef(SearchConfigurationConstants.PROPERTY_REFRESH_IMMEDIATE)
    private ConfigurationValue<Boolean> refreshImmediate;
    /**
     * Constructor.
     */
    public RelationMergeIndexingExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void point(RelationMergeContext ctx) {
        MeasurementPoint.start();
        try {
            generateIndexing(ctx);
        } finally {
            MeasurementPoint.stop();
        }
    }

    private void generateIndexing(RelationMergeContext ctx) {

        Timeline<OriginRelation> currentTimeline = ctx.currentTimeline();
        RelationKeys currentKeys = ctx.relationKeys(); // The duplicate rel key

        Timeline<OriginRelation> next = ctx.nextTimeline();
        RelationKeys nextKeys = next.getKeys();

        RelationMergeChangeSet set = ctx.changeSet();

        List<EtalonRelation> updates = new ArrayList<>();
        List<ManagedIndexId> deletes = new ArrayList<>();

        currentTimeline.forEach(interval -> {
            deletes.add(
                    RelationFromIndexId.of(
                            currentKeys.getFromEntityName(),
                            currentKeys.getRelationName(),
                            currentKeys.getEtalonKey().getFrom().getId(),
                            currentKeys.getEtalonKey().getTo().getId(),
                            interval.getPeriodId()));
            deletes.add(
                    RelationToIndexId.of(
                            currentKeys.getToEntityName(),
                            currentKeys.getRelationName(),
                            currentKeys.getEtalonKey().getFrom().getId(),
                            currentKeys.getEtalonKey().getTo().getId(),
                            interval.getPeriodId()));
        });

        checkPreviousMasterState(ctx, nextKeys, deletes);

        // Updates
        for (TimeInterval<OriginRelation> interval : next) {
            updates.add(interval.getCalculationResult());
        }

        set.getIndexRequestContexts().add(IndexRequestContext.builder()
                .index(RelationIndexingConverter.convert(nextKeys, updates))
                .delete(deletes)
                .drop(!deletes.isEmpty())
                .entity(nextKeys.getFromEntityName())
                .refresh(!ctx.isBatchOperation() && refreshImmediate.getValue())
                .build());
    }

    private void checkPreviousMasterState(RelationMergeContext ctx, RelationKeys nextKeys, List<ManagedIndexId> deletes) {

        MergeRelationMasterState state = ctx.masterState();
        Timeline<OriginRelation> previousMasterState = ctx.getDirection() == RelativeDirection.FROM
                ? state.getPreviousFromTimeline(nextKeys)
                : state.getPreviousToTimeline(nextKeys);

        if (Objects.isNull(previousMasterState)) {
            return;
        }

        previousMasterState.forEach(interval -> {
            deletes.add(
                    RelationFromIndexId.of(
                            nextKeys.getFromEntityName(),
                            nextKeys.getRelationName(),
                            nextKeys.getEtalonKey().getFrom().getId(),
                            nextKeys.getEtalonKey().getTo().getId(),
                            interval.getPeriodId()));
            deletes.add(
                    RelationToIndexId.of(
                            nextKeys.getToEntityName(),
                            nextKeys.getRelationName(),
                            nextKeys.getEtalonKey().getFrom().getId(),
                            nextKeys.getEtalonKey().getTo().getId(),
                            interval.getPeriodId()));
        });
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?> start) {
        return RelationMergeContext.class.isAssignableFrom(start.getInputTypeClass());
    }
}
