package org.unidata.mdm.data.context;

import java.util.Date;

import org.unidata.mdm.data.service.segments.relations.RelationRestoreStartExecutor;
import org.unidata.mdm.data.type.keys.RecordKeys;

/**
 * @author Mikhail Mikhailov on May 3, 2020
 */
public class RestoreToRelationRequestContext
    extends AbstractRelationIdentityContext
    implements
            RelationToIdentityContext,
            RelationRestoreContext {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = -5614833553851688251L;
    /**
     * Set range from.
     */
    private Date validFrom;
    /**
     * Set range to.
     */
    private Date validTo;
    /**
     * Last update date to use (optional).
     */
    private final Date lastUpdate;
    /**
     * For a particular date (as of).
     */
    private final Date forDate;
    /**
     * Constructor.
     * @param b the builder
     */
    private RestoreToRelationRequestContext(RestoreToRelationRequestContextBuilder b) {
        super(b);
        this.toKeys(b.toKeys);
        this.forDate = b.forDate;
        this.lastUpdate = b.lastUpdate;
        this.validFrom = b.validFrom;
        this.validTo = b.validTo;

        flags.set(DataContextFlags.FLAG_IS_PERIOD_RESTORE, b.restorePeriod);
        flags.set(DataContextFlags.FLAG_BATCH_OPERATION, b.batchOperation);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getStartTypeId() {
        return RelationRestoreStartExecutor.SEGMENT_ID;
    }
    /**
     * @return the forDate
     */
    @Override
    public Date getForDate() {
        return forDate;
    }
    /**
     * @return the lastUpdate
     */
    @Override
    public Date getLastUpdate() {
        return lastUpdate;
    }
    /**
     * @return the validFrom
     */
    @Override
    public Date getValidFrom() {
        return validFrom;
    }
    /**
     * @return the validTo
     */
    @Override
    public Date getValidTo() {
        return validTo;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void setValidFrom(Date from) {
        this.validFrom = from;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void setValidTo(Date to) {
        this.validTo = to;
    }
    /**
     * The builder.
     * @return builder
     */
    public static RestoreToRelationRequestContextBuilder builder() {
        return new RestoreToRelationRequestContextBuilder();
    }
    /**
     * The builder.
     * @author Mikhail Mikhailov on May 3, 2020
     */
    public static class RestoreToRelationRequestContextBuilder extends AbstractRelationIdentityContextBuilder<RestoreToRelationRequestContextBuilder> {
        /**
         * The to-side keys.
         */
        private RecordKeys toKeys;
        /**
         * Set range from.
         */
        private Date validFrom;
        /**
         * Set range to.
         */
        private Date validTo;
        /**
         * The TL point.
         */
        private Date forDate;
        /**
         * Last update date to use (optional).
         */
        private Date lastUpdate;
        /**
         * define that is restore period request.
         */
        private boolean restorePeriod;
        /**
         * This context is participating in a batch upsert. Collect artifacts instead of upserting immediately.
         */
        private boolean batchOperation;
        /**
         * Constructor.
         */
        private RestoreToRelationRequestContextBuilder() {
            super();
        }
        /**
         * Sets the to-side keys.
         * Relevant for a single rel merge only.
         * In all other cases happens automatically.
         * @param keys the to side keys
         * @return self
         */
        public RestoreToRelationRequestContextBuilder toKeys(RecordKeys keys) {
            this.toKeys = keys;
            return self();
        }
        /**
         * @param validFrom the range from to set
         */
        public RestoreToRelationRequestContextBuilder validFrom(Date validFrom) {
            this.validFrom = validFrom;
            return this;
        }
        /**
         * @param validTo the range to to set
         */
        public RestoreToRelationRequestContextBuilder validTo(Date validTo) {
            this.validTo = validTo;
            return this;
        }
        /**
         * @param lastUpdate the last update to set
         */
        public RestoreToRelationRequestContextBuilder lastUpdate(Date lastUpdate) {
            this.lastUpdate = lastUpdate;
            return this;
        }
        /**
         * @param forDate the forDate to set
         */
        public RestoreToRelationRequestContextBuilder forDate(Date forDate) {
            this.forDate = forDate;
            return this;
        }
        /**
         * @param batchUpsert the flag
         * @return self
         */
        public RestoreToRelationRequestContextBuilder batchOperation(boolean batchUpsert) {
            this.batchOperation = batchUpsert;
            return this;
        }
        /**
         * define that that is a period restore request.
         * @param periodRestore
         * @return self
         */
        public RestoreToRelationRequestContextBuilder periodRestore(boolean periodRestore) {
            this.restorePeriod = periodRestore;
            return this;
        }
        /**
         * {@inheritDoc}
         */
        @Override
        public RestoreToRelationRequestContext build() {
            return new RestoreToRelationRequestContext(this);
        }
    }
}
