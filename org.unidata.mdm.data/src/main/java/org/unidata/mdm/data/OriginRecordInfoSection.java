package org.unidata.mdm.data;

import java.io.Serializable;
import javax.xml.datatype.XMLGregorianCalendar;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;


/**
 * <p>Java class for OriginRecordInfoSection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OriginRecordInfoSection"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="entityName" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="createdBy" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="updatedBy" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="status" type="{http://data.mdm.unidata.org/}RecordStatus" /&gt;
 *       &lt;attribute name="approval" type="{http://data.mdm.unidata.org/}ApprovalState" /&gt;
 *       &lt;attribute name="rangeFrom" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&gt;
 *       &lt;attribute name="rangeTo" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&gt;
 *       &lt;attribute name="createDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&gt;
 *       &lt;attribute name="updateDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" /&gt;
 *       &lt;attribute name="revision" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
public class OriginRecordInfoSection implements Serializable {

    private final static long serialVersionUID = 12345L;
    @JacksonXmlProperty(isAttribute = true)
    protected String entityName;
    @JacksonXmlProperty(isAttribute = true)
    protected String createdBy;
    @JacksonXmlProperty(isAttribute = true)
    protected String updatedBy;
    @JacksonXmlProperty(isAttribute = true)
    protected RecordStatus status;
    @JacksonXmlProperty(isAttribute = true)
    protected ApprovalState approval;
    @JacksonXmlProperty(isAttribute = true)
    protected XMLGregorianCalendar rangeFrom;
    @JacksonXmlProperty(isAttribute = true)
    protected XMLGregorianCalendar rangeTo;
    @JacksonXmlProperty(isAttribute = true)
    protected XMLGregorianCalendar createDate;
    @JacksonXmlProperty(isAttribute = true)
    protected XMLGregorianCalendar updateDate;
    @JacksonXmlProperty(isAttribute = true)
    protected Integer revision;

    /**
     * Gets the value of the entityName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityName() {
        return entityName;
    }

    /**
     * Sets the value of the entityName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityName(String value) {
        this.entityName = value;
    }

    /**
     * Gets the value of the createdBy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * Sets the value of the createdBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreatedBy(String value) {
        this.createdBy = value;
    }

    /**
     * Gets the value of the updatedBy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUpdatedBy() {
        return updatedBy;
    }

    /**
     * Sets the value of the updatedBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUpdatedBy(String value) {
        this.updatedBy = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link RecordStatus }
     *     
     */
    public RecordStatus getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link RecordStatus }
     *     
     */
    public void setStatus(RecordStatus value) {
        this.status = value;
    }

    /**
     * Gets the value of the approval property.
     * 
     * @return
     *     possible object is
     *     {@link ApprovalState }
     *     
     */
    public ApprovalState getApproval() {
        return approval;
    }

    /**
     * Sets the value of the approval property.
     * 
     * @param value
     *     allowed object is
     *     {@link ApprovalState }
     *     
     */
    public void setApproval(ApprovalState value) {
        this.approval = value;
    }

    /**
     * Gets the value of the rangeFrom property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRangeFrom() {
        return rangeFrom;
    }

    /**
     * Sets the value of the rangeFrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRangeFrom(XMLGregorianCalendar value) {
        this.rangeFrom = value;
    }

    /**
     * Gets the value of the rangeTo property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRangeTo() {
        return rangeTo;
    }

    /**
     * Sets the value of the rangeTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRangeTo(XMLGregorianCalendar value) {
        this.rangeTo = value;
    }

    /**
     * Gets the value of the createDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreateDate() {
        return createDate;
    }

    /**
     * Sets the value of the createDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreateDate(XMLGregorianCalendar value) {
        this.createDate = value;
    }

    /**
     * Gets the value of the updateDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getUpdateDate() {
        return updateDate;
    }

    /**
     * Sets the value of the updateDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setUpdateDate(XMLGregorianCalendar value) {
        this.updateDate = value;
    }

    /**
     * Gets the value of the revision property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRevision() {
        return revision;
    }

    /**
     * Sets the value of the revision property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRevision(Integer value) {
        this.revision = value;
    }

    public OriginRecordInfoSection withEntityName(String value) {
        setEntityName(value);
        return this;
    }

    public OriginRecordInfoSection withCreatedBy(String value) {
        setCreatedBy(value);
        return this;
    }

    public OriginRecordInfoSection withUpdatedBy(String value) {
        setUpdatedBy(value);
        return this;
    }

    public OriginRecordInfoSection withStatus(RecordStatus value) {
        setStatus(value);
        return this;
    }

    public OriginRecordInfoSection withApproval(ApprovalState value) {
        setApproval(value);
        return this;
    }

    public OriginRecordInfoSection withRangeFrom(XMLGregorianCalendar value) {
        setRangeFrom(value);
        return this;
    }

    public OriginRecordInfoSection withRangeTo(XMLGregorianCalendar value) {
        setRangeTo(value);
        return this;
    }

    public OriginRecordInfoSection withCreateDate(XMLGregorianCalendar value) {
        setCreateDate(value);
        return this;
    }

    public OriginRecordInfoSection withUpdateDate(XMLGregorianCalendar value) {
        setUpdateDate(value);
        return this;
    }

    public OriginRecordInfoSection withRevision(Integer value) {
        setRevision(value);
        return this;
    }

}
