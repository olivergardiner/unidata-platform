/*
 *
 *  * Unidata Platform
 *  * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *  *
 *  * Commercial License
 *  * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *  *
 *  * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 *  * For clarification or additional options, please contact: info@unidata-platform.com
 *  * -------
 *  * Disclaimer:
 *  * -------
 *  * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 *  * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 *  * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 *  * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 *
 */

package org.unidata.mdm.data.service.segments.records;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.type.calculables.CalculableHolder;
import org.unidata.mdm.core.type.data.RecordStatus;
import org.unidata.mdm.core.type.timeline.MutableTimeInterval;
import org.unidata.mdm.core.type.timeline.Timeline;
import org.unidata.mdm.core.type.timeline.impl.RevisionSlider;
import org.unidata.mdm.data.context.DeleteRequestContext;
import org.unidata.mdm.data.context.UpsertRequestContext;
import org.unidata.mdm.data.module.DataModule;
import org.unidata.mdm.data.service.DataRecordsService;
import org.unidata.mdm.data.service.impl.CommonRecordsComponent;
import org.unidata.mdm.data.service.segments.RecordDraftTimelineSupport;
import org.unidata.mdm.data.type.data.OriginRecord;
import org.unidata.mdm.data.type.draft.DataDraftConstants;
import org.unidata.mdm.draft.context.DraftPublishContext;
import org.unidata.mdm.draft.dto.DraftPublishResult;
import org.unidata.mdm.draft.type.Draft;
import org.unidata.mdm.draft.type.Edition;
import org.unidata.mdm.system.type.pipeline.Finish;
import org.unidata.mdm.system.type.pipeline.Start;
import org.unidata.mdm.system.type.support.IdentityHashSet;

/**
 * @author Alexey Tsarapkin
 */
@Component(RecordDraftPublishFinishExecutor.SEGMENT_ID)
public class RecordDraftPublishFinishExecutor extends Finish<DraftPublishContext, DraftPublishResult> implements RecordDraftTimelineSupport {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = DataModule.MODULE_ID + "[RECORD_DRAFT_PUBLISH_FINISH]";
    /**
     * This segment description.
     */
    private static final String SEGMENT_DESCRIPTION = DataModule.MODULE_ID + ".record.draft.publish.finish.description";
    /**
     * PC.
     */
    @Autowired
    private DataRecordsService dataRecordsService;
    /**
     * CRC.
     */
    @Autowired
    private CommonRecordsComponent commonRecordsComponent;
    /**
     * Constructor.
     */
    public RecordDraftPublishFinishExecutor(){
        super(SEGMENT_ID, SEGMENT_DESCRIPTION, DraftPublishResult.class);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public DraftPublishResult finish(DraftPublishContext ctx) {

        DraftPublishResult result = new DraftPublishResult(publish(ctx));
        result.setDraft(ctx.currentDraft());

        return result;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?> start) {
        return DraftPublishContext.class.isAssignableFrom(start.getInputTypeClass());
    }
    /**
     * Does publish periods to persistent storage.
     * @param ctx the context
     * @return true, if successful
     */
    private boolean publish(DraftPublishContext ctx) {

        Draft draft = ctx.currentDraft();
        Edition edition = ctx.currentEdition();

        boolean isNew = draft.getProperties().valueGet(DataDraftConstants.IS_NEW_RECORD);

        Set<CalculableHolder<OriginRecord>> supply
            = new TreeSet<>(RevisionSlider.POP_TOP_COMPARATOR);

        Timeline<OriginRecord> timeline = timeline(draft, edition);
        supply.addAll(timeline.stream()
            .map(ti -> (MutableTimeInterval<OriginRecord>) ti)
            .map(MutableTimeInterval::toModifications)
            .map(Map::values)
            .flatMap(Collection::stream)
            .flatMap(Collection::stream)
            .collect(Collectors.toCollection(IdentityHashSet::new)));

        // Default tree set ascending iterator
        Iterator<CalculableHolder<OriginRecord>> it = supply.iterator();
        while (it.hasNext()) {

            CalculableHolder<OriginRecord> ch = it.next();
            if (ch.getStatus() == RecordStatus.ACTIVE) {

                UpsertRequestContext iCtx = UpsertRequestContext.builder()
                        .record(ch.getValue())
                        .etalonKey(isNew ? null : timeline.getKeys().getEtalonKey().getId())
                        .sourceSystem(ch.getSourceSystem())
                        .externalId(ch.getExternalId())
                        .entityName(ch.getTypeName())
                        .validFrom(ch.getValidFrom())
                        .validTo(ch.getValidTo())
                        .applyDraft(true)
                        .build();

                dataRecordsService.upsertRecord(iCtx);
            } else {

                DeleteRequestContext dCtx = DeleteRequestContext.builder()
                        .etalonKey(isNew ? null : timeline.getKeys().getEtalonKey().getId())
                        .sourceSystem(ch.getSourceSystem())
                        .externalId(ch.getExternalId())
                        .entityName(ch.getTypeName())
                        .inactivatePeriod(true)
                        .validFrom(ch.getValidFrom())
                        .validTo(ch.getValidTo())
                        .build();

                dataRecordsService.deleteRecord(dCtx);
            }
        }

        return true;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public CommonRecordsComponent getCommonRecordsComponent() {
        return commonRecordsComponent;
    }
}
