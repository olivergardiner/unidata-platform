package org.unidata.mdm.data.dto;

import org.unidata.mdm.core.dto.ResourceSpecificRightDTO;
import org.unidata.mdm.core.type.timeline.Timeline;
import org.unidata.mdm.data.type.data.OriginRecord;
import org.unidata.mdm.draft.type.DraftPayloadResponse;

/**
 * @author Mikhail Mikhailov on Sep 24, 2020
 */
public class GetDraftTimelineResponse implements DraftPayloadResponse {
    /**
     * The timeline.
     */
    private Timeline<OriginRecord> timeline;
    /**
     * Rights.
     */
    private ResourceSpecificRightDTO rights;
    /**
     * Constructor.
     */
    public GetDraftTimelineResponse() {
        super();
    }
    /**
     * @return the timeline
     */
    public Timeline<OriginRecord> getTimeline() {
        return timeline;
    }
    /**
     * @param timeline the timeline to set
     */
    public void setTimeline(Timeline<OriginRecord> timeline) {
        this.timeline = timeline;
    }
    /**
     * @return the rights
     */
    public ResourceSpecificRightDTO getRights() {
        return rights;
    }
    /**
     * @param rights the rights to set
     */
    public void setRights(ResourceSpecificRightDTO rights) {
        this.rights = rights;
    }
}
