/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.data.converter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.springframework.util.CollectionUtils;
import org.unidata.mdm.data.type.keys.RecordOriginKey;
import org.unidata.mdm.rest.data.ro.OriginKeyRO;

/**
 * @author Mikhail Mikhailov
 *
 */
public class OriginKeyConverter {

    /**
     * Constructor.
     */
    private OriginKeyConverter() {
        super();
    }

    public static OriginKeyRO to(RecordOriginKey key) {

        if (Objects.isNull(key)) {
            return null;
        }

        OriginKeyRO result = new OriginKeyRO();
        result.setEnrichment(key.isEnrichment());
        result.setEntityName(key.getEntityName());
        result.setExternalId(key.getExternalId());
        result.setSourceSystem(key.getSourceSystem());
        result.setId(key.getId());

        return result;
    }

    public static List<OriginKeyRO> to(List<RecordOriginKey> keys) {

        if (CollectionUtils.isEmpty(keys)) {
            return Collections.emptyList();
        }

        List<OriginKeyRO> result = new ArrayList<>(keys.size());
        for (RecordOriginKey key : keys) {
            OriginKeyRO ro = to(key);
            if (Objects.isNull(ro)) {
                continue;
            }

            result.add(ro);
        }

        return result;
    }
}
