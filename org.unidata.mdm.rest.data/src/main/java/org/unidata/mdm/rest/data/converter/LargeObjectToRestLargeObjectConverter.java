/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
/**
 *
 */
package org.unidata.mdm.rest.data.converter;

import org.unidata.mdm.rest.data.ro.LargeObjectRO;
import org.unidata.mdm.core.dto.LargeObjectDTO;

/**
 * @author Mikhail Mikhailov
 *
 */
public class LargeObjectToRestLargeObjectConverter {

    /**
     * Constructor.
     */
    private LargeObjectToRestLargeObjectConverter() {
        super();
    }

    public static LargeObjectRO convert(LargeObjectDTO source) {
        LargeObjectRO target = new LargeObjectRO();

        target.setId(source.getId());
        target.setFileName(source.getFileName());
        target.setMimeType(source.getMimeType());
        target.setSize(source.getSize());

        return target;
    }
}
