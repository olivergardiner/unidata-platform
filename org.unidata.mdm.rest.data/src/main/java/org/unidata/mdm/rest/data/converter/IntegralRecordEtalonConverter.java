/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.data.converter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.unidata.mdm.core.type.data.DataRecord;
import org.unidata.mdm.data.type.data.EtalonRecordInfoSection;
import org.unidata.mdm.data.type.data.EtalonRelation;
import org.unidata.mdm.data.type.data.impl.EtalonRecordImpl;
import org.unidata.mdm.rest.data.ro.EtalonRecordRO;
import org.unidata.mdm.rest.data.ro.EtalonIntegralRecordRO;

/**
 * @author Mikhail Mikhailov
 * Converts {@link EtalonIntegralRecordRO} type relations.
 */
public class IntegralRecordEtalonConverter {

    /**
     * Constructor.
     */
    private IntegralRecordEtalonConverter() {
        super();
    }

    /**
     * Converts to the REST type.
     * @param source the source
     * @return target
     */
    public static EtalonIntegralRecordRO to(EtalonRelation source) {

        if (source == null) {
            return null;
        }

        EtalonIntegralRecordRO target = new EtalonIntegralRecordRO();
        EtalonRecordRO record = DataRecordEtalonConverter
            .to(new EtalonRecordImpl()
                    .withDataRecord(source)
                    .withInfoSection(new EtalonRecordInfoSection()
                            .withOperationType(source.getInfoSection().getOperationType())
                            .withApproval(source.getInfoSection().getApproval())
                            .withCreateDate(source.getInfoSection().getCreateDate())
                            .withCreatedBy(source.getInfoSection().getCreatedBy())
                            .withEntityName(source.getInfoSection().getToEntityName())
                            .withEtalonKey(source.getInfoSection().getToEtalonKey())
                            .withValidFrom(source.getInfoSection().getValidFrom())
                            .withValidTo(source.getInfoSection().getValidTo())), null);

        target.setEtalonRecord(record);
        target.setEtalonId(source.getInfoSection().getRelationEtalonKey());
        target.setRelName(source.getInfoSection().getRelationName());
        target.setCreateDate(source.getInfoSection().getCreateDate());
        target.setCreatedBy(source.getInfoSection().getCreatedBy());
        target.setStatus(source.getInfoSection().getStatus().name());
        target.setUpdateDate(source.getInfoSection().getUpdateDate());
        target.setUpdatedBy(source.getInfoSection().getUpdatedBy());

        return target;
    }

    /**
     * Converts from the REST type.
     * @param source the source
     * @return target
     */
    public static DataRecord from(EtalonIntegralRecordRO source) {

        if (source == null) {
            return null;
        }

        return DataRecordEtalonConverter.from(source.getEtalonRecord());
    }

    /**
     * Converts a list of {@link RelationToRO} type relations.
     * @param source the source list
     * @param target the destination
     */
    public static List<EtalonIntegralRecordRO> to(List<EtalonRelation> source) {

        if (source == null || source.isEmpty()) {
            return Collections.emptyList();
        }

        List<EtalonIntegralRecordRO> target = new ArrayList<>();
        for (EtalonRelation r : source) {
            target.add(to(r));
        }

        return target;
    }

    /**
     * Converts a list of {@link RelationToRO} type relations.
     * @param source the source list
     * @param target the destination
     * @param relName relation name
     */
    public static List<DataRecord> from(List<EtalonIntegralRecordRO> source) {

        if (source == null || source.isEmpty()) {
            return Collections.emptyList();
        }

        List<DataRecord> target = new ArrayList<>();
        for (EtalonIntegralRecordRO r : source) {
            target.add(from(r));
        }

        return target;
    }
}
