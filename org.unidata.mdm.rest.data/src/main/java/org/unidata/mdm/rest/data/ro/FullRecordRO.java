/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.data.ro;

import java.util.HashMap;
import java.util.Map;

import org.unidata.mdm.rest.system.ro.RestInputSource;
import org.unidata.mdm.rest.system.ro.RestOutputSink;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * @author Dmitry Kopin on 19.06.2018.
 *         Full record for 'atomic' upsert
 */
public class FullRecordRO implements RestInputSource, RestOutputSink {
    /**
     * Data record
     */
    private EtalonRecordRO dataRecord;
    /**
     * Another optional information (Relations, Classifiers and etc.)
     */
    private Map<String, JsonNode> any = new HashMap<>();
    /**
     * {@inheritDoc}
     */
    @Override
    public void setAny(String name, JsonNode value) {
        any.put(name, value);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, JsonNode> getAny() {
        return any;
    }

    public EtalonRecordRO getDataRecord() {
        return dataRecord;
    }

    public void setDataRecord(EtalonRecordRO dataRecord) {
        this.dataRecord = dataRecord;
    }
}
