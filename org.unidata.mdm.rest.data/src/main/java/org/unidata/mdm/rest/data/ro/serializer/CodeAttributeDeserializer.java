/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.data.ro.serializer;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import org.unidata.mdm.rest.data.ro.CodeAttributeRO;
import org.unidata.mdm.rest.system.ro.CodeDataTypeRO;

/**
 * @author Michael Yashin. Created on 03.06.2015.
 */
public class CodeAttributeDeserializer extends JsonDeserializer<CodeAttributeRO> {

    @Override
    public CodeAttributeRO deserialize(JsonParser jsonParser,
                            DeserializationContext deserializationContext) throws IOException {

        ObjectCodec oc = jsonParser.getCodec();
        JsonNode node = oc.readTree(jsonParser);
        JsonNode nameNode = node.get("name");
        JsonNode typeNode = node.get("type");
        JsonNode valueNode = node.get("value");

        if (nameNode == null) {
            throw new JsonMappingException("CodeAttributeRO: Name node is required");
        }

        if (typeNode == null) {
            throw new JsonMappingException("CodeAttributeRO: Type node is required");
        }

        if (valueNode == null) {
            throw new JsonMappingException("CodeAttributeRO: Value node is required");
        }

        CodeDataTypeRO type = CodeDataTypeRO.fromValue(typeNode.asText());
        CodeAttributeRO attr = new CodeAttributeRO();
        attr.setName(nameNode.asText());
        attr.setType(type);

        if (!valueNode.isNull()) {

            if (!valueNode.isIntegralNumber() && !valueNode.isTextual()) {
                throw new JsonMappingException("CodeAttributeRO: Value node must be either an integer or string");
            }

            switch (type) {
                case INTEGER:
                    attr.setValue(Long.valueOf(valueNode.asLong()));
                    break;
                case STRING:
                    attr.setValue(valueNode.asText());
                    break;
                default:
                    throw new JsonMappingException("Unsupported data type: " + type.name());
            }
        }

        return attr;
    }
}
