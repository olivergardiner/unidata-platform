/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.data.ro;

import java.util.List;

/**
 * Wrapper object for contains relation (only diff)
 *
 * @author Dmitry Kopin on 19.06.2018.
 */
public class RelationContainsWrapperRO {
    /**
     * records for update
     */
    private List<EtalonIntegralRecordRO> toUpdate;
    /**
     * records for delete
     */
    private List<RelationDeleteWrapperRO> toDelete;

    public List<EtalonIntegralRecordRO> getToUpdate() {
        return toUpdate;
    }

    public void setToUpdate(List<EtalonIntegralRecordRO> toUpdate) {
        this.toUpdate = toUpdate;
    }

    public List<RelationDeleteWrapperRO> getToDelete() {
        return toDelete;
    }

    public void setToDelete(List<RelationDeleteWrapperRO> toDelete) {
        this.toDelete = toDelete;
    }
}
