/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.data.converter;

import static java.util.Objects.nonNull;

import org.unidata.mdm.data.dto.GetRecordDTO;
import org.unidata.mdm.data.type.data.OriginRecord;
import org.unidata.mdm.rest.data.ro.EtalonRecordRO;
import org.unidata.mdm.rest.data.ro.ExtendedRecordRO;
import org.unidata.mdm.rest.data.ro.OriginRecordRO;

public class EtalonPreviewConverter {

    private EtalonPreviewConverter() {
        super();
    }

    public static ExtendedRecordRO convert(GetRecordDTO record) {

        ExtendedRecordRO extendedRecordRO = new ExtendedRecordRO();
        extendedRecordRO.setAttributeWinnersMap(record.getAttributeWinnersMap());
        if (nonNull(record.getEtalon())) {

            EtalonRecordRO nestedRecordRO = new EtalonRecordRO();
            SimpleAttributeConverter.to(record.getEtalon().getSimpleAttributes(), nestedRecordRO.getSimpleAttributes());
            ComplexAttributeConverter.to(record.getEtalon().getComplexAttributes(), nestedRecordRO.getComplexAttributes());

            nestedRecordRO.getCodeAttributes().addAll(CodeAttributeConverter.to(record.getEtalon().getCodeAttributes()));
            nestedRecordRO.getArrayAttributes().addAll(ArrayAttributeConverter.to(record.getEtalon().getArrayAttributes()));
            extendedRecordRO.setRecord(nestedRecordRO);

        } else if (nonNull(record.getOrigins()) && record.getOrigins().size() == 1) {

            OriginRecord originRecord = record.getOrigins().get(0);
            OriginRecordRO nestedRecordRO = new OriginRecordRO();
            SimpleAttributeConverter.to(originRecord.getSimpleAttributes(), nestedRecordRO.getSimpleAttributes());
            ComplexAttributeConverter.to(originRecord.getComplexAttributes(), nestedRecordRO.getComplexAttributes());
            nestedRecordRO.getCodeAttributes().addAll(CodeAttributeConverter.to(record.getEtalon().getCodeAttributes()));
            nestedRecordRO.getArrayAttributes().addAll(ArrayAttributeConverter.to(record.getEtalon().getArrayAttributes()));
            extendedRecordRO.setRecord(nestedRecordRO);
        }

        return extendedRecordRO;
    }

}
