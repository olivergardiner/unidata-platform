/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.data.util;

import java.util.Date;
import java.util.List;

import javax.ws.rs.core.PathSegment;

import org.unidata.mdm.data.exception.DataProcessingException;
import org.unidata.mdm.rest.data.exception.DataRestExceptionIds;
import org.unidata.mdm.system.util.ConvertUtils;

/**
 * @author Michael Yashin. Created on 02.04.2015.
 */
public class RestUtils {

    /**
     * Constructor.
     */
    private RestUtils() {
        super();
    }
    /**
     * Extract starting time stamp from list of path segments.
     * @param timestamps list of path segments
     * @return date
     */
    public static Date extractStart(List<PathSegment> timestamps){
        throwIfSegmentsNotEven(timestamps);
        return "null".equals(timestamps.get(0).getPath()) ? null : ConvertUtils.string2Date(timestamps.get(0).getPath());
    }
    /**
     * Extract ending time stamp from list of path segments.
     * @param timestamps list of path segments
     * @return date
     */
    public static Date extractEnd(List<PathSegment> timestamps){
        throwIfSegmentsNotEven(timestamps);
        return "null".equals(timestamps.get(1).getPath()) ? null : ConvertUtils.string2Date(timestamps.get(1).getPath());
    }
    /**
     * Throws, if list is empty or contains more or less then two elements.
     * @param timestamps the list
     */
    private static void throwIfSegmentsNotEven(List<PathSegment> timestamps) {
        if (timestamps == null || timestamps.size() != 2) {
            throw new DataProcessingException("Timestamps aren't even!",
                    DataRestExceptionIds.EX_DATA_BOUNDARY_TIMESTAMPS_ARE_NOT_EVEN);
        }
    }
}
