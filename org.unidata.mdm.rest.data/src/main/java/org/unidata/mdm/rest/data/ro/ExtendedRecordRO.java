/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.data.ro;


import java.util.HashMap;
import java.util.Map;

import org.unidata.mdm.rest.system.ro.RestOutputSink;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.databind.JsonNode;

/**
 *
 */
public class ExtendedRecordRO implements RestOutputSink {
    /**
     * ANy unknown nodes.
     */
    private Map<String, JsonNode> any = new HashMap<>();
    /**
     * nested record
     */
    private NestedRecordRO record;
    /**
     * Attribute winner map
     */
    private Map<String, String> attributeWinnersMap;

    private String winnerEtalonId;

    /**
     * Constructor
     */
    public ExtendedRecordRO() {
    }

    /**
     * @param record record
     * @param attributeWinnersMap - attribute winner map.
     */
    public ExtendedRecordRO(NestedRecordRO record, Map<String, String> attributeWinnersMap) {
        this.record = record;
        this.attributeWinnersMap = attributeWinnersMap;
    }

    public NestedRecordRO getRecord() {
        return record;
    }

    public void setRecord(NestedRecordRO record) {
        this.record = record;
    }

    public Map<String, String> getAttributeWinnersMap() {
        return attributeWinnersMap;
    }

    public void setAttributeWinnersMap(Map<String, String> accessoryMap) {
        this.attributeWinnersMap = accessoryMap;
    }

    public String getWinnerEtalonId() {
        return winnerEtalonId;
    }

    public void setWinnerEtalonId(String winnerEtalonId) {
        this.winnerEtalonId = winnerEtalonId;
    }

    /**
     * {@inheritDoc}
     */
    @JsonAnySetter
    @Override
    public void setAny(String name, JsonNode value) {
        any.put(name, value);
    }
}
