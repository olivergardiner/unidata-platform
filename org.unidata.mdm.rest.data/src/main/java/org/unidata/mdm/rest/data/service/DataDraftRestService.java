/*
 *
 *  * Unidata Platform
 *  * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *  *
 *  * Commercial License
 *  * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *  *
 *  * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 *  * For clarification or additional options, please contact: info@unidata-platform.com
 *  * -------
 *  * Disclaimer:
 *  * -------
 *  * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 *  * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 *  * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 *  * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 *
 */

package org.unidata.mdm.rest.data.service;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.PathSegment;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.unidata.mdm.core.type.data.OperationType;
import org.unidata.mdm.data.dto.GetDraftPeriodResponse;
import org.unidata.mdm.data.dto.GetDraftTimelineResponse;
import org.unidata.mdm.data.dto.UpsertDraftPeriodResponse;
import org.unidata.mdm.data.exception.DataProcessingException;
import org.unidata.mdm.data.type.draft.DataDraftConstants;
import org.unidata.mdm.data.type.draft.DraftDataGetOperation;
import org.unidata.mdm.data.type.draft.DraftDataUpsertOperation;
import org.unidata.mdm.draft.context.DraftGetContext;
import org.unidata.mdm.draft.context.DraftQueryContext;
import org.unidata.mdm.draft.context.DraftUpsertContext;
import org.unidata.mdm.draft.context.DraftUpsertContext.DraftUpsertContextBuilder;
import org.unidata.mdm.draft.dto.DraftGetResult;
import org.unidata.mdm.draft.dto.DraftUpsertResult;
import org.unidata.mdm.draft.service.DraftService;
import org.unidata.mdm.meta.service.MetaModelService;
import org.unidata.mdm.rest.core.converter.RoleRoConverter;
import org.unidata.mdm.rest.data.converter.DataRecordEtalonConverter;
import org.unidata.mdm.rest.data.converter.RecordDiffStateConverter;
import org.unidata.mdm.rest.data.converter.TimelineToTimelineROConverter;
import org.unidata.mdm.rest.data.ro.EtalonRecordRO;
import org.unidata.mdm.rest.data.ro.FullRecordRO;
import org.unidata.mdm.rest.data.ro.TimelineRO;
import org.unidata.mdm.rest.data.type.rendering.DataRestInputRenderingAction;
import org.unidata.mdm.rest.data.type.rendering.DataRestOutputRenderingAction;
import org.unidata.mdm.rest.data.util.RestUtils;
import org.unidata.mdm.rest.search.ro.SearchRequestRO;
import org.unidata.mdm.rest.system.ro.ErrorInfo;
import org.unidata.mdm.rest.system.ro.ErrorResponse;
import org.unidata.mdm.rest.system.ro.RestResponse;
import org.unidata.mdm.rest.system.ro.UpdateResponse;
import org.unidata.mdm.rest.system.service.AbstractRestService;
import org.unidata.mdm.rest.system.util.RestConstants;
import org.unidata.mdm.system.service.RenderingService;
import org.unidata.mdm.system.util.ConvertUtils;
import org.unidata.mdm.system.util.TextUtils;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

/**
 * @author Alexey Tsarapkin
 */
@Path("draft")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class DataDraftRestService extends AbstractRestService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DataDraftRestService.class);

    private static final String DRAFT_ID_PARAM = "draftId";

    @Autowired
    private MetaModelService metaModelService;

    @Autowired
    private RenderingService renderingService;

    /**
     * The draft service.
     */
    @Autowired
    private DraftService draftService;

    @GET
    @Path("{draftId}")
    @Operation(
            description = "Get recrd's period draft",
            method = HttpMethod.GET,
            responses = {
                    @ApiResponse(content = @Content(schema = @Schema(implementation = Boolean.class)), responseCode = "200"),
                    @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
            }
    )
    public Response get(@Parameter(in = ParameterIn.PATH, description = "The draft ID") @PathParam("draftId") final long draftId,
                        @Parameter(in = ParameterIn.PATH, description = "Date on timeline") @PathParam(RestConstants.DATA_PARAM_DATE) String dateAsString,
                        @Parameter(in = ParameterIn.QUERY, description = "Return diff to etalon") @QueryParam(RestConstants.DATA_PARAM_DIFF_TO_DRAFT) String diffToDraftAsString,
                        @Parameter(in = ParameterIn.QUERY, description = "Return diff to previous state") @QueryParam(RestConstants.DATA_PARAM_DIFF_TO_PREVIOUS) String diffToPreviousAsString) {

        Date asOf = ConvertUtils.string2Date(dateAsString);

        DraftGetContext dgc = DraftGetContext.builder()
                .draftId(draftId)
                .parameter(DataDraftConstants.AS_OF, asOf)
                .parameter(DataDraftConstants.DRAFT_OPERATION, DraftDataGetOperation.GET_PERIOD)
                .build();

        DraftGetResult result = draftService.get(dgc);
        if (result.hasPayload()) {

            GetDraftPeriodResponse response = result.narrow();

            EtalonRecordRO ro = DataRecordEtalonConverter.to(response.getRecord(), Collections.emptyList());
            ro.setRights(RoleRoConverter.convertResourceSpecificRights(response.getRights()));
            ro.setDiffToDraft(RecordDiffStateConverter.to(response.getDiffToDraft()));
            ro.setEntityType(metaModelService.isEntity(ro.getEntityName())
                    ? RestConstants.REGISTER_ENTITY_TYPE
                    : RestConstants.LOOKUP_ENTITY_TYPE);

            return ok(new RestResponse<>(ro));
        }

        return ok(null);
    }

    @GET
    @Path("/" + RestConstants.PATH_PARAM_TIMELINE + "/{" + RestConstants.DATA_PARAM_ID + "}")
    @Operation(
            description = "Запросить таймлайн черновика. Запросить данные изменения черновика по времени.",
            method = HttpMethod.GET,
            responses = {
                    @ApiResponse(content = @Content(schema = @Schema(implementation = TimelineRO.class)), responseCode = "200"),
                    @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
            }
    )
    public Response timeline(
            @Parameter(description = "ID", in = ParameterIn.PATH) @PathParam(RestConstants.DATA_PARAM_ID) Long draftId,
            @Parameter(description = "Включать неактивные версии в эталон или нет (true/false)", in = ParameterIn.QUERY) @QueryParam(RestConstants.DATA_PARAM_INCLUDE_INACTIVE) String includeInactiveAsString) {

        DraftGetContext dgc = DraftGetContext.builder()
                .draftId(draftId)
                .parameter(DataDraftConstants.DRAFT_OPERATION, DraftDataGetOperation.GET_TIMELINE)
                .build();

        DraftGetResult result = draftService.get(dgc);
        if (result.hasPayload()) {

            GetDraftTimelineResponse response = result.narrow();
            return ok(new RestResponse<>(TimelineToTimelineROConverter.convert(response.getTimeline())));
        }

        return ok(null);
    }

    @DELETE
    @Operation(
            description = "Пометить версию периода черновика, как удаленную",
            method = "DELETE",
            responses = {
                    @ApiResponse(content = @Content(schema = @Schema(implementation = UpdateResponse.class)), responseCode = "200"),
                    @ApiResponse(content = @Content(schema = @Schema(implementation = org.unidata.mdm.rest.system.ro.ErrorResponse.class)), responseCode = "500")
            }
    )
    public Response deletePeriod(
            @Parameter(description = "ID черновика записи", in = ParameterIn.PATH) @PathParam(DRAFT_ID_PARAM) Long draftId,
            @Parameter(description = "Значения границ интервалов. Всегда 2 элемента", in = ParameterIn.PATH) @PathParam(RestConstants.DATA_PARAM_TIMESTAMPS) List<PathSegment> timestamps) {

        Date validFrom = RestUtils.extractStart(timestamps);
        Date validTo = RestUtils.extractEnd(timestamps);

        DraftUpsertContext duc = DraftUpsertContext.builder()
                .draftId(draftId)
                .parameter(DataDraftConstants.VALID_FROM, validFrom)
                .parameter(DataDraftConstants.VALID_TO, validTo)
                .parameter(DataDraftConstants.DRAFT_OPERATION, DraftDataUpsertOperation.DELETE_PERIOD)
                .build();

        DraftUpsertResult result = draftService.upsert(duc);
        return ok(new UpdateResponse(true, result.getDraft().getSubjectId()));
    }

    /**
     * Restore previously deleted period.
     *
     * @param draftId the record draft id.
     * @param timestamps the tomestamp as path segments
     * @return {@link RestResponse}
     * @throws Exception
     */
    @PUT
    @Path("/" + RestConstants.PATH_PARAM_PERIOD_RESTORE + "/{" + RestConstants.DATA_PARAM_ID + "}/{" + RestConstants.DATA_PARAM_TIMESTAMPS + ":.*}")
    @Operation(
            description = "Восстанавливает период в черновике записи по ID ченовика и значениям from и to.",
            method = "PUT",
            responses = {
                    @ApiResponse(content = @Content(schema = @Schema(implementation = RestResponse.class)), responseCode = "200"),
                    @ApiResponse(content = @Content(schema = @Schema(implementation = org.unidata.mdm.rest.system.ro.ErrorResponse.class)), responseCode = "500")
            }
    )
    public Response restorePeriod(
            @Parameter(description = "ID черновика записи", in = ParameterIn.PATH) @PathParam(DRAFT_ID_PARAM) Long draftId,
            @Parameter(description = "Значения границ интервалов. Всегда 2 элемента", in = ParameterIn.PATH) @PathParam(RestConstants.DATA_PARAM_TIMESTAMPS) List<PathSegment> timestamps) {

        final RestResponse<EtalonRecordRO> response = new RestResponse<>();

        try {

            Date validFrom = RestUtils.extractStart(timestamps);
            Date validTo = RestUtils.extractEnd(timestamps);

            DraftUpsertContext duc = DraftUpsertContext.builder()
                    .draftId(draftId)
                    .parameter(DataDraftConstants.VALID_FROM, validFrom)
                    .parameter(DataDraftConstants.VALID_TO, validTo)
                    .parameter(DataDraftConstants.DRAFT_OPERATION, DraftDataUpsertOperation.RESTORE_PERIOD)
                    .build();

            DraftUpsertResult result = draftService.upsert(duc);
            if (result.hasEdition()) {
                response.setSuccess(true);
                response.setContent(DataRecordEtalonConverter.to(result.getEdition().getContent(), Collections.<String>emptyList()));
            }

        } catch (Exception exc) {
            LOGGER.error("Can't restore record period.", exc);
            response.setSuccess(false);
            if (exc instanceof DataProcessingException) {
                ErrorInfo error = new ErrorInfo();
                error.setSeverity(ErrorInfo.Severity.LOW);
                error.setInternalMessage(exc.getMessage());
                error.setUserMessage(TextUtils.getText(((DataProcessingException) exc).getId().code(), ((DataProcessingException) exc).getArgs()));
                response.setErrors(Arrays.asList(error));
                return error(response);
            }
        }

        return ok(response);
    }

    @POST
    @Path("/search/count")
    @Operation(
            description = "Количество черновиков",
            method = HttpMethod.POST,
            requestBody = @RequestBody(content = @Content(schema = @Schema(implementation = SearchRequestRO.class)), description = "DraftResult request"),
            responses = {
                    @ApiResponse(content = @Content(schema = @Schema(implementation = RestResponse.class)), responseCode = "200"),
                    @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
            }
    )
    public Response count(SearchRequestRO searchRequestRO) {
        // TODO Fix this!
        return ok(new RestResponse<>(draftService.count(DraftQueryContext.builder()
                .build())));
//
//        return ok(new RestResponse<>(draftDataService.searchCount(DraftDataSearchRequestContext.builder()
//                .entityName(searchRequestRO.getEntityName())
//                .etalons(searchRequestRO.getEtalons())
//                .count(true)
//                .build())));
    }

    @POST
    @Path("/search")
    @Operation(
            description = "Найти черновики",
            method = HttpMethod.POST,
            requestBody = @RequestBody(content = @Content(schema = @Schema(implementation = SearchRequestRO.class)), description = "DraftResult request"),
            responses = {
                    @ApiResponse(content = @Content(schema = @Schema(implementation = RestResponse.class)), responseCode = "200"),
                    @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
            }
    )
    public Response search(@Parameter(description = "Select from", in = ParameterIn.QUERY) @QueryParam("start") @DefaultValue("0")
                                   Long start,
                           @Parameter(description = "Select limit", in = ParameterIn.QUERY) @QueryParam("limit") @DefaultValue("10")
                                   Long limit,
                           SearchRequestRO searchRequestRO) {

        // TODO Fix this

//        Collection<DraftDataDTO> searchResult = draftDataService.search(DraftDataSearchRequestContext.builder()
//                .entityName(searchRequestRO.getEntityName())
//                .etalons(searchRequestRO.getEtalons())
//                .limit(limit)
//                .start(start)
//                .count(false)
//                .build());
//
//        if (searchRequestRO.isGroupByEtalon()) {
//            Map<String, Map<String, List<DraftDataRO>>> drafts = searchResult.stream().map(dto -> new DraftDataRO(dto)).collect(
//                    Collectors.groupingBy(DraftDataRO::getEntityName, Collectors.groupingBy(draftDataRO -> StringUtils.isBlank(draftDataRO.getEntityId()) ? "" : draftDataRO.getEntityId())
//                    ));
//            return ok(new RestResponse<>(drafts));
//        } else {
//            Map<String, List<DraftDataRO>> drafts = searchResult.stream().map(dto -> new DraftDataRO(dto)).collect(
//                    Collectors.groupingBy(DraftDataRO::getEntityName
//                    ));
//            return ok(new RestResponse<>(drafts));
//        }

        return ok(null);
    }
    /**
     * Update record draft.
     *
     * @param draftId the record draft id
     * @param atomicUpsert the full record to save
     * @return updated record
     */
    @PUT
    @Path("/atomic/{" + DRAFT_ID_PARAM + "}")
    @Operation(
            description = "Обновить запись. На обновление присылается полная запись",
            method = "PUT",
            requestBody = @RequestBody(content = @Content(schema = @Schema(implementation = FullRecordRO.class)), description = "Запись"),
            responses = {
                    @ApiResponse(content = @Content(schema = @Schema(implementation = RestResponse.class)), responseCode = "200"),
                    @ApiResponse(content = @Content(schema = @Schema(implementation = org.unidata.mdm.rest.system.ro.ErrorResponse.class)), responseCode = "500")
            }
    )
    public Response atomic(
            @Parameter(description = "ID сущности", in = ParameterIn.PATH) @PathParam(DRAFT_ID_PARAM) String draftId,
            FullRecordRO atomicUpsert) {

        EtalonRecordRO record = atomicUpsert.getDataRecord();

        OperationType operationType = StringUtils.isNoneBlank(record.getOperationType())
                ? OperationType.valueOf(record.getOperationType())
                : null;

        Date validFrom = ConvertUtils.localDateTime2Date(record.getValidFrom());
        Date validTo = ConvertUtils.localDateTime2Date(record.getValidTo());

        DraftUpsertContextBuilder ducb = DraftUpsertContext.builder()
                .payload(DataRecordEtalonConverter.from(record))
                .draftId(Long.valueOf(draftId))
                .parameter(DataDraftConstants.VALID_FROM, validFrom)
                .parameter(DataDraftConstants.VALID_TO, validTo)
                .parameter(DataDraftConstants.OPERATION_TYPE, operationType)
                .parameter(DataDraftConstants.DRAFT_OPERATION, DraftDataUpsertOperation.UPSERT_PERIOD);

        // TODO fix this
        renderingService.renderInput(DataRestInputRenderingAction.ATOMIC_UPSERT_INPUT, ducb, atomicUpsert);

        DraftUpsertResult dur = draftService.upsert(ducb.build());

        RestResponse<EtalonRecordRO> retval = new RestResponse<>();
        retval.setSuccess(dur.isSuccess());

        if (dur.isSuccess() && dur.hasEdition() && dur.hasPayload()) {

            UpsertDraftPeriodResponse response = dur.narrow();

            EtalonRecordRO result = DataRecordEtalonConverter.to(response.getRecord(), Collections.<String>emptyList());
            result.setRights(RoleRoConverter.convertResourceSpecificRights(response.getRights()));
            result.setEntityType(metaModelService.isEntity(result.getEntityName())
                    ? RestConstants.REGISTER_ENTITY_TYPE
                    : RestConstants.LOOKUP_ENTITY_TYPE);

            // TODO fix this
            renderingService.renderOutput(DataRestOutputRenderingAction.ATOMIC_UPSERT_OUTPUT, dur, result);

            retval.setContent(result);
        }

        return ok(retval);
    }
}
