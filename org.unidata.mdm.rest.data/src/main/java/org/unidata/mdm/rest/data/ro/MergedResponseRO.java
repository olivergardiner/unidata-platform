/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.data.ro;

import java.util.HashMap;
import java.util.Map;

import org.unidata.mdm.rest.system.ro.RestOutputSink;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.JsonNode;

/**
 * @author ibykov
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MergedResponseRO implements RestOutputSink {
    /**
     * Any unknown nodes.
     */
    private Map<String, JsonNode> any = new HashMap<>();

    /** The etalon id. */
    private String etalonId;

    /**
     * Gets the etalon id.
     *
     * @return the etalonId
     */
    public String getEtalonId() {
        return etalonId;
    }

    /**
     * Sets the etalon id.
     *
     * @param etalonId            the etalonId to set
     */
    public void setEtalonId(String etalonId) {
        this.etalonId = etalonId;
    }
    /**
     * @param any the any to set
     */
    @Override
    @JsonAnySetter
    public void setAny(String name, JsonNode value) {
        this.any.put(name, value);
    }
}
