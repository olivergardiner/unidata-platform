/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.data.ro;


import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * @author Dmitry Kopin on 23.04.2019.
 */
public class RelationViewRO {
    /**
     * Etalon id of the From side.
     */
    private String etalonIdFrom;
    /**
     * Etalon id of the TO side.
     */
    private String etalonIdTo;
    /**
     * Etalon display name for 'To side'.
     */
    private String etalonDisplayNameTo;

    /**
     * Relation name.
     */
    private String relName;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS", timezone = "Europe/Moscow")
    private Date validFrom;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS", timezone = "Europe/Moscow")
    private Date validTo;

    public String getEtalonIdFrom() {
        return etalonIdFrom;
    }

    public void setEtalonIdFrom(String etalonIdFrom) {
        this.etalonIdFrom = etalonIdFrom;
    }

    public String getEtalonIdTo() {
        return etalonIdTo;
    }

    public void setEtalonIdTo(String etalonIdTo) {
        this.etalonIdTo = etalonIdTo;
    }

    public String getEtalonDisplayNameTo() {
        return etalonDisplayNameTo;
    }

    public void setEtalonDisplayNameTo(String etalonDisplayNameTo) {
        this.etalonDisplayNameTo = etalonDisplayNameTo;
    }

    public String getRelName() {
        return relName;
    }

    public void setRelName(String relName) {
        this.relName = relName;
    }

    /**
     * Range boundary from.
     */
    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    /**
     * Range boundary to.
     */
    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }
}
