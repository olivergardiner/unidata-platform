/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.data.ro;

import java.util.List;

/**
 * @author Dmitry Kopin on 23.04.2019.
 */
public class RelationSearchRO {

    private List<RecordTimelineRO> fromEtalonIds;

    private List<String> relationNames;

    private String entity;

    private Integer maxCount = 10000;


    public List<String> getRelationNames() {
        return relationNames;
    }

    public void setRelationNames(List<String> relationNames) {
        this.relationNames = relationNames;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public Integer getMaxCount() {
        return maxCount;
    }

    public void setMaxCount(Integer maxCount) {
        this.maxCount = maxCount;
    }


    public List<RecordTimelineRO> getFromEtalonIds() {
        return fromEtalonIds;
    }

    public void setFromEtalonIds(List<RecordTimelineRO> fromEtalonIds) {
        this.fromEtalonIds = fromEtalonIds;
    }
}
