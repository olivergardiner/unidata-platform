/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.data.ro;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.unidata.mdm.rest.system.ro.RestInputSource;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.JsonNode;

/**
 *
 * @author ibykov
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MergeRequestRO implements RestInputSource {

    /** The etalon ids. */
    private List<String> etalonIds;

    /** The winner etalon id. */
    private String winnerEtalonId;

    private Map<String, JsonNode> any = new HashMap<>();
    /**
     * Gets the winner etalon id.
     *
     * @return the winnerEtalonId
     */
    public String getWinnerEtalonId() {
        return winnerEtalonId;
    }

    /**
     * Sets the winner etalon id.
     *
     * @param winnerEtalonId the winnerEtalonId to set
     */
    public void setWinnerEtalonId(String winnerEtalonId) {
        this.winnerEtalonId = winnerEtalonId;
    }

    /** The winners. */
    private List<WinnerRO> winners;

    /** The entity name. */
    private String entityName;

    /**
     * Gets the entity name.
     *
     * @return the entityName
     */
    public String getEntityName() {
        return entityName;
    }

    /**
     * Sets the entity name.
     *
     * @param entityName the entityName to set
     */
    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    /**
     * Gets the etalon ids.
     *
     * @return the etalonIds
     */
    public List<String> getEtalonIds() {

        if (this.etalonIds == null) {
            this.etalonIds= new ArrayList<>();
        }
        return etalonIds;
    }

    /**
     * Sets the etalon ids.
     *
     * @param etalonIds
     *            the etalonIds to set
     */
    public void setEtalonIds(List<String> etalonIds) {
        this.etalonIds = etalonIds;
    }

    /**
     * Gets the winners.
     *
     * @return the winners
     */
    public List<WinnerRO> getWinners() {
        if (this.winners == null) {
            this.winners = new ArrayList<>();
        }
        return winners;
    }

    /**
     * Sets the winners.
     *
     * @param winners
     *            the winners to set
     */
    public void setWinners(List<WinnerRO> winners) {
        this.winners = winners;
    }

    @JsonAnyGetter
    @Override
    public Map<String, JsonNode> getAny() {
        return any;
    }
}
