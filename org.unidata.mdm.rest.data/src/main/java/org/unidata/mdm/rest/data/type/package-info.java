/**
 * Common REST types, related to data sub-sys.
 * @author Mikhail Mikhailov on Jan 30, 2020
 */
package org.unidata.mdm.rest.data.type;