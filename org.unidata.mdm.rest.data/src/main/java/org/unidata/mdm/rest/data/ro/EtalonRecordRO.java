/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.data.ro;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.unidata.mdm.rest.core.ro.ResourceSpecificRightRO;
import org.unidata.mdm.rest.system.ro.RestInputSource;
import org.unidata.mdm.rest.system.ro.RestOutputSink;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;

/**
 * @author Michael Yashin. Created on 02.06.2015.
 */
public class EtalonRecordRO extends NestedRecordRO implements RestInputSource, RestOutputSink {
    /**
     * Any unknown nodes.
     */
    private Map<String, JsonNode> any = new HashMap<>();
    /**
     * List of code attributes.
     */
    private List<CodeAttributeRO> codeAttributes = new ArrayList<>();
    /**
     * Entity key.
     */
    protected String etalonId;
    /**
     * Is record modified? (it's needed for restore functionality)
     */
    protected boolean modified;
    /**
     * Entity version.
     */
    protected int version;
    /**
     * Entity name.
     */
    protected String entityName;
    /**
     * Entity type.
     */
    protected String entityType;
    /**
     * Duplicate ids.
     */
    protected List<String> duplicateIds;
    /**
     * Optional validity range start date.
     */
    /**
     * Valid from period
     */
    protected LocalDateTime validFrom;
    /**
     * Optional validity range end date.
     */
    /**
     * Valid from period
     */
    protected LocalDateTime validTo;
    /**
     * Status {ACTIVE|INACTIVE|PENDING|MERGED}.
     */
    protected String status;
    /**
     * Status {PENDING|APPROVED|DECLINED}.
     */
    protected String approval;
    /**
     * Global sequence number.
     */
    protected String gsn;

    /**
     * Create date of the origin (source definition) record.
     */
    @JsonFormat(timezone = "DEFAULT_TIMEZONE")
    protected Date createDate;
    /**
     * Created by.
     */
    protected String createdBy;
    /**
     * Update date of the origin version record.
     */
    @JsonFormat(timezone = "DEFAULT_TIMEZONE")
    protected Date updateDate;
    /**
     * Updated by.
     */
    protected String updatedBy;
    /**
     * Rights.
     */
    protected ResourceSpecificRightRO rights;
    /**
     * Status {DIRECT|CASCADE|COPY}.
     */
    protected String operationType;
    /**
     * Diff table.
     */
    protected List<DiffToPreviousAttributeRO> diffToDraft;
    /**
     * This property is set for new records only and only for the time of the call.
     */
    protected String externalId = null;
    /**
     * @return the externalId
     */
    @JsonIgnore
    public String getExternalId() {
        return externalId;
    }
    /**
     * @param externalId the externalId to set
     */
    @JsonIgnore
    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }
    /**
     * @return the any
     */
    @Override
    @JsonAnyGetter
    public Map<String, JsonNode> getAny() {
        return any;
    }
    /**
     * @param any the any to set
     */
    @Override
    @JsonAnySetter
    public void setAny(String name, JsonNode value) {
        this.any.put(name, value);
    }
    /**
     * @return the validFrom
     */
    public LocalDateTime getValidFrom() {
        return validFrom;
    }

    /**
     * @param validFrom the validFrom to set
     */
    public void setValidFrom(LocalDateTime validFrom) {
        this.validFrom = validFrom;
    }

    /**
     * @return the validTo
     */
    public LocalDateTime getValidTo() {
        return validTo;
    }

    /**
     * @param validTo the validTo to set
     */
    public void setValidTo(LocalDateTime validTo) {
        this.validTo = validTo;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }


    /**
     * @return the approval
     */
    public String getApproval() {
        return approval;
    }


    /**
     * @param approval the approval to set
     */
    public void setApproval(String approval) {
        this.approval = approval;
    }

    /**
     * @return the etalonId
     */
    public String getEtalonId() {
        return etalonId;
    }

    /**
     * @param entityKey the entity key to set
     */
    public void setEtalonId(String entityKey) {
        this.etalonId = entityKey;
    }

    /**
     * @return the entityName
     */
    public String getEntityName() {
        return entityName;
    }

    /**
     * @param entityName the entityName to set
     */
    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    /**
     * @return the version
     */
    public int getVersion() {
        return version;
    }

    /**
     * @param entityVersion the version to set
     */
    public void setVersion(int entityVersion) {
        this.version = entityVersion;
    }

    /**
     * @return the duplicateIds
     */
    public List<String> getDuplicateIds() {
        return duplicateIds;
    }

    /**
     * @param duplicateIds the duplicateIds to set
     */
    public void setDuplicateIds(List<String> duplicateIds) {
        this.duplicateIds = duplicateIds;
    }
    /**
     * @return the createDate
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate the createDate to set
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public boolean isModified() {
        return modified;
    }


    public void setModified(boolean modified) {
        this.modified = modified;
    }


    /**
	 * @return the entityType
	 */
	public String getEntityType() {
		return entityType;
	}

	/**
	 * @param entityType the entityType to set
	 */
	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}

	/**
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * @return the updateDate
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate the updateDate to set
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * @return the updatedBy
     */
    public String getUpdatedBy() {
        return updatedBy;
    }

    /**
     * @param updatedBy the updatedBy to set
     */
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    /**
     * @return the rights
     */
    public ResourceSpecificRightRO getRights() {
        return rights;
    }

    /**
     * @param rights the rights to set
     */
    public void setRights(ResourceSpecificRightRO rights) {
        this.rights = rights;
    }

    /**
     * @return the codeAttributes
     */
    public List<CodeAttributeRO> getCodeAttributes() {
        return codeAttributes;
    }

    /**
     * @param codeAttributes the codeAttributes to set
     */
    public void setCodeAttributes(List<CodeAttributeRO> codeAttributes) {
        this.codeAttributes = codeAttributes;
    }

    /**
     * @return gsn.
     */
    public String getGsn() {
        return gsn;
    }
    /**
     * @param gsn the gsn to set
     */
    public void setGsn(String gsn) {
        this.gsn = gsn;
    }

    /**
     * @return the diffTable
     */
    public List<DiffToPreviousAttributeRO> getDiffToDraft() {
        return diffToDraft;
    }

    /**
     * @param diffTable the diffTable to set
     */
    public void setDiffToDraft(List<DiffToPreviousAttributeRO> diffTable) {
        this.diffToDraft = diffTable;
    }

    /**
     * Status {DIRECT|CASCADE|COPY}.
     */
    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }
}
