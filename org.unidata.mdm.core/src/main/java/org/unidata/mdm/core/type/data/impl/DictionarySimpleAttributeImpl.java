package org.unidata.mdm.core.type.data.impl;

public class DictionarySimpleAttributeImpl extends StringSimpleAttributeImpl {


    public DictionarySimpleAttributeImpl(String name) {
        super(name);
    }

    public DictionarySimpleAttributeImpl(String name, String value) {
        super(name, value);
    }

    @Override
    public DataType getDataType() {
        return DataType.DICTIONARY;
    }

}

