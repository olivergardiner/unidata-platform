package org.unidata.mdm.core.migration.pipelines;

import nl.myndocs.database.migrator.MigrationScript;
import nl.myndocs.database.migrator.definition.Migration;
import org.unidata.mdm.system.util.ResourceUtils;

/**
 * Init system pipelines
 *
 * @author Alexandr Serov
 * @since 17.07.2020
 **/
public class PipelineInitialization implements MigrationScript {
    @Override
    public void migrate(Migration migration) {
        migration.raw()
            .sql(ResourceUtils.asStrings("classpath:/migration/pipelines-initialization.sql"))
            .save();
    }

    @Override
    public String migrationId() {
        return "PipelinesInitialization";
    }

    @Override
    public String author() {
        return "alexander.serov";
    }

}
