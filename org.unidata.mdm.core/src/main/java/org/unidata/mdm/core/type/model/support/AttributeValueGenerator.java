package org.unidata.mdm.core.type.model.support;

import org.unidata.mdm.core.context.DataRecordContext;
import org.unidata.mdm.core.type.model.AttributeModelElement;

/**
 * Generates a value for an attribute.
 * Extend this interface to run custom attribute value generation.
 * @author Mikhail Mikhailov on May 15, 2020
 */
public interface AttributeValueGenerator {
    /**
     * Generates a value, using (or not using) the input.
     * @param input the input
     * @return generated value
     */
    Object generate(AttributeModelElement attribute, DataRecordContext input);
    /**
     * Gets the output type class.
     * @return output type class
     */
    Class<?> getOutputType();
}
