/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.core.type.measurement;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Measured values are such values, that can be given in conjunction with particular measurement units.
 * For instance, for an attribute, configured as mass units in the model,
 * the values can be sent in grams, kylograms, pounds, tons or whatever.
 * There is a base measurement unit for each measurement category.
 * For example, it can be grams for mass units.
 * The values, that were given by the user, are considered to be initial values.
 * If the input was given not in base units, the value in base units is also calculated, possibly, using user-defined conversion functions.
 * The value in base units is used as the reference value for DQ processing, search, indexing etc. afterwards.
 */
public class MeasurementValue implements Serializable{
    /**
     * SVUID.
     */
    private static final long serialVersionUID = 4851410968298290660L;
    /**
     * User defined unique id of measurement value
     */
    private String id;
    //in future store this names in separated table.
    /**
     * value display name
     */
    private String name;
    /**
     * short display name
     */
    private String shortName;
    /**
     * base unit id
     */
    private String baseUnitId;
    /**
     * Map of all included measurement units
     */
    private Map<String, MeasurementUnit> measurementUnits = new HashMap<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getBaseUnitId() {
        return baseUnitId;
    }

    public void setBaseUnitId(String baseUnitId) {
        this.baseUnitId = baseUnitId;
    }

    @Nullable
    public MeasurementUnit getUnitById(@Nonnull String unitId) {
        return measurementUnits.get(unitId);
    }

    @Nonnull
    public MeasurementUnit getBaseUnit() {
        return measurementUnits.get(baseUnitId);
    }

    @Nonnull
    public Collection<MeasurementUnit> getMeasurementUnits() {
        return measurementUnits.values();
    }

    public void setMeasurementUnits(Map<String, MeasurementUnit> measurementUnits) {
        this.measurementUnits = measurementUnits;
    }

    /**
     * @param unitId - unit id
     * @return true if unit present in value, otherwise false
     */
    public boolean present(@Nonnull String unitId) {
        return measurementUnits.keySet().contains(unitId);
    }

    @Override
    public String toString() {
        return "MeasurementValue{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", shortName='" + shortName + '\'' +
                ", baseUnitId=" + baseUnitId +
                ", measurementUnits=" + measurementUnits +
                '}';
    }
}
