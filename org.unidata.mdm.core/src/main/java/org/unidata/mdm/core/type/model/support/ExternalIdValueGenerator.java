package org.unidata.mdm.core.type.model.support;

import org.unidata.mdm.core.context.DataRecordContext;
import org.unidata.mdm.core.type.model.EntityModelElement;
/**
 * Generates external id value.
 * Extends this interface to run custom entity external ID value generation.
 * @author Mikhail Mikhailov on May 15, 2020
 */
public interface ExternalIdValueGenerator {
    /**
     * Generates a value, using (or not using) the input.
     * @param input the input
     * @return generated value
     */
    String generate(EntityModelElement entity, DataRecordContext input);
}
