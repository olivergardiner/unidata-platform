/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.core.type.model;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

/**
 * @author Mikhail Mikhailov
 * Marks an abstract entity holder.
 */
public interface EntityModelElement extends ContainerModelElement, AttributedModelElement {
    /**
     * Tells, whether this element is a top level entity.
     * @return true, for entities, false otherwise
     */
    default boolean isEntity() {
        return false;
    }
    /**
     * Tells, whether this element is a top level lookup.
     * @return true, for lookups, false otherwise
     */
    default boolean isLookup() {
        return false;
    }
    /**
     * Tells, whether this element is a top level relation.
     * @return true, for relations, false otherwise
     */
    default boolean isRelation() {
        return false;
    }
    /**
     * Tells, whether this element is BVT capable.
     * @return true, for BVT capable, false otherwise
     */
    default boolean isBvtCapable() {
        return false;
    }
    /**
     * Tells, whether this elements is indexed one.
     * @return true, if indexed, false otherwise
     */
    default boolean isIndexed() {
        return false;
    }
    /**
     * Returns true, if the code attribute is a generating one (i. e. its values come from a supplied generating strategy).
     * @return true for attributes with value generation support, false otherwise
     */
    default boolean isGenerating() {
        return false;
    }
    /**
     * Returns true, if this entity has generating attributes.
     * @return true, if has, false otherwise
     */
    default boolean hasGeneratingAttributes() {
        return false;
    }
    /**
     * Gets filtered value generating attributes.
     * @return list of value generating attributes
     */
    default List<AttributeModelElement> getGeneratingAttributes() {
        return Collections.emptyList();
    }
    /**
     * Returns the BVT element.
     * @return BVT element
     */
    default BvtMapModelElement getBvt() {
        return null;
    }
    /**
     * Gets the indexed element or null, if not supported.
     * @return element or null
     */
    default IndexedModelElement getIndexed() {
        return null;
    }
    /**
     * Gets code attributed element, if this is one (i. e. {@link #isLookup()} returns true) or null
     * @return code attributed or null
     */
    default CodeAttributedModelElement getCodeAttributed() {
        return null;
    }
    /**
     * Gets generating element, if this entity is an EXTERNAL ID generating one.
     * @return generating element
     */
    default GeneratingModelElement getGenerating() {
        return null;
    }
    /**
     * Gets custom properties defined on the entity, if any.
     * @return map
     */
    Map<String, String> getCustomProperties();
    /**
     * Gets the validity period start.
     * @return the validityStart
     */
    @Nullable
    Date getValidityStart();
    /**
     * Gets the validity period end.
     * @return the validityEnd
     */
    @Nullable
    Date getValidityEnd();
}
