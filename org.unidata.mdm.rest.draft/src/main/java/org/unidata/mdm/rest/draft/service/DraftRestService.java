/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.draft.service;

import java.util.Collections;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.collections4.MapUtils;
import org.unidata.mdm.draft.context.DraftPublishContext;
import org.unidata.mdm.draft.context.DraftQueryContext;
import org.unidata.mdm.draft.context.DraftRemoveContext;
import org.unidata.mdm.draft.context.DraftUpsertContext;
import org.unidata.mdm.draft.dto.DraftPublishResult;
import org.unidata.mdm.draft.dto.DraftQueryResult;
import org.unidata.mdm.draft.dto.DraftUpsertResult;
import org.unidata.mdm.draft.service.DraftService;
import org.unidata.mdm.rest.draft.converter.DraftConverter;
import org.unidata.mdm.rest.draft.ro.DraftPublishRO;
import org.unidata.mdm.rest.draft.ro.DraftRemoveRO;
import org.unidata.mdm.rest.draft.ro.DraftRequestRO;
import org.unidata.mdm.rest.draft.ro.DraftUpsertRO;
import org.unidata.mdm.rest.system.ro.ErrorResponse;
import org.unidata.mdm.rest.system.ro.RestResponse;
import org.unidata.mdm.rest.system.service.AbstractRestService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

/**
 * @author Alexander Malyshev
 * @author Alexey Tsarapkin
 */
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
@Path("/")
public class DraftRestService extends AbstractRestService {

    private final DraftService draftService;

    public DraftRestService(final DraftService draftService) {
        this.draftService = draftService;
    }

    @POST
    @Path("count")
    @Operation(
            description = "Get drafts count for request",
            method = "GET",
            requestBody = @RequestBody(content = @Content(schema = @Schema(implementation = DraftRequestRO.class)), description = "DraftResult request"),
            responses = {
                    @ApiResponse(content = @Content(schema = @Schema(implementation = RestResponse.class)), responseCode = "200"),
                    @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
            }
    )
    public Response draftsCount(DraftRequestRO request) {

        DraftQueryContext dqc = DraftQueryContext.builder()
            .provider(request.getType())
            .subjectId(request.getEntityId())
            .draftId(Objects.nonNull(request.getDraftId()) ? Long.valueOf(request.getDraftId()) : null)
            .owner(request.getDraftOwner())
            .build();

        return ok(new RestResponse<>(draftService.count(dqc)));
    }

    /**
     * Find drafts for request
     *
     * @return List of drafts
     */
    @POST
    @Path("drafts")
    @Operation(
            description = "Find drafts for request",
            method = "POST",
            requestBody = @RequestBody(content = @Content(schema = @Schema(implementation = DraftRequestRO.class)), description = "DraftResult request"),
            responses = {
                    @ApiResponse(content = @Content(schema = @Schema(implementation = RestResponse.class)), responseCode = "200"),
                    @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
            }
    )
    public Response drafts(
            @Parameter(description = "Select from", in = ParameterIn.QUERY) @QueryParam("start") @DefaultValue("0")
                    Integer start,
            @Parameter(description = "Select limit", in = ParameterIn.QUERY) @QueryParam("limit") @DefaultValue("10")
                    Integer limit,
            DraftRequestRO draftRequest) {

        DraftQueryContext dqc = DraftQueryContext.builder()
                .provider(draftRequest.getType())
                .subjectId(draftRequest.getEntityId())
                .draftId(Objects.nonNull(draftRequest.getDraftId()) ? Long.valueOf(draftRequest.getDraftId()) : null)
                .owner(draftRequest.getDraftOwner())
                .start(start)
                .limit(limit)
                .build();

        DraftQueryResult dqr = draftService.drafts(dqc);
        return ok(new RestResponse<>(dqr.getDrafts().stream()
                .map(DraftConverter::from)
                .collect(Collectors.toList())));
    }

    /**
     * Find drafts for request
     *
     * @return List of drafts
     */
    @POST
    @Path("upsert")
    @Operation(
            description = "Upsert draft, create or update display name if draft exists",
            method = "POST",
            requestBody = @RequestBody(content = @Content(schema = @Schema(implementation = DraftUpsertRO.class)), description = "DraftResult request"),
            responses = {
                    @ApiResponse(content = @Content(schema = @Schema(implementation = RestResponse.class)), responseCode = "200"),
                    @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
            }
    )
    public Response upsert(DraftUpsertRO draftUpsert) {

        DraftUpsertContext ctx = DraftUpsertContext.builder()
            .provider(draftUpsert.getType())
            .subjectId(draftUpsert.getSubjectId())
            .draftId(Objects.nonNull(draftUpsert.getDraftId()) ? Long.valueOf(draftUpsert.getDraftId()) : null)
            .owner(draftUpsert.getOwner())
            .description(draftUpsert.getDecsription())
            .parameters(MapUtils.isEmpty(draftUpsert.getParameters())
                    ? Collections.emptyMap()
                    : draftUpsert.getParameters()
                        .entrySet()
                        .stream()
                        .collect(Collectors.toMap(Entry::getKey, v -> (Object) v)))
            .build();

        DraftUpsertResult result = draftService.upsert(ctx);
        return ok(new RestResponse<>(DraftConverter.from(result.getDraft())));
    }

    /**
     * Find drafts for request
     *
     * @return result
     */
    @POST
    @Path("publish")
    @Operation(
            description = "Publish draft",
            method = "POST",
            requestBody = @RequestBody(content = @Content(schema = @Schema(implementation = DraftPublishRO.class)), description = "DraftResult request"),
            responses = {
                    @ApiResponse(content = @Content(schema = @Schema(implementation = RestResponse.class)), responseCode = "200"),
                    @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
            }
    )
    public Response publish(DraftPublishRO draftPublish) {

        DraftPublishContext ctx = DraftPublishContext.builder()
            .draftId(Objects.nonNull(draftPublish.getDraftId()) ? Long.valueOf(draftPublish.getDraftId()) : null)
            .force(draftPublish.isForce())
            .build();

        DraftPublishResult result = draftService.publish(ctx);
        return ok(new RestResponse<>(DraftConverter.from(result.getDraft())));
    }

    /**
     * Remove draft for id
     *
     * @return result
     */
    @POST
    @Path("remove")
    @Operation(
            description = "Remove draft",
            method = "POST",
            requestBody = @RequestBody(content = @Content(schema = @Schema(implementation = DraftRemoveRO.class)), description = "DraftResult request"),
            responses = {
                    @ApiResponse(content = @Content(schema = @Schema(implementation = RestResponse.class)), responseCode = "200"),
                    @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
            }
    )
    public Response remove(DraftRemoveRO draftRemove) {

        DraftRemoveContext ctx = DraftRemoveContext.builder()
            .provider(draftRemove.getType())
            .draftId(Objects.nonNull(draftRemove.getDraftId()) ? Long.valueOf(draftRemove.getDraftId()) : null)
            .subjectId(draftRemove.getEntityId())
            .owner(draftRemove.getDraftOwner())
            .build();

        return ok(new RestResponse<>(draftService.remove(ctx).getCount()));
    }
}
