package org.unidata.mdm.rest.draft.ro;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DraftRequestRO {
    private String type;
    private String entityId;
    private String etalonId;
    private String draftId;
    private String draftOwner;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getDraftId() {
        return draftId;
    }

    public void setDraftId(String draftId) {
        this.draftId = draftId;
    }

    public String getDraftOwner() {
        return draftOwner;
    }

    public void setDraftOwner(String draftOwner) {
        this.draftOwner = draftOwner;
    }

    public String getEtalonId() {
        return etalonId;
    }

    public void setEtalonId(String etalonId) {
        this.etalonId = etalonId;
    }
}
