package org.unidata.mdm.rest.draft.ro;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DraftUpsertRO {
    private String type;
    private String draftId;
    private String subjectId;
    private String owner;
    private String decsription;
    private Map<String, String> parameters;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String entityId) {
        this.subjectId = entityId;
    }

    public String getDraftId() {
        return draftId;
    }

    public void setDraftId(String draftId) {
        this.draftId = draftId;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String draftOwner) {
        this.owner = draftOwner;
    }

    public String getDecsription() {
        return decsription;
    }

    public void setDecsription(String displayName) {
        this.decsription = displayName;
    }

    /**
     * @return the parameters
     */
    public Map<String, String> getParameters() {
        return parameters;
    }

    /**
     * @param parameters the parameters to set
     */
    public void setParameters(Map<String, String> parameters) {
        this.parameters = parameters;
    }
}
