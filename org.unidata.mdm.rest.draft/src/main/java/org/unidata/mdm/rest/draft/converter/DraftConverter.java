package org.unidata.mdm.rest.draft.converter;

import java.util.Objects;

import org.unidata.mdm.core.configuration.CoreConfiguration;
import org.unidata.mdm.core.dto.UserDTO;
import org.unidata.mdm.core.service.UserService;
import org.unidata.mdm.draft.type.Draft;
import org.unidata.mdm.rest.draft.ro.DraftRO;

/**
 * @author Mikhail Mikhailov on Sep 11, 2020
 */
public class DraftConverter {

    private static UserService userService;

    /**
     * Constructor.
     */
    private DraftConverter() {
        super();
    }

    public static void init() {
        userService = CoreConfiguration.getBean(UserService.class);
    }

    public static DraftRO from(Draft d) {

        if (Objects.isNull(d)) {
            return null;
        }

        DraftRO result = new DraftRO();
        result.setEntityId(d.getSubjectId() == null ? null : d.getSubjectId());
        result.setDraftId(d.getDraftId() == null ? null : d.getDraftId().toString());

        if (Objects.nonNull(d.getOwner())) {

            result.setOwner(d.getOwner());

            UserDTO udto = userService.getUserByName(d.getOwner());
            if (Objects.nonNull(udto)) {

                result.setOwnerFullName(udto.getFullName());
            }
        }

        result.setUpdateDate(d.getUpdateDate());
        result.setCreateDate(d.getCreateDate());
        result.setCreatedBy(d.getCreatedBy());
        result.setUpdatedBy(d.getUpdatedBy());
        result.setDisplayName(d.getDescription());

        return result;
    }
}
