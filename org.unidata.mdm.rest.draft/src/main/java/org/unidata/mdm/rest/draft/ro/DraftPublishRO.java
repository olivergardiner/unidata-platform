package org.unidata.mdm.rest.draft.ro;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DraftPublishRO {
    private String type;
    private String entityId;
    private String draftId;
    private String draftOwner;
    private Long start;
    private Long limit;
    private boolean force;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getDraftId() {
        return draftId;
    }

    public void setDraftId(String draftId) {
        this.draftId = draftId;
    }

    public String getDraftOwner() {
        return draftOwner;
    }

    public void setDraftOwner(String draftOwner) {
        this.draftOwner = draftOwner;
    }

    public boolean isForce() {
        return force;
    }

    public void setForce(boolean forse) {
        this.force = forse;
    }

    public Long getStart() {
        return start;
    }

    public void setStart(Long start) {
        this.start = start;
    }

    public Long getLimit() {
        return limit;
    }

    public void setLimit(Long limit) {
        this.limit = limit;
    }
}
