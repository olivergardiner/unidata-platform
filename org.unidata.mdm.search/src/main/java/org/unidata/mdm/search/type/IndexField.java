/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.search.type;

/**
 * Index field.
 */
public interface IndexField {
    /**
     * @return filed name in search index
     */
    String getName();
    /**
     * Gets the full path of the field.
     * Same as name for the first level fields.
     * Parents name, separated by dot for nested fields i. e. 'parameters.severity.severity_key'.
     * @return
     */
    default String getPath() {
        return getName();
    }
    /**
     * Must be true for analyzed string fields.
     * @return true for analyzed string fields, false otherwise
     */
    default boolean isAnalyzed() {
        return false;
    }
    /**
     * Gets the field's data type.
     * @return
     */
    FieldType getFieldType();
}
