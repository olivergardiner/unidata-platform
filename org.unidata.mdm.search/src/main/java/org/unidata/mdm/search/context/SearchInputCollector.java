package org.unidata.mdm.search.context;

import org.unidata.mdm.system.context.InputCollector;

/**
 * @author Mikhail Mikhailov on Mar 6, 2020
 */
public interface SearchInputCollector extends InputCollector {
    /**
     * Type of the container.
     */
    enum SearchInputCollectorType {
        SIMPLE,
        COMPLEX
    }
    /**
     * Gets the precise type of the collector.
     * @return type
     */
    SearchInputCollectorType getCollectorType();
}
