package org.unidata.mdm.search.type.query;

/**
 * @author Mikhail Mikhailov on Feb 20, 2020
 */
public class StringSearchQuery implements SearchQuery {
    /**
     * The QS.
     */
    private final String query;
    /**
     * Constructor.
     * @param query the query
     */
    protected StringSearchQuery(String query) {
        super();
        this.query = query;
    }
    /**
     * @return the query
     */
    public String getQuery() {
        return query;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public SearchQueryType getType() {
        return SearchQueryType.STRING;
    }
}
