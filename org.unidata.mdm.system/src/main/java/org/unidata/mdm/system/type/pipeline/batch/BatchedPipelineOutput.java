package org.unidata.mdm.system.type.pipeline.batch;

import org.unidata.mdm.system.type.pipeline.PipelineOutput;

/**
 * @author Mikhail Mikhailov on Jan 15, 2020
 * Just a marker to divide inheritance trees.
 */
public interface BatchedPipelineOutput extends PipelineOutput {}
