/**
 * @author Mikhail Mikhailov on Jan 15, 2020
 * Types, related to I/O rendering process.
 */
package org.unidata.mdm.system.type.rendering;