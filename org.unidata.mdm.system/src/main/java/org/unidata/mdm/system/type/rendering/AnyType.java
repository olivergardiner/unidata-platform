package org.unidata.mdm.system.type.rendering;

import java.util.Arrays;
import java.util.Objects;

/**
 * Any value type
 *
 * @author Alexandr Serov
 * @since 17.09.2020
 **/
public class AnyType {

    private static final AnyType[] EMPTY_PARAMETERS = new AnyType[0];

    private final Class<?> javaClass;
    private final AnyType[] parameters;
    private final boolean array;
    private final int paramCount;

    private AnyType(Class<?> javaClass, AnyType[] parameters, boolean array) {
        this.javaClass = javaClass;
        this.parameters = parameters;
        this.array = array;
        paramCount = parameters.length;
    }

    public static AnyType anyType(Class<?> javaClass) {
        return new AnyType(javaClass, EMPTY_PARAMETERS, false);
    }

    public static AnyType arrayAnyType(Class<?> javaClass) {
        return new AnyType(javaClass, EMPTY_PARAMETERS, true);
    }

    public AnyType param(int index) {
        if (index >= 0 || index < paramCount) {
            return param(index);
        } else {
            throw new IllegalArgumentException("Illegal parameter index: " + index);
        }
    }

    @Override
    public String toString() {
        String result;
        String className = javaClass.getSimpleName();
        if (paramCount > 0) {
            StringBuilder builder = new StringBuilder(className);
            builder.append('<').append(parameters[0]);
            for (int i = 1; i < paramCount; i++) {
                builder.append(',').append(parameters[i]);
            }
            builder.append('>');
            if (array) {
                builder.append("[]");
            }
            result = builder.toString();
        } else {
            result = array ? className : className + "[]";
        }
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AnyType anyType = (AnyType) o;
        return array == anyType.array &&
            javaClass.equals(anyType.javaClass) &&
            Arrays.equals(parameters, anyType.parameters);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(javaClass, array);
        result = 31 * result + Arrays.hashCode(parameters);
        return result;
    }

    /**
     * @return type param count
     */
    public int paramCount() {
        return paramCount;
    }

    /**
     * @return base type
     */
    public Class<?> getJavaClass() {
        return javaClass;
    }

    /**
     * @return true if type is array
     */
    public boolean isArray() {
        return array;
    }
}
