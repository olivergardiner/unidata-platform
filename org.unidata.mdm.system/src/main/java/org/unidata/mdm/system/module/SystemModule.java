/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.system.module;

import javax.annotation.Nullable;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.unidata.mdm.system.configuration.SystemConfigurationConstants;
import org.unidata.mdm.system.configuration.SystemConfigurationProperty;
import org.unidata.mdm.system.context.DatabaseMigrationContext;
import org.unidata.mdm.system.migration.SystemMigrations;
import org.unidata.mdm.system.service.DatabaseMigrationService;
import org.unidata.mdm.system.service.PipelineService;
import org.unidata.mdm.system.service.PlatformConfiguration;
import org.unidata.mdm.system.service.RenderingService;
import org.unidata.mdm.system.type.configuration.ConfigurationProperty;
import org.unidata.mdm.system.type.module.Module;
import org.unidata.mdm.system.util.DataSourceUtils;
import org.unidata.mdm.system.util.IdUtils;
import org.unidata.mdm.system.util.PipelineUtils;
import org.unidata.mdm.system.util.ResourceUtils;
import org.unidata.mdm.system.util.ServiceUtils;
import org.unidata.mdm.system.util.TextUtils;

/**
 * @author Alexander Malyshev
 */
public class SystemModule implements Module {

    private static final Logger LOGGER = LoggerFactory.getLogger(SystemModule.class);

    @Autowired
    private DataSource systemDataSource;

    @Autowired
    private PipelineService pipelineService;

    @Autowired
    private RenderingService renderingService;

    @Autowired
    private DatabaseMigrationService migrationService;

    @Autowired
    private PlatformConfiguration platformConfiguration;

    @Autowired
    private DisposableBean transactionManager;

    private boolean install;

    /**
     * This module id.
     */
    public static final String MODULE_ID = "org.unidata.mdm.system";

    @Override
    public String getId() {
        return MODULE_ID;
    }

    @Override
    public String getVersion() {
        return "6.0";
    }

    @Override
    public String getName() {
        return "Unidata system";
    }

    @Override
    public String getDescription() {
        return "Unidata system (root) module";
    }

    @Nullable
    @Override
    public String getTag() {
        return null;
    }

    @Override
    public void install() {

        LOGGER.info("Install");

        IdUtils.init();
        ResourceUtils.init();
        ServiceUtils.init();
        TextUtils.init();

        migrate();
        install = true;
    }

    @Override
    public void uninstall() {
        // NO-OP
    }

    @Override
    public void start() {

        LOGGER.info("Start");

        IdUtils.init();
        ResourceUtils.init();
        ServiceUtils.init();
        TextUtils.init();
        PipelineUtils.init();

        if (platformConfiguration.isDeveloperMode() && !install) {
            migrate();
        }
    }

    @Override
    public void ready() {
        LOGGER.info("Ready");
        pipelineService.loadPipelines();
        renderingService.loadRendrerers();
    }

    @Override
    public void stop() {
        DataSourceUtils.shutdown(systemDataSource);
        try {
            transactionManager.destroy();
        } catch (Exception e) {
            LOGGER.warn("Failed to destroy transaction manager.", e);
        }
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public ConfigurationProperty<?>[] getConfigurationProperties() {
        return SystemConfigurationProperty.values();
    }

    private void migrate() {
        migrationService.migrate(DatabaseMigrationContext.builder()
                .schemaName(SystemConfigurationConstants.SYSTEM_SCHEMA_NAME)
                .logName(SystemConfigurationConstants.SYSTEM_MIGRATION_LOG_NAME)
                .dataSource(systemDataSource)
                .migrations(SystemMigrations.migrations())
                .build());
    }

    @Override
    public String[] getResourceBundleBasenames() {
        return new String[]{"system_messages"};
    }
}
