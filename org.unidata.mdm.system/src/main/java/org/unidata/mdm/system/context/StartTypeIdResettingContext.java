package org.unidata.mdm.system.context;

/**
 * @author Mikhail Mikhailov on Sep 25, 2020
 * Marks a context as capable to reset start type id.
 */
public interface StartTypeIdResettingContext {
    /**
     * Overrides or sets the start type id.
     * @param startTypeId the id
     */
    void setStartTypeId(String startTypeId);
}
