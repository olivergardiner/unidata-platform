package org.unidata.mdm.system.type.pipeline;

/**
 * The split outcome direction.
 * @author Mikhail Mikhailov on May 24, 2020
 */
public interface Outcome {
    /**
     * Gets the direction name.
     * @return name
     */
    String getName();
    /**
     * Gets direction's description.
     * @return description
     */
    String getDescription();
}
