/**
 * Types, related to messaging via camel based bus service.
 * @author Mikhail Mikhailov on Jun 29, 2020
 */
package org.unidata.mdm.system.type.messaging;