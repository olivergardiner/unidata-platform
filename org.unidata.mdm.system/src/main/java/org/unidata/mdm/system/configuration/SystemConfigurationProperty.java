package org.unidata.mdm.system.configuration;

import java.util.Locale;

import org.apache.commons.lang3.LocaleUtils;
import org.unidata.mdm.system.module.SystemModule;
import org.unidata.mdm.system.type.configuration.ConfigurationProperty;
import org.unidata.mdm.system.type.format.DumpTargetFormat;
import org.unidata.mdm.system.type.runtime.MeasurementPoint;

/**
 * @author Mikhail Mikhailov on Apr 18, 2020
 */
public final class SystemConfigurationProperty {

    private static final Locale RU = new Locale("ru");
    /**
     * No-instance constructor.
     */
    private SystemConfigurationProperty() {
        super();
    }
    /**
     * Enable/disable runtime profiler.
     * Old:
     * name: unidata.simon.enabled
     * group: unidata.properties.group.simon
     */
    public static final ConfigurationProperty<Boolean> SYSTEM_SIMON_ENABLED = ConfigurationProperty.bool()
        .key(SystemConfigurationConstants.PROPERTY_SIMON_ENABLED)
        .groupKey(SystemConfigurationConstants.PROPERTY_SYSTEM_GROUP)
        .moduleId(SystemModule.MODULE_ID)
        .setter(MeasurementPoint::setEnabled)
        .defaultValue(Boolean.FALSE)
        .readOnly(false)
        .required(false)
        .build();
    /**
     * Platform version (really needed?).
     * Old:
     * name: unidata.platform.version
     * group: unidata.properties.group.version
     */
    public static final ConfigurationProperty<String> SYSTEM_PLATFORM_VERSION = ConfigurationProperty.string()
        .key(SystemConfigurationConstants.PROPERTY_PLATFORM_VERSION)
        .groupKey(SystemConfigurationConstants.PROPERTY_SYSTEM_GROUP)
        .moduleId(SystemModule.MODULE_ID)
        .readOnly(true)
        .required(true)
        .build();
    /**
     * Node id.
     * Old:
     * name: unidata.node.id
     * group: unidata.properties.group.node.id
     */
    public static final ConfigurationProperty<String> SYSTEM_NODE_ID = ConfigurationProperty.string()
        .key(SystemConfigurationConstants.PROPERTY_NODE_ID)
        .groupKey(SystemConfigurationConstants.PROPERTY_SYSTEM_GROUP)
        .moduleId(SystemModule.MODULE_ID)
        .readOnly(true)
        .required(true)
        .build();
    /**
     * Data dump target format.
     * Old:
     * name: unidata.dump.target.format
     * group: not grouped
     */
    public static final ConfigurationProperty<DumpTargetFormat> SYSTEM_DUMP_TARGET_FORMAT = ConfigurationProperty.custom(DumpTargetFormat.class)
        .key(SystemConfigurationConstants.PROPERTY_DUMP_TARGET_FORMAT)
        .groupKey(SystemConfigurationConstants.PROPERTY_SYSTEM_GROUP)
        .moduleId(SystemModule.MODULE_ID)
        .deserializer(DumpTargetFormat::fromValue)
        .serializer(DumpTargetFormat::name)
        .defaultValue(DumpTargetFormat.PROTOSTUFF)
        .readOnly(true)
        .required(true)
        .build();
    /**
     * Default system locale.
     * Old:
     * name: unidata.default.locale
     * group: not grouped
     */
    public static final ConfigurationProperty<Locale> SYSTEM_DEFAULT_LOCALE = ConfigurationProperty.custom(Locale.class)
        .key(SystemConfigurationConstants.PROPERTY_DEFAULT_LOCALE)
        .groupKey(SystemConfigurationConstants.PROPERTY_SYSTEM_GROUP)
        .moduleId(SystemModule.MODULE_ID)
        .deserializer(LocaleUtils::toLocale)
        .defaultValue(RU)
        .readOnly(true)
        .required(true)
        .build();
    /**
     * Developer mode switch.
     * Old:
     * name: new
     * group: not grouped
     */
    public static final ConfigurationProperty<Boolean> SYSTEM_DEVELOPER_MODE = ConfigurationProperty.bool()
        .key(SystemConfigurationConstants.PROPERTY_DEVELOPER_MODE)
        .groupKey(SystemConfigurationConstants.PROPERTY_SYSTEM_GROUP)
        .defaultValue(Boolean.FALSE)
        .moduleId(SystemModule.MODULE_ID)
        .readOnly(true)
        .required(false)
        .build();
    /**
     * Developer mode switch.
     * Old:
     * name: new
     * group: not grouped
     */
    public static final ConfigurationProperty<Long> SYSTEM_EVENT_REPLAY_TIMEOUT = ConfigurationProperty.integer()
        .key(SystemConfigurationConstants.PROPERTY_EVENT_REPLAY_TIMEOUT)
        .groupKey(SystemConfigurationConstants.PROPERTY_SYSTEM_GROUP)
        .defaultValue(Long.valueOf(3000L))
        .moduleId(SystemModule.MODULE_ID)
        .readOnly(false)
        .required(true)
        .build();
    /**
     * Unidata cache password.
     * Not part of runtim econfiguration properties.
     * Old unidata.cache.password
     */
    public static final ConfigurationProperty<String> SYSTEM_CACHE_PASSWORD = ConfigurationProperty.string()
        .key(SystemConfigurationConstants.PROPERTY_SYSTEM_CACHE_PASSWORD)
        .groupKey(SystemConfigurationConstants.PROPERTY_SYSTEM_CACHE_GROUP)
        .moduleId(SystemModule.MODULE_ID)
        .readOnly(true)
        .required(false)
        .build();
    /**
     * Unidata cache port.
     * Not part of runtime econfiguration properties.
     * Old unidata.cache.port
     */
    public static final ConfigurationProperty<Long> SYSTEM_CACHE_PORT = ConfigurationProperty.integer()
        .key(SystemConfigurationConstants.PROPERTY_SYSTEM_CACHE_PORT)
        .groupKey(SystemConfigurationConstants.PROPERTY_SYSTEM_CACHE_GROUP)
        .defaultValue(Long.valueOf(5701L))
        .moduleId(SystemModule.MODULE_ID)
        .readOnly(true)
        .required(false)
        .build();
    /**
     * Unidata cache port autoincrement.
     * Not part of runtime econfiguration properties.
     * Old unidata.cache.port.autoincreament
     */
    public static final ConfigurationProperty<Boolean> SYSTEM_CACHE_PORT_AUTOINCREMENT = ConfigurationProperty.bool()
        .key(SystemConfigurationConstants.PROPERTY_SYSTEM_CACHE_PORT_AUTOINCREMENT)
        .groupKey(SystemConfigurationConstants.PROPERTY_SYSTEM_CACHE_GROUP)
        .defaultValue(Boolean.FALSE)
        .moduleId(SystemModule.MODULE_ID)
        .readOnly(true)
        .required(false)
        .build();
    /**
     * Unidata cache port autoincrement.
     * Not part of runtime econfiguration properties.
     * Old unidata.cache.port.autoincreament
     */
    public static final ConfigurationProperty<Boolean> SYSTEM_CACHE_MULTICAST_ENABLED = ConfigurationProperty.bool()
        .key(SystemConfigurationConstants.PROPERTY_SYSTEM_CACHE_MULTICAST_ENABLED)
        .groupKey(SystemConfigurationConstants.PROPERTY_SYSTEM_CACHE_GROUP)
        .defaultValue(Boolean.FALSE)
        .moduleId(SystemModule.MODULE_ID)
        .readOnly(true)
        .required(false)
        .build();
    /**
     * Unidata cache multicast group.
     * Not part of runtime econfiguration properties.
     * Old unidata.cache.multicast.group
     */
    public static final ConfigurationProperty<String> SYSTEM_CACHE_MULTICAST_GROUP = ConfigurationProperty.string()
        .key(SystemConfigurationConstants.PROPERTY_SYSTEM_CACHE_MULTICAST_GROUP)
        .groupKey(SystemConfigurationConstants.PROPERTY_SYSTEM_CACHE_GROUP)
        .moduleId(SystemModule.MODULE_ID)
        .readOnly(true)
        .required(false)
        .build();
    /**
     * Unidata cache multicast port.
     * Not part of runtime econfiguration properties.
     * Old unidata.cache.multicast.port
     */
    public static final ConfigurationProperty<Long> SYSTEM_CACHE_MULTICAST_PORT = ConfigurationProperty.integer()
        .key(SystemConfigurationConstants.PROPERTY_SYSTEM_CACHE_MULTICAST_PORT)
        .groupKey(SystemConfigurationConstants.PROPERTY_SYSTEM_CACHE_GROUP)
        .moduleId(SystemModule.MODULE_ID)
        .readOnly(true)
        .required(false)
        .build();
    /**
     * Unidata cache multicast TTL.
     * Not part of runtime econfiguration properties.
     * Old unidata.cache.multicast.ttl
     */
    public static final ConfigurationProperty<Long> SYSTEM_CACHE_MULTICAST_TTL = ConfigurationProperty.integer()
        .key(SystemConfigurationConstants.PROPERTY_SYSTEM_CACHE_MULTICAST_TTL)
        .groupKey(SystemConfigurationConstants.PROPERTY_SYSTEM_CACHE_GROUP)
        .moduleId(SystemModule.MODULE_ID)
        .readOnly(true)
        .required(false)
        .build();
    /**
     * Unidata cache multicast timeout.
     * Not part of runtime econfiguration properties.
     * Old unidata.cache.multicast.timeout
     */
    public static final ConfigurationProperty<Long> SYSTEM_CACHE_MULTICAST_TIMEOUT = ConfigurationProperty.integer()
        .key(SystemConfigurationConstants.PROPERTY_SYSTEM_CACHE_MULTICAST_TIMEOUT)
        .groupKey(SystemConfigurationConstants.PROPERTY_SYSTEM_CACHE_GROUP)
        .moduleId(SystemModule.MODULE_ID)
        .readOnly(true)
        .required(false)
        .build();
    /**
     * Unidata cache TCP/IP enabled.
     * Not part of runtime econfiguration properties.
     * Old unidata.cache.tcp-ip.enabled
     */
    public static final ConfigurationProperty<Boolean> SYSTEM_CACHE_TCP_IP_ENABLED = ConfigurationProperty.bool()
        .key(SystemConfigurationConstants.PROPERTY_SYSTEM_CACHE_TCP_IP_ENABLED)
        .groupKey(SystemConfigurationConstants.PROPERTY_SYSTEM_CACHE_GROUP)
        .moduleId(SystemModule.MODULE_ID)
        .defaultValue(Boolean.FALSE)
        .readOnly(true)
        .required(false)
        .build();
    /**
     * Unidata cache TCP/IP members.
     * Not part of runtime econfiguration properties.
     * Old unidata.cache.tcp-ip.members
     */
    public static final ConfigurationProperty<String> SYSTEM_CACHE_TCP_IP_MEMBERS = ConfigurationProperty.string()
        .key(SystemConfigurationConstants.PROPERTY_SYSTEM_CACHE_TCP_IP_MEMBERS)
        .groupKey(SystemConfigurationConstants.PROPERTY_SYSTEM_CACHE_GROUP)
        .moduleId(SystemModule.MODULE_ID)
        .readOnly(true)
        .required(false)
        .build();
    /**
     * Values as array.
     */
    private static final ConfigurationProperty<?>[] VALUES = {
            SYSTEM_SIMON_ENABLED,
            SYSTEM_PLATFORM_VERSION,
            SYSTEM_NODE_ID,
            SYSTEM_DUMP_TARGET_FORMAT,
            SYSTEM_DEFAULT_LOCALE,
            SYSTEM_DEVELOPER_MODE,
            SYSTEM_EVENT_REPLAY_TIMEOUT,
            SYSTEM_CACHE_PASSWORD,
            SYSTEM_CACHE_PORT,
            SYSTEM_CACHE_PORT_AUTOINCREMENT,
            SYSTEM_CACHE_MULTICAST_ENABLED,
            SYSTEM_CACHE_MULTICAST_GROUP,
            SYSTEM_CACHE_MULTICAST_PORT,
            SYSTEM_CACHE_MULTICAST_TTL,
            SYSTEM_CACHE_MULTICAST_TIMEOUT,
            SYSTEM_CACHE_TCP_IP_ENABLED,
            SYSTEM_CACHE_TCP_IP_MEMBERS
    };
    /**
     * Enum like array accessor.
     * @return array of values.
     */
    public static ConfigurationProperty<?>[] values() {
        return VALUES;
    }
}
