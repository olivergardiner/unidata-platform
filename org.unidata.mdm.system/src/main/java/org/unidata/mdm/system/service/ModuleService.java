/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.system.service;

import java.util.Collection;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.ApplicationContext;
import org.unidata.mdm.system.dto.ModuleInfo;
import org.unidata.mdm.system.type.module.Module;

/**
 * @author Alexander Malyshev
 * Basic functionality around business modules.
 */
public interface ModuleService extends BeanPostProcessor, ModularPostProcessingRegistrar {
    /**
     * Sets current context.
     * @param applicationContext the context to set
     */
    void setRootContext(ApplicationContext applicationContext);
    /**
     * Loads modules during platform startup.
     * @param modulesClasses set of module classes to load
     */
    void loadModules(Set<Class<Module>> modulesClasses);
    /**
     * Gets running modules info.
     * @return modules info
     */
    Collection<ModuleInfo> modulesInfo();
    /**
     * Finds module by module id.
     * @param moduleId the id
     * @return module or null, if not found
     */
    Optional<Module> findModuleById(String moduleId);
    /**
     * Gets a collection of registered modules.
     * @return collection
     */
    Collection<Module> getModules();
    /**
     * Creates singleton beans, that can inject beans from any context after 'ready' was called.
     * @param <T> the bean type
     * @param klass the class
     * @return bean
     */
    <T> T createRuntimeModularSingleton(Class<T> klass);
    /**
     * Creates singleton beans, that can inject beans from any context after 'ready' was called.
     * @param <T> the bean type
     * @param beanName alternative bean name
     * @param klass the class
     * @return bean
     */
    <T> T createRuntimeModularSingleton(String beanName, Class<T> klass);
}
