package org.unidata.mdm.system.type.pipeline.connection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.unidata.mdm.system.type.pipeline.Pipeline;
import org.unidata.mdm.system.type.pipeline.Segment;
import org.unidata.mdm.system.util.PipelineUtils;

/**
 * @author Mikhail Mikhailov on May 26, 2020
 */
public class MultiPipelineConnection extends PipelineConnection {
    /**
     * Resolved pipelines.
     */
    protected List<Pipeline> pipelines;
    /**
     * Links to resolve.
     */
    protected List<Pair<String, String>> links;
    /**
     * Constructor.
     * @param pipelineId serialized pipeline id
     */
    protected MultiPipelineConnection() {
        super();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public ConnectionType getConnectionType() {
        return ConnectionType.MULTIPLE;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void connect(Segment segment) {

        if (isConnected()) {
            return;
        }

        if (CollectionUtils.isNotEmpty(links)) {

            List<Pipeline> collected = new ArrayList<>(links.size());
            links.forEach(pair -> {

                Pipeline p = PipelineUtils.findPipeline(pair.getKey(), pair.getValue());
                throwPipelineNotFoundIfNull(p, pair.getKey(), pair.getValue());

                collected.add(p);
            });

            setConnected(CollectionUtils.isNotEmpty(collected));
            if (isConnected()) {
                pipelines = collected;
                links.clear();
                links = null;
            }
        }
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> serialize() {
        return isConnected()
                ? pipelines.stream()
                    .map(PipelineUtils::toSerializablePipelineId)
                    .collect(Collectors.toList())
                : null;
    }
    /**
     * @return the pipeline
     */
    public List<Pipeline> getPipelines() {
        return Objects.isNull(pipelines) ? Collections.emptyList() : pipelines;
    }
}
