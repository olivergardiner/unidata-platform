package org.unidata.mdm.system.type.pipeline.connection;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.unidata.mdm.system.exception.PipelineException;
import org.unidata.mdm.system.exception.SystemExceptionIds;
import org.unidata.mdm.system.type.pipeline.Outcome;
import org.unidata.mdm.system.type.pipeline.Pipeline;
import org.unidata.mdm.system.type.pipeline.Segment;
import org.unidata.mdm.system.util.PipelineUtils;

/**
 * @author Mikhail Mikhailov on May 25, 2020
 */
public abstract class PipelineConnection {
    /**
     * Type of connection.
     */
    public enum ConnectionType {
        SINGLE,
        MULTIPLE,
        OUTCOME
    }
    /**
     * Connection mark.
     */
    protected boolean connected;
    /**
     * Constructor.
     */
    protected PipelineConnection() {
        super();
    }
    /**
     * @return the connected
     */
    public boolean isConnected() {
        return connected;
    }
    /**
     * @param connected the connected to set
     */
    public void setConnected(boolean connected) {
        this.connected = connected;
    }
    /**
     * Gets type of this connection.
     * @return type of connection
     */
    public abstract ConnectionType getConnectionType();
    /**
     * Tries to activate this connection if it is not active and can be activated (requires post-processing).
     * @param segment the connecting segment
     */
    public abstract void connect(Segment segment);
    /**
     * Returns serializable form of self.
     * @return self as ready-to-write to JSON object.
     */
    public abstract Object serialize();
    /**
     * Throws if this PL is already closed.
     */
    protected void throwPipelineNotFoundIfNull(Pipeline p, String start, String subject) {
        if (Objects.isNull(p)) {
            throw new PipelineException("Pipeline not found by id [{}], subject [{}].",
                    SystemExceptionIds.EX_PIPELINE_NOT_FOUND_BY_ID_AND_SUBJECT, start, subject);
        }
    }

    public static PipelineConnection of(String pipelineId) {

        String start = null;
        String subject = null;
        String[] parts = PipelineUtils.fromSerializedPipelineId(pipelineId);
        if (ArrayUtils.isNotEmpty(parts)) {
            if (parts.length == 1) {
                start = parts[0];
            } else if (parts.length == 2) {
                subject = parts[0];
                start = parts[1];
            }
        }

        return of(start, subject);
    }

    public static PipelineConnection of(String startId, String subjectId) {

        SinglePipelineConnection connection = new SinglePipelineConnection();
        connection.subject = startId;
        connection.start = subjectId;

        return connection;
    }

    public static PipelineConnection direct(Pipeline p) {

        SinglePipelineConnection connection = new SinglePipelineConnection();
        connection.pipeline = p;
        connection.setConnected(Objects.nonNull(p));

        return connection;
    }

    /**
     * Constructor.
     * @param pipelineId serialized pipeline id
     */
    public static PipelineConnection of(List<String> pipelineIds) {

        MultiPipelineConnection connection = new MultiPipelineConnection();
        if (CollectionUtils.isNotEmpty(pipelineIds)) {

            List<Pair<String, String>> links = new ArrayList<>(pipelineIds.size());
            for (String pipelineId : pipelineIds) {

                String[] parts = PipelineUtils.fromSerializedPipelineId(pipelineId);
                if (ArrayUtils.isNotEmpty(parts)) {
                    if (parts.length == 1) {
                        links.add(Pair.of(parts[0], null));
                    } else if (parts.length == 2) {
                        links.add(Pair.of(parts[1], parts[0]));
                    }
                }
            }

            connection.links = links;
        }

        return connection;
    }

    public static PipelineConnection direct(List<Pipeline> p) {

        MultiPipelineConnection connection = new MultiPipelineConnection();
        connection.pipelines = p;
        connection.setConnected(CollectionUtils.isNotEmpty(p));

        return connection;
    }

    public static PipelineConnection of(Map<String, String> spec) {

        OutcomesPipelineConnection connection = new OutcomesPipelineConnection();
        connection.links = spec;

        return connection;
    }

    public static PipelineConnection direct(Map<Outcome, Pipeline> spec) {

        OutcomesPipelineConnection connection = new OutcomesPipelineConnection();
        connection.outcomes = spec;
        connection.setConnected(MapUtils.isNotEmpty(spec));

        return connection;
    }
}
