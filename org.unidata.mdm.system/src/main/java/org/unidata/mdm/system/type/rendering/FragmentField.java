package org.unidata.mdm.system.type.rendering;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;

/**
 * Fragment field definition
 * <p>
 * Describes the data structure that will be processed by renders
 * <p>
 * object {
 * fieldToRendering: [
 * partFieldName : AnyType
 * ]
 * }
 *
 * @author Alexandr Serov
 * @see InputSource
 * @see OutputSink
 * @since 17.09.2020
 **/
public class FragmentField {

    public static final String CURRENT_VERSION = "current";

    /**
     * Target object field
     */
    private final String fieldName;


    /**
     * Fragment version
     */
    private final String version;

    /**
     * Trarget object type
     */
    private final Class<?> javaClass;

    /**
     * Fragment types any -> propertyName: propertyType
     */
    private final Map<String, AnyType> parts;

    /**
     * @param javaClass InputSource/OutputSink fragment type
     * @param fieldName field in InputSource/OutputSink class
     * @param version fragment version
     * @param parts composite part of fragment field
     */
    private FragmentField(Class<?> javaClass, String fieldName, String version, Map<String, AnyType> parts) {
        this.javaClass = javaClass;
        this.fieldName = fieldName;
        this.version = version;
        this.parts = parts;
    }

    /**
     * @param javaClass InputSource/OutputSink fragment type
     * @param fieldName field in InputSource/OutputSink class
     * @param parts composite part of fragment field
     * @return new FragmentField instance
     */
    public static FragmentField fragmentField(Class<?> javaClass, String fieldName, Map<String, AnyType> parts) {
        return new FragmentField(javaClass, fieldName, CURRENT_VERSION, Collections.unmodifiableMap(parts));
    }

    /**
     * @param javaClass InputSource/OutputSink fragment type
     * @param fieldName field in InputSource/OutputSink class
     * @param partFieldName composite part field name
     * @param type composite part field type
     * @return new FragmentField instance
     */
    public static FragmentField fragmentField(Class<?> javaClass, String fieldName, String partFieldName, AnyType type) {
        return new FragmentField(javaClass, fieldName, CURRENT_VERSION, Map.of(partFieldName, type));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FragmentField fragmentField = (FragmentField) o;
        return javaClass.equals(fragmentField.javaClass) &&
            fieldName.equals(fragmentField.fieldName) &&
            parts.equals(fragmentField.parts);
    }

    @Override
    public int hashCode() {
        return Objects.hash(javaClass, parts);
    }

    public Class<?> getJavaClass() {
        return javaClass;
    }

    /**
     * @return field in InputSource/OutputSink class
     */
    public String getFieldName() {
        return fieldName;
    }

    /**
     * @return fragment api version
     */
    public String getVersion() {
        return version;
    }

    /**
     * @return rendered field part types
     */
    public Map<String, AnyType> getParts() {
        return parts;
    }
}
