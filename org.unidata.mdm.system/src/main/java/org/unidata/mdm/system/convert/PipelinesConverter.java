/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.system.convert;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.unidata.mdm.system.exception.PipelineException;
import org.unidata.mdm.system.exception.SystemExceptionIds;
import org.unidata.mdm.system.serialization.json.ConnectorSegmentJS;
import org.unidata.mdm.system.serialization.json.FallbackSegmentJS;
import org.unidata.mdm.system.serialization.json.FinishSegmentJS;
import org.unidata.mdm.system.serialization.json.PipelineJS;
import org.unidata.mdm.system.serialization.json.PointSegmentJS;
import org.unidata.mdm.system.serialization.json.SegmentJS;
import org.unidata.mdm.system.serialization.json.SplitterSegmentJS;
import org.unidata.mdm.system.serialization.json.StartSegmentJS;
import org.unidata.mdm.system.type.pipeline.Connector;
import org.unidata.mdm.system.type.pipeline.Fallback;
import org.unidata.mdm.system.type.pipeline.Finish;
import org.unidata.mdm.system.type.pipeline.Pipeline;
import org.unidata.mdm.system.type.pipeline.Point;
import org.unidata.mdm.system.type.pipeline.Segment;
import org.unidata.mdm.system.type.pipeline.SegmentType;
import org.unidata.mdm.system.type.pipeline.Splitter;
import org.unidata.mdm.system.type.pipeline.Start;
import org.unidata.mdm.system.type.pipeline.connection.OutcomesPipelineConnection;
import org.unidata.mdm.system.type.pipeline.connection.PipelineConnection;
import org.unidata.mdm.system.type.pipeline.connection.SinglePipelineConnection;
import org.unidata.mdm.system.util.PipelineUtils;

/**
 * @author Mikhail Mikhailov on Nov 25, 2019
 */
public class PipelinesConverter {

    private PipelinesConverter() {
        super();
    }

    public static List<PipelineJS> toPipelines(Collection<Pipeline> pipelines) {

        if (CollectionUtils.isEmpty(pipelines)) {
            return Collections.emptyList();
        }

        List<PipelineJS> result = new ArrayList<>(pipelines.size());
        for (Pipeline p : pipelines) {

            PipelineJS pro = to(p);
            if (Objects.isNull(pro)) {
                continue;
            }

            result.add(pro);
        }

        return result;
    }

    public static PipelineJS to (Pipeline p) {

        if (Objects.isNull(p)) {
            return null;
        }

        PipelineJS result = new PipelineJS();
        result.setStartId(p.getStartId());
        result.setSubjectId(p.getSubjectId());
        result.setDescription(p.getDescription());
        result.setSegments(toSegments(Stream.concat(p.getSegments().stream(), p.getFallbacks().stream()).collect(Collectors.toList()), p));

        return result;
    }

    public static List<SegmentJS> toSegments(Collection<Segment> segments, Pipeline p) {

        if (CollectionUtils.isEmpty(segments)) {
            return Collections.emptyList();
        }

        List<SegmentJS> result = new ArrayList<>(segments.size());
        for (Segment s : segments) {

            SegmentJS sro = to(s, p);
            if (Objects.isNull(sro)) {
                continue;
            }

            result.add(sro);
        }

        return result;
    }

    public static SegmentJS to(Segment s, Pipeline p) {

        if (Objects.isNull(s)) {
            return null;
        }

        SegmentJS result = null;
        if (s.getType() == SegmentType.SPLITTER) {
            OutcomesPipelineConnection c = p.getConnection(s);
            result = new SplitterSegmentJS(c.serialize());
        } else if (s.getType() == SegmentType.CONNECTOR) {
            SinglePipelineConnection c = p.getConnection(s);
            result = new ConnectorSegmentJS(Objects.isNull(c) ? null : c.serialize());
        } else if (s.getType() == SegmentType.FALLBACK) {
            result = new FallbackSegmentJS();
        } else if (s.getType() == SegmentType.FINISH) {
            result = new FinishSegmentJS();
        } else if (s.getType() == SegmentType.POINT) {
            result = new PointSegmentJS();
        } else if (s.getType() == SegmentType.START) {
            result = new StartSegmentJS();
        }

        Objects.requireNonNull(result, "Trying to marshal a segment of unknown type.");

        result.setId(s.getId());
        result.setSegmentType(s.getType().name());

        return result;
    }

    public static Pipeline from(PipelineJS js) {

        if (Objects.isNull(js)) {
            return null;
        }

        // Gather segments
        List<Pair<Segment, PipelineConnection>> gathered = fromSegments(js.getSegments());
        Start<?> s = (Start<?>) gathered.get(0).getKey();

        Pipeline p = Pipeline.start(s, js.getSubjectId(), js.getDescription());
        for (int i = 1; i < gathered.size(); i++) {
            Pair<Segment, PipelineConnection> segment = gathered.get(i);
            if (segment.getKey().getType() == SegmentType.POINT) {
                p.with((Point<?>) segment.getKey());
            } else if (segment.getKey().getType() == SegmentType.CONNECTOR) {
                if (Objects.nonNull(segment.getValue())) {
                    p.with((Connector<?, ?>) segment.getKey(), segment.getValue());
                } else {
                    p.with((Connector<?, ?>) segment.getKey());
                }
            } else if (segment.getKey().getType() == SegmentType.FALLBACK) {
                p.fallback((Fallback<?>) segment.getKey());
            } else if (segment.getKey().getType() == SegmentType.FINISH) {
                p.end((Finish<?, ?>) segment.getKey());
            } else if (segment.getKey().getType() == SegmentType.SPLITTER) {
                p.split((Splitter<?, ?>) segment.getKey(), segment.getValue());
            }
        }

        return p;
    }

    public static List<Pair<Segment, PipelineConnection>> fromSegments(List<SegmentJS> ros) {

        if (CollectionUtils.isEmpty(ros)) {
            return Collections.emptyList();
        }

        List<Pair<Segment, PipelineConnection>> result = new ArrayList<>(ros.size());
        for (SegmentJS ro : ros) {
            result.add(from(ro));
        }

        return result;
    }

    public static Pair<Segment, PipelineConnection> from(SegmentJS js) {

        if (Objects.isNull(js)) {
            return null;
        }

        Segment hit = PipelineUtils.findSegment(js.getId());
        if (Objects.isNull(hit)) {
            throw new PipelineException("Segment not found by id [{}].",
                    SystemExceptionIds.EX_PIPELINE_SEGMENT_NOT_FOUND_BY_ID,
                    js.getId());
        } else if (!hit.getType().name().equals(js.getSegmentType())) {
            throw new PipelineException("Segment found by ID, but is of different type [{}].",
                    SystemExceptionIds.EX_PIPELINE_SEGMENT_OF_WRONG_TYPE, js.getSegmentType());
        }

        if (hit.getType() == SegmentType.CONNECTOR) {
            ConnectorSegmentJS csjs = (ConnectorSegmentJS) js;
            if (StringUtils.isNotBlank(csjs.getConnectedId())) {
                return Pair.of(hit, PipelineConnection.of(csjs.getConnectedId()));
            }
        } else if (hit.getType() == SegmentType.SPLITTER) {
            SplitterSegmentJS ssjs = (SplitterSegmentJS) js;
            if (MapUtils.isNotEmpty(ssjs.getOutcomes())) {
                return Pair.of(hit, PipelineConnection.of(ssjs.getOutcomes()));
            }
        }

        return Pair.of(hit, null);
    }
}
