package org.unidata.mdm.system.type.pipeline.connection;

import java.util.Objects;

import org.unidata.mdm.system.type.pipeline.Pipeline;
import org.unidata.mdm.system.type.pipeline.Segment;
import org.unidata.mdm.system.util.PipelineUtils;

/**
 * @author Mikhail Mikhailov on May 26, 2020
 */
public class SinglePipelineConnection extends PipelineConnection {
    /**
     * Resolved pipeline.
     */
    protected Pipeline pipeline;
    /**
     * Start id to resolve.
     */
    protected String start;
    /**
     * Subject id to resolve.
     */
    protected String subject;
    /**
     * Constructor.
     */
    protected SinglePipelineConnection() {
        super();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public ConnectionType getConnectionType() {
        return ConnectionType.SINGLE;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void connect(Segment segment) {

        if (isConnected()) {
            return;
        }

        if (Objects.nonNull(start)) {

            Pipeline p = PipelineUtils.findPipeline(start, subject);
            throwPipelineNotFoundIfNull(p, start, subject);

            setConnected(Objects.nonNull(p));
            if (isConnected()) {
                pipeline = p;
                start = null;
                subject = null;
            }
        }
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String serialize() {
        return isConnected() ? PipelineUtils.toSerializablePipelineId(pipeline) : null;
    }
    /**
     * @return the pipeline
     */
    public Pipeline getPipeline() {
        return pipeline;
    }
}
