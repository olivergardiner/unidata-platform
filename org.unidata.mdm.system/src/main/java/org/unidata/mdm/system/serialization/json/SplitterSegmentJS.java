package org.unidata.mdm.system.serialization.json;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Mikhail Mikhailov on May 25, 2020
 */
public class SplitterSegmentJS extends SegmentJS {

    private Map<String, String> outcomes = new HashMap<>();
    /**
     * Constructor.
     */
    public SplitterSegmentJS() {
        super();
    }
    /**
     * Constructor.
     */
    public SplitterSegmentJS(Map<String, String> outcomes) {
        super();
        this.outcomes.putAll(outcomes);
    }
    /**
     * @return the outcomes
     */
    public Map<String, String> getOutcomes() {
        return outcomes;
    }
    /**
     * @param outcomes the outcomes to set
     */
    public void setOutcomes(Map<String, String> outcomes) {
        this.outcomes = outcomes;
    }
}
