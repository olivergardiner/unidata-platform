/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.web;

import java.util.EnumSet;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.apache.cxf.transport.servlet.CXFServlet;
import org.javasimon.console.SimonConsoleServlet;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.unidata.mdm.configuration.SpringConfiguration;
import org.unidata.mdm.system.module.SystemModule;
import org.unidata.mdm.system.service.impl.module.ModularAnnotationConfigWebApplicationContext;

public class ApplicationInitializer implements WebApplicationInitializer {

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {

        final ModularAnnotationConfigWebApplicationContext context = new ModularAnnotationConfigWebApplicationContext();
        context.setId(SystemModule.MODULE_ID);
        context.register(SpringConfiguration.class);

        servletContext.addListener(new ContextLoaderListener(context));

        final ServletRegistration.Dynamic dispatcher = servletContext.addServlet("CXFServlet", new CXFServlet());
        dispatcher.setLoadOnStartup(2);
        dispatcher.addMapping("/api/*");

        final ServletRegistration.Dynamic simon = servletContext.addServlet("SimonConsoleServlet", new SimonConsoleServlet());
        simon.setLoadOnStartup(3);
        simon.setInitParameter("url-prefix", "/javasimon-console");
        simon.addMapping("/javasimon-console/*");

        final FilterRegistration.Dynamic corsFilter = servletContext.addFilter("CorsFilter", "org.apache.catalina.filters.CorsFilter");
        corsFilter.setInitParameter("cors.allowed.origins", "*");
        corsFilter.setInitParameter("cors.allowed.methods", "GET,POST,HEAD,OPTIONS,PUT,DELETE");
        corsFilter.setInitParameter("cors.allowed.headers", "Content-Type,X-Requested-With,accept,Origin,Access-Control-Request-Method,Access-Control-Request-Headers,Authorization,PROLONG_TTL");
        corsFilter.setInitParameter("cors.exposed.headers", "Access-Control-Allow-Origin,Access-Control-Allow-Credentials,Content-Disposition,Authorization,PROLONG_TTL");
        corsFilter.addMappingForServletNames(EnumSet.allOf(DispatcherType.class), true, "CXFServlet", "SimonConsoleServlet");
    }
}
