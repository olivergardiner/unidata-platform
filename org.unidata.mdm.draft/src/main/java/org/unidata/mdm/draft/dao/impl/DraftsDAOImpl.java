package org.unidata.mdm.draft.dao.impl;

import java.sql.Types;
import java.util.List;
import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.unidata.mdm.draft.dao.DraftsDAO;
import org.unidata.mdm.draft.po.DraftPO;
import org.unidata.mdm.draft.po.EditionPO;
import org.unidata.mdm.system.dao.impl.BaseDAOImpl;
import org.unidata.mdm.system.serialization.SystemSerializer;
import org.unidata.mdm.system.type.runtime.MeasurementPoint;
import org.unidata.mdm.system.type.variables.Variables;

/**
 * @author Mikhail Mikhailov on Sep 9, 2020
 */
@Repository
public class DraftsDAOImpl extends BaseDAOImpl implements DraftsDAO {
    /**
     * Default RM for draft objects.
     */
    private static final RowMapper<DraftPO> DEFAULT_DRAFT_ROW_MAPPER = (rs, row) -> {

        DraftPO result = new DraftPO();

        long v = rs.getLong(DraftPO.FIELD_ID);
        result.setId(v == 0 ? null : v);

        v = rs.getLong(DraftPO.FIELD_PARENT_ID);
        result.setParentId(v == 0 ? null : v);

        result.setProvider(rs.getString(DraftPO.FIELD_PROVIDER));
        result.setSubject(rs.getString(DraftPO.FIELD_SUBJECT));
        result.setOwner(rs.getString(DraftPO.FIELD_OWNER));
        result.setDescription(rs.getString(DraftPO.FIELD_DESCRIPTION));
        result.setOperationId(rs.getString(DraftPO.FIELD_OPERATION_ID));
        result.setProperties(SystemSerializer.variablesFromProtostuff(rs.getBytes(DraftPO.FIELD_PROPERTIES)));

        result.setCreateDate(rs.getTimestamp(DraftPO.FIELD_CREATE_DATE));
        result.setCreatedBy(rs.getString(DraftPO.FIELD_CREATED_BY));
        result.setUpdateDate(rs.getTimestamp(DraftPO.FIELD_UPDATE_DATE));
        result.setUpdatedBy(rs.getString(DraftPO.FIELD_UPDATED_BY));
        result.setEditionsCount(rs.getInt(DraftPO.FIELD_EDITIONS_COUNT));

        return result;
    };
    /**
     * Default RM for edition objects.
     */
    private static final RowMapper<EditionPO> DEFAULT_EDITION_ROW_MAPPER = (rs, row) -> {

        EditionPO result = new EditionPO();

        result.setCreateDate(rs.getTimestamp(EditionPO.FIELD_CREATE_DATE));
        result.setCreatedBy(rs.getString(EditionPO.FIELD_CREATED_BY));
        result.setDraftId(rs.getLong(EditionPO.FIELD_DRAFT_ID));
        result.setProvider(rs.getString(EditionPO.FIELD_PROVIDER));
        result.setRevision(rs.getInt(EditionPO.FIELD_REVISION));
        result.setContent(rs.getBytes(EditionPO.FIELD_CONTENT));

        return result;
    };
    /**
     * Drafts filter SQL types.
     */
    private static final int[] DRAFTS_FILTER_TYPES = {
        Types.VARCHAR,
        Types.VARCHAR,
        Types.VARCHAR,
        Types.VARCHAR,
        Types.VARCHAR,
        Types.VARCHAR,
        Types.VARCHAR,
        Types.VARCHAR,
    };

    private final String countDraftsSQL;
    private final String loadDraftsSQL;
    private final String loadDraftByIdSQL;
    private final String loadEditionsByDraftIdSQL;
    private final String loadCurrentEditionByDraftIdSQL;
    private final String putEditionSQL;
    private final String putDraftSQL;
    private final String putDraftPropertiesByIdSQL;
    private final String deleteDraftByIdSQL;
    private final String deleteDraftsSQL;

    /**
     * Constructor.
     */
    @Autowired
    public DraftsDAOImpl(
            @Qualifier("draftDataSource") final DataSource dataSource,
            @Qualifier("drafts-sql") final Properties sql) {
        super(dataSource);
        countDraftsSQL = sql.getProperty("countDraftsSQL");
        loadDraftsSQL = sql.getProperty("loadDraftsSQL");
        loadDraftByIdSQL = sql.getProperty("loadDraftByIdSQL");
        loadEditionsByDraftIdSQL = sql.getProperty("loadEditionsByDraftIdSQL");
        loadCurrentEditionByDraftIdSQL = sql.getProperty("loadCurrentEditionByDraftIdSQL");
        putEditionSQL = sql.getProperty("putEditionSQL");
        putDraftSQL = sql.getProperty("putDraftSQL");
        putDraftPropertiesByIdSQL = sql.getProperty("putDraftPropertiesByIdSQL");
        deleteDraftByIdSQL = sql.getProperty("deleteDraftByIdSQL");
        deleteDraftsSQL = sql.getProperty("deleteDraftsSQL");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long countDrafts(String providerId, String subjectId, String owner, String operationId) {
        MeasurementPoint.start();
        try {

            final String[] draftsFilter = new String[] {
                providerId, providerId,
                subjectId, subjectId,
                owner, owner,
                operationId, operationId
            };

            return jdbcTemplate.queryForObject(countDraftsSQL, draftsFilter, DRAFTS_FILTER_TYPES, Long.class);
        } finally {
            MeasurementPoint.stop();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<DraftPO> loadDrafts(String providerId, String subjectId, String owner, String operationId) {
        MeasurementPoint.start();
        try {

            final String[] draftsFilter = new String[] {
                providerId, providerId,
                subjectId, subjectId,
                owner, owner,
                operationId, operationId
            };

            return jdbcTemplate.query(loadDraftsSQL, draftsFilter, DRAFTS_FILTER_TYPES, DEFAULT_DRAFT_ROW_MAPPER);
        } finally {
            MeasurementPoint.stop();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DraftPO loadDraft(long id) {
        MeasurementPoint.start();
        try {
            return jdbcTemplate.query(loadDraftByIdSQL, rs -> rs.next() ? DEFAULT_DRAFT_ROW_MAPPER.mapRow(rs, 0) : null, id);
        } finally {
            MeasurementPoint.stop();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EditionPO loadCurrentEdition(long id, boolean withData) {
        MeasurementPoint.start();
        try {
            return jdbcTemplate.query(loadCurrentEditionByDraftIdSQL, rs -> rs.next() ? DEFAULT_EDITION_ROW_MAPPER.mapRow(rs, 0) : null, withData, id);
        } finally {
            MeasurementPoint.stop();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<EditionPO> loadEditions(long id, boolean withData) {
        MeasurementPoint.start();
        try {
            return jdbcTemplate.query(loadEditionsByDraftIdSQL, DEFAULT_EDITION_ROW_MAPPER, withData, id);
        } finally {
            MeasurementPoint.stop();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long putDraft(DraftPO draft) {
        MeasurementPoint.start();
        try {
            return jdbcTemplate.queryForObject(putDraftSQL, Long.class,
                    draft.getParentId(),
                    draft.getProvider(),
                    draft.getSubject(),
                    draft.getOwner(),
                    draft.getDescription(),
                    draft.getOperationId(),
                    draft.getCreatedBy(),
                    draft.getProperties() != null && !draft.getProperties().isEmpty()
                        ? SystemSerializer.variablesToProtostuff(draft.getProperties())
                        : null);
        } finally {
            MeasurementPoint.stop();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int putEdition(EditionPO subject) {
        MeasurementPoint.start();
        try {
            return jdbcTemplate.queryForObject(putEditionSQL, Integer.class,
                    subject.getDraftId(),
                    subject.getDraftId(),
                    subject.getContent(),
                    subject.getCreatedBy());
        } finally {
            MeasurementPoint.stop();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void putDraftProperties(long draftId, Variables variables, String description) {
        MeasurementPoint.start();
        try {
            jdbcTemplate.update(putDraftPropertiesByIdSQL,
                    variables != null && !variables.isEmpty()
                        ? SystemSerializer.variablesToProtostuff(variables)
                        : null,
                    description,
                    draftId);
        } finally {
            MeasurementPoint.stop();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int wipeDraft(long draftId) {
        MeasurementPoint.start();
        try {
            return jdbcTemplate.update(deleteDraftByIdSQL, draftId);
        } finally {
            MeasurementPoint.stop();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int wipeDrafts(String providerId, String subjectId, String owner, String operationId) {
        MeasurementPoint.start();
        try {

            final String[] draftsFilter = new String[] {
                providerId, providerId,
                subjectId, subjectId,
                owner, owner,
                operationId, operationId
            };

            return jdbcTemplate.update(deleteDraftsSQL, draftsFilter, DRAFTS_FILTER_TYPES);
        } finally {
            MeasurementPoint.stop();
        }
    }
}
