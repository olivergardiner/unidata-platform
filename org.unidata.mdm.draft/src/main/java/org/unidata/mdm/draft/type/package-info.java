/**
 * @author Mikhail Mikhailov on Sep 9, 2020
 * Various types related to draft module.
 */
package org.unidata.mdm.draft.type;