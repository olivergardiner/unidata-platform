/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.draft.context;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.collections4.MapUtils;
import org.unidata.mdm.system.context.AbstractCompositeRequestContext;
import org.unidata.mdm.system.context.StartTypeIdResettingContext;
import org.unidata.mdm.system.type.pipeline.PipelineInput;

/**
 * @author Alexander Malyshev
 */
public abstract class AbstractDraftBaseContext
    extends AbstractCompositeRequestContext
    implements DraftCapableContext, PipelineInput, StartTypeIdResettingContext {
    /**
     * SVUID.
     */
    private static final long serialVersionUID = -6534102043881429128L;
    /**
     * The draft id.
     */
    private final Long draftId;
    /**
     * The parent draft id.
     */
    private final Long parentDraftId;
    /**
     * General purpose parameters.
     */
    private final transient Map<String, Object> parameters;
    /**
     * The start type id, provided by draft provider.
     */
    private String startTypeId;
    /**
     * Constructor.
     * @param b builder instance
     */
    protected AbstractDraftBaseContext(AbstractDraftBaseContextBuilder<?> b) {
        super(b);
        this.draftId = b.draftId;
        this.parentDraftId = b.parentDraftId;
        this.parameters = b.parameters;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getStartTypeId() {
        // Returned by draft provider.
        return startTypeId;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void setStartTypeId(String startTypeId) {
        this.startTypeId = startTypeId;
    }
    /**
     * Gets draft id
     * @return id
     */
    public Long getDraftId() {
        return draftId;
    }
    /**
     * @return the parentDraftId
     */
    public Long getParentDraftId() {
        return parentDraftId;
    }
    /**
     * @return the parameters
     */
    public Map<String, Object> getParameters() {
        return Objects.isNull(parameters) ? Collections.emptyMap() : parameters;
    }
    /**
     * Returns true, if a parameter with the key exists.
     * @param key the key
     * @return true, if exists (but may be mapped to null value)
     */
    public boolean hasParameter(String key) {
        return getParameters().containsKey(key);
    }
    /**
     * Returns true, if a parameter with the key was set and the value is not null.
     * @param key the key
     * @return true, if set to a non-null value
     */
    public boolean isParameterSet(String key) {
        return Objects.nonNull(getParameters().get(key));
    }
    /**
     * Casting parameter getter.
     * @param <X> the type to cast to
     * @param key the key
     * @return parameter or null, if not set
     */
    @SuppressWarnings("unchecked")
    public <X> X getParameter(String key) {
        return (X) getParameters().get(key);
    }
    /**
     * @author Mikhail Mikhailov on Sep 16, 2020
     * Builder.
     */
    public abstract static class AbstractDraftBaseContextBuilder<X extends AbstractDraftBaseContextBuilder<X>> extends AbstractCompositeRequestContextBuilder<X> {
        /**
         * The draft id.
         */
        private Long draftId;
        /**
         * The parent draft id.
         */
        private Long parentDraftId;
        /**
         * General purpose parameters.
         */
        private Map<String, Object> parameters;
        /**
         * Constructor.
         * @param other the context to copy fields from
         */
        protected AbstractDraftBaseContextBuilder(AbstractCompositeRequestContext other) {
            super(other);
        }
        /**
         * Constructor.
         */
        protected AbstractDraftBaseContextBuilder() {
            super();
        }
        /**
         * Sets draft id
         * @param draftId the draft id
         * @return self
         */
        public X draftId(Long draftId) {
            this.draftId = draftId;
            return self();
        }
        /**
         * Sets parent draft id
         * @param parentDraftId the parent draft id
         * @return self
         */
        public X parentDraftId(Long parentDraftId) {
            this.parentDraftId = parentDraftId;
            return self();
        }
        /**
         * Sets a parameter to context,
         * @param key the parameter's key. Null values are not accepted
         * @param value the parameter's value.
         * @return self
         */
        public X parameter(String key, Object value) {
            if (Objects.nonNull(key)) {
                if (Objects.isNull(this.parameters)) {
                    this.parameters = new HashMap<>();
                }
                this.parameters.put(key, value);
            }
            return self();
        }
        /**
         * Sets a bunch ofparameters to context,
         * @param key the parameter's key. Null values are not accepted
         * @param value the parameter's value.
         * @return self
         */
        public X parameters(Map<String, Object> parameters) {
            if (MapUtils.isNotEmpty(parameters)) {
                if (Objects.isNull(this.parameters)) {
                    this.parameters = new HashMap<>();
                }
                this.parameters.putAll(parameters);
            }
            return self();
        }
    }
}
