package org.unidata.mdm.draft.dao;

import java.util.List;

import javax.annotation.Nullable;

import org.unidata.mdm.draft.po.DraftPO;
import org.unidata.mdm.draft.po.EditionPO;
import org.unidata.mdm.system.type.variables.Variables;

/**
 * @author Mikhail Mikhailov on Sep 9, 2020
 * The drafts DAO interface.
 */
public interface DraftsDAO {
    /**
     * Count drafts for the given provider ID and subject.
     * @param providerId the provider ID
     * @param subjectId the draft subject ID
     * @param owner draft owner user name
     * @param operationId the operation id
     * @return count of draft objects
     */
    long countDrafts(String providerId, String subjectId, String owner, String operationId);
    /**
     * Loads drafts collection for the given provider ID and owner.
     * @param providerId the provider ID
     * @param subjectId the draft subject ID
     * @param owner draft owner user name
     * @param operationId the operation id
     * @return collection of draft objects
     */
    List<DraftPO> loadDrafts(String providerId, String subjectId, String owner, String operationId);
    /**
     * Loads a draft object by id.
     * @param id the draft id
     * @return draft object or null
     */
    @Nullable
    DraftPO loadDraft(long id);
    /**
     * Loads "current" (the youngest) subject by draft id.
     * @param id the draft id
     * @param withData load data bytes
     * @return subject or null
     */
    @Nullable
    EditionPO loadCurrentEdition(long id, boolean withData);
    /**
     * Loads all subjects by draft id.
     * @param id the draft id
     * @param withData load data bytes
     * @return subject collection
     */
    @Nullable
    List<EditionPO> loadEditions(long id, boolean withData);
    /**
     * Puts a new draft object to DB.
     * @param draft the draft object
     * @return new draft id
     */
    long putDraft(DraftPO draft);
    /**
     * Puts a new subject to DB.
     * @param subject the subject
     * @return new subject revision number
     */
    int putEdition(EditionPO subject);
    /**
     * Updates properties for an existing draft id.
     * @param draftId the draft id to update
     * @param variables the variables to save
     * @param description the description to update
     */
    void putDraftProperties(long draftId, Variables variables, String description);
    /**
     * Wipes a draft object by id.
     * @param draftId the draft id
     * @return count of wiped objects
     */
    int wipeDraft(long draftId);
    /**
     * Wipes all drafts by provider ID, owner user name and subject ID.
     * @param providerId the provider ID
     * @param subjectId the subject id
     * @param owner the owner user name
     * @param operationId the operation id
     * @return count of wiped objects
     */
    int wipeDrafts(String providerId, String subjectId, String owner, String operationId);
}
