/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.draft.context;

/**
 * @author Alexander Malyshev
 */
public class DraftQueryContext extends AbstractDraftFieldsContext {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = -8847138502973733754L;
    /**
     * ID of the operation, which created a draft object,
     * we're looking for (don't confuse with parent's operationId for THIS operation).
     */
    private final String forOperationId;
    /**
     * Fetch postional from.
     */
    private final Integer start;
    /**
     * Fetch postional count.
     */
    private final Integer limit;
    /**
     * Load edition objects for this draft.
     */
    private final boolean withEditions;
    /**
     * Load current data view of the draft object.
     */
    private final boolean withData;
    /**
     * Constructor.
     * @param b the builder
     */
    private DraftQueryContext(final DraftQueryContextBuilder b) {
        super(b);
        this.forOperationId = b.forOperationId;
        this.start = b.start;
        this.limit = b.limit;
        this.withEditions = b.withEditions;
        this.withData = b.withData;
    }
    /**
     * Gets Operation ID to filter for.
     * @return the forOperationId
     */
    public String getForOperationId() {
        return forOperationId;
    }
    /**
     * Gets postional from.
     * @return from position
     */
    public Integer getStart() {
        return start;
    }
    /**
     * Gets count to return.
     * @return count
     */
    public Integer getLimit() {
        return limit;
    }
    /**
     * @return the withEditions
     */
    public boolean withEditions() {
        return withEditions;
    }
    /**
     * @return the withData
     */
    public boolean withData() {
        return withData;
    }
    /**
     * Builder method.
     * @return builder instance
     */
    public static DraftQueryContextBuilder builder() {
        return new DraftQueryContextBuilder();
    }
    /**
     * @author Mikhail Mikhailov on Sep 16, 2020
     * Builder class.
     */
    public static class DraftQueryContextBuilder extends AbstractDraftFieldsContextBuilder<DraftQueryContextBuilder> {
        /**
         * ID of the operation, which created a draft object,
         * we're looking for (don't confuse with parent's operationId for THIS operation).
         */
        private String forOperationId;
        /**
         * Fetch postional from.
         */
        private Integer start;
        /**
         * Fetch postional count.
         */
        private Integer limit;
        /**
         * Load edition objects for this draft.
         */
        private boolean withEditions;
        /**
         * Load current data view of the draft object.
         */
        private boolean withData;
        /**
         * Constructor.
         */
        private DraftQueryContextBuilder() {
            super();
        }
        /**
         * Sets ID of the operation, which created a draft object,
         * we're looking for (don't confuse with parent's operationId for THIS operation).
         * @param forOperationId op ID
         * @return self
         */
        public DraftQueryContextBuilder forOperationId(String forOperationId) {
            this.forOperationId = forOperationId;
            return self();
        }
        /**
         * Start portion.
         * @param start the start point
         * @return self
         */
        public DraftQueryContextBuilder start(Integer start) {
            this.start = start;
            return self();
        }
        /**
         * The count to return.
         * @param limit count to return
         * @return self
         */
        public DraftQueryContextBuilder limit(Integer limit) {
            this.limit = limit;
            return self();
        }
        /**
         * Fetch data.
         * @param withData fetch data if true
         * @return self
         */
        public DraftQueryContextBuilder withData(boolean withData) {
            this.withData = withData;
            return self();
        }
        /**
         * Fetch editions info.
         * @param withEditions fetch editions info if true
         * @return self
         */
        public DraftQueryContextBuilder withEditions(boolean withEditions) {
            this.withEditions = withEditions;
            return self();
        }
        /**
         * {@inheritDoc}
         */
        @Override
        public DraftQueryContext build() {
            return new DraftQueryContext(this);
        }
    }
}
