package org.unidata.mdm.draft.type;

import javax.annotation.Nonnull;

/**
 * @author Mikhail Mikhailov on Sep 9, 2020
 * The draft provider interface.
 * This should be implemented by the modules and subsystems, that want to use draft functionality.
 * @param <X> the target data type
 */
public interface DraftProvider<X> {
    /**
     * Gets the unique draft provider ID.
     * @return provider id
     */
    @Nonnull
    String getId();
    /**
     * Gets translated provider description.
     * @return description
     */
    @Nonnull
    String getDescription();
    /**
     * Returns the pipeline id for the given draft operation.
     * @param operation the operation to support
     * @return pipeline id
     */
    @Nonnull
    String getPipelineId(@Nonnull DraftOperation operation);
    /**
     * Converts serialized data to target type.
     * @param data the bytes to convert
     * @return target type
     */
    X fromBytes(byte[] data);
    /**
     * Converts target type to bytes.
     * @param data the data
     * @return bytes or null
     */
    byte[] toBytes(X data);
}
