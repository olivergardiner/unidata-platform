package org.unidata.mdm.draft.context;

import org.unidata.mdm.draft.type.Draft;
import org.unidata.mdm.draft.type.Edition;
import org.unidata.mdm.system.context.StorageCapableContext;
import org.unidata.mdm.system.context.StorageId;

/**
 * @author Mikhail Mikhailov on Sep 16, 2020
 */
public interface DraftCapableContext extends StorageCapableContext {
    /**
     * Current draft object.
     */
    StorageId SID_DRAFT_OBJECT = new StorageId("DRAFT_OBJECT");
    /**
     * Current edition object.
     */
    StorageId SID_EDITION_OBJECT = new StorageId("EDITION_OBJECT");
    /**
     * Get draft.
     * @return draft object
     */
    default Draft currentDraft() {
        return getFromStorage(SID_DRAFT_OBJECT);
    }
    /**
     * Put draft.
     * @param draft
     */
    default void currentDraft(Draft draft) {
        putToStorage(SID_DRAFT_OBJECT, draft);
    }
    /**
     * Get edition.
     * @return edition object
     */
    default Edition currentEdition() {
        return getFromStorage(SID_EDITION_OBJECT);
    }
    /**
     * Put edition.
     * @param edition
     */
    default void currentEdition(Edition edition) {
        putToStorage(SID_EDITION_OBJECT, edition);
    }
}
