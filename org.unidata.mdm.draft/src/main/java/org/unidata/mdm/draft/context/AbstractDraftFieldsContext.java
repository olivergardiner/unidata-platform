package org.unidata.mdm.draft.context;

import org.unidata.mdm.system.context.AbstractCompositeRequestContext;

/**
 * @author Mikhail Mikhailov on Sep 18, 2020
 */
public abstract class AbstractDraftFieldsContext extends AbstractDraftBaseContext {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = -6111969031432991565L;
    /**
     * The draft provider ID.
     */
    private final String provider;
    /**
     * Subject id.
     */
    private final String subjectId;
    /**
     * Current owner.
     */
    private final String owner;
    /**
     * Constructor.
     * @param b
     */
    public AbstractDraftFieldsContext(AbstractDraftFieldsContextBuilder<?> b) {
        super(b);
        this.provider = b.provider;
        this.subjectId = b.subjectId;
        this.owner = b.owner;
    }
    /**
     * Gets draft provider id.
     * @return the id
     */
    public String getProvider() {
        return provider;
    }
    /**
     * Gets subject id.
     * @return subject id
     */
    public String getSubjectId() {
        return subjectId;
    }
    /**
     * Gets owner user name
     * @return user name
     */
    public String getOwner() {
        return owner;
    }
    /**
     * @author Mikhail Mikhailov on Sep 16, 2020
     * Builder.
     */
    public abstract static class AbstractDraftFieldsContextBuilder<X extends AbstractDraftFieldsContextBuilder<X>> extends AbstractDraftBaseContextBuilder<X> {
        /**
         * The draft provider ID.
         */
        private String provider;
        /**
         * Subject id.
         */
        private String subjectId;
        /**
         * Current owner.
         */
        private String owner;
        /**
         * Constructor.
         * @param other the context to copy fields from
         */
        protected AbstractDraftFieldsContextBuilder(AbstractCompositeRequestContext other) {
            super(other);
        }
        /**
         * Constructor.
         */
        protected AbstractDraftFieldsContextBuilder() {
            super();
        }
        /**
         * Sets draft provider id.
         * @param provider the id
         * @return self
         */
        public X provider(String provider) {
            this.provider = provider;
            return self();
        }
        /**
         * Sets subject id
         * @param entityId the subject id
         * @return self
         */
        public X subjectId(String entityId) {
            this.subjectId = entityId;
            return self();
        }
        /**
         * Sets the owner's username
         * @param draftOwner the owner user name
         * @return self
         */
        public X owner(String draftOwner) {
            this.owner = draftOwner;
            return self();
        }
    }
}
