/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.draft.context;

import org.unidata.mdm.system.context.SetupAwareContext;
import org.unidata.mdm.system.type.pipeline.PipelineInput;

/**
 * @author Alexander Malyshev
 */
public class DraftPublishContext extends AbstractDraftBaseContext implements PipelineInput, SetupAwareContext {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = -5615063242829365939L;
    /**
     * Force publish, even if validation returns some errors or other problems exist.
     */
    private boolean force;
    /**
     * Constructor.
     * @param b the builder
     */
    public DraftPublishContext(final DraftPublishContextBuilder b) {
        super(b);
        this.force = b.force;
    }
    /**
     * Gets a builder instance
     * @return builder instance
     */
    public static DraftPublishContextBuilder builder() {
        return new DraftPublishContextBuilder();
    }
    /**
     * Tells, whether the publication should be forced.
     * @return true, if so, false otherwise
     */
    public boolean isForce(){
        return force;
    }
    /**
     * @author Mikhail Mikhailov on Sep 13, 2020
     * Simple builder.
     */
    public static class DraftPublishContextBuilder extends AbstractDraftBaseContextBuilder<DraftPublishContextBuilder> {
        /**
         * Force publication, event if problems exist.
         */
        private boolean force;
        /**
         * Constructor.
         */
        private DraftPublishContextBuilder() {
            super();
        }
        /**
         * Sets force flag.
         * @param force the flag
         * @return self
         */
        public DraftPublishContextBuilder force(boolean force){
            this.force = force;
            return this;
        }
        /**
         * {@inheritDoc}
         */
        @Override
        public DraftPublishContext build() {
            return new DraftPublishContext(this);
        }
    }
}
