/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.draft.context;

/**
 * @author Alexander Malyshev
 */
public class DraftRemoveContext extends AbstractDraftFieldsContext {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = 3987153718107917035L;
    /**
     * ID of the operation, which created a draft object,
     * we're looking for (don't confuse with parent's operationId for THIS operation).
     */
    private final String forOperationId;
    /**
     * Constructor.
     * @param b the builder
     */
    public DraftRemoveContext(final DraftRemoveContextBuilder b) {
        super(b);
        this.forOperationId = b.forOperationId;
    }
    /**
     * Gets Operation ID to filter for.
     * @return the forOperationId
     */
    public String getForOperationId() {
        return forOperationId;
    }
    /**
     * Builder instance.
     * @return builder
     */
    public static DraftRemoveContextBuilder builder() {
        return new DraftRemoveContextBuilder();
    }
    /**
     * @author Mikhail Mikhailov on Sep 18, 2020
     * Almost empty builder
     */
    public static class DraftRemoveContextBuilder extends AbstractDraftFieldsContextBuilder<DraftRemoveContextBuilder> {
        /**
         * ID of the operation, which created a draft object,
         * we're looking for (don't confuse with parent's operationId for THIS operation).
         */
        private String forOperationId;
        /**
         * Constructor.
         */
        private DraftRemoveContextBuilder() {
            super();
        }
        /**
         * Sets ID of the operation, which created a draft object,
         * we're looking for (don't confuse with parent's operationId for THIS operation).
         * @param forOperationId op ID
         * @return self
         */
        public DraftRemoveContextBuilder forOperationId(String forOperationId) {
            this.forOperationId = forOperationId;
            return self();
        }
        /**
         * {@inheritDoc}
         */
        @Override
        public DraftRemoveContext build() {
            return new DraftRemoveContext(this);
        }
    }
}
