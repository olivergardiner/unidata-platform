/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.draft.dto;

import java.util.Objects;

import org.unidata.mdm.draft.type.Draft;
import org.unidata.mdm.draft.type.DraftPayloadResponse;
import org.unidata.mdm.draft.type.Edition;
import org.unidata.mdm.system.dto.AbstractCompositeResult;
import org.unidata.mdm.system.type.pipeline.PipelineOutput;
import org.unidata.mdm.system.type.variables.Variables;

/**
 * @author Alexander Malyshev
 * Upsert result.
 * Some notes to operation success mark.
 * If this is set to false after execution
 * - for new drafts - no draft objects will be created
 * - for existing drafts - no editions will be created.
 * This is a polite way to prevent any changes to draft without throwing an exception.
 * This is useful in some cases.
 * - Each time {@link Variables} object is returned it is updated in the draft store.
 * - Each time {@link Edition} is returned, it is inserted.
 * Nothing is done if both objects are null
 */
public class DraftUpsertResult extends AbstractCompositeResult implements PipelineOutput {
    /**
     * The draft.
     */
    private Draft draft;
    /**
     * Operation success mark.
     */
    private final boolean success;
    /**
     * Resulting edition.
     */
    private Edition edition;
    /**
     * Variables to save alone with draft object.
     */
    private Variables variables;
    /**
     * Any payload, that this draft provider additionally produces.
     * This is not processed by the draft subsystem.
     */
    private DraftPayloadResponse payload;
    /**
     * Constructor.
     * @param success the operation success mark.
     */
    public DraftUpsertResult(boolean success) {
        super();
        this.success = success;
    }
    /**
     * Gets the draft object.
     * @return the draft
     */
    public Draft getDraft() {
        return draft;
    }
    /**
     * Sets the draft object.
     * @param draft the draft to set
     */
    public void setDraft(Draft draft) {
        this.draft = draft;
    }
    /**
     * Has draft set.
     * @return true if set, false otherwise
     */
    public boolean hasDraft() {
        return Objects.nonNull(draft);
    }
    /**
     * @return the edition
     */
    public Edition getEdition() {
        return edition;
    }
    /**
     * @param edition the edition to set
     */
    public void setEdition(Edition payload) {
        this.edition = payload;
    }
    /**
     * Has edition set.
     * @return true if set, false otherwise
     */
    public boolean hasEdition() {
        return Objects.nonNull(edition);
    }
    /**
     * @return the success
     */
    public boolean isSuccess() {
        return success;
    }
    /**
     * @return the variables
     */
    public Variables getVariables() {
        return variables;
    }
    /**
     * @param variables the variables to set
     */
    public void setVariables(Variables variables) {
        this.variables = variables;
    }
    /**
     * Has variables set.
     * @return true if set, false otherwise
     */
    public boolean hasVariables() {
        return Objects.nonNull(variables) && !variables.isEmpty();
    }
    /**
     * Gets the payload.
     * @return the payload
     */
    public DraftPayloadResponse getPayload() {
        return payload;
    }
    /**
     * @param payload the payload to set
     */
    public void setPayload(DraftPayloadResponse data) {
        this.payload = data;
    }
    /**
     * Narrows payload to a particular type.
     * @param <T> the type to narrow to
     * @return payload
     * @throws ClassCastException if you try to narrow the payload to a wrong type
     */
    @SuppressWarnings("unchecked")
    public <T extends DraftPayloadResponse> T narrow() {
        return (T) getPayload();
    }
    /**
     * Has payload set.
     * @return true if set, false otherwise
     */
    public boolean hasPayload() {
        return Objects.nonNull(payload);
    }
}
