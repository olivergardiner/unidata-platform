/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.draft.module;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.unidata.mdm.draft.configuration.DraftConfiguration;
import org.unidata.mdm.draft.configuration.DraftConfigurationConstants;
import org.unidata.mdm.draft.migration.DraftSchemaMigrations;
import org.unidata.mdm.system.context.DatabaseMigrationContext;
import org.unidata.mdm.system.service.DatabaseMigrationService;
import org.unidata.mdm.system.service.PlatformConfiguration;
import org.unidata.mdm.system.type.module.AbstractModule;
import org.unidata.mdm.system.type.module.Dependency;
import org.unidata.mdm.system.type.pipeline.Segment;

/**
 * @author Alexander Malyshev
 */
public class DraftModule extends AbstractModule {

    private static final Logger LOGGER = LoggerFactory.getLogger(DraftModule.class);

    public static final String MODULE_ID = "org.unidata.mdm.draft";

    private static final Set<Dependency> DEPENDENCIES = Collections.singleton(
        new Dependency("org.unidata.mdm.system", "6.0")
    );

    @Autowired
    private PlatformConfiguration platformConfiguration;

    @Autowired
    private DraftConfiguration configuration;

    @Autowired
    private DatabaseMigrationService migrationService;

    @Autowired
    private DataSource draftDataSource;

    private boolean install;

    @Override
    public String getId() {
        return MODULE_ID;
    }

    @Override
    public String getVersion() {
        return "6.0";
    }

    @Override
    public String getName() {
        return "DraftResult module";
    }

    @Override
    public String getDescription() {
        return "DraftResult module";
    }

    @Override
    public Collection<Dependency> getDependencies() {
        return DEPENDENCIES;
    }

    @Override
    public void install() {
        LOGGER.info("Install");
        migrate();
        install = true;
    }

    @Override
    public void start() {

        LOGGER.info("Starting...");

        if (platformConfiguration.isDeveloperMode() && !install) {
            migrate();
        }

        addSegments(configuration.getBeansOfType(Segment.class).values());

        LOGGER.info("Started");
    }

    private void migrate() {
        migrationService.migrate(DatabaseMigrationContext.builder()
                .schemaName(DraftConfigurationConstants.DRAFT_SCHEMA_NAME)
                .logName(DraftConfigurationConstants.DRAFT_LOG_NAME)
                .dataSource(draftDataSource)
                .migrations(DraftSchemaMigrations.migrations())
                .build());
    }
}
