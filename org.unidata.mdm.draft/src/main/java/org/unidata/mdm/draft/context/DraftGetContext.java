/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.draft.context;

import org.unidata.mdm.system.context.SetupAwareContext;

/**
 * @author Alexander Malyshev
 */
public class DraftGetContext extends AbstractDraftBaseContext implements SetupAwareContext {
    /**
     * SVUID.
     */
    private static final long serialVersionUID = 6416674923103891012L;
    /**
     * Fetch postional from.
     */
    private final Integer start;
    /**
     * Fetch postional count.
     */
    private final Integer limit;
    /**
     * Load edition objects for this draft.
     */
    private final boolean withEditions;
    /**
     * Load current data view of the draft object.
     */
    private final boolean withData;

    private DraftGetContext(final DraftGetContextBuilder b) {
        super(b);
        this.start = b.start;
        this.limit = b.limit;
        this.withEditions = b.withEditions;
        this.withData = b.withData;
    }
    /**
     * Gets postional from.
     * @return from position
     */
    public Integer getStart() {
        return start;
    }
    /**
     * Gets count to return.
     * @return count
     */
    public Integer getLimit() {
        return limit;
    }
    /**
     * @return the withEditions
     */
    public boolean withEditions() {
        return withEditions;
    }
    /**
     * @return the withData
     */
    public boolean withData() {
        return withData;
    }
    /**
     * Builder method.
     * @return builder instance
     */
    public static DraftGetContextBuilder builder() {
        return new DraftGetContextBuilder();
    }
    /**
     * @author Mikhail Mikhailov on Sep 16, 2020
     * Builder class.
     */
    public static class DraftGetContextBuilder extends AbstractDraftBaseContextBuilder<DraftGetContextBuilder> {
        /**
         * Fetch postional from.
         */
        private Integer start;
        /**
         * Fetch postional count.
         */
        private Integer limit;
        /**
         * Load edition objects for this draft.
         */
        private boolean withEditions;
        /**
         * Load current data view of the draft object.
         */
        private boolean withData;

        private DraftGetContextBuilder() {
            super();
        }

        public DraftGetContextBuilder start(Integer start) {
            this.start = start;
            return self();
        }

        public DraftGetContextBuilder limit(Integer limit) {
            this.limit = limit;
            return self();
        }

        public DraftGetContextBuilder withData(boolean withData) {
            this.withData = withData;
            return self();
        }

        public DraftGetContextBuilder withEditions(boolean withEditions) {
            this.withEditions = withEditions;
            return self();
        }

        @Override
        public DraftGetContext build() {
            return new DraftGetContext(this);
        }
    }
}
