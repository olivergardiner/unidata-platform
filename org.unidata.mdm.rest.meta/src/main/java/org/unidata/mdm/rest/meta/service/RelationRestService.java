/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.meta.service;

import java.util.Collections;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.unidata.mdm.meta.type.model.entities.Relation;
import org.unidata.mdm.meta.context.DeleteModelRequestContext.DeleteModelRequestContextBuilder;
import org.unidata.mdm.meta.context.UpdateModelRequestContext.UpdateModelRequestContextBuilder;
import org.unidata.mdm.meta.dto.GetEntitiesByRelationSideDTO;
import org.unidata.mdm.meta.service.MetaDraftService;
import org.unidata.mdm.meta.service.MetaModelService;
import org.unidata.mdm.meta.type.RelativeDirection;
import org.unidata.mdm.rest.meta.converter.EntitiesDefFilteredByRelationSideConverter;
import org.unidata.mdm.rest.meta.converter.RelationDefConverter;
import org.unidata.mdm.rest.meta.converter.RelationDefinitionConverter;
import org.unidata.mdm.rest.meta.ro.RelationDefinition;
import org.unidata.mdm.rest.system.ro.ErrorResponse;
import org.unidata.mdm.rest.system.ro.RestResponse;
import org.unidata.mdm.rest.system.ro.UpdateResponse;
import org.unidata.mdm.rest.system.service.AbstractRestService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

/**
 * Relation rest service
 */
@Path(RelationRestService.SERVICE_PATH)
@Consumes({ "application/json" })
@Produces({ "application/json" })
public class RelationRestService extends AbstractRestService {

    public static final String SERVICE_PATH = "relations";

    /** The meta model service. */
    @Autowired
    private MetaModelService metaModelService;
    @Autowired
    private MetaDraftService metaDraftService;

    /**
     * Find all.
     *
     * @return the response
     */
    @GET
    @Operation(
        description = "Список связей",
        method = HttpMethod.GET,
        responses = {
            @ApiResponse(content = @Content(array = @ArraySchema(schema = @Schema(implementation = RelationDefinition.class))), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response findAll(@Parameter(description = "Черновик?,", in = ParameterIn.QUERY) @QueryParam("draft") @DefaultValue("false") boolean draft) {
    	List<Relation> result;
    	if(draft) {
    		result = metaDraftService.getRelationsList();
    	}else {
    		result = metaModelService.getRelationsList();
    	}
        return ok(RelationDefConverter.convert(result));
    }

    /**
     * Gets entities and their relations view by from side.
     * @param entityName entity name
     * @return result
     * @throws Exception
     */
    @GET
    @Path("from/{name}")
    @Operation(
        description = "Список связей где сущность - правая сторона (to)",
        method = HttpMethod.GET,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = RestResponse.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response getFrom(
            @Parameter(in = ParameterIn.PATH) @PathParam("name") String entityName,
            @Parameter(description = "Черновик?,", in = ParameterIn.QUERY) @QueryParam("draft") @DefaultValue("false") boolean draft) {

        GetEntitiesByRelationSideDTO result;
        if (draft) {
        	result = metaDraftService.getEntitiesFilteredByRelationSide(entityName, RelativeDirection.FROM);
        } else {
        	result = metaModelService.getEntitiesFilteredByRelationSide(entityName, RelativeDirection.FROM);
        }
        return ok(new RestResponse<>(EntitiesDefFilteredByRelationSideConverter.convert(result)));
    }

    @GET
    @Path("to/{name}")
    @Operation(
        description = "Список связей где сущность - левая сторона (from)",
        method = HttpMethod.GET,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = RestResponse.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response getTo(
            @Parameter(in = ParameterIn.PATH) @PathParam("name") String entityName,
            @Parameter(description = "Черновик?,", in = ParameterIn.QUERY) @QueryParam("draft") @DefaultValue("false") boolean draft) {

        GetEntitiesByRelationSideDTO result;
        if(draft) {
        	result= metaDraftService.getEntitiesFilteredByRelationSide(entityName, RelativeDirection.TO);
        }else {
        	result= metaModelService.getEntitiesFilteredByRelationSide(entityName, RelativeDirection.TO);
        }

        return ok(new RestResponse<>(EntitiesDefFilteredByRelationSideConverter.convert(result)));
    }

    /**
     * Upsert.
     *
     * @param relations
     *            the relations
     * @param name
     *            the name
     * @return the response
     */
    @PUT
    @Path("{name}")
    @Operation(
        description = "Добавить новые/обновить существующие",
        method = HttpMethod.PUT,
        requestBody = @RequestBody(content = @Content(array = @ArraySchema(schema = @Schema(implementation = RelationDefinition.class))), description = "Запрос на вставку"),
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = UpdateResponse.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response upsert(List<RelationDefinition> relations,
            @Parameter(in = ParameterIn.PATH) @PathParam("name") String name,
            @Parameter(description = "Черновик?,", in = ParameterIn.QUERY) @QueryParam("draft") @DefaultValue("false") boolean draft){
        metaDraftService.update(new UpdateModelRequestContextBuilder()
                .relationsUpdate(RelationDefinitionConverter.convert(relations))
                .build());
        return ok(new UpdateResponse(true, name));
    }

    /**
     * Delete.
     *
     * @param name
     *            the name
     * @return the response
     */
    @DELETE
    @Path("{name}")
    @Operation(
        description = "Удалить запись",
        method = HttpMethod.DELETE,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = String.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response delete(
            @Parameter(in = ParameterIn.PATH) @PathParam("name") String name,
            @Parameter(description = "Черновик?,", in = ParameterIn.QUERY) @QueryParam("draft") @DefaultValue("false") boolean draft) {

    	metaDraftService.remove(new DeleteModelRequestContextBuilder()
                .relationIds(Collections.singletonList(name)).build());
        return Response.accepted().build();
    }
}
