/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.meta.converter;

import java.util.ArrayList;
import java.util.List;

import org.unidata.mdm.meta.type.model.attributes.AttributeGroup;
import org.unidata.mdm.meta.type.model.entities.Entity;
import org.unidata.mdm.meta.type.model.entities.NestedEntity;
import org.unidata.mdm.meta.type.model.entities.Relation;
import org.unidata.mdm.meta.type.model.entities.RelationGroup;
import org.unidata.mdm.meta.dto.GetEntityDTO;
import org.unidata.mdm.rest.meta.ro.GroupsRO;
import org.unidata.mdm.rest.meta.ro.EntityDefinition;
import org.unidata.mdm.rest.meta.ro.RelationDefinition;
import org.unidata.mdm.rest.meta.ro.RelationGroupsRO;
import org.unidata.mdm.rest.meta.ro.RelationTypeRO;


/**
 * @author Michael Yashin. Created on 26.05.2015.
 */
public class EntityDefinitionConverter extends AbstractEntityDefinitionConverter {

    /**
     * Converts internal representation of an entity - {@link Entity} to a REST capable {@link EntityDefinition}.
     * @param input {@link GetEntityDTO} object
     * @return REST capable entity definition
     */
    public static EntityDefinition convert(GetEntityDTO input) {

        EntityDefinition result = new EntityDefinition();
        Entity source = input.getEntity();

        List<NestedEntity> refs = input.getRefs();
        List<Relation> relationDefs = input.getRelations();

        result.setGroupName(source.getGroupName());
        result.setValidityPeriod(PeriodBoundaryConverter.to(source.getValidityPeriod()));

        copyAbstractEntityData(source, result);

        result.getSimpleAttributes().addAll(toSimpleAttrs(source.getSimpleAttribute(), source.getName()));
        result.getArrayAttributes().addAll(toArrayAttrs(source.getArrayAttribute(), source.getName()));
        result.getComplexAttributes().addAll(to(source.getComplexAttribute(), refs, source.getName()));
        result.setCustomProperties(to(source.getCustomProperties()));

        convertAttributeGroups(input.getEntity().getAttributeGroups(), result.getAttributeGroups());
        convertRelationGroups(input.getEntity().getRelationGroups(), result.getRelationGroups());

        result.setDashboardVisible(source.isDashboardVisible());
        result.setMergeSettings(MergeSettingsConverter.to(source.getMergeSettings()));
        result.setExternalIdGenerationStrategy(ExternalIdGenerationStrategyConverter.to(source.getExternalIdGenerationStrategy()));

        List<RelationDefinition> targetRelationDefs = new ArrayList<>();
        for (Relation relationDef : relationDefs) {
            targetRelationDefs.add(RelationDefConverter.convert(relationDef));
        }

        result.setRelations(targetRelationDefs);

        return result;
    }


	/**
     * Convert Model object to Request object.
     * @param sourceAttributeGroups - will be used for filling
     * @param targetAttributeGroups - will be filled
     */
    private static void convertAttributeGroups(List<AttributeGroup> sourceAttributeGroups, List<GroupsRO> targetAttributeGroups) {
        for (AttributeGroup attributeGroup : sourceAttributeGroups) {
            GroupsRO attributeGroupRo = new GroupsRO()
                    .withColumn(attributeGroup.getColumn())
                    .withRow(attributeGroup.getRow())
                    .withTitle(attributeGroup.getTitle())
                    .withAttributes(attributeGroup.getAttributes());
            targetAttributeGroups.add(attributeGroupRo);
        }
    }

    /**
     * Convert Model object to Request object.
     * @param sourceRelationGroups - will be used for filling
     * @param targetRelationGroups - will be filled
     */
    private static void convertRelationGroups(List<RelationGroup> sourceRelationGroups, List<RelationGroupsRO> targetRelationGroups) {
        for (RelationGroup relationGroup : sourceRelationGroups) {
            RelationGroupsRO relationGroupRo = new RelationGroupsRO()
                    .withColumn(relationGroup.getColumn())
                    .withRow(relationGroup.getRow())
                    .withTitle(relationGroup.getTitle())
                    .withRelType(RelationTypeRO.fromValue(relationGroup.getRelType().value()))
                    .withRelations(relationGroup.getRelations());
            targetRelationGroups.add(relationGroupRo);
        }
    }

}
