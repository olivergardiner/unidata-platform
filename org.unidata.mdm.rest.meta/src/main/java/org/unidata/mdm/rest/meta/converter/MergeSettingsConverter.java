/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
/**
 * 
 */
package org.unidata.mdm.rest.meta.converter;

import org.unidata.mdm.meta.type.model.merge.BVRMergeType;
import org.unidata.mdm.meta.type.model.merge.BVTMergeType;
import org.unidata.mdm.meta.type.model.merge.MergeAttribute;
import org.unidata.mdm.meta.type.model.merge.MergeSettings;
import org.unidata.mdm.rest.meta.ro.BVRMergeSettingsDefRO;
import org.unidata.mdm.rest.meta.ro.BVTMergeSettingsDefRO;
import org.unidata.mdm.rest.meta.ro.MergeAttributeDefRO;
import org.unidata.mdm.rest.meta.ro.MergeSettingsRO;

import java.util.List;

/**
 * @author mikhail
 * Merge settings converter.
 */
public class MergeSettingsConverter {
	
	/**
	 * Converts to RO.
	 * @param source the conversion source
	 * @return RO
	 */
	public static MergeSettingsRO to(MergeSettings source) {
		
		if (source == null) {
			return null;
		}

		MergeSettingsRO target = new MergeSettingsRO();
		if (source.getBvrSettings() != null) {

			BVRMergeSettingsDefRO bvrRo = new BVRMergeSettingsDefRO();
			bvrRo.getSourceSystemsConfig().addAll(
					SourceSystemConverter.to(source.getBvrSettings().getSourceSystemsConfigs()));

			target.setBvrMergeSettings(bvrRo);
		}
		
		if (source.getBvtSettings() != null) {
			BVTMergeSettingsDefRO bvtRo = new BVTMergeSettingsDefRO();
			List<MergeAttribute> attributes = source.getBvtSettings().getAttributes();
			for (int i = 0; attributes != null && i < attributes.size(); i++) {
				MergeAttribute attDef = attributes.get(i);
				MergeAttributeDefRO attRo = new MergeAttributeDefRO();
				attRo.setName(attDef.getName());
				attRo.setSourceSystemsConfig(SourceSystemConverter.to(attDef.getSourceSystemsConfigs()));
				bvtRo.getAttributes().add(attRo);
			}
				
			target.setBvtMergeSettings(bvtRo);
		}
		
		return target;
	}
	
	/**
	 * Converts to system format.
	 * @param source the conversion source
	 * @return system object
	 */
	public static MergeSettings from(MergeSettingsRO source) {
		if (source == null) {
			return null;
		}
		
		MergeSettings target = new MergeSettings();
		if (source.getBvrMergeSettings() != null
		 && source.getBvrMergeSettings().getSourceSystemsConfig() != null) {

			BVRMergeType bvrType = new BVRMergeType();
			bvrType.getSourceSystemsConfigs().addAll(
					SourceSystemConverter.from(
							source.getBvrMergeSettings().getSourceSystemsConfig()));

			target.setBvrSettings(bvrType);
		}
		
		if (source.getBvtMergeSettings() != null) {
			BVTMergeType bvtType = new BVTMergeType();
			List<MergeAttributeDefRO> attributes = source.getBvtMergeSettings().getAttributes();
			for (int i = 0; attributes != null && i < attributes.size(); i++) {
				MergeAttributeDefRO attRo = attributes.get(i);
				MergeAttribute attDef = new MergeAttribute();
				attDef.setName(attRo.getName());
				attDef.setSourceSystemsConfigs(SourceSystemConverter.from(attRo.getSourceSystemsConfig()));
				bvtType.getAttributes().add(attDef);
			}
				
			target.setBvtSettings(bvtType);
		}

		return target;
	}
}
