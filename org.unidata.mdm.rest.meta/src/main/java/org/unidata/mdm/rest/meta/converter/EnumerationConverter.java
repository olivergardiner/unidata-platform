/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.meta.converter;

import java.util.List;

import org.unidata.mdm.meta.type.model.enumeration.EnumerationDataType;
import org.unidata.mdm.meta.type.model.enumeration.EnumerationValue;
import org.unidata.mdm.rest.meta.ro.EnumerationDefinitionRO;
import org.unidata.mdm.rest.meta.ro.EnumerationValueRO;

/**
 * The Class EnumerationConverter.
 */
public final class EnumerationConverter {

    private EnumerationConverter() { }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.springframework.core.convert.converter.Converter#convert(java.lang
     * .Object)
     */
    public static EnumerationDefinitionRO convert(EnumerationDataType source) {
        if (source == null) {
            return null;
        }
        EnumerationDefinitionRO target = new EnumerationDefinitionRO();
        target.setName(source.getName());
        target.setDisplayName(source.getDisplayName());
        List<EnumerationValue> enumerationValues = source.getEnumVal();
        for (EnumerationValue enumerationValue : enumerationValues) {
            target.addValue(convertEnumerationValue(enumerationValue));
        }
        return target;
    }

    /**
     * Convert from {@link EnumerationValue} to
     * {@link EnumerationValueRO}.
     *
     * @param source
     *            convert from
     * @return converted value
     */
    private static EnumerationValueRO convertEnumerationValue(EnumerationValue source) {
        if (source == null) {
            return null;
        }
        EnumerationValueRO target = new EnumerationValueRO();
        target.setName(source.getName());
        target.setDisplayName(source.getDisplayName());
        return target;
    }
}
