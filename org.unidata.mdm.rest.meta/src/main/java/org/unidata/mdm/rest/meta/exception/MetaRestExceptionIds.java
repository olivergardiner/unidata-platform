/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.meta.exception;

import org.unidata.mdm.system.exception.ExceptionId;

/**
 * @author Alexander Malyshev
 */
public final class MetaRestExceptionIds {
    private MetaRestExceptionIds() { }

    public static final ExceptionId EX_META_GROUP_NAME_OR_TITLE_ABSENT =
            new ExceptionId("EX_META_GROUP_NAME_OR_TITLE_ABSENT", "");

    public static final ExceptionId EX_META_ENTITY_NOT_FOUND =
            new ExceptionId("EX_META_ENTITY_NOT_FOUND", "app.meta.entityNotFound");

    public static final ExceptionId EX_META_LOOKUP_ENTITY_NOT_FOUND =
            new ExceptionId("EX_META_LOOKUP_ENTITY_NOT_FOUND", "app.meta.lookupEntityNotFound");

    public static final ExceptionId EX_MEASUREMENT_IMPORT_TYPE_UNSUPPORTED =
            new ExceptionId("EX_MEASUREMENT_IMPORT_TYPE_UNSUPPORTED", "app.measurement.content.type.unsupported");

    public static final ExceptionId EX_MEASUREMENT_IMPORT_EMPTY =
            new ExceptionId("EX_MEASUREMENT_IMPORT_EMPTY", "app.measurement.empty.import.data");

    public static final ExceptionId EX_MEASUREMENT_MARSHAL_FAILED =
            new ExceptionId("EX_MEASUREMENT_MARSHAL_FAILED", "app.measurement.marshal.failed");

    public static final ExceptionId EX_META_IMPORT_MODEL_INVALID_CONTENT_TYPE =
            new ExceptionId("EX_META_IMPORT_MODEL_INVALID_CONTENT_TYPE", "app.meta.importModelInvalidContentType");

    public static final ExceptionId EX_META_SOURCE_SYSTEM_ALREADY_EXISTS =
            new ExceptionId("EX_META_SOURCE_SYSTEM_ALREADY_EXISTS", "app.meta.sourceSystemAlreadyExists");

    public static final ExceptionId EX_META_IMPORT_MODEL_INVALID_FILE_FORMAT =
            new ExceptionId("EX_META_IMPORT_MODEL_INVALID_FILE_FORMAT", "app.meta.enum.unrecognized");

    public static final ExceptionId EX_ENUM_WRONG_VALUE =
            new ExceptionId("EX_ENUM_WRONG_VALUE", "app.meta.importModelInvalidFileFormat");
}
