package org.unidata.mdm.rest.meta.ro;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.unidata.mdm.meta.type.input.meta.MetaVertex;


/**
 * The Class MetaVertexROToDTOConverter.
 *
 * @author ilya.bykov
 */
public class MetaVertexROToDTOConverter {

    private MetaVertexROToDTOConverter() {
    }

    /**
     * Convert.
     *
     * @param source the source
     * @return the list
     */
    public static List<MetaVertex> convert(List<MetaVertexRO> source) {
        if (source == null) {
            return Collections.emptyList();
        }
        List<MetaVertex> target = new ArrayList<>();
        for (MetaVertexRO s : source) {
            target.add(convert(s));
        }
        return target;

    }

    /**
     * Convert.
     *
     * @param source the source
     * @return the meta vertex
     */
    public static MetaVertex convert(MetaVertexRO source) {
        if (source == null) {
            return null;
        }


		return new MetaVertex.MetaVertexBuilder()
				.id(source.getId())
				.displayName(source.getDisplayName())
				.type(MetaTypeROToDTOConverter.convert(source.getType()))
				.action(MetaActionROToDTOConverter.convert(source.getAction()))
				.status(MetaExistenceROToDTOConverter.convert(source.getExistence()))
				.build();

    }
}
