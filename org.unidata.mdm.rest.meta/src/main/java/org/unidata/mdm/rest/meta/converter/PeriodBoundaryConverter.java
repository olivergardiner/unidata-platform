/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
/**
 *
 */
package org.unidata.mdm.rest.meta.converter;

import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.unidata.mdm.meta.type.model.DateGranularityMode;
import org.unidata.mdm.meta.type.model.PeriodBoundary;
import org.unidata.mdm.meta.util.ValidityPeriodUtils;
import org.unidata.mdm.rest.meta.ro.PeriodBoundaryDefinitionRO;

/**
 * @author Mikhail Mikhailov
 * Validity period boundary converter.
 */
public class PeriodBoundaryConverter {

    /**
     * Constructor.
     */
    private PeriodBoundaryConverter() {
        super();
    }

    /**
     * The 'To' converting method.
     * @param source the source
     * @return REST target
     */
    public static PeriodBoundaryDefinitionRO to(PeriodBoundary source) {

        if (Objects.isNull(source)) {
            return null;
        }

        PeriodBoundaryDefinitionRO target = new PeriodBoundaryDefinitionRO();

        target.setEnd(source.getEnd());
        target.setStart(source.getStart());
        target.setMode(source.getMode() == null ? ValidityPeriodUtils.getGlobalDateGranularityMode().name() : source.getMode().name());

        return target;
    }

    /**
     * The 'From' converting method.
     * @param source the source
     * @return XML target
     */
    public static PeriodBoundary from(PeriodBoundaryDefinitionRO source) {
        PeriodBoundary result = null;
        if (source != null) {
            result = new PeriodBoundary()
                .withEnd(source.getEnd())
                .withStart(source.getStart())
                .withMode(StringUtils.isBlank(source.getMode()) ? ValidityPeriodUtils.getGlobalDateGranularityMode() : DateGranularityMode.fromValue(source.getMode()));
        }
        return result;
    }
}
