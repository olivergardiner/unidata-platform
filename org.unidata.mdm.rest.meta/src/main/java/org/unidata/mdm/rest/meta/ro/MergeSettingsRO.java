/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.meta.ro;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author mikhail
 * Merge settings aggregating type.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MergeSettingsRO {
	/**
	 * BVT merge settings.
	 */
	private BVTMergeSettingsDefRO bvtMergeSettings;
	
	/**
	 * BVR merge settings.
	 */
	private BVRMergeSettingsDefRO bvrMergeSettings;

	/**
	 * @return the bvtMergeSettings
	 */
	public BVTMergeSettingsDefRO getBvtMergeSettings() {
		return bvtMergeSettings;
	}

	/**
	 * @param bvtMergeSettings the bvtMergeSettings to set
	 */
	public void setBvtMergeSettings(BVTMergeSettingsDefRO bvtMergeSettings) {
		this.bvtMergeSettings = bvtMergeSettings;
	}

	/**
	 * @return the bvrMergeSettings
	 */
	public BVRMergeSettingsDefRO getBvrMergeSettings() {
		return bvrMergeSettings;
	}

	/**
	 * @param bvrMergeSettings the bvrMergeSettings to set
	 */
	public void setBvrMergeSettings(BVRMergeSettingsDefRO bvrMergeSettings) {
		this.bvrMergeSettings = bvrMergeSettings;
	}
}
