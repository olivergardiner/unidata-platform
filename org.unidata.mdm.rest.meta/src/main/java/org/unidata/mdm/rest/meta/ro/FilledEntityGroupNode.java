/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.meta.ro;

import java.util.Collection;

public class FilledEntityGroupNode extends EntityGroupNode {

    private Collection<GroupEntityDefinition> lookupEntities;

    private Collection<GroupEntityDefinition> entities;

    public Collection<GroupEntityDefinition> getLookupEntities() {
        return lookupEntities;
    }

    public FilledEntityGroupNode(String title, String groupName, Collection<GroupEntityDefinition> lookupEntities, Collection<GroupEntityDefinition> entities) {
        super(title, groupName);
        this.lookupEntities = lookupEntities;
        this.entities = entities;
    }

    public FilledEntityGroupNode() {
    }

    public void setLookupEntities(Collection<GroupEntityDefinition> lookupEntities) {
        this.lookupEntities = lookupEntities;
    }

    public Collection<GroupEntityDefinition> getEntities() {
        return entities;
    }

    public void setEntities(Collection<GroupEntityDefinition> entities) {
        this.entities = entities;
    }
}
