/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.meta.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.unidata.mdm.meta.type.model.enumeration.EnumerationDataType;
import org.unidata.mdm.meta.service.MetaDraftService;
import org.unidata.mdm.meta.service.MetaModelService;
import org.unidata.mdm.rest.meta.converter.EnumerationConverter;
import org.unidata.mdm.rest.meta.ro.EnumerationDefinitionRO;
import org.unidata.mdm.rest.system.ro.ErrorResponse;
import org.unidata.mdm.rest.system.service.AbstractRestService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

/**
 * @author Michael Yashin. Created on 19.05.2015.
 */
@Path(EnumerationRestService.SERVICE_PATH)
@Consumes({ "application/json" })
@Produces({ "application/json" })
public class EnumerationRestService extends AbstractRestService {

    public static final String SERVICE_PATH = "enumerations";

    @Autowired
    private MetaDraftService metaDraftService;
    @Autowired
    private MetaModelService metaModelService;

    @GET
    @Operation(
        description = "Список перечислений",
        method = HttpMethod.GET,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = List.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response findAll(
            @Parameter(description = "Черновик?,", in = ParameterIn.QUERY) @QueryParam("draft") @DefaultValue("false") boolean draft) {

		List<EnumerationDataType> result;
		if (draft) {
			result = metaDraftService.getEnumerationsList();
		} else {
			result = metaModelService.getEnumerationsList();
		}

        List<EnumerationDefinitionRO> target = result.stream()
                .map(EnumerationConverter::convert)
                .collect(Collectors.toList());

        return ok(target);
    }

}
