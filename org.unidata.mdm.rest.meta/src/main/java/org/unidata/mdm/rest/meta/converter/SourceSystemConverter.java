/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.meta.converter;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.unidata.mdm.meta.type.model.SourceSystem;
import org.unidata.mdm.rest.meta.ro.SourceSystemDefinition;
import org.unidata.mdm.rest.meta.ro.SourceSystemList;

/**
 * The Class SourceSystemConverter. Convert instance of {@see SourceSystemDef}
 * to {@see SourceSystemDefinition}.
 *
 */
public class SourceSystemConverter {

    private static final Comparator<SourceSystemDefinition> COMPARATOR = (o1, o2) ->
            String.CASE_INSENSITIVE_ORDER.compare(o1.getName(),o2.getName());

    /*
     * (non-Javadoc)
     *
     * @see
     * org.springframework.core.convert.converter.Converter#convert(java.lang
     * .Object)
     */
    public static SourceSystemList convert(List<SourceSystem> source) {
        if (source == null) {
            return null;
        }

        String adminSystemName = null;
        SourceSystemList target = new SourceSystemList();
        for (SourceSystem sourceSystemDef : source) {
            target.addSourceSystem(SourceSystemConverter.to(sourceSystemDef));
            if (sourceSystemDef.isAdmin()) {
                adminSystemName = sourceSystemDef.getName();
            }
        }
        target.setAdminSystemName(adminSystemName);
        Collections.sort(target.getSourceSystem(), COMPARATOR);
        return target;
    }

    /**
     * Convert instance of {@see SourceSystemDef} to {@see
     * SourceSystemDefinition}.
     *
     * @param source
     *            the source
     * @return the source system definition
     */
    public static SourceSystemDefinition to(SourceSystem source) {
        if (source == null) {
            return null;
        }
        SourceSystemDefinition target = new SourceSystemDefinition();
        target.setName(source.getName());
        target.setDescription(source.getDescription());
        target.setWeight(source.getWeight() == null ? 0 : source.getWeight()
        	.intValue());
        target.setCustomProperties(AbstractEntityDefinitionConverter.to(source.getCustomProperties()));
        return target;
    }

    /**
     * Convert instance of {@see SourceSystemDef} to {@see
     * SourceSystemDefinition}.
     *
     * @param source
     *            the source
     * @return the source system definition
     */
    public static List<SourceSystemDefinition> to(List<SourceSystem> source) {
        if (source == null || source.isEmpty()) {
            return Collections.emptyList();
        }

        List<SourceSystemDefinition> target = new ArrayList<>();
        for (SourceSystem s : source) {
        	target.add(to(s));
        }

        return target;
    }

    /**
     * Converts from RO to system.
     * @param source the source
     * @return system object
     */
    public static SourceSystem from(SourceSystemDefinition source) {
        SourceSystem result = null;
    	if (source != null) {
            result = new SourceSystem()
                .withName(source.getName())
                .withDescription(source.getDescription())
                .withWeight(BigInteger.valueOf(source.getWeight()))
                .withCustomProperties(ToCustomPropertyDefConverter.convert(source.getCustomProperties()));
        }
    	return result;
    }

    /**
     * Convert instance of {@see SourceSystemDef} to {@see
     * SourceSystemDefinition}.
     *
     * @param source
     *            the source
     * @return the source system definition
     */
    public static List<SourceSystem> from(List<SourceSystemDefinition> source) {
        if (source == null || source.isEmpty()) {
            return Collections.emptyList();
        }

        List<SourceSystem> target = new ArrayList<>();
        for (SourceSystemDefinition s : source) {
        	target.add(from(s));
        }

        return target;
    }
}
