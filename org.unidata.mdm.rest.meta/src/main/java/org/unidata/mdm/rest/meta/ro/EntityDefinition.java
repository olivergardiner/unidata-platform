/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.meta.ro;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.JsonNode;
import org.unidata.mdm.rest.system.ro.RestInputSource;
import org.unidata.mdm.rest.system.ro.RestOutputSink;

/**
 * @author Michael Yashin. Created on 25.05.2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class EntityDefinition extends NestedEntityDefinition implements RestInputSource, RestOutputSink {
    /**
     * Has some data already or not.
     */
    protected boolean hasData;
    protected String modelName;
    /**
     * Relations if any.
     */
    protected List<RelationDefinition> relations = new ArrayList<>();
    /**
     * Merge settings.
     */
    protected MergeSettingsRO mergeSettings;
    /**
     * Attribute groups
     */
    protected List<GroupsRO> attributeGroups = new ArrayList<>();
    /**
     * Relation groups
     */
    protected List<RelationGroupsRO> relationGroups = new ArrayList<>();
    /**
     * Is visible on statistic dashboard.
     */
    protected boolean isDashboardVisible;
    /**
     * Name of group where this entity contains
     */
    protected String groupName;

    /**
     * Customized for this register validity period.
     */
    protected PeriodBoundaryDefinitionRO validityPeriod;
    /**
     * Ext. ID generation strategy.
     */
    protected ExternalIdGenerationStrategyRO externalIdGenerationStrategy;

    private final Map<String, JsonNode> any = new HashMap<>();
    /**
     * @return the hasData
     */
    public boolean isHasData() {
        return hasData;
    }

    /**
     * @param hasData the hasData to set
     */
    public void setHasData(boolean hasData) {
        this.hasData = hasData;
    }

    public List<RelationDefinition> getRelations() {
        return relations;
    }

    public void setRelations(List<RelationDefinition> relations) {
        this.relations = relations;
    }

    public MergeSettingsRO getMergeSettings() {
        return mergeSettings;
    }

    public void setMergeSettings(MergeSettingsRO mergeSettings) {
        this.mergeSettings = mergeSettings;
    }

    public boolean isDashboardVisible() {
        return isDashboardVisible;
    }

    public void setDashboardVisible(boolean isDashboardVisible) {
        this.isDashboardVisible = isDashboardVisible;
    }

	public PeriodBoundaryDefinitionRO getValidityPeriod() {
		return validityPeriod;
	}

	public void setValidityPeriod(PeriodBoundaryDefinitionRO validityPeriod) {
		this.validityPeriod = validityPeriod;
	}

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public List<GroupsRO> getAttributeGroups() {
        return attributeGroups;
    }

    public void setAttributeGroups(List<GroupsRO> attributeGroups) {
        this.attributeGroups = attributeGroups;
    }

    public List<RelationGroupsRO> getRelationGroups() {
        return relationGroups;
    }

    public void setRelationGroups(List<RelationGroupsRO> relationGroups) {
        this.relationGroups = relationGroups;
    }

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}
    /**
     * @return the externalIdGenerationStrategy
     */
    public ExternalIdGenerationStrategyRO getExternalIdGenerationStrategy() {
        return externalIdGenerationStrategy;
    }

    /**
     * @param externalIdGenerationStrategy the externalIdGenerationStrategy to set
     */
    public void setExternalIdGenerationStrategy(ExternalIdGenerationStrategyRO externalIdGenerationStrategy) {
        this.externalIdGenerationStrategy = externalIdGenerationStrategy;
    }


    @Override
    @JsonAnyGetter
    public Map<String, JsonNode> getAny() {
        return any;
    }

    @Override
    @JsonAnySetter
    public void setAny(String name, JsonNode value) {
        any.put(name, value);
    }

}
