/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.meta.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.unidata.mdm.meta.type.model.entities.Entity;
import org.unidata.mdm.meta.type.model.entities.LookupEntity;
import org.unidata.mdm.meta.type.model.entities.AbstractEntity;
import org.unidata.mdm.meta.type.model.merge.MergeAttribute;
import org.unidata.mdm.meta.type.model.merge.MergeSettings;
import org.unidata.mdm.meta.type.model.entities.NestedEntity;
import org.unidata.mdm.meta.type.model.SourceSystem;
import org.unidata.mdm.meta.context.DeleteModelRequestContext.DeleteModelRequestContextBuilder;
import org.unidata.mdm.meta.context.UpdateModelRequestContext.UpdateModelRequestContextBuilder;
import org.unidata.mdm.meta.service.MetaDraftService;
import org.unidata.mdm.meta.service.MetaModelService;
import org.unidata.mdm.meta.service.impl.facades.AbstractModelElementFacade;
import org.unidata.mdm.rest.meta.converter.SourceSystemConverter;
import org.unidata.mdm.rest.meta.exception.MetaRestExceptionIds;
import org.unidata.mdm.rest.meta.ro.SourceSystemDefinition;
import org.unidata.mdm.rest.meta.ro.SourceSystemList;
import org.unidata.mdm.rest.system.ro.ErrorResponse;
import org.unidata.mdm.rest.system.ro.UpdateResponse;
import org.unidata.mdm.rest.system.service.AbstractRestService;
import org.unidata.mdm.system.exception.PlatformBusinessException;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

/**
 * Source system service.
 *
 * @author Michael Yashin. Created on 19.05.2015.
 */
@Path(SourceSystemRestService.SERVICE_PATH)
@Consumes({"application/json"})
@Produces({"application/json"})
public class SourceSystemRestService extends AbstractRestService {

    public static final String SERVICE_PATH = "source-systems";

    /**
     * The metamodel service.
     */
    @Autowired
    private MetaModelService metaModelService;
    /**
     * Meta draft service.
     */
    @Autowired
    private MetaDraftService metaDraftService;

    /**
     * Load and return list of all source systems.
     *
     * @return list of source systems. {@see SourceSystemList}.
     */
    @GET
    @Operation(
        description = "вернуть список всех систем",
        method = HttpMethod.GET,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = SourceSystemList.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response findAll(@Parameter(description = "Черновик?,", in = ParameterIn.QUERY) @QueryParam("draft") @DefaultValue("false") boolean draft) {
     	List<SourceSystem> source;
    	if(draft) {
    		source = metaDraftService.getSourceSystemsList();

    	}else {
    		source = metaModelService.getSourceSystemsList();
    	}
        SourceSystemList target = SourceSystemConverter.convert(source);
        target.setAdminSystemName(metaModelService.getAdminSourceSystem().getName());
        return ok(target);
    }

    /**
     * Creates new source system.
     *
     * @param req Source system definition.
     * @return HTTP response.
     */
    @POST
    @Operation(
        description = "Создать новую систему",
        method = HttpMethod.POST,
        requestBody = @RequestBody(content = @Content(schema = @Schema(implementation = SourceSystemDefinition.class)), description = "Запрос на вставку"),
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = UpdateResponse.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response create(final SourceSystemDefinition req,
            @Parameter(description = "Черновик?,", in = ParameterIn.QUERY) @QueryParam("draft") @DefaultValue("false") boolean draft) {
        validateSourceSystemBeforeCreate(req, draft);

        UpdateModelRequestContextBuilder updateModelRequestContextBuilder = new UpdateModelRequestContextBuilder();
        SourceSystem newSourceSystem = SourceSystemConverter.from(req);

        AbstractModelElementFacade.validateCustomProperties(newSourceSystem.getCustomProperties());

        updateModelRequestContextBuilder.sourceSystemsUpdate(Collections.singletonList(newSourceSystem));

        // Update entity defs UN-3053
        List<Entity> entitiesForUpdate = null;
		if (draft) {
			entitiesForUpdate = buildEntitiesForCreateNewSourceSystem(newSourceSystem,
					metaDraftService.getEntitiesList());
		} else {
			entitiesForUpdate = buildEntitiesForCreateNewSourceSystem(newSourceSystem,
					metaModelService.getEntitiesList());
		}
        if (CollectionUtils.isNotEmpty(entitiesForUpdate)) {
            updateModelRequestContextBuilder.entityUpdate(entitiesForUpdate);

            List<NestedEntity> nestedEntityDefs = extractNestedEntities(entitiesForUpdate);
            if (CollectionUtils.isNotEmpty(nestedEntityDefs)) {
                updateModelRequestContextBuilder.nestedEntityUpdate(nestedEntityDefs);
            }
        }
        // Update lookup entity defs UN-3053
        List<LookupEntity> lookupEntitiesForUpdate = null;
		if (draft) {
			lookupEntitiesForUpdate = buildEntitiesForCreateNewSourceSystem(newSourceSystem,
					metaDraftService.getLookupEntitiesList());
		} else {
			lookupEntitiesForUpdate = buildEntitiesForCreateNewSourceSystem(newSourceSystem,
					metaModelService.getLookupEntitiesList());
		}

        if (CollectionUtils.isNotEmpty(lookupEntitiesForUpdate)) {
            updateModelRequestContextBuilder.lookupEntityUpdate(lookupEntitiesForUpdate);
        }
		if (draft) {
			metaDraftService.update(updateModelRequestContextBuilder.build());
		} else {
			updateModelRequestContextBuilder.direct(true);
			metaModelService.upsertModel(updateModelRequestContextBuilder.build());
		}
        // needed for sencha
        return Response.accepted(new UpdateResponse(true, req.getName())).build();
    }

    private List<NestedEntity> extractNestedEntities(List<Entity> entitiesForUpdate) {
        Map<String, NestedEntity> nestedEntitiesMap = new HashMap<>();
        entitiesForUpdate.forEach(entityDef -> {
            List<NestedEntity> nestedEntities = metaModelService.getNestedEntitiesByTopLevelId(entityDef.getName());
            if (CollectionUtils.isNotEmpty(nestedEntities)) {
                nestedEntities.forEach(nestedEntity -> nestedEntitiesMap.put(nestedEntity.getName(), nestedEntity));
            }
        });
        return new ArrayList<>(nestedEntitiesMap.values());
    }

    private void validateSourceSystemBeforeCreate(SourceSystemDefinition req, boolean isDraft) {
        //TODO: move validation from the rest service, currently not possible to distinguish update and create on metamodel level.
        SourceSystemList current = null;
		if (isDraft) {
			current = SourceSystemConverter.convert(metaDraftService.getSourceSystemsList());
		} else {
			current = SourceSystemConverter.convert(metaModelService.getSourceSystemsList());
		}
        if (current.getSourceSystem().stream().anyMatch(s -> StringUtils.equals(req.getName(), s.getName()))) {
            throw new PlatformBusinessException(
                    "Source system with this name already exists!",
                    MetaRestExceptionIds.EX_META_SOURCE_SYSTEM_ALREADY_EXISTS
            );
        }
        //end todo
    }

    /**
     * get entity defs with custom merge settings for add new sourceSystem
     *
     * @param newSourceSystem new source system
     * @param entityDefList   current entity defs list
     * @return entity defs list with added new source system
     */
    private <T extends AbstractEntity> List<T> buildEntitiesForCreateNewSourceSystem(SourceSystem newSourceSystem, List<T> entityDefList) {
        List<T> result = new ArrayList<>();
        for (T entityDef : entityDefList) {
            if (entityDef.getMergeSettings() != null) {
                boolean needUpdate = false;

                MergeSettings mergeSettings = entityDef.getMergeSettings();
                if (mergeSettings.getBvrSettings()!=null
                		&& CollectionUtils.isNotEmpty(mergeSettings.getBvrSettings().getSourceSystemsConfigs())
                        && mergeSettings.getBvrSettings().getSourceSystemsConfigs()
                        .stream()
                        .noneMatch(sourceSystemDef -> sourceSystemDef.getName().equals(newSourceSystem.getName()))) {
                    entityDef.getMergeSettings().getBvrSettings().getSourceSystemsConfigs().add(newSourceSystem);
                    needUpdate = true;
                }

                if (mergeSettings.getBvtSettings() != null
                        && CollectionUtils.isNotEmpty(mergeSettings.getBvtSettings().getAttributes())) {
                    for (MergeAttribute mergeAttributeDef : mergeSettings.getBvtSettings().getAttributes()) {
                        if (CollectionUtils.isNotEmpty(mergeAttributeDef.getSourceSystemsConfigs())
                                && mergeAttributeDef.getSourceSystemsConfigs()
                                .stream()
                                .noneMatch(sourceSystemDef -> sourceSystemDef.getName().equals(newSourceSystem.getName()))) {
                            mergeAttributeDef.getSourceSystemsConfigs().add(newSourceSystem);
                            needUpdate = true;
                        }
                    }
                }

                if (needUpdate) {
                    result.add(entityDef);
                }
            }
        }
        return result;
    }

    /**
     * Delete existing source system.
     *
     * @param sourceSystemName the source system name
     * @return HTTP response.
     */
    @DELETE
    @Path("{sourceSystemName}")
    @Operation(
        description = "Удалить существующую систему",
        method = HttpMethod.DELETE,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = String.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response delete(
            @Parameter(description = "Идентификатор,", in = ParameterIn.PATH) @PathParam(value = "sourceSystemName") String sourceSystemName,
            @Parameter(description = "Черновик?,", in = ParameterIn.QUERY) @QueryParam("draft") @DefaultValue("false") boolean draft) {

        DeleteModelRequestContextBuilder deleteModelRequestContextBuilder = new DeleteModelRequestContextBuilder();
        deleteModelRequestContextBuilder.sourceSystemIds(Collections.singletonList(sourceSystemName)).build();
        metaDraftService.remove(deleteModelRequestContextBuilder.build());
        return Response.ok().build();
    }

    /**
     * Updates existing source system.
     *
     * @param sourceSystemName the source system name
     * @param req              Updated source system. {@see SourceSystemDefinition}
     * @return HTTP response.
     */
    @PUT
    @Path("{sourceSystemName}")
    @Operation(
        description = "Обновить существующую систему",
        method = HttpMethod.PUT,
        requestBody = @RequestBody(content = @Content(schema = @Schema(implementation = SourceSystemDefinition.class)), description = "Запрос на вставку"),
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = UpdateResponse.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response update(
            @Parameter(description = "Идентификатор,", in = ParameterIn.PATH) @PathParam("sourceSystemName") String sourceSystemName,
            @Parameter(description = "Черновик?,", in = ParameterIn.QUERY) @QueryParam("draft") @DefaultValue("false") boolean draft,
            SourceSystemDefinition req) {

        UpdateModelRequestContextBuilder updateModelRequestContextBuilder = new UpdateModelRequestContextBuilder();
        final SourceSystem sourceSystemDef = SourceSystemConverter.from(req);

        AbstractModelElementFacade.validateCustomProperties(sourceSystemDef.getCustomProperties());

        updateModelRequestContextBuilder.sourceSystemsUpdate(Collections.singletonList(sourceSystemDef));

        if (!StringUtils.equals(sourceSystemName, req.getName())) {
            DeleteModelRequestContextBuilder deleteModelRequestContextBuilder = new DeleteModelRequestContextBuilder();
            deleteModelRequestContextBuilder.sourceSystemIds(Collections.singletonList(sourceSystemName)).build();
            metaDraftService.remove(deleteModelRequestContextBuilder.build());
        }

        metaDraftService.update(updateModelRequestContextBuilder.build());
        // needed for sencha
        return Response.accepted(new UpdateResponse(true, req.getName())).build();
    }

    /**
     * Gets existing source system by name.
     *
     * @param sourceSystemName the source system name
     * @return Source system definition. {@see SourceSystemDefinition}
     */
    @GET
    @Path("{sourceSystemName}")
    @Operation(
        description = "Вернуть существующую систему",
        method = HttpMethod.GET,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = SourceSystemDefinition.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response get(
            @Parameter(description = "Идентификатор,", in = ParameterIn.PATH) @PathParam("sourceSystemName") String sourceSystemName,
            @Parameter(description = "Черновик?,", in = ParameterIn.QUERY) @QueryParam("draft") @DefaultValue("false") boolean draft) {
    	List<SourceSystem> source;
    	if(draft) {
    		source = metaDraftService.getSourceSystemsList();
    	}else {
    		source = metaModelService.getSourceSystemsList();
    	}
        SourceSystemList target = SourceSystemConverter.convert(source);
        SourceSystemDefinition result = new SourceSystemDefinition();
        target.getSourceSystem().stream().filter(e -> StringUtils.equals(sourceSystemName, e.getName())).forEach(e -> {
            result.setDescription(e.getDescription());
            result.setName(e.getName());
            result.setWeight(e.getWeight());
            result.setCustomProperties(e.getCustomProperties());
        });
        return Response.ok(result).build();
    }
}
