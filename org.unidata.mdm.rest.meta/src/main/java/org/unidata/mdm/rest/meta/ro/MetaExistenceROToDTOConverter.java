package org.unidata.mdm.rest.meta.ro;


import org.unidata.mdm.meta.type.input.meta.MetaExistence;

/**
 * The Class MetaStatusROToDTOConverter.
 * @author ilya.bykov
 */
public class MetaExistenceROToDTOConverter {

	/**
	 * Convert.
	 *
	 * @param source
	 *            the source
	 * @return the meta type
	 */
	public static MetaExistence convert(MetaExistenceRO source) {
		if (source == null) {
			return null;
		}
		return MetaExistence.valueOf(source.name());
	}
}
