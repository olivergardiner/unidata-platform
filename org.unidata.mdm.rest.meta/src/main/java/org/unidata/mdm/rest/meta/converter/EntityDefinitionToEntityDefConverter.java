/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.meta.converter;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.unidata.mdm.meta.type.model.MetaModelAttribute;
import org.unidata.mdm.meta.type.model.attributes.AttributeGroup;
import org.unidata.mdm.meta.type.model.attributes.ComplexMetaModelAttribute;
import org.unidata.mdm.meta.type.model.entities.Entity;
import org.unidata.mdm.meta.type.model.entities.NestedEntity;
import org.unidata.mdm.meta.type.model.entities.RelType;
import org.unidata.mdm.meta.type.model.entities.Relation;
import org.unidata.mdm.meta.type.model.entities.RelationGroup;
import org.unidata.mdm.meta.context.UpdateModelRequestContext;
import org.unidata.mdm.meta.context.UpdateModelRequestContext.UpdateModelRequestContextBuilder;
import org.unidata.mdm.meta.type.model.entities.AbstractEntity;
import org.unidata.mdm.rest.meta.ro.AbstractEntityDefinition;
import org.unidata.mdm.rest.meta.ro.ComplexAttributeDefinition;
import org.unidata.mdm.rest.meta.ro.EntityDefinition;
import org.unidata.mdm.rest.meta.ro.GroupsRO;
import org.unidata.mdm.rest.meta.ro.NestedEntityDefinition;
import org.unidata.mdm.rest.meta.ro.RelationDefinition;
import org.unidata.mdm.rest.meta.ro.RelationGroupsRO;
import org.unidata.mdm.rest.system.ro.AbstractAttributeDefinition;

/**
 *
 * @author Michael Yashin. Created on 26.05.2015.
 */
public final class EntityDefinitionToEntityDefConverter {

    private EntityDefinitionToEntityDefConverter() { }

    /**
     * Converts a REST entity definition DTO into internal format.
     *
     * @param source
     *            REST DTO
     * @return request context
     */
    public static UpdateModelRequestContextBuilder convert(EntityDefinition source, boolean draft) {

        Entity result = new Entity();
        Map<String, NestedEntity> nestedEntities = new HashMap<>();

        copyAbstractEntityData(source, result);
        SimpleAttributeDefConverter.copySimpleAttributeDataList(source.getSimpleAttributes(), result.getSimpleAttribute());
        ArrayAttributeDefConverter.copySimpleAttributeDataList(source.getArrayAttributes(), result.getArrayAttribute());
        copyComplexAttributeDataList(source.getComplexAttributes(), result.getComplexAttribute(), nestedEntities);

        result.setDashboardVisible(source.isDashboardVisible());
        result.setGroupName(source.getGroupName());
        result.setValidityPeriod(PeriodBoundaryConverter.from(source.getValidityPeriod()));
        result.setMergeSettings(MergeSettingsConverter.from(source.getMergeSettings()));
        result.setExternalIdGenerationStrategy(ExternalIdGenerationStrategyConverter.from(source.getExternalIdGenerationStrategy()));

        if (source.getAttributeGroups() != null) {
            convertAttributeGroups(source.getAttributeGroups(), result.getAttributeGroups());
        }

        if (source.getRelationGroups() != null) {
            convertRelationGroups(source.getRelationGroups(), result.getRelationGroups());
        }


        List<RelationDefinition> sourceDefs = source.getRelations();
        List<Relation> targetDefs = new ArrayList<>();

        for (RelationDefinition relationDefinition : sourceDefs) {
            targetDefs.add(RelationDefinitionConverter.convert(relationDefinition));
        }


        return UpdateModelRequestContext.builder()
                .entityUpdate(Collections.singletonList(result))
                .nestedEntityUpdate(new ArrayList<>(nestedEntities.values()))
                .relationsUpdate(targetDefs)
                .draft(draft);
    }

	/**
     * Convert Request object to Model object.
     * @param sourceAttributeGroups - will be used for filling
     * @param targetAttributeGroups - will be filled
     */
    private static void convertAttributeGroups(List<GroupsRO> sourceAttributeGroups, List<AttributeGroup> targetAttributeGroups) {
        for (GroupsRO attributeGroup : sourceAttributeGroups) {
            AttributeGroup attributeGroupDef = new AttributeGroup()
                    .withColumn(attributeGroup.getColumn())
                    .withRow(attributeGroup.getRow())
                    .withTitle(attributeGroup.getTitle())
                    .withAttributes(attributeGroup.getAttributes());
            targetAttributeGroups.add(attributeGroupDef);
        }
    }

    /**
     * Convert Request object to Model object.
     * @param sourceRelationGroups - will be used for filling
     * @param targetRelationGroups - will be filled
     */
    private static void convertRelationGroups(List<RelationGroupsRO> sourceRelationGroups, List<RelationGroup> targetRelationGroups) {
        for (RelationGroupsRO relationGroup : sourceRelationGroups) {
            RelationGroup relationGroupDef = new RelationGroup()
                    .withColumn(relationGroup.getColumn())
                    .withRow(relationGroup.getRow())
                    .withTitle(relationGroup.getTitle())
                    .withRelType(RelType.fromValue(relationGroup.getRelType()))
                    .withRelations(relationGroup.getRelations());
            targetRelationGroups.add(relationGroupDef);
        }
    }

    /**
     * Converts complex attributes from REST to intenal.
     *
     * @param source            REST type
     * @param targetList the target list
     * @param nestedEntities the model
     */
    private static void convertComplexAttribute(
            ComplexAttributeDefinition source,
            List<ComplexMetaModelAttribute> targetList,
            Map<String, NestedEntity> nestedEntities
    ) {


        ComplexMetaModelAttribute result = new ComplexMetaModelAttribute();

        copyAbstractAttributeData(source, result);

        if (source.getMinCount() != null) {
            result.setMinCount(BigInteger.valueOf(source.getMinCount()));
        }
        if (source.getMaxCount() != null) {
            result.setMaxCount(BigInteger.valueOf(source.getMaxCount()));
        }

        result.setOrder(source.getOrder());
        result.setSubEntityKeyAttribute(source.getSubEntityKeyAttribute());

        if (source.getNestedEntity() != null
        && !nestedEntities.containsKey(source.getNestedEntity().getName())) {
            NestedEntity nested = convertNestedEntity(source.getNestedEntity(), nestedEntities);
            nestedEntities.put(nested.getName(), nested);
            result.setNestedEntityName(nested.getName());
        }

        targetList.add(result);
    }

    /**
     * Converts nested entity from REST to internal.
     *
     * @param source REST
     * @param nestedEntities model
     * @return internal
     */
    private static NestedEntity convertNestedEntity(NestedEntityDefinition source, Map<String, NestedEntity> nestedEntities) {
        NestedEntity result = new NestedEntity();

        copyAbstractEntityData(source, result);
        SimpleAttributeDefConverter.copySimpleAttributeDataList(source.getSimpleAttributes(), result.getSimpleAttribute());
        ArrayAttributeDefConverter.copySimpleAttributeDataList(source.getArrayAttributes(), result.getArrayAttribute());
        copyComplexAttributeDataList(source.getComplexAttributes(), result.getComplexAttribute(), nestedEntities);

        return result;
    }

    /**
     * Copy abstract entity definition from REST to internal.
     *
     * @param source
     *            REST source
     * @param target
     *            internal
     */
    private static void copyAbstractEntityData(AbstractEntityDefinition source, AbstractEntity target) {
        target.setName(source.getName());
        target.setDisplayName(source.getDisplayName());
        target.setDescription(source.getDescription());
        target.withCustomProperties(ToCustomPropertyDefConverter.convert(source.getCustomProperties()));
    }

    /**
     * Copy abstract attribute data from REST to internal.
     *
     * @param source
     *            REST source
     * @param target
     *            internal
     */
    private static void copyAbstractAttributeData(AbstractAttributeDefinition source, MetaModelAttribute target) {
        target.setName(source.getName());
        target.setDisplayName(source.getDisplayName());
        target.setDescription(source.getDescription());
        target.setHidden(source.isHidden());
        target.setReadOnly(source.isReadOnly());
        target.setCustomProperties(new ArrayList<>(ToCustomPropertyDefConverter.convert(source.getCustomProperties())));
    }

    /**
     * Copy list of REST complex attributes to internal target.
     *
     * @param source internal
     * @param target REST
     * @param nestedEntities the model
     */
    private static void copyComplexAttributeDataList(
            List<ComplexAttributeDefinition> source,
            List<ComplexMetaModelAttribute> target,
            Map<String, NestedEntity> nestedEntities) {

        if (source == null) {
            return;
        }

        for (ComplexAttributeDefinition attr : source) {
            convertComplexAttribute(attr, target, nestedEntities);
        }
    }
}
