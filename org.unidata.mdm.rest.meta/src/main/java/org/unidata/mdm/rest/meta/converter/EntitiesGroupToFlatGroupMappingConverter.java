/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.meta.converter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.unidata.mdm.meta.type.model.entities.EntitiesGroup;
import org.unidata.mdm.meta.type.model.entities.Entity;
import org.unidata.mdm.meta.type.model.entities.LookupEntity;
import org.unidata.mdm.meta.dto.GetEntitiesGroupsDTO;
import org.unidata.mdm.rest.meta.ro.EntityGroupNode;
import org.unidata.mdm.rest.meta.ro.FilledEntityGroupNode;
import org.unidata.mdm.rest.meta.ro.FlatGroupMapping;
import org.unidata.mdm.rest.meta.ro.GroupEntityDefinition;

public final class EntitiesGroupToFlatGroupMappingConverter {
    private EntitiesGroupToFlatGroupMappingConverter() { }

    private static final Comparator<EntityGroupNode> groupComparator = (e1, e2) -> e1.getTitle().toLowerCase().compareTo(e2.getTitle().toLowerCase());

    private static final Comparator<GroupEntityDefinition> definitionComparator = (e1, e2) -> e1.getDisplayName().toLowerCase().compareTo(e2.getDisplayName().toLowerCase());

    public static FlatGroupMapping convertToFlatGroup(GetEntitiesGroupsDTO groupDefs) {
        Collection<EntityGroupNode> groupNodes = groupDefs.getGroups().entrySet().stream()
                .map(group -> new EntityGroupNode(
                     group.getValue().getTitle(),
                     group.getKey()))
                .sorted(groupComparator)
                .collect(Collectors.toList());
        return new FlatGroupMapping(groupNodes);
    }

    public static FlatGroupMapping convertToFullFilledFlatGroup(GetEntitiesGroupsDTO groupDefs) {

        List<FilledEntityGroupNode> groupNodes = new ArrayList<>();
        for (Entry<String, EntitiesGroup> wrapper : groupDefs.getGroups().entrySet()) {
            FilledEntityGroupNode filledEntityGroupNode = new FilledEntityGroupNode();
            filledEntityGroupNode.setGroupName(wrapper.getKey());
            filledEntityGroupNode.setTitle(wrapper.getValue().getTitle());

            List<Entity> nestedEntities = groupDefs.getNestedEntities(wrapper.getKey());
            Collection<GroupEntityDefinition> entityDefinitions = nestedEntities.stream()
                    .map(entityDef -> new GroupEntityDefinition(entityDef.getName(), entityDef.getDisplayName(), entityDef.isDashboardVisible()))
                    .sorted(definitionComparator)
                    .collect(Collectors.toList());

            List<LookupEntity> nestedLookups = groupDefs.getNestedLookupEntities(wrapper.getKey());
            Collection<GroupEntityDefinition> lookupEntityDefinitions = nestedLookups.stream()
                    .map(entityDef -> new GroupEntityDefinition(entityDef.getName(), entityDef.getDisplayName(), entityDef.isDashboardVisible()))
                    .sorted(definitionComparator)
                    .collect(Collectors.toList());
            filledEntityGroupNode.setEntities(entityDefinitions);
            filledEntityGroupNode.setLookupEntities(lookupEntityDefinitions);
            groupNodes.add(filledEntityGroupNode);
        }
        Collections.sort(groupNodes, groupComparator);
        return new FlatGroupMapping(groupNodes);
    }
}
