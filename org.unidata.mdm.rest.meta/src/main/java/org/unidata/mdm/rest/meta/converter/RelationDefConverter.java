/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.meta.converter;

import org.apache.commons.collections4.CollectionUtils;
import org.unidata.mdm.meta.type.model.entities.Relation;
import org.unidata.mdm.rest.meta.ro.RelationDefinition;
import org.unidata.mdm.rest.meta.ro.RelationTypeRO;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class RelationDefConverter extends AbstractEntityDefinitionConverter {

    public static RelationDefinition convert(Relation source) {
        if (source == null) {
            return null;
        }
        RelationDefinition target = new RelationDefinition();
        target.setFromEntity(source.getFromEntity());
        target.setName(source.getName());
        target.setDisplayName(source.getDisplayName());
        target.setRelType(RelationTypeRO.fromValue(source.getRelType().value()));
        target.setRequired(source.isRequired());
        target.setToEntity(source.getToEntity());
        target.setToEntityDefaultDisplayAttributes(source.getToEntityDefaultDisplayAttributes());
        target.setToEntitySearchAttributes(source.getToEntitySearchAttributes());
        target.setSimpleAttributes(toSimpleAttrs(source.getSimpleAttribute(), target.getName()));
        target.setUseAttributeNameForDisplay(source.isUseAttributeNameForDisplay());

        target.setCustomProperties(to(source.getCustomProperties()));

        return target;
    }

    public static List<RelationDefinition> convert(List<Relation> source) {

        if (CollectionUtils.isEmpty(source)) {
            return Collections.emptyList();
        }

        List<RelationDefinition> result = new ArrayList<>();
        for (Relation def : source) {
            if (Objects.isNull(def)) {
                continue;
            }

            result.add(convert(def));
        }

        return result;
    }
}
