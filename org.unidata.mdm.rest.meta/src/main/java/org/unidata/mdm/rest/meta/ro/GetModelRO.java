/*
  Unidata Platform
  Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.

  Commercial License
  This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.

  Please see the Unidata Licensing page at: https://unidata-platform.com/license/
  For clarification or additional options, please contact: info@unidata-platform.com
  -------
  Disclaimer:
  -------
  THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
  REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
  IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
  FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
  THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.meta.ro;

import org.unidata.mdm.system.type.rendering.InputSource;

/**
 * @author Alexey Tsarapkin
 */
public class GetModelRO implements InputSource {
    private String id;
    private boolean draft;
    private boolean checkData = true;

    public GetModelRO(){}

    public GetModelRO(boolean draft, String id, boolean checkData){
        this.id = id;
        this.draft = draft;
        this.checkData = checkData;
    }

    public GetModelRO(boolean draft, String id){
        this.id = id;
        this.draft = draft;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isDraft() {
        return draft;
    }

    public void setDraft(boolean draft) {
        this.draft = draft;
    }

    public boolean isCheckData() {
        return checkData;
    }

    public void setCheckData(boolean checkData) {
        this.checkData = checkData;
    }

}
