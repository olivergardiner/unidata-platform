/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.meta.service;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.unidata.mdm.meta.type.model.entities.LookupEntity;
import org.unidata.mdm.meta.context.DeleteModelRequestContext.DeleteModelRequestContextBuilder;
import org.unidata.mdm.meta.context.GetModelRequestContext;
import org.unidata.mdm.meta.context.UpdateModelRequestContext;
import org.unidata.mdm.meta.dto.GetModelDTO;
import org.unidata.mdm.meta.service.MetaDraftService;
import org.unidata.mdm.meta.service.MetaModelService;
import org.unidata.mdm.rest.meta.converter.AbstractEntityDefToEntityListElementConverter;
import org.unidata.mdm.rest.meta.converter.LookupEntityDefToLookupEntityDefinitionConverter;
import org.unidata.mdm.rest.meta.converter.LookupEntityDefinitionToLookupEntityDefConverter;
import org.unidata.mdm.rest.meta.exception.MetaRestExceptionIds;
import org.unidata.mdm.rest.meta.ro.DeleteModelEntityRO;
import org.unidata.mdm.rest.meta.ro.EntityInfoDefinition;
import org.unidata.mdm.rest.meta.ro.GetModelRO;
import org.unidata.mdm.rest.meta.ro.LookupEntityDefinition;
import org.unidata.mdm.rest.meta.type.rendering.MetaModelInputRenderingAction;
import org.unidata.mdm.rest.meta.type.rendering.MetaModelOutputRenderingAction;
import org.unidata.mdm.rest.meta.util.ListUtils;
import org.unidata.mdm.rest.system.ro.ErrorResponse;
import org.unidata.mdm.rest.system.ro.RestPageRequest;
import org.unidata.mdm.rest.system.ro.RestResponse;
import org.unidata.mdm.rest.system.service.AbstractRestService;
import org.unidata.mdm.system.exception.PlatformBusinessException;
import org.unidata.mdm.system.service.ExecutionService;
import org.unidata.mdm.system.service.RenderingService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

/**
 * @author Michael Yashin. Created on 19.05.2015.
 */
@Path(LookupEntityRestService.SERVICE_PATH)
@Consumes({ "application/json" })
@Produces({ "application/json" })
public class LookupEntityRestService extends AbstractRestService {

    public static final String SERVICE_PATH = "lookup-entities";

    /**
     * Meta model service.
     */
    @Autowired
    private MetaModelService metaModelService;
    @Autowired
    private MetaDraftService metaDraftService;

    @Autowired
    private ExecutionService executionService;

    @Autowired
    private RenderingService renderingService;

    // TODO @Modules
//	/**
//	 * Search service.
//	 */
//	@Autowired
//	private SearchService searchService;
//
//	@Autowired
//	private RegistrationService registrationService;

    /**
     * Gets a list of lookup entities.
     *
     * @param pageRequest
     *            the page request
     * @return list of entity info
     */
    @GET
    @Operation(
        description = "Список справочников",
        method = HttpMethod.GET,
        parameters = {
                @Parameter(name = "page", description = "Номер страницы, начинается с 1", in = ParameterIn.QUERY),
                @Parameter(name = "size", description = "Размер страницы", in = ParameterIn.QUERY),
                @Parameter(name = "sort", description = "Параметры сортировки, URL-encoded JSON", in = ParameterIn.QUERY)
        },
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = RestResponse.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response findAll(
            @Parameter(hidden = true) @QueryParam("") RestPageRequest pageRequest,
            @Parameter(description = "Черновик?,", in = ParameterIn.QUERY) @QueryParam("draft") @DefaultValue("false") boolean draft) {
        long offset = pageRequest.getPageRequest().getOffset();
        int pageSize = pageRequest.getPageRequest().getPageSize();
        return ok(elementsToPage(getLookupEntities((int)offset, pageSize, draft), pageRequest.getPageRequest()));
    }

    /**
     * Gets a lookup entity by id (name).
     *
     * @param id
     *            the id (name)
     * @return lookup entity
     */
    @GET
    @Path("{id}")
    @Operation(
        description = "Получить запись по ID",
        method = HttpMethod.GET,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = RestResponse.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response getById(
            @Parameter(description = "Идентификатор,", in = ParameterIn.PATH) @PathParam("id") String id,
            @Parameter(description = "Черновик?,", in = ParameterIn.QUERY) @QueryParam("draft") @DefaultValue("false") boolean draft,
            @Parameter(description = "Проверить данные?,", in = ParameterIn.QUERY) @QueryParam("checkData") @DefaultValue("true") boolean checkData) {

        GetModelRO ro = new GetModelRO(draft, id, checkData);

        GetModelRequestContext.GetModelRequestContextBuilder context = GetModelRequestContext.builder()
                .lookupIds(Collections.singletonList(ro.getId()))
                .draft(BooleanUtils.toBoolean(ro.isDraft()));

        renderingService.renderInput(MetaModelInputRenderingAction.GET_LOOKUP_MODEL_INPUT, context, ro);

        GetModelDTO model = executionService.execute(context.build());

        if (model.getLookups().isEmpty()) {
            throw new PlatformBusinessException("Entity not found", MetaRestExceptionIds.EX_META_LOOKUP_ENTITY_NOT_FOUND);
        }

        LookupEntityDefinition response = LookupEntityDefToLookupEntityDefinitionConverter.convert(model.getLookups().get(0).getLookup());

        renderingService.renderOutput(MetaModelOutputRenderingAction.GET_LOOKUP_MODEL_OUTPUT, model, response);

        return ok(wrapEntity(response));


        // TODO @Modules @Mikhail_Mikhailov ask don't touch
//		LookupEntityRegistryKey registryKey = new LookupEntityRegistryKey(id);
//		Set<UniqueRegistryKey> contains = registrationService.getContains(registryKey);
//		List<ReferenceInfo> infos = contains.stream().filter(key -> key.keyType() == UniqueRegistryKey.Type.ATTRIBUTE)
//				.map(key -> toRefInfos(registrationService.getReferencesTo(key), key)).flatMap(Collection::stream)
//				.collect(Collectors.toList());
//		infos.addAll(toRefInfos(registrationService.getReferencesTo(registryKey), registryKey));
//		response.setEntityDependency(infos);
//		if (checkData) {
//
//		    SearchRequestContext ctx = SearchRequestContext.builder(entity.getName())
//                    .storageId(SecurityUtils.getCurrentUserStorageId())
//                    .build();
//
//			response.setHasData(searchService.countAll(ctx) > 0);
//		}

    }

    /**
     * Creates a lookup entity.
     *
     * @param lookupEntity
     *            entity to create
     * @return created entity
     */
    @POST
    @Operation(
        description = "Создать справочник",
        method = HttpMethod.POST,
        requestBody = @RequestBody(content = @Content(schema = @Schema(implementation = LookupEntityDefinition.class)), description = "Запрос на вставку"),
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = RestResponse.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response create(
            LookupEntityDefinition lookupEntity,
            @Parameter(description = "Черновик?,", in = ParameterIn.QUERY) @QueryParam("draft") @DefaultValue("false") boolean draft,
            @Parameter(description = "Проверить данные?,", in = ParameterIn.QUERY) @QueryParam("checkData") @DefaultValue("true") boolean checkData) {


        UpdateModelRequestContext.UpdateModelRequestContextBuilder upsertModelRequestContext =
                LookupEntityDefinitionToLookupEntityDefConverter.convert(lookupEntity, draft);

        renderingService.renderInput(MetaModelInputRenderingAction.UPSERT_LOOKUP_MODEL_INPUT, upsertModelRequestContext, lookupEntity);

        executionService.execute(upsertModelRequestContext.build());

        GetModelRequestContext.GetModelRequestContextBuilder getContextBuilder = GetModelRequestContext.builder()
                .lookupIds(Collections.singletonList(lookupEntity.getName()))
                .draft(draft);


        GetModelRO ro = new GetModelRO(draft, lookupEntity.getName());

        renderingService.renderInput(MetaModelInputRenderingAction.GET_LOOKUP_MODEL_INPUT, getContextBuilder, ro);

        GetModelDTO getModelDTOResult = executionService.execute(getContextBuilder.build());


        if (getModelDTOResult.getLookups().isEmpty()) {
            throw new PlatformBusinessException("Entity not found", MetaRestExceptionIds.EX_META_LOOKUP_ENTITY_NOT_FOUND);
        }

        LookupEntityDefinition response = LookupEntityDefToLookupEntityDefinitionConverter.convert(
                getModelDTOResult.getLookups().get(0).getLookup()
        );

        renderingService.renderOutput(MetaModelOutputRenderingAction.GET_LOOKUP_MODEL_OUTPUT, getModelDTOResult, response);

        return ok(wrapEntity(response));

    }

    /**
     * Updates lookup entity.
     *
     * @param lookupEntity
     *            the entity
     * @return updated entity
     */
    @PUT
    @Path("{id}")
    @Operation(
        description = "Обновить справочник. На обновление присылается полная информация о справочнике.",
        method = HttpMethod.PUT,
        requestBody = @RequestBody(content = @Content(schema = @Schema(implementation = LookupEntityDefinition.class)), description = "Запрос на вставку"),
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = RestResponse.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response update(
            LookupEntityDefinition lookupEntity,
            @Parameter(description = "Идентификатор,", in = ParameterIn.PATH) @PathParam("id") String possiblyOldName,
            @Parameter(description = "Черновик?,", in = ParameterIn.QUERY) @QueryParam("draft") @DefaultValue("false") boolean draft) {

        UpdateModelRequestContext.UpdateModelRequestContextBuilder upsertModelRequestContext =
                LookupEntityDefinitionToLookupEntityDefConverter.convert(lookupEntity, draft);


        renderingService.renderInput(MetaModelInputRenderingAction.UPSERT_LOOKUP_MODEL_INPUT, upsertModelRequestContext, lookupEntity);

        executionService.execute(upsertModelRequestContext.build());

        GetModelRO ro = new GetModelRO(draft, lookupEntity.getName());

        GetModelRequestContext.GetModelRequestContextBuilder getContextBuilder = GetModelRequestContext.builder()
                .lookupIds(Collections.singletonList(lookupEntity.getName()))
                .draft(ro.isDraft());

        renderingService.renderInput(MetaModelInputRenderingAction.GET_LOOKUP_MODEL_INPUT, getContextBuilder, ro);

        GetModelDTO model = executionService.execute(getContextBuilder.build());



        if (model.getLookups().isEmpty()) {
            throw new PlatformBusinessException("Entity not found", MetaRestExceptionIds.EX_META_LOOKUP_ENTITY_NOT_FOUND);
        }

        LookupEntityDefinition response = LookupEntityDefToLookupEntityDefinitionConverter.convert(
                model.getLookups().get(0).getLookup()
        );

        renderingService.renderOutput(MetaModelOutputRenderingAction.GET_LOOKUP_MODEL_OUTPUT, model, response);


        return ok(wrapEntity(response));
    }

    /**
     * Delete lookup entity.
     *
     * @param id
     *            id to delete
     * @return 200 Ok
     */
    @DELETE
    @Path("{id}")
    @Operation(
        description = "Удалить справочник.",
        method = HttpMethod.DELETE,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = String.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response delete(
            @Parameter(description = "Идентификатор,", in = ParameterIn.PATH) @PathParam("id") String id,
            @Parameter(description = "Черновик?,", in = ParameterIn.QUERY) @QueryParam("draft") @DefaultValue("false") boolean draft) {

        DeleteModelRequestContextBuilder deleteRequestContextBuilder = new DeleteModelRequestContextBuilder()
                .lookupEntitiesIds(Collections.singletonList(id))
                .draft(draft);

        renderingService.renderInput(
                MetaModelInputRenderingAction.DELETE_LOOKUP_META_MODEL_INPUT,
                deleteRequestContextBuilder,
                new DeleteModelEntityRO(id)
        );

        executionService.execute(deleteRequestContextBuilder.build());

        return ok("");
    }

    /**
     * Gets lookup entities as list.
     *
     * @param offset
     *            offset
     * @param pageSize
     *            page size
     * @return list of entities
     */
    @SuppressWarnings("unchecked")
    private List<EntityInfoDefinition> getLookupEntities(int offset, int pageSize, boolean draft) {
        List<LookupEntity> entities;
        if(draft) {
            entities= metaDraftService.getLookupEntitiesList();
        }else {
            entities= metaModelService.getLookupEntitiesList();
        }

        List<LookupEntity> list = ListUtils.safeSubList(entities, offset, offset + pageSize);

        return AbstractEntityDefToEntityListElementConverter.to(list);
    }
    /**
     * Get all available tags for this entity.
     * @param id entity name
     * @param draft is draft
     * @return all available tags for thos entity.
     */
    @GET
    @Path(value = "/tags/{id}")
    @Operation(
            description = "Получить список тэгов для справочника.",
            method = HttpMethod.GET,
            responses = {
                @ApiResponse(content = @Content(schema = @Schema(implementation = RestResponse.class)), responseCode = "200"),
                @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
            }
        )

    public Response getAllTags(
            @Parameter(description = "Идентификатор,", in = ParameterIn.PATH) @PathParam("id") String id,
            @Parameter(description = "Черновик?,", in = ParameterIn.QUERY) @QueryParam("draft") @DefaultValue("false") boolean draft) {

        LookupEntity lookupEntity;
        if (draft) {
            lookupEntity = metaDraftService.getLookupEntityById(id);
        } else {
            lookupEntity = metaModelService.getLookupEntityById(id);
        }
        if (Objects.isNull(lookupEntity)) {
            throw new PlatformBusinessException("Lookup entity not found", MetaRestExceptionIds.EX_META_LOOKUP_ENTITY_NOT_FOUND);
        }
        Set<String> tags = new HashSet<>();
        // TODO @Modules
/*		lookupEntity.getDataQualities().forEach(dq -> {
			tags.addAll(dq.getTags());
		});*/
        return ok(new RestResponse<>(tags));
    }
    /**
     * Gets a page of entities.
     *
     * @param elements
     *            the elements to put on page
     * @param pageRequest
     *            page request
     * @return {@link Page}
     */
    private Page<EntityInfoDefinition> elementsToPage(List<EntityInfoDefinition> elements, PageRequest pageRequest) {
        return new PageImpl<>(elements, pageRequest, elements.size());
    }

    private RestResponse<LookupEntityDefinition> wrapEntity(LookupEntityDefinition entityDefinition) {
        return new RestResponse<>(entityDefinition);
    }

    // DO NOT REMOVE! This crappy workaround required for Swagger to generate API
    // docs
    private static class LookupEntityRestResponse extends RestResponse<LookupEntityDefinition> {
        @Override
        public LookupEntityDefinition getContent() {
            return null;
        }
    }
}