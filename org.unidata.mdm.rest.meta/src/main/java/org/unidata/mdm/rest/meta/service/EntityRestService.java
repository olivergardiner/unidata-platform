/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.meta.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.unidata.mdm.core.type.model.AttributeModelElement;
import org.unidata.mdm.core.type.model.EntityModelElement;
import org.unidata.mdm.meta.type.model.entities.Entity;
import org.unidata.mdm.meta.context.DeleteModelRequestContext.DeleteModelRequestContextBuilder;
import org.unidata.mdm.meta.context.GetModelRequestContext;
import org.unidata.mdm.meta.context.UpdateModelRequestContext;
import org.unidata.mdm.meta.dto.GetEntityDTO;
import org.unidata.mdm.meta.dto.GetModelDTO;
import org.unidata.mdm.meta.service.MetaModelService;
import org.unidata.mdm.rest.meta.converter.AbstractEntityDefToEntityListElementConverter;
import org.unidata.mdm.rest.meta.converter.EntityDefinitionConverter;
import org.unidata.mdm.rest.meta.converter.EntityDefinitionToEntityDefConverter;
import org.unidata.mdm.rest.meta.exception.MetaRestExceptionIds;
import org.unidata.mdm.rest.meta.ro.DeleteModelEntityRO;
import org.unidata.mdm.rest.meta.ro.EntityDefinition;
import org.unidata.mdm.rest.meta.ro.EntityInfoDefinition;
import org.unidata.mdm.rest.meta.ro.GetModelRO;
import org.unidata.mdm.rest.meta.ro.ReferenceInfo;
import org.unidata.mdm.rest.meta.ro.references.EntityReferenceRO;
import org.unidata.mdm.rest.meta.ro.references.LookupReferenceRO;
import org.unidata.mdm.rest.meta.type.rendering.MetaModelInputRenderingAction;
import org.unidata.mdm.rest.meta.type.rendering.MetaModelOutputRenderingAction;
import org.unidata.mdm.rest.meta.util.ListUtils;
import org.unidata.mdm.rest.system.ro.ErrorResponse;
import org.unidata.mdm.rest.system.ro.RestPageRequest;
import org.unidata.mdm.rest.system.ro.RestResponse;
import org.unidata.mdm.rest.system.service.AbstractRestService;
import org.unidata.mdm.system.exception.PlatformBusinessException;
import org.unidata.mdm.system.service.ExecutionService;
import org.unidata.mdm.system.service.RenderingService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

/**
 * @author Michael Yashin. Created on 19.05.2015.
 */
@Path(EntityRestService.SERVICE_PATH)
@Consumes({"application/json"})
@Produces({"application/json"})
public class EntityRestService extends AbstractRestService {

    /**
     * Service path.
     */
    public static final String SERVICE_PATH = "entities";

    @Autowired
    private RenderingService renderingService;
    /**
     * Meta model service.
     */
    @Autowired
    private MetaModelService metaModelService;

    @Autowired
    private ExecutionService executionService;

    // TODO @Modules
//    /**
//     * Search service.
//     */
//    @Autowired
//    private SearchService searchService;
//
//    @Autowired
//    private RegistrationService registrationService;
//
//    @Autowired
//    private RelationsServiceComponent relationsServiceComponent;

    /**
     * Gets entities in paged fashion with defaults.
     * @param pageRequest the page request
     * @return list of entities
     */
    @GET
    @Operation(
        description = "Список справочников",
        method = HttpMethod.GET,
        parameters = {
                @Parameter(name = "page", description = "Номер страницы, начинается с 1", in = ParameterIn.QUERY),
                @Parameter(name = "size", description = "Размер страницы", in = ParameterIn.QUERY),
                @Parameter(name = "sort", description = "Параметры сортировки, URL-encoded JSON", in = ParameterIn.QUERY)
        },
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = RestResponse.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response findAll(
            @Parameter(hidden = true) @QueryParam("") RestPageRequest pageRequest,
            @Parameter(description = "Черновик?,", in = ParameterIn.QUERY) @QueryParam("draft") @DefaultValue("false") boolean draft) {

        long offset = pageRequest.getPageRequest().getOffset();
        int pageSize = pageRequest.getPageRequest().getPageSize();
        return ok(elementsToPage(getEntities((int) offset, pageSize, draft), pageRequest.getPageRequest()));
    }

    /**
     * Gets an entity by id.
     * @param id entity id
     * @return entity
     * @throws Exception if something went wrong
     */
    @GET
    @Path("{id}")
    @Operation(
        description = "Получить реестр по ID",
        method = HttpMethod.GET,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = RestResponse.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response getById(
            @Parameter(description = "Идентификатор,", in = ParameterIn.PATH) @PathParam("id") String id,
            @Parameter(description = "Черновик?,", in = ParameterIn.QUERY) @QueryParam("draft") @DefaultValue("false") boolean draft,
            @Parameter(description = "Проверить данные?,", in = ParameterIn.QUERY) @QueryParam("checkData") @DefaultValue("true") boolean checkData) {

        GetModelRO ro = new GetModelRO(draft, id, checkData);
        GetModelRequestContext.GetModelRequestContextBuilder requestContext = GetModelRequestContext.builder()
                .entityIds(Collections.singletonList(id))
                .draft(ro.isDraft());


        renderingService.renderInput(MetaModelInputRenderingAction.GET_ENTITY_MODEL_INPUT, requestContext, ro);

        GetModelDTO model = executionService.execute(requestContext.build());

		if (model.getEntities().isEmpty()) {
			throw new PlatformBusinessException("Entity not found", MetaRestExceptionIds.EX_META_ENTITY_NOT_FOUND);
		}

        EntityDefinition response = EntityDefinitionConverter.convert(model.getEntities().get(0));


        renderingService.renderOutput(MetaModelOutputRenderingAction.GET_ENTITY_MODEL_OUTPUT, model, response);

        return ok(wrapEntity(response));

        // TODO @Modules @Mikhail_Mikhailov ask don't touch
//        EntityRegistryKey registryKey = new EntityRegistryKey(id);
//        Set<UniqueRegistryKey> contains = registrationService.getContains(registryKey);
//        List<ReferenceInfo> infos = contains.stream()
//                                            .filter(key -> key.keyType() == UniqueRegistryKey.Type.ATTRIBUTE)
//                                            .map(key -> toRefInfos(registrationService.getReferencesTo(key), key))
//                                            .flatMap(Collection::stream)
//                                            .collect(Collectors.toList());
//        response.setEntityDependency(infos);
//        if (checkData) {
//
//            SearchRequestContext ctx = SearchRequestContext.builder(response.getName())
//                    .storageId(SecurityUtils.getCurrentUserStorageId())
//                    .build();
//
//            response.setHasData(searchService.countAll(ctx) > 0);
//            response.getRelations()
//                    .forEach(rel -> rel.setHasData(relationsServiceComponent.checkExistDataByRelName(rel.getName())));
//        }

    }




    /**
     * Creates a new entity.
     * @param entity new entity
     * @return the entity just created
     * @throws Exception if something went wrong
     */
    @POST
    @Operation(
        description = "Создать реестр",
        method = HttpMethod.POST,
        requestBody = @RequestBody(content = @Content(schema = @Schema(implementation = EntityDefinition.class)), description = "Запрос на вставку"),
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = RestResponse.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
	public Response create(EntityDefinition entity,
	        @Parameter(description = "Черновик?,", in = ParameterIn.QUERY) @QueryParam("draft") @DefaultValue("false") boolean draft) {

        // upsert

        UpdateModelRequestContext.UpdateModelRequestContextBuilder upsertContextBuilder =
                EntityDefinitionToEntityDefConverter.convert(entity, draft);

        renderingService.renderInput(MetaModelInputRenderingAction.UPSERT_ENTITY_MODEL_INPUT, upsertContextBuilder, entity);

        executionService.execute(upsertContextBuilder.build());


        GetModelRO ro = new GetModelRO(draft, entity.getName(), true);

        GetModelRequestContext.GetModelRequestContextBuilder getContextBuilder = GetModelRequestContext.builder()
                .entityIds(Collections.singletonList(entity.getName()))
                .draft(ro.isDraft());

        renderingService.renderInput(MetaModelInputRenderingAction.GET_ENTITY_MODEL_INPUT, getContextBuilder, ro);

        GetModelDTO model = executionService.execute(getContextBuilder.build());

        if (model.getEntities().isEmpty()) {
            throw new PlatformBusinessException("Entity not found", MetaRestExceptionIds.EX_META_ENTITY_NOT_FOUND);
        }

        EntityDefinition response = EntityDefinitionConverter.convert(model.getEntities().get(0));

        renderingService.renderOutput(MetaModelOutputRenderingAction.GET_ENTITY_MODEL_OUTPUT, model, response);

        return ok(wrapEntity(response));
    }

    /**
     * Updates an entity definition.
     * @param entity the entity
     * @return updated entity
     * @throws Exception
     */
    @PUT
    @Path("{id}")
    @Operation(
        description = "Обновить реестр. На обновление присылается полная информация о реестре.",
        method = HttpMethod.PUT,
        requestBody = @RequestBody(content = @Content(schema = @Schema(implementation = EntityDefinition.class)), description = "Запрос на вставку"),
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = RestResponse.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response update(
            @Parameter(description = "Идентификатор,", in = ParameterIn.PATH) @PathParam("id") String possiblyOldName,
            EntityDefinition entity,
            @Parameter(description = "Черновик?,", in = ParameterIn.QUERY) @QueryParam("draft") @DefaultValue("false") boolean draft) {

        // upsert
        // 1. Convert ro object
        UpdateModelRequestContext.UpdateModelRequestContextBuilder upsertContextBuilder = EntityDefinitionToEntityDefConverter.convert(entity, draft);
        // 2. Run upsert execution
        renderingService.renderInput(MetaModelInputRenderingAction.UPSERT_ENTITY_MODEL_INPUT, upsertContextBuilder, entity);
        executionService.execute(upsertContextBuilder.build());

        // 3.  Get new model after upsert
        GetModelRO ro = new GetModelRO(draft, entity.getName(), true);
        GetModelRequestContext.GetModelRequestContextBuilder getContextBuilder = GetModelRequestContext.builder()
                .entityIds(Collections.singletonList(entity.getName()))
                .draft(ro.isDraft());

        renderingService.renderInput(MetaModelInputRenderingAction.GET_ENTITY_MODEL_INPUT, getContextBuilder, ro);
        GetModelDTO model = executionService.execute(getContextBuilder.build());

        if (model.getEntities().isEmpty()) {
            throw new PlatformBusinessException("Entity not found", MetaRestExceptionIds.EX_META_ENTITY_NOT_FOUND);
        }

        // 4. Convert and return output result
        EntityDefinition response = EntityDefinitionConverter.convert(model.getEntities().get(0));
        renderingService.renderOutput(MetaModelOutputRenderingAction.GET_ENTITY_MODEL_OUTPUT, model, response);

        return ok(wrapEntity(response));
    }




    /**
     * Deletes an entity.
     * @param id entity id
     * @return empty string
     * @throws Exception
     */
    @DELETE
    @Path("{id}")
    @Operation(
        description = "Удалить реестр.",
        method = HttpMethod.DELETE,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = String.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response delete(
            @Parameter(description = "Идентификатор,", in = ParameterIn.PATH) @PathParam("id") String id,
            @Parameter(description = "Черновик?,", in = ParameterIn.QUERY) @QueryParam("draft") @DefaultValue("false") boolean draft) {

        DeleteModelRequestContextBuilder deleteRequestContextBuilder = new DeleteModelRequestContextBuilder()
                .entitiesIds(Collections.singletonList(id))
                .draft(draft);

        renderingService.renderInput(
                MetaModelInputRenderingAction.DELETE_ENTITY_META_MODEL_INPUT,
                deleteRequestContextBuilder,
                new DeleteModelEntityRO(id)
        );

        executionService.execute(deleteRequestContextBuilder.build());

        return ok("");
    }


    /**
     * Get all records with lookup link to sending lookup id
     * @param lookupId lookup id
     * @return records linked to lookup
     * @throws Exception if something went wrong
     */
    @GET
    @Path("lookup-links/{id}")
    @Operation(
        description = "Получить объекты, имеющие атрибуты-ссылки на справочник.",
        method = HttpMethod.GET,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = RestResponse.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response getEntitiesReferencingThisLookup(
            @Parameter(description = "Идентификатор,", in = ParameterIn.PATH) @PathParam("id") String lookupId,
            @Parameter(description = "Черновик?,", in = ParameterIn.QUERY) @QueryParam("draft") @DefaultValue("false") boolean draft) {

        List<ReferenceInfo> result;
        if (draft) {
            result = Collections.emptyList();
        } else {
            Map<EntityModelElement, Set<AttributeModelElement>> dependencies = metaModelService.getEntitiesReferencingThisLookup(lookupId);
            result = new ArrayList<>();
            if (MapUtils.isNotEmpty(dependencies)) {
                for (EntityModelElement entityDef : dependencies.keySet()) {
                    ReferenceInfo referenceInfo = new ReferenceInfo();
                    referenceInfo.setTargetKey(new LookupReferenceRO(lookupId));
                    referenceInfo.setSourceKey( new EntityReferenceRO(entityDef.getName()));
                    referenceInfo.setSourceType(referenceInfo.getSourceKey().keyType().getName());
                    referenceInfo.setTargetType(referenceInfo.getTargetKey().keyType().getName());
                    result.add(referenceInfo);
                }
            }

            Map<EntityModelElement, Set<AttributeModelElement>> lookupDependencies = metaModelService.getLookupsReferencingThisLookup(lookupId);
            if (MapUtils.isNotEmpty(lookupDependencies)) {
                for (EntityModelElement entityDef : lookupDependencies.keySet()) {
                    ReferenceInfo referenceInfo = new ReferenceInfo();
                    referenceInfo.setTargetKey(new LookupReferenceRO(lookupId));
                    referenceInfo.setSourceKey( new LookupReferenceRO(entityDef.getName()));
                    referenceInfo.setSourceType(referenceInfo.getSourceKey().keyType().getName());
                    referenceInfo.setTargetType(referenceInfo.getTargetKey().keyType().getName());
                    result.add(referenceInfo);
                }
            }
        }

        return ok(new RestResponse<>(result));
    }
    /**
     * Get all available tags for this entity.
     * @param id entity name
     * @param draft is draft
     * @return all available tags for thos entity.
     */
    @GET
    @Path(value = "/tags/{id}")
    @Operation(
        description = "Получить список тэгов для реестра.",
        method = HttpMethod.GET,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = RestResponse.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
	public Response getAllTags(
	        @Parameter(description = "Идентификатор,", in = ParameterIn.PATH) @PathParam("id") String id,
            @Parameter(description = "Черновик?,", in = ParameterIn.QUERY) @QueryParam("draft") @DefaultValue("false") boolean draft) {

        GetModelDTO model = executionService.execute(
                GetModelRequestContext.builder()
                        .entityIds(Collections.singletonList(id))
                        .draft(BooleanUtils.toBoolean(draft))
                        .build()
        );

        if (model.getEntities().isEmpty()) {
            throw new PlatformBusinessException("Entity not found", MetaRestExceptionIds.EX_META_ENTITY_NOT_FOUND);
        }

        /*
        TODO read dq
        entity.getEntity().getDataQualities().stream().forEach(dq -> {
			tags.addAll(dq.getTags());
		});
         */

		return ok(new RestResponse<>(new HashSet<>()));
	}
    /**
     * Gets entities as list.
     * @param offset offset
     * @param pageSize page size
     * @return list of entities
     */
    private List<EntityInfoDefinition> getEntities(int offset, int pageSize, boolean draft){

        GetModelRequestContext ctx = GetModelRequestContext.builder()
                .allEntities(true)
                .reduced(true)
                .draft(draft)
                .build();


        GetModelDTO dto =  executionService.execute(ctx);

        List<Entity> entities = dto.getEntities().stream()
            .map(GetEntityDTO::getEntity)
            .collect(Collectors.toList());

        List<Entity> list = ListUtils.safeSubList(
                entities,
                offset,
                offset + pageSize
        );

        return AbstractEntityDefToEntityListElementConverter.to(list);
    }

    /**
     * Gets a page of entities.
     * @param elements the elements to put on page
     * @param pageRequest page request
     * @return {@link Page}
     */
    private Page<EntityInfoDefinition> elementsToPage(List<EntityInfoDefinition> elements, PageRequest pageRequest) {
        return new PageImpl<>(elements, pageRequest, elements.size());
    }

    private RestResponse<EntityDefinition> wrapEntity(EntityDefinition entityDefinition) {
        return new RestResponse<>(entityDefinition);
    }

    //DO NOT REMOVE! This crappy workaround required for Swagger to generate API docs
    private static class EntityRestResponse extends RestResponse<EntityDefinition> {
        @Override
        public EntityDefinition getContent() {
            return null;
        }
    }
}