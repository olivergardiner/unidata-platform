/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.meta.converter;

import java.math.BigInteger;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.unidata.mdm.meta.type.model.attributes.ArrayMetaModelAttribute;
import org.unidata.mdm.meta.type.model.ArrayValueType;
import org.unidata.mdm.meta.type.model.SimpleDataType;
import org.unidata.mdm.rest.meta.ro.ArrayAttributeDefinitionRO;

/**
 * @author Mikhail Mikhailov
 *
 */
public class ArrayAttributeDefConverter {

    /**
     * Constructor.
     */
    private ArrayAttributeDefConverter() {
        super();
    }

    /**
     * Copies simple attributes from list to list.
     *
     * @param source
     *            the source
     * @param target
     *            the target
     */
    public static void copySimpleAttributeDataList(List<ArrayAttributeDefinitionRO> source,
            List<ArrayMetaModelAttribute> target) {

        if (source == null) {
            return;
        }

        for (ArrayAttributeDefinitionRO sourceAttr : source) {
            ArrayMetaModelAttribute targetAttr = new ArrayMetaModelAttribute();//MetaJaxbUtils.getMetaObjectFactory().createArrayAttributeDef();
            copyArrayAttributeData(sourceAttr, targetAttr);
            target.add(targetAttr);
        }
    }

    /**
     * Copy simple attributes data from REST to internal.
     *
     * @param source
     *            REST source
     * @param target
     *            internal
     */
    private static void copyArrayAttributeData(ArrayAttributeDefinitionRO source, ArrayMetaModelAttribute target) {

        SimpleAttributeDefConverter.copyAbstractAttributeData(source, target);

        target.setMask(source.getMask());

        target.setNullable(source.isNullable());
        target.setArrayValueType(
                source.getArrayDataType() == null
                ? null
                : ArrayValueType.fromValue(SimpleDataType.fromValue(source.getArrayDataType().value())));

        target.setLookupEntityType(source.getLookupEntityType());

        if (StringUtils.isNotEmpty(source.getLookupEntityType())
                && CollectionUtils.isNotEmpty(source.getLookupEntityDisplayAttributes())) {
            target.withLookupEntityDisplayAttributes(source.getLookupEntityDisplayAttributes());
        }

        if (StringUtils.isNotEmpty(source.getLookupEntityType())
                && CollectionUtils.isNotEmpty(source.getLookupEntitySearchAttributes())) {
            target.withLookupEntitySearchAttributes(source.getLookupEntitySearchAttributes());
        }

        if (StringUtils.isNotEmpty(source.getDictionaryDataType())) {
            target.withDictionaryDataType(source.getDictionaryDataType());
        }

        target.setOrder(source.getOrder());
        target.setSearchable(source.isSearchable());
        target.setDisplayable(source.isDisplayable());
        target.setMainDisplayable(source.isMainDisplayable());

        target.setSearchMorphologically(source.isSearchMorphologically());
        target.setSearchCaseInsensitive(source.isSearchCaseInsensitive());
        target.setExchangeSeparator(source.getExchangeSeparator());
        target.setUseAttributeNameForDisplay(source.isUseAttributeNameForDisplay());
    }
}
