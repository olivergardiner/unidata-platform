/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.meta.service;

import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.apache.cxf.jaxrs.ext.multipart.Multipart;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.unidata.mdm.meta.type.model.enumeration.EnumerationDataType;
import org.unidata.mdm.meta.type.model.Model;
import org.unidata.mdm.meta.context.DropDraftModelRequestContext;
import org.unidata.mdm.meta.context.PublishModelRequestContext;
import org.unidata.mdm.meta.context.UpdateModelRequestContext;
import org.unidata.mdm.meta.context.UpdateModelRequestContext.UpdateModelRequestContextBuilder;
import org.unidata.mdm.meta.service.MetaDependencyService;
import org.unidata.mdm.meta.service.MetaDraftService;
import org.unidata.mdm.meta.service.MetaModelService;
import org.unidata.mdm.meta.type.input.meta.MetaGraph;
import org.unidata.mdm.meta.type.input.meta.MetaType;
import org.unidata.mdm.meta.util.MetaJaxbUtils;
import org.unidata.mdm.meta.util.ModelUtils;
import org.unidata.mdm.rest.meta.converter.graph.MetaGraphDTOToROConverter;
import org.unidata.mdm.rest.meta.converter.graph.MetaTypeROToDTOConverter;
import org.unidata.mdm.rest.meta.exception.MetaRestExceptionIds;
import org.unidata.mdm.rest.meta.ro.MetaDependencyRequestRO;
import org.unidata.mdm.rest.meta.ro.ModelVersionRO;
import org.unidata.mdm.rest.meta.type.rendering.MetaModelInputRenderingAction;
import org.unidata.mdm.rest.system.ro.ErrorResponse;
import org.unidata.mdm.rest.system.ro.RestResponse;
import org.unidata.mdm.rest.system.ro.UpdateResponse;
import org.unidata.mdm.rest.system.service.AbstractRestService;
import org.unidata.mdm.system.exception.PlatformBusinessException;
import org.unidata.mdm.system.service.ExecutionService;
import org.unidata.mdm.system.service.RenderingService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

/**
 * The Class MetaModelRestService.
 *
 * @author Mikhail Mikhailov Meta model management REST interface.
 */
@Path(MetaModelRestService.SERVICE_PATH)
@Consumes({ "application/json" })
@Produces({ "application/json" })
public class MetaModelRestService extends AbstractRestService {
	/**
	 * Logger.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(MetaModelRestService.class);
	/**
	 * Service path.
	 */
	static final String SERVICE_PATH = "model";
	/**
	 * Path param all.
	 */
	private static final String PATH_PARAM_ALL = "all";
	/**
	 * Storage id param.
	 */
	private static final String PATH_PARAM_STORAGE_ID = "id";
	/**
	 * Path param dependency.
	 */
	private static final String PATH_PARAM_DEPENDENCY = "dependency";
	/**
	 * Path param model name.
	 */
	private static final String PATH_PARAM_MODEL_NAME = "model-name";

	/** The Constant PATH_PARAM_REMOVE_DRAFT. */
	private static final String PATH_PARAM_REMOVE_DRAFT = "remove-draft";
	/**
	 * File name (attachment param).
	 */
	private static final String DATA_PARAM_FILE = "file";
	/**
	 * Recreate he model or not.
	 */
	private static final String DATA_PARAM_RECREATE = "recreate";
	/**
	 * Meta model service.
	 */
	@Autowired
	private MetaModelService metaModelService;
	/**
	 * Meta dependency service.
	 */
	@Autowired
	private MetaDependencyService metaDependencyService;

	/** The meta draft service. */
	@Autowired
	private MetaDraftService metaDraftService;


    @Autowired
    private ExecutionService executionService;

	@Autowired
	private RenderingService renderingService;
    /**
     * The name.
     */
    private String name = "DEFAULT";

	/**
	 * Constructor.
	 */
	public MetaModelRestService() {
		super();
	}

	/**
	 * Gets entities in paged fashion with defaults.
	 *
	 * @return list of entities
	 */
	@GET
	@Path("/" + PATH_PARAM_ALL)
	@Operation(
        description = "Список идентификаторов моделей.",
        method = HttpMethod.GET,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = RestResponse.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
	public Response findAll() {
		return ok(new RestResponse<>(metaModelService.getStorageIdsList()));
	}

	/**
	 * Model name.
	 *
	 * @return the response
	 */
	@GET
	@Path("/" + PATH_PARAM_MODEL_NAME)
	@Operation(
        description = "Имя и версия метамодели.",
        method = HttpMethod.GET,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = RestResponse.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
	public Response modelName() {
		ModelVersionRO modelVersionRO = new ModelVersionRO();
		modelVersionRO.setStorageId("DEFAULT");
		modelVersionRO.setVersion(metaModelService.getRootGroup(null).getVersion().intValue());
		modelVersionRO.setName(name);
		return ok(new RestResponse<>(modelVersionRO));
	}

	/**
	 * Change or set metamodel name for given storage id.
	 *
	 * @param modelVersionRO the model version RO
	 * @return the response
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/" + PATH_PARAM_MODEL_NAME)
	@Operation(
        description = "Меняет имя метамодели для заданого storage id.",
        method = HttpMethod.POST,
        requestBody = @RequestBody(content = @Content(schema = @Schema(implementation = ModelVersionRO.class)), description = "Запрос на вставку"),
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = RestResponse.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
	public Response modelName(ModelVersionRO modelVersionRO) {
		name = modelVersionRO.getName();
		return ok(new RestResponse<>(modelVersionRO));
	}

	/**
	 * Apply draft for the given storage id.
	 *
	 * @param modelVersionRO the model version RO
	 * @return the response
	 */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("apply-draft")
    @Operation(
            description = "Применяет черновик для заданого storage id.",
            method = HttpMethod.POST,
            requestBody = @RequestBody(content = @Content(schema = @Schema(implementation = ModelVersionRO.class)), description = "Запрос на вставку"),
            responses = {
                    @ApiResponse(content = @Content(schema = @Schema(implementation = RestResponse.class)), responseCode = "200"),
                    @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
            }
    )
    public Response applyDraft(ModelVersionRO modelVersionRO) {

		PublishModelRequestContext.PublishModelRequestContextBuilder builder = PublishModelRequestContext.builder();

		renderingService.renderInput(MetaModelInputRenderingAction.PUBLISH_META_MODEL_INPUT, builder, modelVersionRO);

        executionService.execute(builder.build());

        return ok(new RestResponse<>(modelVersionRO));
    }

	/**
	 * Remove draft for the given storage id.
	 *
	 * @param modelVersionRO the model version RO
	 * @return the response
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/" + PATH_PARAM_REMOVE_DRAFT)
	@Operation(
        description = "Удаляет черновик для заданого storage id.",
        method = HttpMethod.POST,
        requestBody = @RequestBody(content = @Content(schema = @Schema(implementation = ModelVersionRO.class)), description = "Запрос на вставку"),
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = RestResponse.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
	public Response removeDraft(ModelVersionRO modelVersionRO) {
		DropDraftModelRequestContext.DropDraftModelRequestContextBuilder builder =
				DropDraftModelRequestContext.builder();

		renderingService.renderInput(MetaModelInputRenderingAction.DELETE_META_MODEL_DRAFT, builder, modelVersionRO);

		executionService.execute(builder.build());

		return ok(new RestResponse<>(modelVersionRO));
	}

	/**
	 * Gets model data associated with optional storage ID. Returns current, if no
	 * storage id specified.
	 *
	 * @param id            storage id.
	 * @return character stream
	 * @throws Exception the exception
	 */
	@GET
	@Path("{p:/?}{" + PATH_PARAM_STORAGE_ID + ": (([_a-zA-Z0-9\\-]{3,})?)}")
	@Produces(MediaType.TEXT_XML)
	@Operation(
        description = "Получить модель в XML по опциональному storage ID.",
        method = HttpMethod.GET,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = StreamingOutput.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
	public Response getById(@Parameter(in = ParameterIn.PATH) @PathParam(PATH_PARAM_STORAGE_ID) String id) throws Exception {

		final Model model = metaModelService.exportModel(id);
		final String encodedFilename = URLEncoder.encode(model.getStorageId() + "_"
				+ DateFormatUtils.format(System.currentTimeMillis(), "yyyy-MM-dd_HH-mm-ss") + ".xml", StandardCharsets.UTF_8.name());
		return Response.ok(createStreamingOutputFromModel(model)).encoding(StandardCharsets.UTF_8.name()).header("Content-Disposition",
				"attachment; filename=" + model.getStorageId() + ".xml" + "; filename*=UTF-8''" + encodedFilename)
				.header("Content-Type", MediaType.TEXT_XML).build();
	}

	/**
	 * Saves binary large object.
	 *
	 * @param id            golden record id
	 * @param recreate the recreate
	 * @param fileAttachment            attachment object
	 * @return ok/nok
	 * @throws Exception the exception
	 */
	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
    @Path("{p:/?}{" + PATH_PARAM_STORAGE_ID + ": (([_a-zA-Z0-9\\-]{3,})?)}")
    @Operation(
            description = "Загрузить данные модели с опциональным storage ID.",
            method = HttpMethod.POST,
            requestBody = @RequestBody(content = @Content(mediaType = MediaType.MULTIPART_FORM_DATA), description = "Запрос на вставку"),
            responses = {
                    @ApiResponse(content = @Content(schema = @Schema(implementation = UpdateResponse.class)), responseCode = "200"),
                    @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
            }
    )
    public Response importModel(
            @Parameter(in = ParameterIn.PATH, description = "ID объекта") @PathParam(PATH_PARAM_STORAGE_ID) String id,
            @Multipart(value = DATA_PARAM_RECREATE) Boolean recreate,
            @Multipart(value = DATA_PARAM_FILE) Attachment fileAttachment) throws IOException {

        if (!MediaType.TEXT_XML_TYPE.equals(fileAttachment.getContentType())) {
            LOGGER.warn(
                    "Invalid content type rejected, while importing model. {}",
                    fileAttachment.getContentType()
            );
            throw new PlatformBusinessException("Import model with the media type [{}] is not allowed.",
                    MetaRestExceptionIds.EX_META_IMPORT_MODEL_INVALID_CONTENT_TYPE, fileAttachment.getContentType().toString());
        }

		Model model = MetaJaxbUtils.createModelFromInputStream(fileAttachment.getObject(InputStream.class));
		if (id != null) {
			model.setStorageId(id);
		}

		boolean isRecreate = recreate == null ? Boolean.FALSE : recreate;
		if (isRecreate && CollectionUtils.isEmpty(model.getSourceSystems())) {
			model.getSourceSystems().add(ModelUtils.createDefaultSourceSystem());
		}

		UpdateModelRequestContext ctx = new UpdateModelRequestContextBuilder()
				.enumerationsUpdate(model.getEnumerations()==null
					?null
							: new ArrayList<>(model
						.getEnumerations()
						.stream()
						.collect(Collectors.toCollection(() ->
								new TreeSet<>(Comparator.comparing(EnumerationDataType::getName))))))
				.sourceSystemsUpdate(model.getSourceSystems())
				.nestedEntityUpdate(model.getNestedEntities())
				.lookupEntityUpdate(model.getLookupEntities())
				.entitiesGroupsUpdate(model.getEntitiesGroup())
				.entityUpdate(model.getEntities())
				.relationsUpdate(model.getRelations())
				// TODO @Modules
				/*.defaultClassifiersUpdate(model.getDefaultClassifiers())
                .cleanseFunctionsUpdate((model.getCleanseFunctions() == null) ? null
            						: enrichDefaultFunctions(model.getCleanseFunctions()).getGroup())*/
				.storageId(id).isForceRecreate(isRecreate ? UpdateModelRequestContext.ModelUpsertType.FULLY_NEW
						: UpdateModelRequestContext.ModelUpsertType.ADDITION)
				.build();
		// TODO @Modules
//		ctx.putToStorage(StorageId.DEFAULT_CLASSIFIERS, model.getDefaultClassifiers());
		metaModelService.upsertModel(ctx);
		metaDraftService.removeDraft();
		// TODO @Modules
//		if (model.getCleanseFunctions() != null
//				&& model.getCleanseFunctions().getGroup() != null) {
//			ListOfCleanseFunctions cleanseFunctions = model.getCleanseFunctions();
//			List<Serializable> cfs = cleanseFunctions.getGroup()
//					.getGroupOrCleanseFunctionOrCompositeCleanseFunction();
//			for (Serializable cf : cfs) {
//				if (cf instanceof CompositeCleanseFunctionDef) {
//					CompositeCleanseFunctionDef ccf = (CompositeCleanseFunctionDef) cf;
//					cleanseFunctionService.upsertCompositeCleanseFunction(ccf.getFunctionName(),
//							(CompositeCleanseFunctionDef) cf);
//				}
//			}
//
//		}
		return ok(new UpdateResponse(true, model.getStorageId()));
	}

	// TODO @Modules
//	/**
//	 * Enrich list with default functions with composite and custom functions from
//	 * model.
//	 *
//	 * @param listOfCleanseFunctions the list of cleanse functions
//	 * @return the list of cleanse functions
//	 */
//	private ListOfCleanseFunctions enrichDefaultFunctions(ListOfCleanseFunctions listOfCleanseFunctions) {
//		CleanseFunctionGroupDef result = ModelUtils.createDefaultCleanseFunctionGroup(MessageUtils.getCurrentLocale());
//		if (listOfCleanseFunctions == null || listOfCleanseFunctions.getGroup() == null) {
//			return new ListOfCleanseFunctions().withGroup(result);
//		}
//		List<Serializable> list = listOfCleanseFunctions.getGroup()
//				.getGroupOrCleanseFunctionOrCompositeCleanseFunction();
//		for (Serializable obj : list) {
//			if (obj instanceof CompositeCleanseFunctionDef) {
//				result.getGroupOrCleanseFunctionOrCompositeCleanseFunction().add(obj);
//			} else if (obj instanceof CleanseFunctionDef) {
//				result.getGroupOrCleanseFunctionOrCompositeCleanseFunction().add(obj);
//			} else if (obj instanceof CleanseFunctionExtendedDef) {
//				result.getGroupOrCleanseFunctionOrCompositeCleanseFunction().add(obj);
//			}
//		}
//
//		return new ListOfCleanseFunctions().withGroup(result);
//	}

	/**
	 * Return dependency graph.
	 *
	 * @param request
	 *            meta elements types.
	 * @return dependency graph.
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/" + PATH_PARAM_DEPENDENCY)
	@Operation(
        description = "Возвращает граф зависимостей между выбранными элементами метамодели.",
        method = HttpMethod.POST,
        requestBody = @RequestBody(content = @Content(schema = @Schema(implementation = MetaDependencyRequestRO.class)), description = "Запрос"),
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = RestResponse.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
	public Response dependency(MetaDependencyRequestRO request) {
		Set<MetaType> forTypes = MetaTypeROToDTOConverter.convert(request.getForTypes());
		Set<MetaType> skipTypes = MetaTypeROToDTOConverter.convert(request.getSkipTypes());
		String storageId = request.getStorageId();
		MetaGraph result = metaDependencyService.calculateDependencies(storageId, forTypes, skipTypes);
		return ok(new RestResponse<>(MetaGraphDTOToROConverter.convert(result)));
	}

	/**
	 * Gets {@link StreamingOutput} for a context.
	 *
	 * @param model the model
	 * @return streaming output
	 */
	private StreamingOutput createStreamingOutputFromModel(final Model model) {

		return output -> {
			String modelAsString = MetaJaxbUtils.marshalMetaModel(model);
			output.write(modelAsString.getBytes(StandardCharsets.UTF_8));
		};
	}
}
