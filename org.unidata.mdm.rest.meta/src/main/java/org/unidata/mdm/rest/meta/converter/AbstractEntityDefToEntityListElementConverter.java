/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.meta.converter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.unidata.mdm.core.convert.ConverterQualifier;
import org.unidata.mdm.meta.type.model.entities.AbstractEntity;
import org.unidata.mdm.rest.meta.ro.EntityInfoDefinition;

/**
 * @author Michael Yashin. Created on 26.05.2015.
 */
@ConverterQualifier
@Component
public class AbstractEntityDefToEntityListElementConverter implements Converter<AbstractEntity, EntityInfoDefinition> {

    public static<T extends AbstractEntity> List<EntityInfoDefinition> to(List<T> source) {

        if (CollectionUtils.isEmpty(source)) {
            Collections.emptyList();
        }

        List<EntityInfoDefinition> result = new ArrayList<>();
        for (AbstractEntity a : source) {
            result.add(to(a));
        }

        return result;
    }

    public static EntityInfoDefinition to(AbstractEntity source) {

        if (Objects.isNull(source)) {
            return null;
        }

        EntityInfoDefinition element = new EntityInfoDefinition();
        element.setName(source.getName());
        element.setDisplayName(source.getDisplayName());
        element.setDescription(source.getDescription());

        return element;
    }

    @Override
    public EntityInfoDefinition convert(AbstractEntity source) {
        return to(source);
    }
}
