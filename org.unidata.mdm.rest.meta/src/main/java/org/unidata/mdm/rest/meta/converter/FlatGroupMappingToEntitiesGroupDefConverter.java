/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.meta.converter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.unidata.mdm.meta.type.model.entities.EntitiesGroup;
import org.unidata.mdm.meta.service.impl.facades.EntitiesGroupModelElementFacade;
import org.unidata.mdm.rest.meta.exception.MetaRestExceptionIds;
import org.unidata.mdm.rest.meta.ro.EntityGroupNode;
import org.unidata.mdm.rest.meta.ro.FlatGroupMapping;
import org.unidata.mdm.system.exception.PlatformBusinessException;

public class FlatGroupMappingToEntitiesGroupDefConverter {
    private FlatGroupMappingToEntitiesGroupDefConverter() { }

    public static EntitiesGroup convert(FlatGroupMapping flatGroupMapping) {
        Map<String, Collection<EntitiesGroup>> flatMap = new HashMap<>();
        Collection<? extends EntityGroupNode> entityGroupNodes = flatGroupMapping.getGroupNodes();
        for (EntityGroupNode groupNode : entityGroupNodes) {
            String[] splitPath = EntitiesGroupModelElementFacade.getSplitPath(groupNode.getGroupName());
            String groupName = splitPath[splitPath.length - 1];
            if (Objects.isNull(groupName) || Objects.isNull(groupNode.getTitle())) {
                throw new PlatformBusinessException(
                        "Group doesn't contain name or title",
                        MetaRestExceptionIds.EX_META_GROUP_NAME_OR_TITLE_ABSENT
                );
            }
            EntitiesGroup groupDef = new EntitiesGroup().withGroupName(groupName).withTitle(groupNode.getTitle());
            String mapKey = splitPath.length == 1 ? null : groupNode.getGroupName().substring(0, groupNode.getGroupName().length() - groupName.length() - 1);
            Collection<EntitiesGroup> groupDefs = flatMap.get(mapKey);
            if (groupDefs == null) {
                groupDefs = new ArrayList<>();
                flatMap.put(mapKey, groupDefs);
            }
            groupDefs.add(groupDef);
        }
        assemble(flatMap.get(null), flatMap, StringUtils.EMPTY);
        return flatMap.get(null).iterator().next();
    }

    private static void assemble(Collection<EntitiesGroup> groupDefs, Map<String, Collection<EntitiesGroup>> flatMap, String parentPath) {
        for (EntitiesGroup groupDef : groupDefs) {
            Collection<EntitiesGroup> innerGroups = flatMap.getOrDefault(EntitiesGroupModelElementFacade.getFullPath(parentPath, groupDef.getGroupName()), new ArrayList<>());
            groupDef.withInnerGroups(innerGroups);
            assemble(innerGroups, flatMap, groupDef.getGroupName());
        }
    }

}
