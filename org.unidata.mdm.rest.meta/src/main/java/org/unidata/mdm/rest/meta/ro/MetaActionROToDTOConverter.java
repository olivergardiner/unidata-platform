package org.unidata.mdm.rest.meta.ro;


import org.unidata.mdm.meta.type.input.meta.MetaAction;

/**
 * The Class MetaActionROToDTOConverter.
 * @author ilya.bykov
 */
public class MetaActionROToDTOConverter {

/**
 * Convert.
 *
 * @param source the source
 * @return the meta action
 */
public static MetaAction convert(MetaActionRO source){
	if(source==null){
		return null;
	}
    return MetaAction.valueOf(source.name());
}
}
