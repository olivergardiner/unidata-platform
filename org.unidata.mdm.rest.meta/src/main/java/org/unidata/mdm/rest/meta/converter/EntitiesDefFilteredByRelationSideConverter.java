/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
/**
 *
 */
package org.unidata.mdm.rest.meta.converter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.lang3.tuple.Pair;
import org.unidata.mdm.meta.type.model.entities.Entity;
import org.unidata.mdm.meta.type.model.entities.NestedEntity;
import org.unidata.mdm.meta.type.model.entities.Relation;
import org.unidata.mdm.meta.dto.GetEntitiesByRelationSideDTO;
import org.unidata.mdm.rest.meta.ro.EntityDefinition;
import org.unidata.mdm.rest.meta.ro.RelationDefinition;

/**
 * @author Mikhail Mikhailov
 *
 */
public class EntitiesDefFilteredByRelationSideConverter {

    /**
     * Constructor.
     */
    private EntitiesDefFilteredByRelationSideConverter() {
        super();
    }

    /**
     * Converter method.
     * @param dto the dto to convert
     * @return list of entity definitions
     */
    public static List<EntityDefinition> convert(GetEntitiesByRelationSideDTO dto) {

        if (dto == null || dto.getEntities() == null || dto.getEntities().isEmpty()) {
            return Collections.emptyList();
        }

        List<EntityDefinition> result = new ArrayList<>();
        for (Entry<Entity, Pair<List<NestedEntity>, List<Relation>>> e
                : dto.getEntities().entrySet()) {

            Entity source = e.getKey();
            EntityDefinition target = new EntityDefinition();
            List<NestedEntity> refs = e.getValue().getLeft();

            EntityDefinitionConverter.copyAbstractEntityData(source, target);
            target.getSimpleAttributes().addAll(
                    EntityDefinitionConverter.toSimpleAttrs(source.getSimpleAttribute(), source.getName()));
            target.getArrayAttributes().addAll(
                    EntityDefinitionConverter.toArrayAttrs(source.getArrayAttribute(), source.getName()));
            target.getComplexAttributes().addAll(
                    EntityDefinitionConverter.to(source.getComplexAttribute(), refs, source.getName()));

            target.setDashboardVisible(source.isDashboardVisible());
            target.setMergeSettings(MergeSettingsConverter.to(source.getMergeSettings()));

            List<RelationDefinition> relations = new ArrayList<>();
            for (Relation relationDef : e.getValue().getRight()) {
                relations.add(RelationDefConverter.convert(relationDef));
            }

            target.setRelations(relations);

            result.add(target);
        }

        return result;
    }
}
