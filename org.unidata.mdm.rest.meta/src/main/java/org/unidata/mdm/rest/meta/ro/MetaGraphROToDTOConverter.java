/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.meta.ro;

import java.util.List;

import org.jgrapht.EdgeFactory;
import org.unidata.mdm.meta.type.input.meta.MetaEdge;
import org.unidata.mdm.meta.type.input.meta.MetaEdgeFactory;
import org.unidata.mdm.meta.type.input.meta.MetaGraph;
import org.unidata.mdm.meta.type.input.meta.MetaVertex;


/**
 * The Class MetaGraphROToDTOConverter.
 *
 * @author ilya.bykov
 */
public class MetaGraphROToDTOConverter {

    /** The ef. */
    private static final EdgeFactory<MetaVertex, MetaEdge<MetaVertex>> EF = new MetaEdgeFactory();

    /**
     * Convert.
     *
     * @param source
     *            the source
     * @return the meta graph
     */
    public static MetaGraph convert(MetaGraphRO source) {
        if (source == null) {
            return null;
        }
        MetaGraph target = new MetaGraph(EF);
        List<MetaVertexRO> vertexes = source.getVertexes();
        for (MetaVertexRO vertex : vertexes) {
            MetaVertex converted = MetaVertexROToDTOConverter.convert(vertex);
            target.addVertex(converted);
        }
        List<MetaEdgeRO> edges = source.getEdges();
        for (MetaEdgeRO edge : edges) {
            target.addEdge(MetaVertexROToDTOConverter.convert(edge.getFrom()),
                    MetaVertexROToDTOConverter.convert(edge.getTo()));
        }
        target.setOverride(source.isOverride());
        target.setId(source.getId());
        target.setFileName(source.getFileName());
        target.setImportRoles(source.isRoles());
        target.setImportUsers(source.isUsers());
        return target;
    }

}
