/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.meta.converter;

import javax.annotation.Nonnull;

import org.unidata.mdm.core.type.measurement.MeasurementUnit;
import org.unidata.mdm.core.type.measurement.MeasurementValue;

import java.util.Comparator;
import java.util.stream.Collectors;
import org.unidata.mdm.rest.meta.ro.MeasurementUnitRO;
import org.unidata.mdm.rest.meta.ro.MeasurementValueRO;

public class MeasurementValueConverter {

    private static final Comparator<MeasurementUnit> MEASUREMENT_UNIT_COMPARATOR = (o1, o2) -> Integer.compare(o1.getOrder(),o2.getOrder());

    @Nonnull
    public static MeasurementValueRO convert(@Nonnull MeasurementValue measurementValue) {
        MeasurementValueRO measurementValueDto = new MeasurementValueRO();
        measurementValueDto.setShortName(measurementValue.getShortName());
        measurementValueDto.setId(measurementValue.getId());
        measurementValueDto.setName(measurementValue.getName());
        measurementValueDto.setMeasurementUnits(measurementValue.getMeasurementUnits()
                .stream()
                .sequential()
                .sorted(MEASUREMENT_UNIT_COMPARATOR)
                .map(MeasurementValueConverter::convert)
                .collect(Collectors.toList()));
        return measurementValueDto;
    }

    @Nonnull
    public static MeasurementUnitRO convert(@Nonnull MeasurementUnit measurementUnit) {
        MeasurementUnitRO measurementUnitDto = new MeasurementUnitRO();
        measurementUnitDto.setName(measurementUnit.getName());
        measurementUnitDto.setId(measurementUnit.getId());
        measurementUnitDto.setShortName(measurementUnit.getShortName());
        measurementUnitDto.setBase(measurementUnit.isBase());
        measurementUnitDto.setConvectionFunction(measurementUnit.getConvertionFunction());
        measurementUnitDto.setValueId(measurementUnit.getValueId());
        return measurementUnitDto;
    }

}
