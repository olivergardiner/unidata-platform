/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.meta.converter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.apache.commons.collections4.CollectionUtils;
import org.unidata.mdm.meta.type.model.entities.RelType;
import org.unidata.mdm.meta.type.model.entities.Relation;
import org.unidata.mdm.meta.type.model.attributes.SimpleMetaModelAttribute;
import org.unidata.mdm.rest.meta.ro.RelationDefinition;

public class RelationDefinitionConverter {

    public static Relation convert(RelationDefinition source) {
        if (source == null) {
            return null;
        }

        Relation target = new Relation();
        target.setFromEntity(source.getFromEntity());
        target.setName(source.getName());
        target.setDisplayName(source.getDisplayName());
        target.setRelType(RelType.fromValue(source.getRelType()));
        target.setRequired(source.isRequired());
        target.setToEntity(source.getToEntity());
        target.setUseAttributeNameForDisplay(source.isUseAttributeNameForDisplay());

        if (CollectionUtils.isNotEmpty(source.getToEntityDefaultDisplayAttributes())){
            target.getToEntityDefaultDisplayAttributes().addAll(source.getToEntityDefaultDisplayAttributes());
        }

        if (CollectionUtils.isNotEmpty(source.getToEntitySearchAttributes())) {
            target.getToEntitySearchAttributes().addAll(source.getToEntitySearchAttributes());
        }

        List<SimpleMetaModelAttribute> attrs = new ArrayList<>();
        SimpleAttributeDefConverter.copySimpleAttributeDataList(source.getSimpleAttributes(), attrs);
        target.withCustomProperties(ToCustomPropertyDefConverter.convert(source.getCustomProperties()));

        target.withSimpleAttribute(attrs);
        return target;
    }

    public static List<Relation> convert(List<RelationDefinition> source) {

        if (CollectionUtils.isEmpty(source)) {
            return Collections.emptyList();
        }

        List<Relation> result = new ArrayList<>();
        for (RelationDefinition def : source) {
            if (Objects.isNull(def)) {
                continue;
            }

            result.add(convert(def));
        }

        return result;
    }
}
