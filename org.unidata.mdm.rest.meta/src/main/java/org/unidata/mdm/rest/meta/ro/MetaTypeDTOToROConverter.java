package org.unidata.mdm.rest.meta.ro;


import org.unidata.mdm.meta.type.input.meta.MetaType;

/**
 * The Class MetaTypeDTOToROConverter.
 * 
 * @author ilya.bykov
 */
public class MetaTypeDTOToROConverter {

	private MetaTypeDTOToROConverter() {
	}

	/**
	 * Convert.
	 *
	 * @param source
	 *            the source
	 * @return the meta type RO
	 */
	public static MetaTypeRO convert(MetaType source) {
		if (source == null) {
			return null;
		}
		return MetaTypeRO.valueOf(source.name());
	}

}
