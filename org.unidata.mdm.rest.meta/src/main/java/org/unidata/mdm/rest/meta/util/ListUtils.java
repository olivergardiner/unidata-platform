/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.meta.util;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alexander Malyshev
 */
public final class ListUtils {

    private ListUtils() { }

    /**
     * Safe sub list.
     *
     * @param <T>
     *            the generic type
     * @param list
     *            the list
     * @param fromIndex
     *            the from index
     * @param toIndex
     *            the to index
     * @return the list
     */
    public static <T> List<T> safeSubList(List<T> list, int fromIndex, int toIndex) {
        if (list == null) {
            return new ArrayList<>();
        }
        int size = list.size();
        int from = Math.min(size, Math.max(0, fromIndex));
        int to = Math.min(size, toIndex);
        return list.subList(from, to);
    }
}
