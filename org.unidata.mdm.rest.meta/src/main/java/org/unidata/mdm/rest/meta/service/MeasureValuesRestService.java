/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.meta.service;

import static java.lang.String.CASE_INSENSITIVE_ORDER;
import static java.util.Objects.isNull;
import static org.unidata.mdm.meta.service.impl.MeasurementValueXmlConverter.convert;
import static org.unidata.mdm.meta.service.impl.MeasurementValueXmlConverter.convertToByteArray;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.apache.cxf.jaxrs.ext.multipart.Multipart;
import org.springframework.beans.factory.annotation.Autowired;
import org.unidata.mdm.core.util.FileUtils;
import org.unidata.mdm.meta.type.model.measurement.MeasurementValue;
import org.unidata.mdm.meta.type.model.measurement.MeasurementValues;
import org.unidata.mdm.meta.service.MetaDraftService;
import org.unidata.mdm.meta.service.MetaMeasurementService;
import org.unidata.mdm.meta.service.impl.MeasurementValueXmlConverter;
import org.unidata.mdm.rest.meta.converter.MeasurementValueConverter;
import org.unidata.mdm.rest.meta.exception.MetaRestExceptionIds;
import org.unidata.mdm.rest.meta.ro.MeasurementValueRO;
import org.unidata.mdm.rest.system.ro.ErrorResponse;
import org.unidata.mdm.rest.system.ro.RestResponse;
import org.unidata.mdm.rest.system.ro.UpdateResponse;
import org.unidata.mdm.rest.system.service.AbstractRestService;
import org.unidata.mdm.system.exception.PlatformBusinessException;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@Path(MeasureValuesRestService.SERVICE_PATH)
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class MeasureValuesRestService extends AbstractRestService {

    public static final String SERVICE_PATH = "measurement-values";

    private static final String VALUE_ID = "valueId";
    private static final String IMPORT_FILE = "file";

    @Autowired
    private MetaMeasurementService metaMeasurementService;
    @Autowired
    private MetaDraftService metaDraftService;

    private static final Comparator<MeasurementValueRO> VALUE_DTO_COMPARATOR = (o1, o2) -> CASE_INSENSITIVE_ORDER
            .compare(o1.getName(), o2.getName());

    @GET
    @Operation(
        description = "Получение всех величин",
        method = HttpMethod.GET,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = RestResponse.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response getAllMeasureValues(@Parameter(description = "Черновик?,", in = ParameterIn.QUERY) @QueryParam("draft") @DefaultValue("false") boolean draft) {
        Collection<MeasurementValueRO> result;
        if (!draft) {
            result = metaMeasurementService.getAllValues().stream().map(MeasurementValueConverter::convert)
                    .sorted(VALUE_DTO_COMPARATOR).collect(Collectors.toList());
        } else {
            result = metaDraftService.getAllValues().stream().map(MeasurementValueConverter::convert)
                    .sorted(VALUE_DTO_COMPARATOR).collect(Collectors.toList());
        }
        return ok(new RestResponse<>(result));
    }

    @DELETE
    @Path("/{" + VALUE_ID + "}")
    @Operation(
        description = "Удалить единицы измерения",
        method = HttpMethod.DELETE,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = UpdateResponse.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response removeValue(
            @Parameter(description = "Идентификатор,", in = ParameterIn.PATH) @PathParam(VALUE_ID) String measureValueIds,
            @Parameter(description = "Черновик?,", in = ParameterIn.QUERY) @QueryParam("draft") @DefaultValue("false") boolean draft) {

        boolean result;
        if (!draft) {
            result = metaMeasurementService.removeValue(measureValueIds);
        } else {
            result = metaDraftService.removeValue(measureValueIds);
        }
        return ok(new RestResponse<>(result));
    }

    @GET
    @Path("/batch-delete")
    @Operation(
        description = "Удалить единицы измерения",
        method = HttpMethod.GET,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = UpdateResponse.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response removeValues(
            @Parameter(description = "Идентификаторы,", in = ParameterIn.PATH) @QueryParam(VALUE_ID) List<String> measureValueIds,
            @Parameter(description = "Черновик?,", in = ParameterIn.QUERY) @QueryParam("draft") @DefaultValue("false") boolean draft) {

        if (measureValueIds.isEmpty()) {
            return ok(new RestResponse<>());
        }
        boolean result;
        if (!draft) {
            result = metaMeasurementService.batchRemove(measureValueIds, false, false);
        } else {
            result = metaDraftService.batchRemove(measureValueIds, false, false);
        }
        return ok(new RestResponse<>(result));
    }

    @POST
    @Path("/import")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Operation(
        description = "Удалить единицы измерения. Поддерживается только xml.",
        method = HttpMethod.POST,
        requestBody = @RequestBody(content = @Content(mediaType = MediaType.MULTIPART_FORM_DATA), description = "Запрос на вставку"),
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = UpdateResponse.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response importValues(
            @Multipart(required = true, value = IMPORT_FILE) Attachment fileAttachment,
            @Parameter(description = "Черновик?,", in = ParameterIn.QUERY) @QueryParam("draft") @DefaultValue("false") boolean draft) throws Exception {
        if (isNull(fileAttachment)) {
            return okOrNotFound(null);
        }
        java.nio.file.Path file = FileUtils.saveFileTempFolder(fileAttachment);
        try {
            String fileName = fileAttachment.getDataHandler().getDataSource().getName();
            if (!fileName.endsWith("xml")) {
                String contentType = fileAttachment.getContentType().toString();
                throw new PlatformBusinessException("Import measurement values with the media type [{}] is not allowed.",
                        MetaRestExceptionIds.EX_MEASUREMENT_IMPORT_TYPE_UNSUPPORTED, contentType);
            }
            MeasurementValues values = convert(file.toFile());
            if (CollectionUtils.isEmpty(values.getValue())) {
                throw new PlatformBusinessException("Import measurement values failed. Empty definition",
                        MetaRestExceptionIds.EX_MEASUREMENT_IMPORT_EMPTY);
            }

            if (!draft) {
                for (MeasurementValue value : values.getValue()) {
                    metaMeasurementService.saveValue(convert(value));
                }
            } else {
                for (MeasurementValue value : values.getValue()) {
                    metaDraftService.saveValue(convert(value));
                }
            }
            return ok(new RestResponse<>());
        } finally {
            file.toFile().delete();
        }
    }

    @GET
    @Path("/export")
    @Produces(MediaType.TEXT_XML)
    @Operation(
        description = "Выгрузить единицы измерения.",
        method = HttpMethod.GET,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = StreamingOutput.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response exportValues(
            @Parameter(description = "Идентификаторы,", in = ParameterIn.PATH) @QueryParam(VALUE_ID) List<String> measureValueIds,
            @Parameter(description = "Черновик?,", in = ParameterIn.QUERY) @QueryParam("draft") @DefaultValue("false") boolean draft) throws Exception {
        if (measureValueIds.isEmpty()) {
            return okOrNotFound(null);
        }

        Collection<MeasurementValue> measurementValue = null;
        if (draft) {
            measurementValue = measureValueIds.stream()
                    .map(metaDraftService::getValueById).filter(Objects::nonNull)
                    .map(MeasurementValueXmlConverter::convert).collect(Collectors.toList());
        } else {
            measurementValue = measureValueIds.stream()
                    .map(metaMeasurementService::getValueById).filter(Objects::nonNull)
                    .map(MeasurementValueXmlConverter::convert).collect(Collectors.toList());
        }

        if (measurementValue.isEmpty()) {
            return okOrNotFound(null);
        }
        MeasurementValues measurementValueDef = new MeasurementValues().withValue(measurementValue);
        final String encodedFilename = URLEncoder.encode(
                "values_" + DateFormatUtils.format(System.currentTimeMillis(), "yyyy-MM-dd_HH-mm-ss") + ".xml",
                StandardCharsets.UTF_8.name());
        return Response.ok(outPutSteam(measurementValueDef)).encoding(StandardCharsets.UTF_8.name())
                .header("Content-Disposition",
                        "attachment; filename=" + encodedFilename + "; filename*=UTF-8''" + encodedFilename)
                .header("Content-Type", MediaType.TEXT_XML).build();
    }

    private StreamingOutput outPutSteam(MeasurementValues measurementValue) throws PlatformBusinessException {
        return output -> {
            try {
                output.write(convertToByteArray(measurementValue));
            } catch (Exception e) {
                throw new PlatformBusinessException("Marshaling is failed", MetaRestExceptionIds.EX_MEASUREMENT_MARSHAL_FAILED);
            }
        };
    }
}
