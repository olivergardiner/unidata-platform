/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.meta.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.unidata.mdm.meta.type.model.entities.EntitiesGroup;
import org.unidata.mdm.meta.context.UpdateModelRequestContext;
import org.unidata.mdm.meta.dto.GetEntitiesGroupsDTO;
import org.unidata.mdm.meta.service.MetaDraftService;
import org.unidata.mdm.meta.service.MetaModelService;
import org.unidata.mdm.rest.meta.converter.EntitiesGroupToFlatGroupMappingConverter;
import org.unidata.mdm.rest.meta.converter.FlatGroupMappingToEntitiesGroupDefConverter;
import org.unidata.mdm.rest.meta.ro.FlatGroupMapping;
import org.unidata.mdm.rest.system.ro.ErrorResponse;
import org.unidata.mdm.rest.system.ro.RestResponse;
import org.unidata.mdm.rest.system.service.AbstractRestService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

/**
 * Group rest service
 */
@Path(EntitiesGroupRestService.SERVICE_PATH)
@Consumes({"application/json"})
@Produces({"application/json"})
public class EntitiesGroupRestService extends AbstractRestService {

    public static final String SERVICE_PATH = "entities-group";

    /**
     * The metamodel service.
     */
    @Autowired
    private MetaModelService metaModelService;

    @Autowired
    private MetaDraftService metaDraftService;

    /**
     * Load and return list of all entities group
     *
     * @return list of source systems. {@see FlatGroupMapping}.
     */
    @GET
    @Operation(
        description = "Вернуть список всех групп, начиная с корня",
        method = HttpMethod.GET,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = RestResponse.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response findAll(
            @Parameter(description = "Заполненные?", in = ParameterIn.QUERY) @QueryParam("filled") @DefaultValue("false") boolean filled,
            @Parameter(description = "Черновик?,", in = ParameterIn.QUERY) @QueryParam("draft") @DefaultValue("false") boolean draft ) {

    	GetEntitiesGroupsDTO result;
    	if(draft) {
    		result = metaDraftService.getEntitiesGroups();
    	}else {
    		result = metaModelService.getEntitiesGroups();
    	}
        if (filled) {
            FlatGroupMapping target = EntitiesGroupToFlatGroupMappingConverter.convertToFullFilledFlatGroup(result);
            return ok(new RestResponse<>(target));
        } else {
            FlatGroupMapping target = EntitiesGroupToFlatGroupMappingConverter.convertToFlatGroup(result);
            return ok(new RestResponse<>(target));
        }
    }


    /**
     * Updates entities group.
     *
     * @param flatGroupMapping all necessary info about group.
     * @return 200 Ok
     */
    @PUT
    @Operation(
        description = "Обновить группу.",
        method = HttpMethod.PUT,
        requestBody = @RequestBody(content = @Content(schema = @Schema(implementation = FlatGroupMapping.class)), description = "Обновление"),
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = RestResponse.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response update(FlatGroupMapping flatGroupMapping,
            @Parameter(description = "Черновик?,", in = ParameterIn.QUERY) @QueryParam("draft") @DefaultValue("true") boolean draft ) {

        EntitiesGroup entitiesGroup = FlatGroupMappingToEntitiesGroupDefConverter.convert(flatGroupMapping);
        UpdateModelRequestContext ctx = new UpdateModelRequestContext
        		.UpdateModelRequestContextBuilder()
                .entitiesGroupsUpdate(entitiesGroup)
                .build();
		if (draft) {
			metaDraftService.update(ctx);
		} else {
			metaModelService.upsertModel(ctx);
		}
        return ok(new RestResponse<>());
    }
}
