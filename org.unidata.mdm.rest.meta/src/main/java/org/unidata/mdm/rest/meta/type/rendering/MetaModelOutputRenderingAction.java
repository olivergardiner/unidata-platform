package org.unidata.mdm.rest.meta.type.rendering;

import org.unidata.mdm.system.type.rendering.OutputRenderingAction;

/**
 * @author Alexey Tsarapkin
 */
public enum MetaModelOutputRenderingAction implements OutputRenderingAction {
    /**
     * GET entity meta model
     */
    GET_ENTITY_MODEL_OUTPUT,
    /**
     * GET lookup meta model
     */
    GET_LOOKUP_MODEL_OUTPUT;
}
